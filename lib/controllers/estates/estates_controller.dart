import 'package:farmcapturev2/models/biodiversity/reservesTypes_model.dart';
import 'package:farmcapturev2/models/estate/divisionpolygon_model.dart';
import 'package:farmcapturev2/models/infrastructure/infrastructurepolygons_model.dart';
import 'package:get/get.dart';

import '../../db/db_helper.dart';

class EstateController extends GetxController {
  final String Url =
      "https://digifiqa.bfusa.com:9090/DigiFc/DigiFcMobileService/";

  // final String Url =
  //     "https://digifipd.bfusa.com/DigiFc/DigiFcMobileService/";

  Future getMainDivisions(var tableName) async {
    print("At Function");
    var res = await DatabaseHelper.instance.select(tableName);

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getBlocksLocalById(var tableName, String id) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(tableName, 'main_division_id', id.toString());

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getReserveLocalById(var tableName, String id) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(tableName, 'reserve_area_id', id.toString());

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getlatlongById(String id) async {
    print("At Function 2");
    var res = await DatabaseHelper.instance.getAllDivisionPolygon(id);

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getDivisionlatlong(String id) async {
    print("At Function 2");
    var res = await DatabaseHelper.instance.getAllDivisionPolygon(id);

    if (res.isNotEmpty) {
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAllBlockslatlongById(String divisionId, String blockId) async {
    
    var res = await DatabaseHelper.instance.getallBlocksPolygon(divisionId, blockId);

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      return null;
    }
  }

  Future getBlockslatlongById(String blockId) async {
    print("At Function");
    var res = await DatabaseHelper.instance.getBlockPolygon(blockId);

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  // SAVE ALL BLOCKS TO LOCAL DB
  saveAll(List list, String tableName) async {
    int? i;

    for (var res in list) {
      i = await DatabaseHelper.instance.insert(tableName, res);
    }
    return i;
  }

  updateBlockLocal(String table, String column, String data, String column1,
      String value) async {
    //int i = 0;
    int i = await DatabaseHelper.instance
        .update1(table, column, data, column1, value);
    return i;
  }

  Future deleteBlockById(String blockId) async {
    print("At Function");
    var res = await DatabaseHelper.deleteItem(
        divisionPolygonTable, 'new_dev_block_id', blockId);
  }

  Future deleteReserveById(String reserveId) async {
    print("At Function");
    var res = await DatabaseHelper.deleteItem(
        reservesTypesTable, 'reserve_area_id', reserveId);
  }

  Future getInfrastructureCategoryById(String id, String id2) async {
    var res = await DatabaseHelper.instance.selectByTwoFields(
        farmerInfrastructureCategoryTable,
        "main_div_id",
        id,
        "infrastructure_type_id",
        id2,
        "_Id");

    if (res.isNotEmpty) {
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getInfrastructurelatlongById(String id) async {
    var res = await DatabaseHelper.instance.getAllColumsById(farmerInfrastructurePolygonTable, "transaction_no", id);

    if (res.isNotEmpty) {
      //print(res.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }
}
