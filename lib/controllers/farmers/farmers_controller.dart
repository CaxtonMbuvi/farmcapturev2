import 'dart:io';

import 'package:farmcapturev2/models/additional_info/nextofkin_model.dart';
import 'package:farmcapturev2/models/biodiversity/biodiversityanswers_model.dart';
import 'package:farmcapturev2/models/technical_update/farmers/girth_measurement.dart';
import 'package:farmcapturev2/models/visit/farmerTrainingMain_model.dart';
import 'package:get/get.dart';

import '../../api/my_api.dart';
import '../../db/db_helper.dart';
//import '../../models/additional_info/nextofkin_model.dart';
import '../../models/farmers/additional_info/inputquestions_model.dart';
import '../../models/farmers/allfarmers_model.dart';
import '../../models/farmers/deduction_model.dart';
import '../../models/response/response_model.dart';
import '../../models/visit/coachingAttendance_model.dart';
import '../../models/visit/visitinspection_model.dart';

class FarmersController extends GetxController {
  final String url3 =
      'https://digifiqa.bfusa.com:9090/WeightCAPTURE/MobileService/GetAllMembers';

  // final String url3 =
  //     'https://digifipd.bfusa.com/WeightCAPTURE/MobileService/GetAllMembers';

  // final String UrlP =
  //     "https://digifipd.bfusa.com/WeightCAPTURE/MobileService/GetAllMembers";
  // final String url2 =
  //     'https://weightcapture.cs4africa.com/capture/FarmCAPTURE/Reports/InputOrders/GetAllFarmServices';

  final String urlUpdateFarmer =
      "https://digifiqa.bfusa.com:9090/DigiFc/DigiFcMobileService/";

  Future<ResponseModel> isInteret() async {
    late ResponseModel responseModel;
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        responseModel = ResponseModel(true, "Internet is Connected");
      }
    } on SocketException catch (_) {
      print(_.toString());
      responseModel = ResponseModel(false, "Internet is Not Connected");
    }

    return responseModel;
  }

  Future getAllFarmersApi(var data) async {
    try {
      var res = await CallApi().postData2(data, url3);

      if (res.statusCode == 200 || res.statusCode == 201) {
        //print("Response is: " + res.body.toString());
        return res.body;
      } else {
        print(res.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("Error is: $e");
      // if (e is DioError) {

      // }
    }
  }

  Future updateAllFarmersApi(var data, var updateUrl) async {
    try {
      var fullUrl = urlUpdateFarmer + updateUrl;
      var res = await CallApi().postData2(data, fullUrl);

      if (res.statusCode == 200 || res.statusCode == 201) {
        print("UPDATE Response is: ${res.body}");
        return res.body;
      } else {
        print(res.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("Error is: $e");
      // if (e is DioError) {

      // }
    }
  }

  Future updateApi(var data, var updateUrl) async {
    try {
      var fullUrl = urlUpdateFarmer + updateUrl;
      var res = await CallApi().postData2(data, fullUrl);

      if (res.statusCode == 200 || res.statusCode == 201) {
        print("UPDATE Response is: ${res.body}");
        return res.body;
      } else {
        print(res.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("Error is: $e");
      // if (e is DioError) {

      // }
    }
  }

// SAVE ALL FARMERS TO LOCAL DB
  saveAll(List farmers, String tableName) async {
    int i = 0;
    for (var res in farmers) {
      print('Result Element is: $res');
      try {
        i = await DatabaseHelper.instance.insert(tableName, res);
      } catch (e) {
        print("Error is: $e");
      }
    }
    return i;
  }

  updateFarmerMainCoaching(
      List items, String tableName, String sessionId) async {
    var res;
    for (var res in items) {
      print('Result Element is: $res');
      try {
        res = await DatabaseHelper.instance.updateFarmerCoachingMain(
            tableName,
            res['start_time'].toString(),
            res['numofpeople'].toString(),
            res['no_fslb_farmers'].toString(),
            res['no_non_fslb_farmers'].toString(),
            res['no_family_members'].toString(),
            res['photo_url'].toString(),
            sessionId);
        if (res != null) {
          print("sUCCESS HEREEEE");
          return 1;
        }
      } catch (e) {
        print("Error is: $e");
      }
    }
    return 0;
  }

  updateEndSession(List items, String tableName, String sessionId) async {
    var res;
    for (var res in items) {
      print('Result Element is: $res');
      try {
        res = await DatabaseHelper.instance.updateEndTimeFarmerCoachingMain(
            tableName, res['end_time'].toString(), sessionId);
        if (res != null) {
          print("sUCCESS HEREEEE");
          return 1;
        }
      } catch (e) {
        print("Error is: $e");
      }
    }
    return 0;
  }

  updateGroupFarmerMainCoaching(
      List items, String tableName, String sessionId) async {
    var res;
    for (var res in items) {
      print('Result Element is: $res');
      try {
        res = await DatabaseHelper.instance.updateGroupFarmerCoachingMain(
            tableName,
            res['start_time'].toString(),
            res['photo_url'].toString(),
            sessionId);
        if (res != null) {
          print("sUCCESS HEREEEE");
          return 1;
        }
      } catch (e) {
        print("Error is: $e");
      }
    }
    return 0;
  }

  // // GET MAX DATECOMPARER FROM LOCAL DB
  // getMaxDateComparer() async {
  //   String i = "0";

  //   i = await DatabaseHelper.instance.readMaxDateComparerUsers();

  //   return i;
  // }

  // SAVE FARM POLYGON TO LOCAL DB
  saveFarmMapLocal(var data) async {
    int i = 0;
    i = await DatabaseHelper.instance.insert('land_coordinates', data);
    return i;
  }

  Future getAllFarmersLocal() async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectAll(farmersTable, 'main_category_code', 'F001', 'full_name');

    if (res.isNotEmpty) {
      print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAllChildrenLocal(String FarmerId) async {
    print("At Function NOK: ");
    print(FarmerId);
    var res = await DatabaseHelper.instance
        .selectSingle(nextKinTable, 'member_no', FarmerId);

    if (res.isNotEmpty) {
      print(res[0]["full_name"]!.toString());
      print(res[0]["member_no"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not DB");
    }
  }

  Future getAllUnUpdatedFarmersLocal() async {
    print("At Function");
    var res = await DatabaseHelper.instance.selectAllUnupdated(
        farmersTable, 'main_category_code', 'F001', "syncId", "p", "full_name");

    if (res.isNotEmpty) {
      print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAllUnUpdatedLocal(String tableName, String colName, String colValue,
      String colName1, String colValue1, String orderBy) async {
    print("At Function");
    var res = await DatabaseHelper.instance.selectAllUnupdated(
        tableName, colName, colValue, colName1, colValue1, orderBy);

    if (res.isNotEmpty) {
      print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getSingleFarmerLocal(String memberNo) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(farmersTable, 'member_no', memberNo);

    if (res.isNotEmpty) {
      print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getSingleLocal(String tableName, String column, String itemId) async {
    print("At Function");
    var res =
        await DatabaseHelper.instance.selectSingle(tableName, column, itemId);

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  updateFarmerLocal(var farmer, var memberNo) async {
    //int i = 0;
    int i = await DatabaseHelper.instance
        .updateFarmer(farmersTable, farmer, memberNo);
    return i;
  }

  // updateLocal(String tableName,var model, var memberNo) async {
  //   //int i = 0;
  //   int i = await DatabaseHelper.instance
  //       .updateFarmer(farmersTable, model, memberNo);
  //   return i;
  // }

  Future getFarmerDeductions(var memberNo) async {
    var res = await DatabaseHelper.instance
        .selectAll(farmersDeductionsTable, 'member_no', memberNo, 'name');

    if (res.isNotEmpty) {
      print(res[0]["name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getFarmQuestions(var identifierCode) async {
    var res = await DatabaseHelper.instance.selectAll(
        inputQuestionsTable, 'identifier_code', identifierCode, 'unitCode');

    if (res.isNotEmpty) {
      print(res[0]["name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAlpQuestionsCategories(var identifierCode) async {
    var res = await DatabaseHelper.instance.selectGroupBy(
        inputQuestionsTable, 'identifier_code', identifierCode, 'mainCategory');

    if (res.isNotEmpty) {
      print(res[0]["name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAlpQuestions(String mainCategoryId) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(inputQuestionsTable, 'main_category_id', mainCategoryId);

    if (res.isNotEmpty) {
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAnswer(String quizId) async {
    var res = await DatabaseHelper.instance.selectByTwoFields(
        "input_questionaire_answers", 'quizid', quizId, "answers", "YES", "Id");

    if (res.isNotEmpty) {
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getReasonsActions(String answer, String type) async {
    var res = await DatabaseHelper.instance.selectByTwoFields(
        "expected_answer_action_reason",
        'farm_inspections_quiz_expected_answers_item_id',
        answer,
        "separator_code",
        type,
        "Id");

    if (res.isNotEmpty) {
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getFarmLivingConditionsQuestions(var identifierCode) async {
    var res = await DatabaseHelper.instance.selectLivingConditions(
        inputQuestionsTable, 'identifier_code', identifierCode);

    if (res.isNotEmpty) {
      print(res[0]["name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAnsweredQuestions(String memberNo) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(farmersTable, 'member_no', memberNo);

    if (res.isNotEmpty) {
      print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future checkFarmerDeductions(var memberNo, var deductionId) async {
    var res = await DatabaseHelper.instance.selectByTwoFields(
        farmersDeductionsTable,
        'member_no',
        memberNo,
        'Id',
        deductionId,
        'name');

    if (res.isNotEmpty) {
      print(res[0]["name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  deleteFarmerDeduction(var deductionId) async {
    var res = await DatabaseHelper.deleteItem(
        farmersDeductionsTable, '_Id', deductionId.toString());

    return res;
  }

  deleteFarmerVisits(var table) async {
    var res = await DatabaseHelper.deleteAllItems(table);
    return res;
  }

  Future checkFarmerVisits(var memberNo, var date) async {
    var res = await DatabaseHelper.instance.selectByTwoFields(
        farmerVisitInspectionTable,
        'member_No',
        memberNo,
        'visit_date',
        date,
        'visit_date');

    if (res.isNotEmpty) {
      print(res[0]["comments"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  // Future getAllFarmerVisits(var memberNo) async {
  //   var res = await DatabaseHelper.instance.selectAll(farmerVisitInspectionTable, 'member_No', memberNo, 'visit_date');

  //   if (res.isNotEmpty) {
  //     print(res[0]["comments"]!.toString());
  //     return res;
  //     //print("List of Users: " + userModelList.length.toString());
  //   } else {
  //     print("Did not Get");
  //   }
  // }

  Future getAllFarmerVisits(var memberNo) async {
    print("At Function");
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    var res = await DatabaseHelper.instance.selectByTwoFields(
        farmerVisitInspectionTable,
        'member_No',
        memberNo,
        'visit_date',
        dateStr,
        'visit_date');

    if (res.isNotEmpty) {
      print(res[0]["visit_date"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAllFarmerTrainingSessions(var memberNo, var column) async {
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";

    print("At Function");
    var res = await DatabaseHelper.instance.selectByTwoFields(
        farmerTrainingMainTable,
        column,
        memberNo.toString(),
        'coaching_date',
        dateStr,
        'coaching_date');

    if (res.isNotEmpty) {
      print(res[0]["title"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getAllGroupFarmerTrainingSessions(
      var userId, var column, var category, var column1) async {
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";

    print("At Function");
    var res = await DatabaseHelper.instance.selectGroupVisits(
      farmerTrainingMainTable,
      column,
      userId.toString(),
      'coaching_date',
      dateStr,
      column1,
      category,
      'coaching_date',
    );

    if (res.isNotEmpty) {
      print(res[0]["title"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getSingleSessionLocal(String sessionId) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(farmerTrainingMainTable, 'sessionNo', sessionId);

    if (res.isNotEmpty) {
      print(res[0]["title"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
      return null;
    }
  }

  Future getSingleSessionLocalMembersPresent(String sessionId) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(farmerCoachingAttendanceTable, 'sessionId', sessionId);

    if (res.isNotEmpty) {
      print(res[0]["farmerId"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
      return null;
    }
  }

  updateLocal(String table, var data, var column, var value) async {
    //int i = 0;
    int i = await DatabaseHelper.instance.update(table, data, column, value);

    return i;
  }

  saveAllAdditional(List items, String tableName) async {
    int i = 0;
    for (var item in items) {
      print('Result Element is: $item');
      try {
        i = await DatabaseHelper.instance.insert(tableName, item);
      } catch (e) {
        print("Error is: $e");
      }
    }
    return i;
  }

// TECHNICAL UPDATE

  Future getAllGirthMeasurements(
      String col1, String divisionId, String col2, String blockId) async {
    print("At Function");
    print("At Function");
    var res = await DatabaseHelper.instance.selectByTwoFields(
        girthMeasurementTable,
        col1,
        divisionId.toString(),
        col2,
        blockId,
        'tree_no');

    if (res.isNotEmpty) {
      print(res[0]["tree_no"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  // TECHNICAL UPDATE

  Future getBioSummaryReport(
      String col1,
      String divisionId,
      String col2,
      String blockId,
      String col3,
      String startDate,
      String col4,
      String endDate) async {
    print("At Function");
    var res = await DatabaseHelper.instance.selectBioRecords(
        farmerBiodiversityAnswSurveysTable,
        col1,
        divisionId.toString(),
        col2,
        blockId,
        col3,
        startDate,
        col4,
        endDate,
        'Id');

    if (res.isNotEmpty) {
      print(res.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }

  Future getChildItemCount(String id) async {
    var res = await DatabaseHelper.instance.getItemCount(nextKinTable, "birth_certificate_no", "member_no", id);
    if(res.isNotEmpty){
      print(res.toString());
      return res;
    } else {
      print("Did not Get");
    }
  }
}
