import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../api/my_api.dart';
import '../../db/db_helper.dart';
import '../../models/auth/user_model.dart';
import '../../models/response/response_model.dart';

class AuthController extends GetxController {
  final String login_url =
      'https://digifiqa.bfusa.com:9090/SystemAccounts/Authentication/Login/Submit';

  // final String login_url =
  // 'https://digifipd.bfusa.com/SystemAccounts/Authentication/Login/Submit';

  List<String> userslist = [];
  List<ResultResult> userModelList = [];

  Future<ResponseModel> login(data) async {
    late ResponseModel responseModel;

    try {
      var userName = data['CurrentUser']['UserName'].toString();
      var password = data['CurrentUser']['PassWord'].toString();

      List results = await getUsersLocal(userName, password);
      if (results.isEmpty) {
        print("Here");
        // responseModel = ResponseModel(false, "No Users");
        try {
          var res = await CallApi().postData(data, login_url);

          print(res.body.toString());

          if (res.statusCode == 200 || res.statusCode == 201) {
            var temp = res.headers["authorization"].toString();
            print("TokenAuthIs: ${temp.toString()}");
            var token = temp.substring(6);
            print("ChangedTokenIs: $token");

            UserModel userModel =
                UserModel.fromJson(jsonDecode(res.body.toString()));
            String? user = userModel.result.result.username;
            String? id = userModel.result.result.resultId.toString();
            // String? pass = userModel.result!.result!.username;

            SharedPreferences localStorage =
                await SharedPreferences.getInstance();
            localStorage.setString('Token', token.toString());
            localStorage.setString('UserId', id.toString());
            localStorage.setString('Data', data.toString());
            localStorage.setString('UserName', userName.toString());
            localStorage.setString('Password', password.toString());

            print(
                "${localStorage.setString('UserName', userName.toString()).toString() + localStorage.setString('Password', password.toString()).toString()}");

            UserModelResultResult newuserModel = UserModelResultResult();
            newuserModel.resultId = userModel.result.result.resultId;
            newuserModel.username = userModel.result.result.username;
            newuserModel.password = password;
            newuserModel.accessLevelId = userModel.result.result.accessLevelId;
            newuserModel.accountId = userModel.result.result.accountId;
            newuserModel.passwordDuration =
                userModel.result.result.passwordDuration;
            newuserModel.phoneNumber = userModel.result.result.phoneNumber;
            newuserModel.statusId = userModel.result.result.statusId;
            newuserModel.useCode = userModel.result.result.useCode;
            newuserModel.useMail = userModel.result.result.useMail;
            newuserModel.useSms = userModel.result.result.useSms;
            newuserModel.userImagePath = userModel.result.result.userImagePath;

            await _saveUserLocal(newuserModel);

            responseModel = ResponseModel(true, "Welcome $userName");
          } else {
            responseModel =
                ResponseModel(false, "None ${res.statusCode.toString()}");
          }
        } catch (e) {
          responseModel = ResponseModel(false, "Oops Something went Wrong +$e");
        }
      } else {
        responseModel = ResponseModel(true, "Welcome $userName");
      }
    } catch (e) {
      responseModel = ResponseModel(false, "Oops Something went Wrong +$e");
    }

    return responseModel;
  }

  Future getUsersLocal(String userName, String password) async {
    List<dynamic> userList = [];
    var res = await DatabaseHelper.instance.readAllUsers(userName, password);
    if (res.isNotEmpty) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('UserName', userName.toString());
      localStorage.setString('Password', password.toString());

      userList = res;

      print(userList.first['resultId']);

      String userId = userList.first['resultId'].toString();
      localStorage.setString('UserId', userId);
    } else {
      print("Error");
    }
    // // userModelList =
    // //     await DatabaseHelper.instance.readAllUsers(userName, password);
    // print("At Function");

    // if (userList.isNotEmpty) {
    //   print(userModelList[0].toString());
    //   // for (var i = 0; i < userModelList.length; i++) {
    //   //   userslist.add(userModelList[i].username.toString());
    //   // }
    //   update();

    //   return userList;

    //   //print("List of Users: " + userModelList.length.toString());
    // } else {
    //   return userList;
    // }

    return userList;
  }

  _saveUserLocal(var user) async {
    // final user = ResultResult(
    //   username: username,
    //   password: pass,
    // );

    print("User Is: $user");

    int i = await DatabaseHelper.instance.insert(usersTable, user);
    print("User Added $i");
  }

  Future<ResponseModel> loginUserApi(var data) async {
    late ResponseModel responseModel;
    try {
      var res = await CallApi().postData(data, login_url);

      print(res.toString());

      if (res.statusCode == 200 || res.statusCode == 201) {
        var temp = res.headers["authorization"].toString();
        print(temp.toString());
        var token = temp.substring(6);
        print(token);

        UserModel userModel =
            UserModel.fromJson(jsonDecode(res.body.toString()));
        String? user = userModel.result.result.username;
        String? id = userModel.result.result.resultId.toString();
        // String? pass = userModel.result!.result!.username;

        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('Token', token.toString());
        localStorage.setString('UserId', id.toString());

        responseModel = ResponseModel(true, "Welcome");
      } else {
        responseModel = ResponseModel(false, "Failed To Login");
      }
    } catch (e) {
      responseModel = ResponseModel(false, e.toString());
    }

    return responseModel;
  }
}
