import 'dart:io';

import 'package:get/get.dart';

import '../../api/my_api.dart';
import '../../db/db_helper.dart';
import '../../models/response/response_model.dart';

class LocationController extends GetxController {
  final String Url =
      "https://digifiqa.bfusa.com:9090/DigiFc/DigiFcMobileService/";
  final String Url1 = "https://digifiqa.bfusa.com:9090/";

  // final String Url =
  //     "https://digifipd.bfusa.com/DigiFc/DigiFcMobileService/";
  // final String Url1 = "https://digifipd.bfusa.com/";

  Future<ResponseModel> isInteret() async {
    late ResponseModel responseModel;
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        responseModel = ResponseModel(true, "Internet is Connected");
      }
    } on SocketException catch (_) {
      print(_.toString());
      responseModel = ResponseModel(false, "Internet is Not Connected");
    }

    return responseModel;
  }

  Future getLocationDetails(var data, String uri) async {
    try {
      var res = await CallApi().postData2(data, Url + uri);

      if (res.statusCode == 200 || res.statusCode == 201) {
        return res.body;
      } else {
        print(res.body.toString());
        print(res.statusCode.toString());
        return null;
      }
    } catch (e) {
      print(e.toString());
      // if (e is DioError) {

      // }
    }
  }

  Future getLocationDetailsDivision(var data, String uri) async {
    try {
      var res = await CallApi().postData2(data, Url1 + uri);

      if (res.statusCode == 200 || res.statusCode == 201) {
        return res.body;
      } else {
        print(res.body.toString());
        print(res.statusCode.toString());
        return null;
      }
    } catch (e) {
      print(e.toString());
      // if (e is DioError) {

      // }
    }
  }

  // SAVE ALL FARMERS TO LOCAL DB
  Future saveLocations(var table, List locations) async {
    int i = 0;
    for (var res in locations) {
      //print('Result Element is: $res');
      var response = await DatabaseHelper.instance.insert(table, res);
      //print(response);
    }
    i = 1;

    return i;
  }

  // SAVE ALL FARMERS TO LOCAL DB
  Future saveLocations1(var table, var res) async {
    int i = await DatabaseHelper.instance.insert(table, res);

    return i;
  }

  Future getLocationsLocal(var tableName) async {
    print("At Function");
    var res = await DatabaseHelper.instance.select(tableName);

    if (res.isNotEmpty) {
      print(res.toString());
      return res;
    } else {
      print("Did not Get");
    }
  }

  Future getLocationsLocalById(var tableName, var columnName, String id) async {
    print("At Function");
    var res = await DatabaseHelper.instance
        .selectSingle(tableName, columnName, id.toString());

    if (res.isNotEmpty) {
      //print(res[0]["full_name"]!.toString());
      return res;
      //print("List of Users: " + userModelList.length.toString());
    } else {
      print("Did not Get");
    }
  }
}
