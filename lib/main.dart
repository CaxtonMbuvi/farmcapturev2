//import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'db/db_helper.dart';
import 'resources/utils/dimensions.dart' as dim;
import 'routes/route_helper.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  DatabaseHelper.instance;
    dim.Dimensions();
    runApp(const MyApp());
   //await dep.Binding();
  // Timer(const Duration(seconds: 1), () async{
    
  // });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Farm Capture',
      theme: ThemeData(fontFamily: 'SpaceGrotesk'),
      //initialBinding: dep.HomeBinding(),
      // initialRoute: RouteHelper.getSplashPage(),
      initialRoute: RouteHelper.getLogInPage(),
      getPages: RouteHelper.routes,
    );
  }
}