// To parse this JSON data, do
//
//     final naturalCauseOfDamageApiModel = naturalCauseOfDamageApiModelFromJson(jsonString);

import 'dart:convert';

const String naturalDamageCauseTable = 'natural_damage_cause';

NaturalCauseOfDamageApiModel naturalCauseOfDamageApiModelFromJson(String str) => NaturalCauseOfDamageApiModel.fromJson(json.decode(str));

String naturalCauseOfDamageApiModelToJson(NaturalCauseOfDamageApiModel data) => json.encode(data.toJson());

class NaturalCauseOfDamageApiModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    NaturalCauseOfDamageApiModelResult? result;
    int? dataSetCount;

    NaturalCauseOfDamageApiModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory NaturalCauseOfDamageApiModel.fromJson(Map<String, dynamic> json) => NaturalCauseOfDamageApiModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: NaturalCauseOfDamageApiModelResult.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class NaturalCauseOfDamageApiModelResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    NaturalCauseOfDamageApiModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    factory NaturalCauseOfDamageApiModelResult.fromJson(Map<String, dynamic> json) => NaturalCauseOfDamageApiModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    int? value;
    int? filterOperator;

    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    //String? id;
    int? resultId;
    String? name;
    String? code;
    String? datecomparer;

    ResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        required this.code,
        required this.datecomparer,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
    };
}

class NaturalDamageCauseModelResultElement {
    //String? id;
    int? resultId;
    String? name;
    String? code;
    String? datecomparer;

    NaturalDamageCauseModelResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        required this.code,
        required this.datecomparer,
    });

    factory NaturalDamageCauseModelResultElement.fromJson(Map<String, dynamic> json) => NaturalDamageCauseModelResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
    };
}
