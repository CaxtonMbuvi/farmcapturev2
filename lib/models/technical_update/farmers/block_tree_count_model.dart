// To parse this JSON data, do
//
//     final addBlocksTreeCountModel = addBlocksTreeCountModelFromJson(jsonString);

import 'dart:convert';

const String farmerBlockTreeCountTable = 'farmer_blocks_tree_count';

AddBlocksTreeCountModel addBlocksTreeCountModelFromJson(String str) => AddBlocksTreeCountModel.fromJson(json.decode(str));

String addBlocksTreeCountModelToJson(AddBlocksTreeCountModel data) => json.encode(data.toJson());

class AddBlocksTreeCountModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;
    int? dataSetCount;

    AddBlocksTreeCountModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory AddBlocksTreeCountModel.fromJson(Map<String, dynamic> json) => AddBlocksTreeCountModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class Result {
    String? id;
    String? mainDivisionId;
    String? blockId;
    String? treesInTapping;
    String? undersizedTrees;
    String? unproductiveTreesTpd;
    String? resultId;
    String? datecomparer;
    String? lastupdatedate;
    String? noOfPlantedTrees;
    String? noOfLine;
    String? noOfLifeTree;
    String? noOfTreeSuppliedPlanting;
    String? averageGirth;

    Result({
        required this.id,
        required this.mainDivisionId,
        required this.blockId,
        required this.treesInTapping,
        required this.undersizedTrees,
        required this.unproductiveTreesTpd,
        required this.resultId,
        required this.datecomparer,
        required this.lastupdatedate,
        required this.noOfPlantedTrees,
        required this.noOfLine,
        required this.noOfLifeTree,
        required this.noOfTreeSuppliedPlanting,
        required this.averageGirth,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["Id"],
        mainDivisionId: json["main_division_id"],
        blockId: json["block_id"],
        treesInTapping: json["trees_in_tapping"],
        undersizedTrees: json["undersized_trees"],
        unproductiveTreesTpd: json["unproductive_trees_tpd"],
        resultId: json["id"],
        datecomparer: json["datecomparer"].toString(),
        lastupdatedate: json["lastupdatedate"].toString(),
        noOfPlantedTrees: json["no_of_planted_trees"],
        noOfLine: json["no_of_line"],
        noOfLifeTree: json["no_of_life_tree"],
        noOfTreeSuppliedPlanting: json["no_of_tree_supplied_planting"],
        averageGirth: json["average_girth"],
    );

    Map<String, dynamic> toJson() => {
        "Id": id,
        "main_division_id": mainDivisionId,
        "block_id": blockId,
        "trees_in_tapping": treesInTapping,
        "undersized_trees": undersizedTrees,
        "unproductive_trees_tpd": unproductiveTreesTpd,
        "id": resultId,
        "datecomparer": datecomparer.toString(),
        "lastupdatedate": lastupdatedate.toString(),
        "no_of_planted_trees": noOfPlantedTrees,
        "no_of_line": noOfLine,
        "no_of_life_tree": noOfLifeTree,
        "no_of_tree_supplied_planting": noOfTreeSuppliedPlanting,
        "average_girth": averageGirth,
    };
}


class BlockTreeCountResultModel {
    //String? id;
    String? mainDivisionId;
    String? blockId;
    String? treesInTapping;
    String? undersizedTrees;
    String? unproductiveTreesTpd;
    String? resultId;
    String? datecomparer;
    String? lastupdatedate;
    String? noOfPlantedTrees;
    String? noOfLine;
    String? noOfLifeTree;
    String? noOfTreeSuppliedPlanting;
    String? averageGirth;

    BlockTreeCountResultModel({
        //required this.id,
         this.mainDivisionId,
         this.blockId,
         this.treesInTapping,
         this.undersizedTrees,
         this.unproductiveTreesTpd,
         this.resultId,
         this.datecomparer,
         this.lastupdatedate,
         this.noOfPlantedTrees,
         this.noOfLine,
         this.noOfLifeTree,
         this.noOfTreeSuppliedPlanting,
         this.averageGirth,
    });

    factory BlockTreeCountResultModel.fromJson(Map<String, dynamic> json) => BlockTreeCountResultModel(
        //id: json["Id"],
        mainDivisionId: json["main_division_id"],
        blockId: json["block_id"],
        treesInTapping: json["trees_in_tapping"],
        undersizedTrees: json["immature_trees"],
        unproductiveTreesTpd: json["unproductive_trees"],
        resultId: json["Id"],
        datecomparer: json["datecomparer"].toString(),
        lastupdatedate: json["lastupdatedate"].toString(),
        noOfPlantedTrees: json["no_of_planted_trees"],
        noOfLine: json["no_of_line"],
        noOfLifeTree: json["no_of_life_tree"],
        noOfTreeSuppliedPlanting: json["no_of_tree_supplied_planting"],
        averageGirth: json["average_girth"],
    );

    Map<String, dynamic> toJson() => {
        //"Id": id,
        "main_division_id": mainDivisionId,
        "block_id": blockId,
        "trees_in_tapping": treesInTapping,
        "immature_trees": undersizedTrees,
        "unproductive_trees": unproductiveTreesTpd,
        "Id": resultId,
        "datecomparer": datecomparer.toString(),
        "lastupdatedate": lastupdatedate.toString(),
        "planted_trees": noOfPlantedTrees,
        "no_of_line": noOfLine,
        "no_of_live_trees": noOfLifeTree,
        "no_of_tree_supplied_planting": noOfTreeSuppliedPlanting,
        "average_girth": averageGirth,
    };
}
