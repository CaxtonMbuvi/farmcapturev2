// To parse this JSON data, do
//
//     final damageTreeCountModel = damageTreeCountModelFromJson(jsonString);

import 'dart:convert';

const String damageTreeCountTable = 'farmer_damage_tree_count_fc';

DamageTreeCountModel damageTreeCountModelFromJson(String str) => DamageTreeCountModel.fromJson(json.decode(str));

String damageTreeCountModelToJson(DamageTreeCountModel data) => json.encode(data.toJson());

class DamageTreeCountModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;
    int? dataSetCount;

    DamageTreeCountModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory DamageTreeCountModel.fromJson(Map<String, dynamic> json) => DamageTreeCountModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class Result {
    //String? id;
    String? resultId;
    String? blockId;
    String? userId;
    String? notOfTreesAffected;
    String? typeOfDamageId;

    Result({
         //this.id,
         this.resultId,
         this.blockId,
         this.userId,
         this.notOfTreesAffected,
         this.typeOfDamageId,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        //id: json["\u0024id"],
        resultId: json["id"],
        blockId: json["block_id"],
        userId: json["user_id"],
        notOfTreesAffected: json["not_of_trees_affected"],
        typeOfDamageId: json["type_of_damage_id"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "block_id": blockId,
        "user_id": userId,
        "not_of_trees_affected": notOfTreesAffected,
        "type_of_damage_id": typeOfDamageId,
    };
}

class DamageTreeCountResultModel {
    //String? id;
    String? resultId;
    String? blockId;
    String? userId;
    String? notOfTreesAffected;
    String? typeOfDamageId;

    DamageTreeCountResultModel({
         //this.id,
         this.resultId,
         this.blockId,
         this.userId,
         this.notOfTreesAffected,
         this.typeOfDamageId,
    });

    factory DamageTreeCountResultModel.fromJson(Map<String, dynamic> json) => DamageTreeCountResultModel(
        //id: json["\u0024id"],
        resultId: json["id"],
        blockId: json["block_id"],
        userId: json["user_id"],
        notOfTreesAffected: json["impacted_trees"],
        typeOfDamageId: json["damage_type"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "block_id": blockId,
        "user_id": userId,
        "impacted_trees": notOfTreesAffected,
        "damage_type": typeOfDamageId,
    };
}
