// To parse this JSON data, do
//
//     final pestAndDiseaseModel = pestAndDiseaseModelFromJson(jsonString);

import 'dart:convert';

import 'package:farmcapturev2/models/estate/maindivisions_model.dart';

const String ipmManagementTable = 'farmer_ipm_table';

const String ipmManagementImagesTable = 'farmer_ipm_photos';

PestAndDiseaseModel pestAndDiseaseModelFromJson(String str) => PestAndDiseaseModel.fromJson(json.decode(str));
 
String pestAndDiseaseModelToJson(PestAndDiseaseModel data) => json.encode(data.toJson());

class PestAndDiseaseModel {
    String? blockId;
    String? memberId;
    String? noOfTreeAffected;
    String? damageCaused;
    int? typeOfDamageId;
    String? transactionNo;
    int? userId;
    DateTime? entryDate;
    List<IntegratedPestManagementPhoto>? integratedPestManagementPhotos;

    PestAndDiseaseModel({
         this.blockId,
         this.memberId,
         this.noOfTreeAffected,
         this.damageCaused,
         this.typeOfDamageId,
         this.transactionNo,
         this.userId,
         this.entryDate,
         this.integratedPestManagementPhotos,
    });

    factory PestAndDiseaseModel.fromJson(Map<String, dynamic> json) => PestAndDiseaseModel(
        blockId: json["block_id"],
        memberId: json["member_id"],
        noOfTreeAffected: json["no_of_tree_affected"],
        damageCaused: json["damage_caused"],
        typeOfDamageId: json["type_of_damage_id"],
        transactionNo: json["transaction_no"],
        userId: json["user_id"],
        entryDate: DateTime.parse(json["entry_date"]),
        integratedPestManagementPhotos: List<IntegratedPestManagementPhoto>.from(json["IntegratedPestManagementPhotos"].map((x) => IntegratedPestManagementPhoto.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "block_id": blockId,
        "member_id": memberId,
        "no_of_tree_affected": noOfTreeAffected,
        "damage_caused": damageCaused,
        "type_of_damage_id": typeOfDamageId,
        "transaction_no": transactionNo,
        "user_id": userId,
        "entry_date": "${entryDate!.year.toString().padLeft(4, '0')}-${entryDate!.month.toString().padLeft(2, '0')}-${entryDate!.day.toString().padLeft(2, '0')}",
        "IntegratedPestManagementPhotos": List<dynamic>.from(integratedPestManagementPhotos!.map((x) => x.toJson())),
    };
}

class IntegratedPestManagementPhoto {
    String photoUrl;
    String photoUniqNo;

    IntegratedPestManagementPhoto({
        required this.photoUrl,
        required this.photoUniqNo,
    });

    factory IntegratedPestManagementPhoto.fromJson(Map<String, dynamic> json) => IntegratedPestManagementPhoto(
        photoUrl: json["photo_url"],
        photoUniqNo: json["photo_uniq_no"],
    );

    Map<String, dynamic> toJson() => {
        "photo_url": photoUrl,
        "photo_uniq_no": photoUniqNo,
    };
}


// class PestAndDiseaseResultModel {
//     String? divisionId;
//     String? blockId;
//     String? memberId;
//     String? noOfTreeAffected;
//     String? damageCaused;
//     String? typeOfDamageId;
//     String? transactionNo;
//     String? userId;
//     String? entryDate;

//     PestAndDiseaseResultModel({
//          this.divisionId,
//          this.blockId,
//          this.memberId,
//          this.noOfTreeAffected,
//          this.damageCaused,
//          this.typeOfDamageId,
//          this.transactionNo,
//          this.userId,
//          this.entryDate,
//     });

//     factory PestAndDiseaseResultModel.fromJson(Map<String, dynamic> json) => PestAndDiseaseResultModel(
//         blockId: json["block_id"],
//         memberId: json["member_id"],
//         noOfTreeAffected: json["no_of_tree_affected"],
//         damageCaused: json["damage_caused"],
//         typeOfDamageId: json["type_of_damage_id"],
//         transactionNo: json["transaction_no"],
//         userId: json["user_id"],
//         entryDate: json["entry_date"].toString(),
//     );

//     Map<String, dynamic> toJson() => {
//         "block_id": blockId,
//         "member_id": memberId,
//         "no_of_tree_affected": noOfTreeAffected,
//         "damage_caused": damageCaused,
//         "type_of_damage_id": typeOfDamageId,
//         "transaction_no": transactionNo,
//         "user_id": userId,
//         "entry_date": entryDate.toString(),
//     };
// }

class PestAndDiseaseResultModel {
    String? divisionId;
    String? blockId;
    String? memberId;
    String? noOfTreeAffected;
    String? damageCaused;
    String? typeOfDamageId;
    String? transactionNo;
    String? userId;
    String? entryDate;


    PestAndDiseaseResultModel({
         this.divisionId,
         this.blockId,
         this.memberId,
         this.noOfTreeAffected,
         this.damageCaused,
         this.typeOfDamageId,
         this.transactionNo,
         this.userId,
         this.entryDate,
    });

    factory PestAndDiseaseResultModel.fromJson(Map<String, dynamic> json) => PestAndDiseaseResultModel(
      divisionId: json["main_division_id"],
      blockId: json["block_id"],
      typeOfDamageId: json["disease_type_id"],
      noOfTreeAffected: json["affected_trees"],
      userId: json["user_id"],
      transactionNo: json["transaction_no"],
      memberId: json["member_id"],
      damageCaused: json["damage_caused"],
      entryDate: json["date_today"].toString(),
    );

    Map<String, dynamic> toJson() => {
        "main_division_id": divisionId,
        "block_id": blockId,
        "disease_type_id": typeOfDamageId,
        "affected_trees": noOfTreeAffected,
        "user_id": userId,
        "transaction_no": transactionNo,
        "member_id": memberId,
        "damage_caused": damageCaused,
        "date_today": entryDate.toString(),
    };
}


class IntegratedPestManagementPhotoResultModel {
    String? photoUrl;
    String? photoUniqNo;
    String? divisionId;
    String? blockId;
    String? memberId;
    String? typeOfDamageId;
    String? transactionNo;
    String? userId;
    String? entryDate;


    IntegratedPestManagementPhotoResultModel({
         this.photoUrl,
         this.photoUniqNo,
         this.divisionId,
         this.blockId,
         this.memberId,
         this.typeOfDamageId,
         this.transactionNo,
         this.userId,
         this.entryDate,
    });

    factory IntegratedPestManagementPhotoResultModel.fromJson(Map<String, dynamic> json) => IntegratedPestManagementPhotoResultModel(
      photoUrl: json["photo_url"],
        photoUniqNo: json["photo_uniq_no"],
      divisionId: json["main_division_id"],
      blockId: json["block_id"],
      typeOfDamageId: json["disease_type_id"],
      userId: json["user_id"],
      transactionNo: json["transaction_no"],
      memberId: json["member_id"],
      entryDate: json["date_today"].toString(),
    );

    Map<String, dynamic> toJson() => {
      "photo_url": photoUrl,
        "photo_uniq_no": photoUniqNo,
        "main_division_id": divisionId,
        "block_id": blockId,
        "disease_type_id": typeOfDamageId,
        "user_id": userId,
        "transaction_no": transactionNo,
        "member_id": memberId,
        "date_today": entryDate.toString(),
    };
}