// To parse this JSON data, do
//
//     final gapFillingModel = gapFillingModelFromJson(jsonString);

import 'dart:convert';

const String gapFillingTable = 'gap_filling_fc';

GapFillingModel gapFillingModelFromJson(String str) => GapFillingModel.fromJson(json.decode(str));

String gapFillingModelToJson(GapFillingModel data) => json.encode(data.toJson());

class GapFillingModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;
    int? dataSetCount;

    GapFillingModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory GapFillingModel.fromJson(Map<String, dynamic> json) => GapFillingModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class Result {
    String? id;
    String? transactionNo;
    String? blockId;
    String? lineNo;
    String? dominantCloneId;
    String? cloneUsedId;
    String? noOfResuppliedTrees;
    String? entryDate;
    String? userId;

    Result({
        required this.id,
        required this.transactionNo,
        required this.blockId,
        required this.lineNo,
        required this.dominantCloneId,
        required this.cloneUsedId,
        required this.noOfResuppliedTrees,
        required this.entryDate,
        required this.userId,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["\u0024id"],
        transactionNo: json["transaction_no"],
        blockId: json["block_id"],
        lineNo: json["line_no"],
        dominantCloneId: json["dominant_clone_id"],
        cloneUsedId: json["clone_used_id"],
        noOfResuppliedTrees: json["no_of_resupplied_trees"],
        entryDate: json["entry_date"].toString(),
        userId: json["user_id"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "transaction_no": transactionNo,
        "block_id": blockId,
        "line_no": lineNo,
        "dominant_clone_id": dominantCloneId,
        "clone_used_id": cloneUsedId,
        "no_of_resupplied_trees": noOfResuppliedTrees,
        "entry_date": entryDate.toString(),
        "user_id": userId,
    };
}

class GapFillingResultModel {
    String? id;
    String? requisitionNo;
    String? transactionNo;
    String? blockId;
    String? mainDivisionId;
    String? lineNo;
    String? dominantCloneId;
    String? cloneUsedId;
    String? noOfResuppliedTrees;
    String? entryDate;
    String? userId;

    GapFillingResultModel({
         this.id,
         this.requisitionNo,
         this.transactionNo,
         this.blockId,
         this.mainDivisionId,
         this.lineNo,
         this.dominantCloneId,
         this.cloneUsedId,
         this.noOfResuppliedTrees,
         this.entryDate,
         this.userId,
    });

    factory GapFillingResultModel.fromJson(Map<String, dynamic> json) => GapFillingResultModel(
        id: json["Id"],
        requisitionNo: json["requisition_no"],
        transactionNo: json["transaction_no"],
        blockId: json["block_id"],
        mainDivisionId: json["main_division_id"],
        lineNo: json["line_no"],
        dominantCloneId: json["dominant_clone_id"],
        cloneUsedId: json["clone_replanting"],
        noOfResuppliedTrees: json["no_of_resupplied_trees"],
        entryDate: json["date_today"].toString(),
        userId: json["user_id"],
    );

    Map<String, dynamic> toJson() => {
        "Id": id,
        "requisition_no" : requisitionNo,
        "transaction_no": transactionNo,
        "block_id": blockId,
        "main_division_id" : mainDivisionId,
        "line_no": lineNo,
        "dominant_clone_id": dominantCloneId,
        "clone_replanting": cloneUsedId,
        "no_of_resupplied_trees": noOfResuppliedTrees,
        "date_today": entryDate.toString(),
        "user_id": userId,
    };
}
