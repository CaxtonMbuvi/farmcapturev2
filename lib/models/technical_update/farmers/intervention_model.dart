// To parse this JSON data, do
//
//     final interventionModel = interventionModelFromJson(jsonString);

import 'dart:convert';

const String farmerInterventionTable = 'farmer_interventions_fc';

InterventionModel interventionModelFromJson(String str) => InterventionModel.fromJson(json.decode(str));

String interventionModelToJson(InterventionModel data) => json.encode(data.toJson());

class InterventionModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    InterventionModelResult? result;
    int? dataSetCount;

    InterventionModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory InterventionModel.fromJson(Map<String, dynamic> json) => InterventionModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: InterventionModelResult.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class InterventionModelResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    InterventionModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    factory InterventionModelResult.fromJson(Map<String, dynamic> json) => InterventionModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    int? value;
    int? filterOperator;

    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    //String? id;
    String? resultId;
    String? name;
    String? code;
    String? datecomparer;

    ResultElement({
        //required this.id,
         this.resultId,
         this.name,
         this.code,
         this.datecomparer,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
       // "\u0024id": id,
        "id": resultId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
    };
}

//  'Id INTEGER,' //this is the sid
//         'main_division_id VARCHAR,'
//         'block_id VARCHAR,'
//         'intervention_type VARCHAR,'
//         'category_used VARCHAR,'
//         'amount_applied VARCHAR,'
//         'date_applied VARCHAR,'
//         'cpa_name VARCHAR,'
//         'cpa_type_used VARCHAR,'
//         'application_methd VARCHAR,'
//         'weather_type VARCHAR,'
//         'user_id VARCHAR,'
//         'transaction_no VARCHAR,'
//         'member_id VARCHAR,'
//         'comment VARCHAR,'

class InterventionResultElement {
    //String? id;
    String? resultId;
    String? main_division_id;
    String? block_id;
    String? intervention_type;
    String? category_used;
    String? amount_applied;
    String? date_applied;
    String? cpa_name;
    String? cpa_type_used;
    String? application_methd;
    String? weather_type;
    String? user_id;
    String? transaction_no;
    String? member_id;
    String? comment;

    InterventionResultElement({
        //required this.id,
         this.resultId,
         this.main_division_id,
         this.block_id,
         this.intervention_type,
         this.category_used,
         this.amount_applied,
         this.date_applied,
         this.cpa_name,
         this.cpa_type_used,
         this.application_methd,
         this.weather_type,
         this.user_id,
         this.transaction_no,
         this.member_id,
         this.comment
    });

    factory InterventionResultElement.fromJson(Map<String, dynamic> json) => InterventionResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        main_division_id: json["main_division_id"],
        block_id: json["block_id"],
        intervention_type: json["intervention_type"],
        category_used: json["category_used"],
        amount_applied: json["amount_applied"],
        date_applied: json["date_applied"],
        cpa_name: json["cpa_name"],
        cpa_type_used: json["cpa_type_used"],
        application_methd: json["application_methd"],
        weather_type: json["weather_type"],
        user_id: json["user_id"],
        transaction_no: json["transaction_no"],
        member_id: json["member_id"],
        comment: json["comment"],
    );

    Map<String, dynamic> toJson() => {
       // "\u0024id": id,
        "id": resultId,
        "main_division_id": main_division_id,
        "block_id": block_id,
        "intervention_type": intervention_type,
        "category_used": category_used,
        "amount_applied": amount_applied,
        "date_applied": date_applied,
        "cpa_name": cpa_name,
        "cpa_type_used": cpa_type_used,
        "application_methd": application_methd,
        "weather_type": weather_type,
        "user_id": user_id,
        "transaction_no": transaction_no,
        "member_id": member_id,
        "comment": comment,
    };
}
