// To parse this JSON data, do
//
//     final girthMeasurementModel = girthMeasurementModelFromJson(jsonString);

import 'dart:convert';

const String girthMeasurementTable = 'girth_measurement';

GirthMeasurementModel girthMeasurementModelFromJson(String str) => GirthMeasurementModel.fromJson(json.decode(str));

String girthMeasurementModelToJson(GirthMeasurementModel data) => json.encode(data.toJson());

class GirthMeasurementModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;
    int? dataSetCount;

    GirthMeasurementModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory GirthMeasurementModel.fromJson(Map<String, dynamic> json) => GirthMeasurementModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class Result {
    String? id;
    String? transactionNo;
    String? blockId;
    String? memberId;
    String? treeNo;
    String? girthLengthCm;
    String? entryDate;
    String? userId;

    Result({
         this.id,
         this.transactionNo,
         this.blockId,
        this.memberId,
         this.treeNo,
         this.girthLengthCm,
         this.entryDate,
         this.userId,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["Id"],
        transactionNo: json["transaction_no"],
        blockId: json["block_id"],
        memberId: json["member_id"],
        treeNo: json["tree_no"],
        girthLengthCm: json["girth_measure"].toString(),
        entryDate: json["date_today"].toString(),
        userId: json["user_id"],
    );

    Map<String, dynamic> toJson() => {
        "Id": id,
        "transaction_no": transactionNo,
        "block_id": blockId,
        "member_id": memberId,
        "tree_no": treeNo,
        "girth_measure": girthLengthCm,
        "date_today": entryDate.toString(),
        "user_id": userId,
    };
}


class GirthMeasurementResultModel {
    String? id;
    String? transactionNo;
    String? divisionId;
    String? blockId;
    String? memberId;
    String? treeNo;
    String? girthLengthCm;
    String? entryDate;
    String? userId;

    GirthMeasurementResultModel({
         this.id,
         this.transactionNo,
         this.divisionId,
         this.blockId,
        this.memberId,
         this.treeNo,
         this.girthLengthCm,
         this.entryDate,
         this.userId,
    });

    factory GirthMeasurementResultModel.fromJson(Map<String, dynamic> json) => GirthMeasurementResultModel(
        id: json["Id"],
        transactionNo: json["transaction_no"],
        divisionId: json["main_division_id"],
        blockId: json["block_id"],
        memberId: json["member_id"],
        treeNo: json["tree_no"],
        girthLengthCm: json["girth_measure"].toString(),
        entryDate: json["date_today"].toString(),
        userId: json["user_id"],
    );

    Map<String, dynamic> toJson() => {
        "Id": id,
        "transaction_no": transactionNo,
        "main_division_id": divisionId,
        "block_id": blockId,
        "member_id": memberId,
        "tree_no": treeNo,
        "girth_measure": girthLengthCm,
        "date_today": entryDate.toString(),
        "user_id": userId,
    };
}

