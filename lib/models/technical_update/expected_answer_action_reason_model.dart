// To parse this JSON data, do
//
//     final expectedAnswerActionsAndReasonsApiModel = expectedAnswerActionsAndReasonsApiModelFromJson(jsonString);

import 'dart:convert';

const String expectedAnswerActionReasonTable = 'expected_answer_action_reason';

ExpectedAnswerActionsAndReasonsApiModel expectedAnswerActionsAndReasonsApiModelFromJson(String str) => ExpectedAnswerActionsAndReasonsApiModel.fromJson(json.decode(str));

String expectedAnswerActionsAndReasonsApiModelToJson(ExpectedAnswerActionsAndReasonsApiModel data) => json.encode(data.toJson());

class ExpectedAnswerActionsAndReasonsApiModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    ExpectedAnswerActionsAndReasonsApiModelResult? result;
    int? dataSetCount;

    ExpectedAnswerActionsAndReasonsApiModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
        required this.dataSetCount,
    });

    factory ExpectedAnswerActionsAndReasonsApiModel.fromJson(Map<String, dynamic> json) => ExpectedAnswerActionsAndReasonsApiModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: ExpectedAnswerActionsAndReasonsApiModelResult.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class ExpectedAnswerActionsAndReasonsApiModelResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    ExpectedAnswerActionsAndReasonsApiModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    factory ExpectedAnswerActionsAndReasonsApiModelResult.fromJson(Map<String, dynamic> json) => ExpectedAnswerActionsAndReasonsApiModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    int? value;
    int? filterOperator;

    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    //String? id;
    int? resultId;
    int? farmInspectionsQuizExpectedAnswersItemId;
    String? answer;
    String? separatorCode;
    String? datecomparer;

    ResultElement({
        //required this.id,
        required this.resultId,
        required this.farmInspectionsQuizExpectedAnswersItemId,
        required this.answer,
        required this.separatorCode,
        required this.datecomparer,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        farmInspectionsQuizExpectedAnswersItemId: json["farm_inspections_quiz_expected_answers_item_id"],
        answer: json["answer"],
        separatorCode: json["separator_code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "farm_inspections_quiz_expected_answers_item_id": farmInspectionsQuizExpectedAnswersItemId,
        "answer": answer,
        "separator_code": separatorCode,
        "datecomparer": datecomparer,
    };
}


class ExpectedAnswerActionReasonModelResultElement {
    //String? id;
    int? resultId;
    int? farmInspectionsQuizExpectedAnswersItemId;
    String? answer;
    String? separatorCode;
    String? datecomparer;

    ExpectedAnswerActionReasonModelResultElement({
        //required this.id,
        required this.resultId,
        required this.farmInspectionsQuizExpectedAnswersItemId,
        required this.answer,
        required this.separatorCode,
        required this.datecomparer,
    });

    factory ExpectedAnswerActionReasonModelResultElement.fromJson(Map<String, dynamic> json) => ExpectedAnswerActionReasonModelResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        farmInspectionsQuizExpectedAnswersItemId: json["farm_inspections_quiz_expected_answers_item_id"],
        answer: json["answer"],
        separatorCode: json["separator_code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "farm_inspections_quiz_expected_answers_item_id": farmInspectionsQuizExpectedAnswersItemId,
        "answer": answer,
        "separator_code": separatorCode,
        "datecomparer": datecomparer,
    };
}
