// To parse this JSON data, do
//
//     final actionsAndReasons = actionsAndReasonsFromJson(jsonString);

import 'dart:convert';

const String actionsReasonsTable = 'action_reasons';

ActionsAndReasonsModel actionsAndReasonsModelFromJson(String str) => ActionsAndReasonsModel.fromJson(json.decode(str));

String actionsAndReasonsModelToJson(ActionsAndReasonsModel data) => json.encode(data.toJson());

class ActionsAndReasonsModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    ActionsAndReasonsResult? result;

    ActionsAndReasonsModel({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
    });

    factory ActionsAndReasonsModel.fromJson(Map<String, dynamic> json) => ActionsAndReasonsModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: ActionsAndReasonsResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class ActionsAndReasonsResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    ActionsAndReasonsResult({
         this.id,
         this.request,
         this.isSuccessull,
         this.dataSetCount,
         this.result,
    });

    factory ActionsAndReasonsResult.fromJson(Map<String, dynamic> json) => ActionsAndReasonsResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
         this.id,
         this.take,
         this.skip,
         this.page,
         this.pageSize,
         this.filter,
         this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
         this.id,
         this.logic,
         this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    int? value;
    int? filterOperator;

    FilterElement({
         this.id,
         this.field,
         this.value,
         this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    //String? id;
    int? resultId;
    int? farmInspectionsQuizExpectedAnswersItemId;
    String? answer;
    String? separatorCode;
    String? datecomparer;

    ResultElement({
         //this.id,
         this.resultId,
         this.farmInspectionsQuizExpectedAnswersItemId,
         this.answer,
         this.separatorCode,
         this.datecomparer,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        farmInspectionsQuizExpectedAnswersItemId: json["farm_inspections_quiz_expected_answers_item_id"],
        answer: json["answer"],
        separatorCode: json["separator_code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
       // "\u0024id": id,
        "id": resultId,
        "farm_inspections_quiz_expected_answers_item_id": farmInspectionsQuizExpectedAnswersItemId,
        "answer": answer,
        "separator_code": separatorCode,
        "datecomparer": datecomparer,
    };
}


class ActionsAndReasonsModelResultElement {
    //String? id;
    int? resultId;
    String? farmInspectionsQuizExpectedAnswersItemId;
    String? answer;
    String? separatorCode;
    String? datecomparer;

    ActionsAndReasonsModelResultElement({
         //this.id,
         this.resultId,
         this.farmInspectionsQuizExpectedAnswersItemId,
         this.answer,
         this.separatorCode,
         this.datecomparer,
    });

    factory ActionsAndReasonsModelResultElement.fromJson(Map<String, dynamic> json) => ActionsAndReasonsModelResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        farmInspectionsQuizExpectedAnswersItemId: json["farm_inspections_quiz_expected_answers_item_id"].toString(),
        answer: json["answer"],
        separatorCode: json["separator_code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
       // "\u0024id": id,
        "id": resultId,
        "farm_inspections_quiz_expected_answers_item_id": farmInspectionsQuizExpectedAnswersItemId,
        "answer": answer,
        "separator_code": separatorCode,
        "datecomparer": datecomparer,
    };
}