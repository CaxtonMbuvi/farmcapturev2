class FarmMapModel {
    FarmMapModel({
       // this.id,
        this.memberId,
        this.memberNo,
        this.polygon,
        this.transactionNo,
        this.mapCategory,
        this.status,
    });

    //int? id;
    String? memberId;
    String? memberNo;
    String? polygon;
    String? transactionNo;
    String? mapCategory;
    String? status;

    factory FarmMapModel.fromJson(Map<String, dynamic> json) => FarmMapModel(
        //id: json["id"],
        memberId: json["member_id"],
        memberNo: json["member_no"],
        polygon: json["polygon"],
        transactionNo: json["transaction_no"],
        mapCategory: json["map_category"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
       // "id": id,
        "member_id": memberId,
        "member_no": memberNo,
        "polygon": polygon,
        "transaction_no": transactionNo,
        "map_category": mapCategory,
        "status": status,
    };
}