// To parse this JSON data, do
//
//     final inpQuestionsModel = inpQuestionsModelFromJson(jsonString);

import 'dart:convert';

const String inputQuestionsTable = 'input_questions';

InpQuestionsModel inpQuestionsModelFromJson(String str) => InpQuestionsModel.fromJson(json.decode(str));

String inpQuestionsModelToJson(InpQuestionsModel data) => json.encode(data.toJson());
 
class InpQuestionsModel {
    InpQuestionsModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    InpQuestionsModelResult? result;

    factory InpQuestionsModel.fromJson(Map<String, dynamic> json) => InpQuestionsModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: InpQuestionsModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class InpQuestionsModelResult {
    InpQuestionsModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory InpQuestionsModelResult.fromJson(Map<String, dynamic> json) => InpQuestionsModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    String? id;
    String? field;
    int? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        this.id,
        this.resultId,
        this.unitCode,
        this.name,
        this.level,
        this.identifierCode,
        this.mainCategoryId,
        this.mainCategory,
        this.isMultipleChoice,
        this.datecomparer,
        this.answerType,
    });

    String? id;
    int? resultId;
    String? unitCode;
    String? name;
    String? level;
    String? identifierCode;
    int? mainCategoryId;
    String? mainCategory;
    bool? isMultipleChoice;
    String? datecomparer;
    String? answerType;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        unitCode: json["unitCode"],
        name: json["name"],
        level: json["level"],
        identifierCode: json["identifier_code"],
        mainCategoryId: json["main_category_id"],
        mainCategory: json["mainCategory"],
        isMultipleChoice: json["is_multiple_choice"],
        datecomparer: json["datecomparer"].toString(),
        answerType: json["answer_type"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "unitCode": unitCode,
        "name": name,
        "level": level,
        "identifier_code": identifierCode,
        "main_category_id": mainCategoryId,
        "mainCategory": mainCategory,
        "is_multiple_choice": isMultipleChoice,
        "datecomparer": datecomparer,
        "answer_type": answerType,
    };
}

class InpQuestionsModelResultElement {
    InpQuestionsModelResultElement({
        this.id,
        this.resultId,
        this.unitCode,
        this.name,
        this.level,
        this.identifierCode,
        this.mainCategoryId,
        this.mainCategory,
        this.isMultipleChoice,
        this.datecomparer,
        this.answerType,
    });

    String? id;
    int? resultId;
    String? unitCode;
    String? name;
    String? level;
    String? identifierCode;
    int? mainCategoryId;
    String? mainCategory;
    bool? isMultipleChoice;
    String? datecomparer;
    String? answerType;

    factory InpQuestionsModelResultElement.fromJson(Map<String, dynamic> json) => InpQuestionsModelResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        unitCode: json["unitCode"],
        name: json["name"],
        level: json["level"],
        identifierCode: json["identifier_code"],
        mainCategoryId: json["main_category_id"],
        mainCategory: json["mainCategory"],
        isMultipleChoice: json["is_multiple_choice"],
        datecomparer: json["datecomparer"].toString(),
        answerType: json["answer_type"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "unitCode": unitCode,
        "name": name,
        "level": level,
        "identifier_code": identifierCode,
        "main_category_id": mainCategoryId,
        "mainCategory": mainCategory,
        "is_multiple_choice": isMultipleChoice,
        "datecomparer": datecomparer,
        "answer_type": answerType,
    };
}
