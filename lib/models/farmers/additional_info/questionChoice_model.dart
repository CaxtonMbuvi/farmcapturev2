class QuestionChoicesModel {
  QuestionChoicesModel({
    this.questionId,
    this.questionName,
    this.categoryId,
    this.choices,
    this.selectedChoice,
  });

  String? questionId;
  String? questionName;
  String? categoryId;
  List<ChoiceModel>? choices;
  ChoiceModel? selectedChoice;
  // String? selectedName;

  factory QuestionChoicesModel.fromJson(Map<String, dynamic> json) =>
      QuestionChoicesModel(
          questionId: json["questionId"],
          questionName: json["questionName"],
          categoryId: json["categoryId"],
          choices: List<ChoiceModel>.from(
              json["filters"].map((x) => ChoiceModel.fromJson(x))),
          selectedChoice: ChoiceModel.fromJson(json["selectedChoice"]));

  Map<String, dynamic> toJson() => {
        "questionId": questionId,
        "questionName": questionName,
        "categoryId": categoryId,
        "filters": List<dynamic>.from(choices!.map((x) => x.toJson())),
      };
}

class ChoiceModel {
  ChoiceModel({
    this.choiceId,
    this.choiceName,
    this.questionId,
    this.questionName,
    //this.categoryId,
  });

  String? choiceId;
  String? choiceName;
  String? questionId;
  String? questionName;
  //String? categoryId;

  factory ChoiceModel.fromJson(Map<String, dynamic> json) => ChoiceModel(
        choiceId: json["choiceId"],
        choiceName: json["choiceName"],
        questionId: json["question_Id"],
        questionName: json["question_Name"],
        //categoryId: json["categoryId"],
      );

  Map<String, dynamic> toJson() => {
        "choiceId": choiceId,
        "choiceName": choiceName,
        "questionId": questionId,
        "questionName": questionName,
        //"categoryId": categoryId,
      };
}

// class AnswerModel {
//   AnswerModel({
//     this.question_Id,
//     this.question_Name,
//     this.choice_Id,
//     this.choice_Name,
//   });

//   String? question_Id;
//   String? question_Name;
//   String? choice_Id;
//   String? choice_Name;

//   factory AnswerModel.fromJson(Map<String, dynamic> json) => AnswerModel(
//         question_Id: json["question_Id"],
//         question_Name: json["question_Name"],
//         choice_Id: json["choice_Id"],
//         choice_Name: json["choice_Name"],
//       );

//   Map<String, dynamic> toJson() => {
//         "questionId": question_Id,
//         "questionName": question_Name,
//         "choiceId": choice_Id,
//         "choiceName": choice_Name,
//       };
// }
