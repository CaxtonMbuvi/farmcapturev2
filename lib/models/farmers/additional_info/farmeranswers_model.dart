const String farmerInputQuestionaireAnswersTable = 'farmers_input_questionaire_answers';

class FarmerAnswersModel {
    FarmerAnswersModel({
        this.id,
        this.farmVisitTransactionNo,
        this.farmInspectorQuizId,
        this.answerOption,
        this.answerId,
        this.farmInspectorVisitId,
        this.transactionNo,
        this.dateTime,
        this.status,
    });

    String? id;
    String? farmVisitTransactionNo;
    String? farmInspectorQuizId;
    String? answerOption;
    String? answerId;
    String? farmInspectorVisitId;
    String? transactionNo;
    String? dateTime;
    String? status;

    factory FarmerAnswersModel.fromJson(Map<String, dynamic> json) => FarmerAnswersModel(
        //id: json["\u0024id"],
        id: json["id"],
        farmVisitTransactionNo: json["farm_visit_transactionNo"],
        farmInspectorQuizId: json["farm_inspections_quiz_id"],
        answerOption: json["answer_option"],
        answerId: json["answer_id"],
        farmInspectorVisitId: json["farm_inspector_visit_id"],
        transactionNo: json["transaction_no"],
        dateTime: json["datetime"],
        status: json["sync_status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "farm_visit_transactionNo": farmVisitTransactionNo,
        "farm_inspections_quiz_id": farmInspectorQuizId,
        "answer_option": answerOption,
        "answer_id": answerId,
        "farm_inspector_visit_id": farmInspectorVisitId,
        "transaction_no": transactionNo,
        "datetime": dateTime,
        "sync_status": status,
    };
}