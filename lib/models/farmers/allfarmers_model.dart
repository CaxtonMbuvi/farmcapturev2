import 'dart:convert';

const String farmersTable = 'farmers';

AllFarmersModel allFarmersModelFromJson(String str) =>
    AllFarmersModel.fromJson(json.decode(str));

String allFarmersModelToJson(AllFarmersModel data) =>
    json.encode(data.toJson());

class AllFarmersModel {
  AllFarmersModel({
    required this.id,
    required this.isOkay,
    required this.message,
    required this.statusCode,
    required this.result,
  });

  String? id;
  bool? isOkay;
  String? message;
  String? statusCode;
  AllFarmersModelResult? result;

  factory AllFarmersModel.fromJson(Map<String, dynamic> json) =>
      AllFarmersModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: AllFarmersModelResult.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
      };
}

class AllFarmersModelResult {
  AllFarmersModelResult({
    required this.id,
    required this.request,
    required this.isSuccessull,
    required this.dataSetCount,
    required this.result,
  });

  String? id;
  Request? request;
  bool? isSuccessull;
  int? dataSetCount;
  List<ResultElement>? result;

  factory AllFarmersModelResult.fromJson(Map<String, dynamic> json) =>
      AllFarmersModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(
            json["Result"].map((x) => ResultElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
      };
}

class Request {
  Request({
    required this.id,
    required this.take,
    required this.skip,
    required this.page,
    required this.pageSize,
    required this.filter,
    this.sort,
  });

  String? id;
  int? take;
  int? skip;
  int? page;
  int? pageSize;
  RequestFilter? filter;
  dynamic sort;

  factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
      };
}

class RequestFilter {
  RequestFilter({
    required this.id,
    required this.logic,
    required this.filters,
  });

  String? id;
  String? logic;
  List<FilterElement>? filters;

  factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(
            json["filters"].map((x) => FilterElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
      };
}

class FilterElement {
  FilterElement({
    required this.id,
    required this.field,
    required this.value,
    required this.filterOperator,
  });

  String? id;
  String? field;
  double? value;
  int? filterOperator;

  factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"]?.toDouble(),
        filterOperator: json["operator"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
      };
}

class ResultElement {
  ResultElement({
    this.id,
    this.sId,
    this.resultId,
    this.memberNo,
    this.fullName,
    this.phone1,
    this.natId,
    this.genderTypeId,
    this.memberCategoryId,
    this.memberSubCategoryId,
    this.empStaffLoginAssignmentId,
    this.isActive,
    this.divisionId,
    this.routeId,
    this.collectionCentreId,
    this.gangId,
    this.departmentId,
    this.datecomparer,
    this.mainCategoryCode,
  });

  String? id;
  String? sId;
  int? resultId;
  String? memberNo;
  String? fullName;
  String? phone1;
  String? natId;
  int? genderTypeId;
  int? memberCategoryId;
  int? memberSubCategoryId;
  dynamic empStaffLoginAssignmentId;
  bool? isActive;
  int? divisionId;
  dynamic routeId;
  dynamic collectionCentreId;
  int? gangId;
  dynamic departmentId;
  String? datecomparer;
  String? mainCategoryCode;

  factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        //sId: json["sid"],
        resultId: json["id"],
        memberNo: json["member_no"],
        fullName: json["full_name"],
        phone1: json["phone1"],
        natId: json["nat_id"],
        genderTypeId: json["gender_type_id"],
        memberCategoryId: json["member_category_id"],
        memberSubCategoryId: json["member_sub_category_id"],
        empStaffLoginAssignmentId: json["emp_staff_login_assignment_id"],
        isActive: json["Is_active"],
        divisionId: json["division_id"],
        routeId: json["route_id"],
        collectionCentreId: json["collection_centre_id"],
        gangId: json["gang_id"],
        departmentId: json["department_id"],
        datecomparer: json["datecomparer"].toString(),
        mainCategoryCode: json["main_category_code"],
      );

  Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        //"sid": sId,
        "id": resultId,
        "member_no": memberNo,
        "full_name": fullName,
        "phone1": phone1,
        "nat_id": natId,
        "gender_type_id": genderTypeId,
        "member_category_id": memberCategoryId,
        "member_sub_category_id": memberSubCategoryId,
        "emp_staff_login_assignment_id": empStaffLoginAssignmentId,
        "Is_active": isActive,
        "division_id": divisionId,
        "route_id": routeId,
        "collection_centre_id": collectionCentreId,
        "gang_id": gangId,
        "department_id": departmentId,
        "datecomparer": datecomparer,
        "main_category_code": mainCategoryCode,
      };
}

class FarmerModelDB {
  String? id;
  String? syncId;
  int? resultId;
  String? memberNo;
  String? fullName;
  String? firstName;
  String? middleName;
  String? lastName;
  String? phone1;
  String? dob;
  String? natId;
  String? genderTypeId;
  String? maritalStatus;
  String? memberCategoryId;
  String? memberSubCategoryId;
  String? empStaffLoginAssignmentId;
  bool? isActive;
  String? divisionId;
  String? routeId;
  String? collectionCentreId;
  String? gangId;
  String? departmentId;
  String? datecomparer;
  String? regionId;
  String? districtId;
  String? wardId;
  String? villageId;
  String? passportUrl;
  String? photoString;
  String? nationalityId;
  String? bankdetailsId;
  String? bankbranchId;
  String? accNo;
  String? mainCategoryCode;

  FarmerModelDB({
    this.id,
    this.syncId,
    this.resultId,
    this.memberNo,
    this.fullName,
    this.firstName,
    this.middleName,
    this.lastName,
    this.phone1,
    this.dob,
    this.natId,
    this.genderTypeId,
    this.maritalStatus,
    this.memberCategoryId,
    this.memberSubCategoryId,
    this.empStaffLoginAssignmentId,
    this.isActive,
    this.divisionId,
    this.routeId,
    this.collectionCentreId,
    this.gangId,
    this.departmentId,
    this.datecomparer,
    this.regionId,
    this.districtId,
    this.wardId,
    this.villageId,
    this.passportUrl,
    this.photoString,
    this.nationalityId,
    this.bankdetailsId,
    this.bankbranchId,
    this.accNo,
    this.mainCategoryCode,
  });

  // //model to map
  factory FarmerModelDB.fromJson(Map<String, dynamic> json) => FarmerModelDB(
        //id: json["\u0024id"],
        syncId: json["syncId"],
        resultId: json["id"],
        memberNo: json["member_no"],
        fullName: json["full_name"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        middleName: json["middle_name"],
        phone1: json["phone1"],
        dob: json["dob"],
        natId: json["nat_id"],
        genderTypeId: json["gender_type_id"],
        maritalStatus: json["marital_status"],
        memberCategoryId: json["member_category_id"],
        memberSubCategoryId: json["member_sub_category_id"],
        empStaffLoginAssignmentId: json["emp_staff_login_assignment_id"],
        isActive: json["Is_active"],
        divisionId: json["division_id"],
        routeId: json["route_id"],
        collectionCentreId: json["collection_centre_id"],
        gangId: json["gang_id"],
        departmentId: json["department_id"],
        datecomparer: json["datecomparer"].toString(),
        regionId: json["regionId"],
        districtId: json["districtId"],
        wardId: json["wardId"],
        villageId: json["villageId"],
        passportUrl: json["passporturl"],
        photoString: json["photoString"],
        nationalityId: json["nationalityId"],
        bankdetailsId: json["bankdetailsId"],
        bankbranchId: json["bankbranchId"],
        accNo: json["accNo"],
        mainCategoryCode: json["main_category_code"],
      );

  Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "syncId": syncId,
        "id": resultId,
        "member_no": memberNo,
        "full_name": fullName,
        "first_name": firstName,
        "last_name": lastName,
        "middle_name": middleName,
        "phone1": phone1,
        "dob": dob,
        "nat_id": natId,
        "gender_type_id": genderTypeId,
        "marital_status": maritalStatus,
        "member_category_id": memberCategoryId,
        "member_sub_category_id": memberSubCategoryId,
        "emp_staff_login_assignment_id": empStaffLoginAssignmentId,
        "Is_active": isActive,
        "division_id": divisionId,
        "route_id": routeId,
        "collection_centre_id": collectionCentreId,
        "gang_id": gangId,
        "department_id": departmentId,
        "datecomparer": datecomparer,
        "regionId": regionId,
        "districtId": districtId,
        "wardId": wardId,
        "villageId": villageId,
        "passporturl": passportUrl,
        "photoString": photoString,
        "nationalityId": nationalityId,
        "bankdetailsId": bankdetailsId,
        "bankbranchId": bankbranchId,
        "accNo": accNo,
        "main_category_code": mainCategoryCode,
      };
}


class UpdateFarmerModel {
    UpdateFarmerModel({
        required this.memberNo,
        required this.middleName,
        required this.lastName,
        required this.otherNames,
        required this.firstName,
        required this.maritalStatusId,
        required this.phone1,
        required this.genderTypeId,
        required this.natId,
        required this.dob,
        this.regionId,
        this.districtId,
        this.wardId,
        this.villageId,
        this.collectionCentreId,
        required this.passporturl,
        required this.photoString,
        this.nationalityId,
        this.bankDetailsId,
        this.bankbranchId,
        required this.accNo,
    });

    String? memberNo;
    String? middleName;
    String? lastName;
    String? otherNames;
    String? firstName;
    String? maritalStatusId;
    String? phone1;
    String? genderTypeId;
    String? natId;
    String? dob;
    String? regionId;
    String? districtId;
    String? wardId;
    String? villageId;
    String? collectionCentreId;
    String? passporturl;
    String? photoString;
    String? nationalityId;
    String? bankDetailsId;
    String? bankbranchId;
    String? accNo;

    factory UpdateFarmerModel.fromJson(Map<String, dynamic> json) => UpdateFarmerModel(
        memberNo: json["member_no"],
        middleName: json["middle_name"],
        lastName: json["last_name"],
        otherNames: json["other_names"],
        firstName: json["first_name"],
        maritalStatusId: json["marital_status_id"],
        phone1: json["phone1"],
        genderTypeId: json["gender_type_id"],
        natId: json["nat_id"],
        dob: DateTime.parse(json["dob"]).toString(),
        regionId: json["region_id"],
        districtId: json["district_id"],
        wardId: json["ward_id"],
        villageId: json["village_id"],
        collectionCentreId: json["collection_centre_Id"],
        passporturl: json["passporturl"],
        photoString: json["photo_string"],
        nationalityId: json["nationality_id"],
        bankDetailsId: json["bank_details_id"],
        bankbranchId: json["bankbranch_id"],
        accNo: json["acc_no"],
    );

    Map<String, dynamic> toJson() => {
        "member_no": memberNo,
        "middle_name": middleName,
        "last_name": lastName,
        "other_names": otherNames,
        "first_name": firstName,
        "marital_status_id": maritalStatusId,
        "phone1": phone1,
        "gender_type_id": genderTypeId,
        "nat_id": natId,
        "dob": dob.toString(),
        "region_id": regionId,
        "district_id": districtId,
        "ward_id": wardId,
        "village_id": villageId,
        "collection_centre_Id": collectionCentreId,
        "passporturl": passporturl,
        "photo_string": photoString,
        "nationality_id": nationalityId,
        "bank_details_id": bankDetailsId,
        "bankbranch_id": bankbranchId,
        "acc_no": accNo,
    };
}