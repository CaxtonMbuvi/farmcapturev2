// To parse this JSON data, do
//
//     final countyModel = countyModelFromJson(jsonString);

import 'dart:convert';

const String countiesTable = 'counties';

CountyModel countyModelFromJson(String str) => CountyModel.fromJson(json.decode(str));

String countyModelToJson(CountyModel data) => json.encode(data.toJson());

class CountyModel {
    CountyModel({
        this.id,
        this.isOkay,
        this.message,
        this.statusCode,
        this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    CountyModelResult? result;

    factory CountyModel.fromJson(Map<String, dynamic> json) => CountyModel(
        id: json["id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: CountyModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class CountyModelResult {
    CountyModelResult({
        this.id,
        this.request,
        this.isSuccessull,
        this.dataSetCount,
        this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory CountyModelResult.fromJson(Map<String, dynamic> json) => CountyModelResult(
        id: json["id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        this.id,
        this.take,
        this.skip,
        this.page,
        this.pageSize,
        this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        this.id,
        this.logic,
        this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        this.id,
        this.field,
        this.value,
        this.filterOperator,
    });

    String? id;
    String? field;
    int? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        //this.id,
        this.resultId,
        this.name,
        this.colorCode,
        this.datecomparer,
    });

    //String? id;
    int? resultId;
    String? name;
    dynamic colorCode;
    String? datecomparer;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
       // id: json["id"],
        resultId: json["Id"],
        name: json["name"],
        colorCode: json["color_code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "Id": resultId,
        "name": name,
        "color_code": colorCode,
        "datecomparer": datecomparer,
    };
}


class CountyResultElement {
    CountyResultElement({
        //this.id,
        this.resultId,
        this.name,
        this.colorCode,
        this.datecomparer,
    });

    //String? id;
    int? resultId;
    String? name;
    dynamic colorCode;
    String? datecomparer;

    factory CountyResultElement.fromJson(Map<String, dynamic> json) => CountyResultElement(
       // id: json["id"],
        resultId: json["Id"],
        name: json["name"],
        colorCode: json["color_code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "Id": resultId,
        "name": name,
        "color_code": colorCode,
        "datecomparer": datecomparer,
    };
}