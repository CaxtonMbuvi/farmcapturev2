// To parse this JSON data, do
//
//     final districtModel = districtModelFromJson(jsonString);

import 'dart:convert';

const String districtTable = 'districts';

DistrictModel districtModelFromJson(String str) => DistrictModel.fromJson(json.decode(str));

String districtModelToJson(DistrictModel data) => json.encode(data.toJson());

class DistrictModel {
    DistrictModel({
        this.id,
        this.isOkay,
        this.message,
        required this.statusCode,
        required this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    DistrictModelResult? result;

    factory DistrictModel.fromJson(Map<String, dynamic> json) => DistrictModel(
        id: json["id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: DistrictModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class DistrictModelResult {
    DistrictModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory DistrictModelResult.fromJson(Map<String, dynamic> json) => DistrictModelResult(
        id: json["id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    String? id;
    String? field;
    int? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        this.colorCode,
        required this.regionId,
        required this.datecomparer,
    });

    
    //String? id;
    int? resultId;
    String? name;
    dynamic colorCode;
    int? regionId;
    String datecomparer;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["Id"],
        name: json["name"],
        colorCode: json["color_code"],
        regionId: json["region_id"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "Id": resultId,
        "name": name,
        "color_code": colorCode,
        "region_id": regionId,
        "datecomparer": datecomparer,
    };
}


class DistrictResultElement {
    DistrictResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        this.colorCode,
        required this.regionId,
        required this.datecomparer,
    });

    
    //String? id;
    int? resultId;
    String? name;
    dynamic colorCode;
    String? regionId;
    String datecomparer;

    factory DistrictResultElement.fromJson(Map<String, dynamic> json) => DistrictResultElement(
        //id: json["\u0024id"],
        resultId: json["Id"],
        name: json["name"],
        colorCode: json["color_code"],
        regionId: json["region_id"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "Id": resultId,
        "name": name,
        "color_code": colorCode,
        "region_id": regionId,
        "datecomparer": datecomparer,
    };
}
