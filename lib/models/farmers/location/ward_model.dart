// To parse this JSON data, do
//
//     final wardModel = wardModelFromJson(jsonString);

import 'dart:convert';

const String wardTable = 'wards';

WardModel wardModelFromJson(String str) => WardModel.fromJson(json.decode(str));

String wardModelToJson(WardModel data) => json.encode(data.toJson());

class WardModel {
    WardModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    WardModelResult? result;

    factory WardModel.fromJson(Map<String, dynamic> json) => WardModel(
        id: json["id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: WardModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class WardModelResult {
    WardModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory WardModelResult.fromJson(Map<String, dynamic> json) => WardModelResult(
        id: json["id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    String? id;
    String? field;
    int? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        this.colorCode,
        required this.districtId,
        required this.datecomparer,
    });

    //String? id;
    int? resultId;
    String? name;
    dynamic colorCode;
    int? districtId;
    String? datecomparer;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["id"],
        resultId: json["Id"],
        name: json["name"],
        colorCode: json["color_code"],
        districtId: json["districtId"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "Id": resultId,
        "name": name,
        "color_code": colorCode,
        "districtId": districtId,
        "datecomparer": datecomparer,
    };
}


class WardResultElement {
    WardResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        this.colorCode,
        required this.districtId,
        required this.datecomparer,
    });

    //String? id;
    int? resultId;
    String? name;
    dynamic colorCode;
    String? districtId;
    String? datecomparer;

    factory WardResultElement.fromJson(Map<String, dynamic> json) => WardResultElement(
        //id: json["id"],
        resultId: json["Id"],
        name: json["name"],
        colorCode: json["color_code"],
        districtId: json["districtId"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "Id": resultId,
        "name": name,
        "color_code": colorCode,
        "districtId": districtId,
        "datecomparer": datecomparer,
    };
}