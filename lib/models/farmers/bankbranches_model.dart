// To parse this JSON data, do
//
//     final bankBranchesModel = bankBranchesModelFromJson(jsonString);

import 'dart:convert';

const String bankBranchesTable = 'bankbranches';

BankBranchesModel bankBranchesModelFromJson(String str) => BankBranchesModel.fromJson(json.decode(str));

String bankBranchesModelToJson(BankBranchesModel data) => json.encode(data.toJson());

class BankBranchesModel {
    BankBranchesModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    BankBranchesModelResult? result;

    factory BankBranchesModel.fromJson(Map<String, dynamic> json) => BankBranchesModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: BankBranchesModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class BankBranchesModelResult {
    BankBranchesModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory BankBranchesModelResult.fromJson(Map<String, dynamic> json) => BankBranchesModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    String? id;
    String? field;
    double? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"]?.toDouble(),
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        //required this.id,
        required this.resultId,
        required this.bankDetailsId,
        required this.name,
        this.code,
        required this.datecomparer,
    });

    //String id;
    int? resultId;
    int? bankDetailsId;
    String? name;
    dynamic code;
    String? datecomparer;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        bankDetailsId: json["bank_details_id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "bank_details_id": bankDetailsId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
    };
}


class BankBranchResultElement {
    BankBranchResultElement({
        //required this.id,
        required this.resultId,
        required this.bankDetailsId,
        required this.name,
        this.code,
        required this.datecomparer,
    });

    //String id;
    int? resultId;
    int? bankDetailsId;
    String? name;
    dynamic code;
    String? datecomparer;

    factory BankBranchResultElement.fromJson(Map<String, dynamic> json) => BankBranchResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        bankDetailsId: json["bank_details_id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "bank_details_id": bankDetailsId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
    };
}