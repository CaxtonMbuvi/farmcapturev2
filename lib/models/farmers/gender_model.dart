// import 'dart:convert';

// final String gendersTable = 'gender_types';

// GenderTypesList genderTypesListFromJson(String str) => GenderTypesList.fromJson(json.decode(str));

// String genderTypesListToJson(GenderTypesList data) => json.encode(data.toJson());


// class GenderTypesList {
//   GenderTypesList({
//     this.id,
//     this.gender,
//     this.genderChar,
//     this.members,
//     this.membersNextKin,
//   });

//   // String? id;
//   int? id;
//   String? gender;
//   dynamic genderChar;
//   List<dynamic>? members;
//   List<dynamic>? membersNextKin;

//   factory GenderTypesList.fromJson(Map<String, dynamic> json) => GenderTypesList(
//     id: json["id"],
//     gender: json["gender"],
//     genderChar: json["gender_char"],
//     members: List<dynamic>.from(json["members"].map((x) => x)),
//     membersNextKin: List<dynamic>.from(json["members_next_kin"].map((x) => x)),
//   );

//   Map<String, dynamic> toJson() => {
//     // "\u0024id": id,
//     "id": id,
//     "gender": gender,
//     "gender_char": genderChar,
//     // "members": List<dynamic>.from(members!.map((x) => x)),
//     // "members_next_kin": List<dynamic>.from(membersNextKin!.map((x) => x)),
//   };

//   GenderTypesList.fromMap(Map<String, dynamic> map){
//     gender = map["gender"];
//   }
// }
