

const String farmerTrainingMainTable = 'farmers_coaching_main';

class FarmersTrainingMainModel {
  FarmersTrainingMainModel({
    //this.id,
    this.sessionNo,
    this.title,
    this.notes,
    this.userId,
    this.coachingDate,
    this.photoUrl,
    this.startTime,
    this.endTime,
    this.farmInspectorVisitNo,
    this.noOfPeople,
    this.memberId,
    this.nofslbFarmers,
    this.no_nonfslbFarmers,
    this.noFamilyMembers,
    this.category,
    this.syncId
  });

  //int? id;
  String? sessionNo;
  String? title;
  String? notes;
  String? userId;
  String? coachingDate;
  String? photoUrl;
  String? startTime;
  String? endTime;
  String? farmInspectorVisitNo;
  String? noOfPeople;
  String? memberId;
  String? nofslbFarmers;
  String? no_nonfslbFarmers;
  String? noFamilyMembers;
  String? category;
  String? syncId;

  factory FarmersTrainingMainModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersTrainingMainModel(
        //id: json["id"],
        sessionNo: json["sessionNo"],
        title: json["title"],
        notes: json["notes"],
        userId: json["userId"],
        coachingDate: json["coaching_date"],
        photoUrl: json["photo_url"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        farmInspectorVisitNo: json["farm_inspector_visitNo"],
        noOfPeople: json["numofpeople"],
        memberId: json["member_id"],
        nofslbFarmers: json["no_fslb_farmers"],
        no_nonfslbFarmers: json["no_non_fslb_farmers"],
        noFamilyMembers: json["no_family_members"],
        category: json["category"],
        syncId: json["syncId"],
      );

  Map<String, dynamic> toJson() => {
        //"id": id,
        "sessionNo": sessionNo,
        "title": title,
        "notes": notes,
        "userId": userId,
        "coaching_date": coachingDate,
        "photo_url": photoUrl,
        "start_time": startTime,
        "end_time": endTime,
        "farm_inspector_visitNo": farmInspectorVisitNo,
        "numofpeople": noOfPeople,
        "member_id": memberId,
        "no_fslb_farmers": nofslbFarmers,
        "no_non_fslb_farmers": no_nonfslbFarmers,
        "no_family_members": noFamilyMembers,
        "category": category,
        "syncId": syncId
      };
}