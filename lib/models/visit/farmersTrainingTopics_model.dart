

const String farmerTrainingTopicsTable = 'farmers_coaching_topics';

class FarmersTrainingTopicsModel {
  FarmersTrainingTopicsModel({
    this.id,
    this.topic,
    this.topicId,
    this.notes,
    this.sessionId,
    this.transactionNo,
    this.syncId
  });

  String? id;
  String? topic;
  int? topicId;
  String? notes;
  String? sessionId;
  String? transactionNo;
  String? syncId;

  factory FarmersTrainingTopicsModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersTrainingTopicsModel(
        id: json["id"],
        topic: json["topic"],
        topicId: json["topicId"],
        notes: json["notes"],
        sessionId: json["sessionId"],
        transactionNo: json["transaction_no"],
        syncId: json["syncId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "topic": topic,
        "topicId": topicId,
        "notes": notes,
        "sessionId": sessionId,
        "transaction_no": transactionNo,
        "syncId": syncId
      };
}