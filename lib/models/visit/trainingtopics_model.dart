// To parse this JSON data, do
//
//     final trainingTopicsModel = trainingTopicsModelFromJson(jsonString);

import 'dart:convert';

const String trainingTopicsTable = 'trainingTopics';

TrainingTopicsModel trainingTopicsModelFromJson(String str) => TrainingTopicsModel.fromJson(json.decode(str));

String trainingTopicsModelToJson(TrainingTopicsModel data) => json.encode(data.toJson());

class TrainingTopicsModel {
    TrainingTopicsModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    TrainingTopicsModelResult? result;

    factory TrainingTopicsModel.fromJson(Map<String, dynamic> json) => TrainingTopicsModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: TrainingTopicsModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class TrainingTopicsModelResult {
    TrainingTopicsModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory TrainingTopicsModelResult.fromJson(Map<String, dynamic> json) => TrainingTopicsModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    String? id;
    String? field;
    int? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        //required this.id,
        required this.resultId,
        required this.name,
        required this.description,
        required this.datecomparer,
        required this.addedDate,
        this.modifiedDate,
    });

    //String? id;
    int? resultId;
    String? name;
    String? description;
    String? datecomparer;
    String? addedDate;
    String? modifiedDate;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        description: json["description"],
        datecomparer: json["datecomparer"].toString(),
        addedDate: DateTime.parse(json["added_date"]).toString(),
        modifiedDate: json["modified_date"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "name": name,
        "description": description,
        "datecomparer": datecomparer,
        "added_date": addedDate.toString(),
        "modified_date": modifiedDate,
    };
}


class TrainingTopicsModelResultElement {
    TrainingTopicsModelResultElement({
        this.id,
        this.resultId,
        this.name,
        this.description,
        this.datecomparer,
        this.addedDate,
        this.modifiedDate,
    });

    int? id;
    int? resultId;
    String? name;
    String? description;
    String? datecomparer;
    String? addedDate;
    String? modifiedDate;

    factory TrainingTopicsModelResultElement.fromJson(Map<String, dynamic> json) => TrainingTopicsModelResultElement(
        id: json["_Id"],
        resultId: json["Id"],
        name: json["name"],
        description: json["description"],
        datecomparer: json["datecomparer"].toString(),
        addedDate: DateTime.parse(json["added_date"]).toString(),
        modifiedDate: json["modified_date"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "name": name,
        "description": description,
        "datecomparer": datecomparer.toString(),
        "added_date": addedDate.toString(),
        "modified_date": modifiedDate.toString(),
    };
}