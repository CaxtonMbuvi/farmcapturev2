
const String farmerCoachingAttendanceTable = 'farmers_coaching_attendance';

class FarmersCoachingModel {
  FarmersCoachingModel({
    this.id,
    this.farmerId,
    this.topic,
    this.dateToday,
    this.sessionId,
    this.transactionNo,
    this.notes,
    this.comments,
    this.syncId
  });

  String? id;
  String? farmerId;
  int? topic;
  String? dateToday;
  String? sessionId;
  String? transactionNo;
  String? notes;
  String? comments;
  String? syncId;

  factory FarmersCoachingModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersCoachingModel(
        id: json["Id"],
        farmerId: json["farmerId"],
        topic: json["topic"],
        dateToday: json["date_today"],
        sessionId: json["sessionId"],
        transactionNo: json["transaction_no"],
        notes: json["notes"],
        comments: json["comments"],
        syncId: json["syncId"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id,
        "farmerId": farmerId,
        "topic": topic,
        "date_today": notes,
        "sessionId": sessionId,
        "transaction_no": transactionNo,
        "notes": notes,
        "comments": comments,
        "syncId": syncId
      };
}