// To parse this JSON data, do
//
//     final visitInspectionModel = visitInspectionModelFromJson(jsonString);

import 'dart:convert';

const String farmerVisitInspectionTable = 'farmer_visit_inspection_table';

VisitInspectionModel visitInspectionModelFromJson(String str) =>
    VisitInspectionModel.fromJson(json.decode(str));

String visitInspectionModelToJson(VisitInspectionModel data) =>
    json.encode(data.toJson());

class VisitInspectionModel {
  VisitInspectionModel({
    required this.id,
    required this.isOkay,
    required this.message,
    required this.statusCode,
    required this.result,
  });

  String? id;
  bool? isOkay;
  String? message;
  String? statusCode;
  Result? result;

  factory VisitInspectionModel.fromJson(Map<String, dynamic> json) =>
      VisitInspectionModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
      };
}

class Result {
  Result({
    required this.id,
    required this.visitTransactionCode,
    this.visitReason,
    required this.visitDate,
    required this.memberNo,
    this.newDevBlocksId,
    required this.comments,
    required this.verificationDate,
    required this.visitReasonId,
    required this.userId,
    required this.lat,
    required this.lon,
  });

  String? id;
  String? visitTransactionCode;
  dynamic visitReason;
  String? visitDate;
  String? memberNo;
  dynamic newDevBlocksId;
  String? comments;
  String? verificationDate;
  int? visitReasonId;
  int? userId;
  String? lat;
  String? lon;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        visitTransactionCode: json["visit_transaction_code"],
        visitReason: json["visit_reason"],
        visitDate: DateTime.parse(json["visit_date"]).toString(),
        memberNo: json["member_No"],
        newDevBlocksId: json["new_dev_blocks_id"],
        comments: json["comments"],
        verificationDate: DateTime.parse(json["verification_date"]).toString(),
        visitReasonId: json["visit_reason_id"],
        userId: json["user_id"],
        lat: json["lat"].toString(),
        lon: json["lon"].toString(),
      );

  Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "visit_transaction_code": visitTransactionCode,
        "visit_reason": visitReason,
        "visit_date": visitDate.toString(),
        "member_No": memberNo,
        "new_dev_blocks_id": newDevBlocksId,
        "comments": comments,
        "verification_date": verificationDate.toString(),
        "visit_reason_id": visitReasonId,
        "user_id": userId,
        "lat": lat.toString(),
        "lon": lon.toString(),
      };
}

class VisitInspectionModelResultElement {
  VisitInspectionModelResultElement({
     this.id,
     this.visitTransactionCode,
    this.visitReason,
     this.visitDate,
     this.memberNo,
    this.newDevBlocksId,
     this.comments,
     this.verificationDate,
     this.visitReasonId,
     this.userId,
     this.lat,
     this.lon,
    this.visitStatus,
    this.syncId
  });

  String? id;
  String? visitTransactionCode;
  dynamic visitReason;
  String? visitDate;
  String? memberNo;
  dynamic newDevBlocksId;
  String? comments;
  String? verificationDate;
  String? visitReasonId;
  String? userId;
  String? lat;
  String? lon;
  String? visitStatus;
  String? syncId;

  factory VisitInspectionModelResultElement.fromJson(
          Map<String, dynamic> json) =>
      VisitInspectionModelResultElement(
        id: json["id"],
        visitTransactionCode: json["visit_transaction_code"],
        visitReason: json["visit_reason"],
        visitDate: DateTime.parse(json["visit_date"]).toString(),
        memberNo: json["member_No"],
        newDevBlocksId: json["new_dev_blocks_id"],
        comments: json["comments"],
        verificationDate: DateTime.parse(json["verification_date"]).toString(),
        visitReasonId: json["visit_reason_id"],
        userId: json["user_id"],
        lat: json["lat"].toString(),
        lon: json["lon"].toString(),
        visitStatus: json["visit_status"],
        syncId: json["syncId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "visit_transaction_code": visitTransactionCode,
        "visit_reason": visitReason,
        "visit_date": visitDate.toString(),
        "member_No": memberNo,
        "new_dev_blocks_id": newDevBlocksId,
        "comments": comments,
        "verification_date": verificationDate.toString(),
        "visit_reason_id": visitReasonId,
        "user_id": userId,
        "lat": lat.toString(),
        "lon": lon.toString(),
        "visit_status": visitStatus,
        "syncId": syncId
      };
}
