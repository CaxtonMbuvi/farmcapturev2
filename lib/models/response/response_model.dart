class ResponseModel {
  final bool _isSuccess;
  final String _message;
  //enables the private variables to be used in the get
  ResponseModel(this._isSuccess, this._message);
  String get message => _message;
  bool get isSuccess => _isSuccess;
}