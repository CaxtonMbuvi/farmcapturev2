import 'dart:convert';

UpdateFarmerApiModel updateFarmerApiModelFromJson(String str) => UpdateFarmerApiModel.fromJson(json.decode(str));

String updateFarmerApiModelToJson(UpdateFarmerApiModel data) => json.encode(data.toJson());

class UpdateFarmerApiModel {
    UpdateFarmerApiModel({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
         this.dataSetCount,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;
    int? dataSetCount;

    factory UpdateFarmerApiModel.fromJson(Map<String, dynamic> json) => UpdateFarmerApiModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class Result {
    Result({
         this.id,
         this.memberNo,
         this.middleName,
         this.lastName,
         this.otherNames,
         this.firstName,
         this.maritalStatusId,
         this.phone1,
         this.genderTypeId,
         this.natId,
         this.dob,
         this.regionId,
         this.districtId,
         this.wardId,
        this.villageId,
        this.collectionCentreId,
        this.passporturl,
        this.photoString,
         this.nationalityId,
         this.bankDetailsId,
         this.bankbranchId,
         this.accNo,
        this.farmersLandDetailList,
        this.membersNextKinList,
        this.fcFarmerDeductionAssignmentList,
    });

    String? id;
    String? memberNo;
    String? middleName;
    String? lastName;
    String? otherNames;
    String? firstName;
    int? maritalStatusId;
    String? phone1;
    int? genderTypeId;
    String? natId;
    String? dob;
    int? regionId;
    int? districtId;
    int? wardId;
    dynamic villageId;
    int? collectionCentreId;
    String? passporturl;
    String? photoString;
    int? nationalityId;
    int? bankDetailsId;
    int? bankbranchId;
    String? accNo;
    dynamic farmersLandDetailList;
    dynamic membersNextKinList;
    dynamic fcFarmerDeductionAssignmentList;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["\u0024id"],
        memberNo: json["member_no"],
        middleName: json["middle_name"],
        lastName: json["last_name"],
        otherNames: json["other_names"],
        firstName: json["first_name"],
        maritalStatusId: json["marital_status_id"],
        phone1: json["phone1"],
        genderTypeId: json["gender_type_id"],
        natId: json["nat_id"],
        dob: DateTime.parse(json["dob"]).toString(),
        regionId: json["region_id"],
        districtId: json["district_id"],
        wardId: json["ward_id"],
        villageId: json["village_id"],
        collectionCentreId: json["collection_centre_Id"],
        passporturl: json["passporturl"],
        photoString: json["photo_string"],
        nationalityId: json["nationality_id"],
        bankDetailsId: json["bank_details_id"],
        bankbranchId: json["bankbranch_id"],
        accNo: json["acc_no"],
        farmersLandDetailList: json["farmers_land_detailList"],
        membersNextKinList: json["membersNextKinList"],
        fcFarmerDeductionAssignmentList: json["fcFarmerDeductionAssignmentList"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "member_no": memberNo,
        "middle_name": middleName,
        "last_name": lastName,
        "other_names": otherNames,
        "first_name": firstName,
        "marital_status_id": maritalStatusId,
        "phone1": phone1,
        "gender_type_id": genderTypeId,
        "nat_id": natId,
        "dob": dob.toString(),
        "region_id": regionId,
        "district_id": districtId,
        "ward_id": wardId,
        "village_id": villageId,
        "collection_centre_Id": collectionCentreId,
        "passporturl": passporturl,
        "photo_string": photoString,
        "nationality_id": nationalityId,
        "bank_details_id": bankDetailsId,
        "bankbranch_id": bankbranchId,
        "acc_no": accNo,
        "farmers_land_detailList": farmersLandDetailList,
        "membersNextKinList": membersNextKinList,
        "fcFarmerDeductionAssignmentList": fcFarmerDeductionAssignmentList,
    };
}
