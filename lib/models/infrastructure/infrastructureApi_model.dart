// To parse this JSON data, do
//
//     final infrastructureApiModel = infrastructureApiModelFromJson(jsonString);



import 'dart:convert';

const String infrastructureTypesTable = 'infrastructureTypes';

InfrastructureApiModel infrastructureApiModelFromJson(String str) => InfrastructureApiModel.fromJson(json.decode(str));

String infrastructureApiModelToJson(InfrastructureApiModel data) => json.encode(data.toJson());

class InfrastructureApiModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    InfrastructureApiModelResult? result;
    int? dataSetCount;

    InfrastructureApiModel({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
         this.dataSetCount,
    });

    factory InfrastructureApiModel.fromJson(Map<String, dynamic> json) => InfrastructureApiModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: InfrastructureApiModelResult.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class InfrastructureApiModelResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    InfrastructureApiModelResult({
         this.id,
         this.request,
         this.isSuccessull,
         this.dataSetCount,
         this.result,
    });

    factory InfrastructureApiModelResult.fromJson(Map<String, dynamic> json) => InfrastructureApiModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
         this.id,
         this.take,
         this.skip,
         this.page,
         this.pageSize,
         this.filter,
        this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
         this.id,
         this.logic,
         this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    int? value;
    int? filterOperator;

    FilterElement({
         this.id,
         this.field,
         this.value,
         this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    //String? id;
    int? resultId;
    String? name;
    String? code;
    String? datecomparer;
    String? isative;
    String? coordinateType;

    ResultElement({
         //this.id,
         this.resultId,
         this.name,
         this.code,
         this.datecomparer,
         this.isative,
         this.coordinateType,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
        isative: json["isative"].toString(),
        coordinateType: json["coordinate_type"],
    );

    Map<String, dynamic> toJson() => {
       // "\u0024id": id,
        "id": resultId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
         "isative": isative,
        "coordinate_type": coordinateType,
    };
}

class InfrastructureResultElement {
    //String? id;
    int? resultId;
    String? name;
    String? code; 
    String? datecomparer;
    String? isative;
    String? coordinateType;

    InfrastructureResultElement({
         //this.id,
         this.resultId,
         this.name,
         this.code,
         this.datecomparer,
         this.isative,
         this.coordinateType,
    });

    factory InfrastructureResultElement.fromJson(Map<String, dynamic> json) => InfrastructureResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        code: json["code"],
        datecomparer: json["datecomparer"].toString(),
        isative: json["isative"],
        coordinateType: json["coordinate_type"],
    );

    Map<String, dynamic> toJson() => {
       // "\u0024id": id,
        "id": resultId,
        "name": name,
        "code": code,
        "datecomparer": datecomparer,
        "isative": isative,
        "coordinate_type": coordinateType,
    };
}
