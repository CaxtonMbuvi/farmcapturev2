const String farmerInfrastructureCategoryTable = 'farmers_infrastructure_category';
const String farmerInfrastructurePolygonTable = 'farmers_infrastructure_polygon';

class FarmersInfrastructurePolygonModel {
  FarmersInfrastructurePolygonModel({
    this.id,
    this.infraTypeId,
    this.polygonId,
    this.infrastructureName,
    this.latitude,
    this.longitude,
    this.blockId,
    this.mainDivId,
    this.transactionNo,
    this.datecomparer,
    this.isActive,
    this.mapCategory,
    this.userId,
    this.landSize,
    this.isFarmerOrEstate,
    this.locationCategoryId,
    this.datetime,
    this.syncId
  });

  String? id;
  String? infraTypeId;
  String? polygonId;
  String? infrastructureName;
  String? latitude;
  String? longitude;
  String? blockId;
  String? mainDivId;
  String? transactionNo;
  String? datecomparer;
  String? isActive;
  String? mapCategory;
  String? userId;
  String? landSize;
  String? isFarmerOrEstate;
  String? locationCategoryId;
  String? datetime;
  String? syncId;

  factory FarmersInfrastructurePolygonModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersInfrastructurePolygonModel(
        id: json["Id"],
        infraTypeId: json["infrastructure_type_id"],
        polygonId: json["polygon_id"],
        infrastructureName: json["infrastructure_name"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        blockId: json["block_id"],
        mainDivId: json["main_div_id"],
        transactionNo: json["transaction_no"],
        datecomparer: json["datecomparer"],
        isActive: json["isactive"],
        mapCategory: json["map_category"],
        userId: json["user_id"],
        landSize: json["landsize"],
        isFarmerOrEstate: json["is_farmer_or_estate"],
        locationCategoryId: json["location_category_id"],
        datetime: json["datetime"],
        syncId: json["sync_status"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id,
        "infrastructure_type_id": infraTypeId,
        "polygon_id": polygonId,
        "infrastructure_name": infrastructureName,
        "latitude": latitude,
        "longitude": longitude,
        "block_id": blockId,
        "main_div_id": mainDivId,
        "transaction_no": transactionNo,
        "datecomparer": datecomparer,
        "isactive": isActive,
        "map_category": mapCategory,
        "user_id": userId,
        "landsize": landSize,
        "is_farmer_or_estate": isFarmerOrEstate,
        "location_category_id": locationCategoryId,
        "datetime": datetime,
        "sync_status": syncId
      };
}


class FarmersInfrastructureCatModel {
  FarmersInfrastructureCatModel({
    //this.id,
    this.infrastructureTypeId,
    this.mainDivId,
    this.transactionNo,
    this.datetime,
    this.syncId
  });

  //String? id;
  String? infrastructureTypeId;
  String? mainDivId;
  String? transactionNo;
  String? datetime;
  String? syncId;

  factory FarmersInfrastructureCatModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersInfrastructureCatModel(
        //id: json["_Id"],
        infrastructureTypeId: json["infrastructure_type_id"],
        mainDivId: json["main_div_id"],
        transactionNo: json["transaction_no"],
        datetime: json["datetime"],
        syncId: json["sync_status"],
      );

  Map<String, dynamic> toJson() => {
        //"_Id": id,
        "infrastructure_type_id": infrastructureTypeId,
        "main_div_id": mainDivId,
        "transaction_no": transactionNo,
        "datetime": datetime,
        "sync_status": syncId
      };
}