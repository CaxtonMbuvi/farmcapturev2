import 'dart:convert';

const String nextKinTable = 'nextOfKin';

NextOfKinModel nextOfKinModelFromJson(String str) =>NextOfKinModel.fromJson(json.decode(str));

String nextOfKinModelToJson(NextOfKinModel data) => json.encode(data.toJson());

class NextOfKinModel {
  String? id;
  bool? isOkay;
  String? message;
  String? statusCode;
  NextOfKinModelResult? result;
  int? dataSetCount;

  NextOfKinModel({
    this.id,
    this.isOkay,
    this.message,
    this.statusCode,
    this.result,
    this.dataSetCount,
  });

  factory NextOfKinModel.fromJson(Map<String, dynamic> json) => NextOfKinModel(
    id: json["\u0024id"],
    isOkay: json["IsOkay"],
    message: json["Message"],
    statusCode: json["statusCode"],
    result: NextOfKinModelResult.fromJson(json["Result"]),
    dataSetCount: json["DataSetCount"],
  );

  Map<String, dynamic> toJson() => {
    "\u0024id": id,
    "IsOkay": isOkay,
    "Message": message,
    "statusCode": statusCode,
    "Result": result!.toJson(),
    "DataSetCount": dataSetCount,
  };
}

class NextOfKinModelResult {
  String? id;
  Request? request;
  bool? isSuccessull;
  int? dataSetCount;
  List<ResultElement>? result;

  NextOfKinModelResult({
    this.id,
    this.request,
    this.isSuccessull,
    this.dataSetCount,
    this.result,
  });

  factory NextOfKinModelResult.fromJson(Map<String, dynamic> json) => NextOfKinModelResult(
    id: json["\u0024id"],
    request: Request.fromJson(json["request"]),
    isSuccessull: json["isSuccessull"],
    dataSetCount: json["DataSetCount"],
    result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "\u0024id": id,
    "request": request!.toJson(),
    "isSuccessull": isSuccessull,
    "DataSetCount": dataSetCount,
    "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
  };
}

class Request {
  String? id;
  int? take;
  int? skip;
  int? page;
  int? pageSize;
  RequestFilter? filter;
  dynamic sort;

  Request({
    this.id,
    this.take,
    this.skip,
    this.page,
    this.pageSize,
    this.filter,
    this.sort,
  });

  factory Request.fromJson(Map<String, dynamic> json) => Request(
    id: json["\u0024id"],
    take: json["take"],
    skip: json["skip"],
    page: json["page"],
    pageSize: json["pageSize"],
    filter: RequestFilter.fromJson(json["filter"]),
    sort: json["sort"],
  );

  Map<String, dynamic> toJson() => {
    "\u0024id": id,
    "take": take,
    "skip": skip,
    "page": page,
    "pageSize": pageSize,
    "filter": filter!.toJson(),
    "sort": sort,
  };
}

class RequestFilter {
  String? id;
  String? logic;
  List<FilterElement>? filters;

  RequestFilter({
    this.id,
    this.logic,
    this.filters,
  });

  factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
    id: json["\u0024id"],
    logic: json["logic"],
    filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "\u0024id": id,
    "logic": logic,
    "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
  };
}


class FilterElement {
  String? id;
  String? field;
  int? value;
  int? filterOperator;

  FilterElement({
    this.id,
    this.field,
    this.value,
    this.filterOperator,
  });

  factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
    id: json["\u0024id"],
    field: json["field"],
    value: json["value"],
    filterOperator: json["operator"],
  );

  Map<String, dynamic> toJson() => {
    "\u0024id": id,
    "field": field,
    "value": value,
    "operator": filterOperator,
  };
}


class ResultElement {
  // String id;
  String? employeeId;
  String? memberNo;
  String? resultElementId;
  String? birthCertificateNo;
  String? fullName;
  String? dateOfBirth;
  String? isSchooled;
  String? isOrphaned;
  String? relationshipId;
  String? maritalStatusId;
  String? phonenumber;
  String? age;
  String? genderTypeId;
  String? levelOfEducationId;
  String? datecomparer;
  String? isActive;

  ResultElement({
    // required this.id,
     this.employeeId,
     this.memberNo,
     this.resultElementId,
     this.birthCertificateNo,
     this.fullName,
     this.dateOfBirth,
     this.isSchooled,
     this.isOrphaned,
     this.relationshipId,
    this.maritalStatusId,
     this.phonenumber,
     this.age,
     this.genderTypeId,
     this.levelOfEducationId,
     this.datecomparer,
     this.isActive,
  });

  factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
    // id: json["\u0024id"],
    employeeId: json["employee_id"],
    memberNo: json["member_no"],
    resultElementId: json["id"].toString(),
    birthCertificateNo: json["birth_certificate_no"],
    fullName: json["full_name"],
    dateOfBirth: json["date_of_birth"].toString(),
    isSchooled: json["is_schooled"],
    isOrphaned: json["is_orphaned"],
    relationshipId: json["relationship_id"],
    maritalStatusId: json["marital_status_id"],
    phonenumber: json["phonenumber"],
    age: json["age"],
    genderTypeId: json["gender_type_id"],
    levelOfEducationId: json["level_of_education_id"],
    datecomparer: json["datecomparer"].toString(),
    isActive: json["Is_active"],
  );

  Map<String, dynamic> toJson() => {
    // "\u0024id": id,
    "employee_id": employeeId,
    "member_no": memberNo,
    "id": resultElementId,
    "birth_certificate_no": birthCertificateNo,
    "full_name": fullName,
    "date_of_birth": dateOfBirth.toString(),
    "is_schooled": isSchooled,
    "is_orphaned": isOrphaned,
    "relationship_id": relationshipId,
    "marital_status_id": maritalStatusId,
    "phonenumber": phonenumber,
    "age": age,
    "gender_type_id": genderTypeId,
    "level_of_education_id": levelOfEducationId,
    "datecomparer": datecomparer,
    "Is_active": isActive,
  };
}


class NextOfKinModelResultElement {
  NextOfKinModelResultElement({
    this.sid,
    this.memberNo,
    this.birthCertificateNo,
    this.fullName,
    this.dateOfBirth,
    this.isSchooled,
    this.relationshipId,
    this.maritalStatusId,
    this.phonenumber,
    this.age,
    this.genderTypeId,
    this.levelOfEducationId,
    this.isOrphaned,
    this.datecomparer,
    this.isActive,
  });

  String? sid;
  String? memberNo;
  String? birthCertificateNo;
  String? fullName;
  String? dateOfBirth;
  String? isSchooled;
  String? relationshipId;
  String? maritalStatusId;
  String? phonenumber;
  String? age;
  String? genderTypeId;
  String? levelOfEducationId;
  String? isOrphaned;
  String? datecomparer;
  String? isActive;

  factory NextOfKinModelResultElement.fromJson(Map<String, dynamic> json) => NextOfKinModelResultElement(
        sid: json["sid"],
        memberNo: json["member_no"],
        birthCertificateNo: json["birth_certificate_no"],
        fullName: json["full_name"],
        dateOfBirth: json["date_of_birth"],
        isSchooled: json["is_schooled"],
        relationshipId: json["relationship_id"],
        maritalStatusId: json["marital_status_id"],
        phonenumber: json["phonenumber"],
        age: json["age"],
        genderTypeId: json["gender_type_id"],
        levelOfEducationId: json["level_of_education_id"],
        isOrphaned: json["is_orphaned"],
      datecomparer: json["datecomparer"],
        isActive: json["Is_active"],
      );

  Map<String, dynamic> toJson() => {
        "sid": sid,
        "member_no": memberNo,
        "birth_certificate_no": birthCertificateNo,
        "full_name": fullName,
        "date_of_birth": dateOfBirth,
        "is_schooled": isSchooled,
        "relationship_id": relationshipId,
        "marital_status_id": maritalStatusId,
        "phonenumber": phonenumber,
        "age": age,
        "gender_type_id": genderTypeId,
        "level_of_education_id": levelOfEducationId,
        "is_orphaned": isOrphaned,"datecomparer": datecomparer,
        "Is_active": isActive,
      };
}
