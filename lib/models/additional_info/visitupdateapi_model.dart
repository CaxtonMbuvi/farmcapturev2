// To parse this JSON data, do
//
//     final updateFarmerVisitApiModel = updateFarmerVisitApiModelFromJson(jsonString);

import 'dart:convert';

UpdateFarmerVisitApiModel updateFarmerVisitApiModelFromJson(String str) => UpdateFarmerVisitApiModel.fromJson(json.decode(str));

String updateFarmerVisitApiModelToJson(UpdateFarmerVisitApiModel data) => json.encode(data.toJson());

class UpdateFarmerVisitApiModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;
    int? dataSetCount;

    UpdateFarmerVisitApiModel({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
         this.dataSetCount,
    });

    factory UpdateFarmerVisitApiModel.fromJson(Map<String, dynamic> json) => UpdateFarmerVisitApiModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class Result {
    String? id;
    String? visitTransactionCode;
    String? visitReason;
    String? visitDate;
    String? memberNo;
    String? newDevBlocksId;
    String? comments;
    String? verificationDate;
    String? visitReasonId;
    String? userId;
    String? lat;
    String? lon;

    Result({
         this.id,
         this.visitTransactionCode,
        this.visitReason,
         this.visitDate,
         this.memberNo,
        this.newDevBlocksId,
         this.comments,
         this.verificationDate,
         this.visitReasonId,
         this.userId,
         this.lat,
         this.lon,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["\u0024id"],
        visitTransactionCode: json["visit_transaction_code"],
        visitReason: json["visit_reason"],
        visitDate: DateTime.parse(json["visit_date"]).toString(),
        memberNo: json["member_No"],
        newDevBlocksId: json["new_dev_blocks_id"],
        comments: json["comments"],
        verificationDate: DateTime.parse(json["verification_date"]).toString(),
        visitReasonId: json["visit_reason_id"],
        userId: json["user_id"],
        lat: json["lat"],
        lon: json["lon"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "visit_transaction_code": visitTransactionCode,
        "visit_reason": visitReason,
        "visit_date": visitDate.toString(),
        "member_No": memberNo,
        "new_dev_blocks_id": newDevBlocksId,
        "comments": comments,
        "verification_date": verificationDate.toString(),
        "visit_reason_id": visitReasonId,
        "user_id": userId,
        "lat": lat,
        "lon": lon,
    };
}
