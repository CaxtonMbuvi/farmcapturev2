// To parse this JSON data, do
//
//     final updateFarmerChildApiModel = updateFarmerChildApiModelFromJson(jsonString);

import 'dart:convert';

UpdateFarmerChildApiModel updateFarmerChildApiModelFromJson(String str) => UpdateFarmerChildApiModel.fromJson(json.decode(str));

String updateFarmerChildApiModelToJson(UpdateFarmerChildApiModel data) => json.encode(data.toJson());

class UpdateFarmerChildApiModel {
    UpdateFarmerChildApiModel({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    Result? result;

    factory UpdateFarmerChildApiModel.fromJson(Map<String, dynamic> json) => UpdateFarmerChildApiModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: Result.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class Result {
    Result({
         this.id,
         this.memberNo,
         this.resultId,
         this.birthCertificateNo,
         this.fullName,
         this.dateOfBirth,
        this.isSchooled,
         this.isOrphaned,
         this.relationshipId,
        this.maritalStatusId,
         this.phonenumber,
         this.age,
         this.genderTypeId,
         this.levelOfEducationId,
    });

    String? id;
    String? memberNo;
    int? resultId;
    String? birthCertificateNo;
    String? fullName;
    String? dateOfBirth;
    String? isSchooled;
    String? isOrphaned;
    int? relationshipId;
    String? maritalStatusId;
    String? phonenumber;
    String? age;
    int? genderTypeId;
    int? levelOfEducationId;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["\u0024id"],
        memberNo: json["member_no"],
        resultId: json["id"],
        birthCertificateNo: json["birth_certificate_no"],
        fullName: json["full_name"],
        dateOfBirth: json["date_of_birth"].toString(),
        isSchooled: json["is_schooled"],
        isOrphaned: json["is_orphaned"],
        relationshipId: json["relationship_id"],
        maritalStatusId: json["marital_status_id"],
        phonenumber: json["phonenumber"],
        age: json["age"],
        genderTypeId: json["gender_type_id"],
        levelOfEducationId: json["level_of_education_id"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "member_no": memberNo,
        "id": resultId,
        "birth_certificate_no": birthCertificateNo,
        "full_name": fullName,
        "date_of_birth": dateOfBirth!.toString(),
        "is_schooled": isSchooled,
        "is_orphaned": isOrphaned,
        "relationship_id": relationshipId,
        "marital_status_id": maritalStatusId,
        "phonenumber": phonenumber,
        "age": age,
        "gender_type_id": genderTypeId,
        "level_of_education_id": levelOfEducationId,
    };
}
