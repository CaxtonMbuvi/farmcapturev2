// To parse this JSON data, do
//
//     final blocksPolygonModel = blocksPolygonModelFromJson(jsonString);

import 'dart:convert';

BlocksPolygonModel blocksPolygonModelFromJson(String str) => BlocksPolygonModel.fromJson(json.decode(str));

String blocksPolygonModelToJson(BlocksPolygonModel data) => json.encode(data.toJson());

class BlocksPolygonModel {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    BlocksPolygonModelResult? result;
    int? dataSetCount;

    BlocksPolygonModel({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
         this.dataSetCount,
    });

    factory BlocksPolygonModel.fromJson(Map<String, dynamic> json) => BlocksPolygonModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: BlocksPolygonModelResult.fromJson(json["Result"]),
        dataSetCount: json["DataSetCount"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
        "DataSetCount": dataSetCount,
    };
}

class BlocksPolygonModelResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    BlocksPolygonModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    factory BlocksPolygonModelResult.fromJson(Map<String, dynamic> json) => BlocksPolygonModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
         this.id,
         this.logic,
         this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    dynamic value;
    int? filterOperator;

    FilterElement({
         this.id,
         this.field,
         this.value,
         this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    //String id;
    int? resultId;
    int? mainDivisionId;
    int? divisionId;
    String? polygonId;
    String? longi;
    String? latitude;
    int? newDevBlockId;
    String? transactionNo;
    String? datecomparer;
    //bool isactive;

    ResultElement({
        //required this.id,
         this.resultId,
         this.mainDivisionId,
        this.divisionId,
         this.polygonId,
         this.longi,
         this.latitude,
         this.newDevBlockId,
         this.transactionNo,
         this.datecomparer,
        //required this.isactive,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        mainDivisionId: json["main_division_id"],
        divisionId: json["division_id"],
        polygonId: json["polygon_id"],
        longi: json["longi"].toString(),
        latitude: json["latitude"].toString(),
        newDevBlockId: json["new_dev_block_id"],
        transactionNo: json["transaction_no"],
        datecomparer: json["datecomparer"].toString(),
        //isactive: json["isactive"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "main_division_id": mainDivisionId,
        "division_id": divisionId,
        "polygon_id": polygonId,
        "longi": longi,
        "latitude": latitude,
        "new_dev_block_id": newDevBlockId,
        "transaction_no": transactionNo,
        "datecomparer": datecomparer,
        //"isactive": isactive,
    };
}
