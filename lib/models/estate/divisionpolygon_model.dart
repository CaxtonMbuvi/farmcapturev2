// To parse this JSON data, do
//
//     final divisionPolygonModel = divisionPolygonModelFromJson(jsonString);

import 'dart:convert';

const String divisionPolygonTable = 'divisionpolygon';

DivisionPolygonModel divisionPolygonModelFromJson(String str) =>
    DivisionPolygonModel.fromJson(json.decode(str));

String divisionPolygonModelToJson(DivisionPolygonModel data) =>
    json.encode(data.toJson());

class DivisionPolygonModel {
  DivisionPolygonModel({
    required this.id,
    required this.isOkay,
    required this.message,
    required this.statusCode,
    required this.result,
  });

  String? id;
  bool? isOkay;
  String? message;
  String? statusCode;
  DivisionPolygonModelResult? result;

  factory DivisionPolygonModel.fromJson(Map<String, dynamic> json) =>
      DivisionPolygonModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: DivisionPolygonModelResult.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
      };
}

class DivisionPolygonModelResult {
  DivisionPolygonModelResult({
    required this.id,
    required this.request,
    required this.isSuccessull,
    required this.dataSetCount,
    required this.result,
  });

  String? id;
  Request? request;
  bool? isSuccessull;
  int? dataSetCount;
  List<ResultElement>? result;

  factory DivisionPolygonModelResult.fromJson(Map<String, dynamic> json) =>
      DivisionPolygonModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(
            json["Result"].map((x) => ResultElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
      };
}

class Request {
  Request({
    required this.id,
    required this.take,
    required this.skip,
    required this.page,
    required this.pageSize,
    required this.filter,
    this.sort,
  });

  String? id;
  int? take;
  int? skip;
  int? page;
  int? pageSize;
  RequestFilter? filter;
  dynamic sort;

  factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
      };
}

class RequestFilter {
  RequestFilter({
    required this.id,
    required this.logic,
    required this.filters,
  });

  String? id;
  String? logic;
  List<FilterElement>? filters;

  factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(
            json["filters"].map((x) => FilterElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
      };
}

class FilterElement {
  FilterElement({
    required this.id,
    required this.field,
    required this.value,
    required this.filterOperator,
  });

  String? id;
  String? field;
  double? value;
  int? filterOperator;

  factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"]?.toDouble(),
        filterOperator: json["operator"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
      };
}

class ResultElement {
  ResultElement({
    //required this.id,
    required this.resultId,
    this.mainDivisionId,
    required this.divisionId,
    required this.polygonId,
    required this.longi,
    required this.latitude,
    this.newDevBlockId,
    required this.transactionNo,
    required this.datecomparer,
  });

  //String id;
  int? resultId;
  int? mainDivisionId;
  int? divisionId;
  String? polygonId;
  String? longi;
  String? latitude;
  int? newDevBlockId;
  String? transactionNo;
  String? datecomparer;

  factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        mainDivisionId: json["main_division_id"],
        divisionId: json["division_id"],
        polygonId: json["polygon_id"],
        longi: json["longi"].toString(),
        latitude: json["latitude"].toString(),
        newDevBlockId: json["new_dev_block_id"],
        transactionNo: json["transaction_no"],
        datecomparer: json["datecomparer"].toString(),
      );

  Map<String, dynamic> toJson() => {
        // "\u0024id": id,
        "id": resultId,
        "main_division_id": mainDivisionId,
        "division_id": divisionId,
        "polygon_id": polygonId,
        "longi": longi,
        "latitude": latitude,
        "new_dev_block_id": newDevBlockId,
        "transaction_no": transactionNo,
        "datecomparer": datecomparer,
      };
}

class DivisionPolygonResultElement {
  DivisionPolygonResultElement({
    //required this.id,
    this.syncId,
    this.blockArea,
    this.isDivision,
    this.resultId,
    this.mainDivisionId,
    this.divisionId,
    this.polygonId,
    this.longi,
    this.latitude,
    this.newDevBlockId,
    this.transactionNo,
    this.datecomparer,
  });

  //String id;
  String? syncId;
  String? blockArea;
  int? isDivision;
  int? resultId;
  int? mainDivisionId;
  int? divisionId;
  String? polygonId;
  String? longi;
  String? latitude;
  String? newDevBlockId;
  String? transactionNo;
  String? datecomparer;

  factory DivisionPolygonResultElement.fromJson(Map<String, dynamic> json) =>
      DivisionPolygonResultElement(
        //id: json["\u0024id"],
        syncId: json["syncId"],
        blockArea: json["blockArea"],
        isDivision: json["isDivision"],
        resultId: json["id"],
        mainDivisionId: json["main_division_id"],
        divisionId: json["division_id"],
        polygonId: json["polygon_id"],
        longi: json["longi"].toString(),
        latitude: json["latitude"].toString(),
        newDevBlockId: json["new_dev_block_id"],
        transactionNo: json["transaction_no"],
        datecomparer: json["datecomparer"].toString(),
      );

  Map<String, dynamic> toJson() => {
        // "\u0024id": id,
        "syncId": syncId,
        "blockArea": blockArea,
        "isDivision": isDivision,
        "id": resultId,
        "main_division_id": mainDivisionId,
        "division_id": divisionId,
        "polygon_id": polygonId,
        "longi": longi,
        "latitude": latitude,
        "new_dev_block_id": newDevBlockId,
        "transaction_no": transactionNo,
        "datecomparer": datecomparer,
      };
}



