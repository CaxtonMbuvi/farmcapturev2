// To parse this JSON data, do
//
//     final blocksModel = blocksModelFromJson(jsonString);

import 'dart:convert';

const String blocksTable = 'blocks';

BlocksModel blocksModelFromJson(String str) =>
    BlocksModel.fromJson(json.decode(str));

String blocksModelToJson(BlocksModel data) => json.encode(data.toJson());

class BlocksModel {
  BlocksModel({
    required this.id,
    required this.isOkay,
    required this.message,
    required this.statusCode,
    required this.result,
  });

  String id;
  bool isOkay;
  String message;
  String statusCode;
  BlocksModelResult result;

  factory BlocksModel.fromJson(Map<String, dynamic> json) => BlocksModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: BlocksModelResult.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result.toJson(),
      };
}

class BlocksModelResult {
  BlocksModelResult({
    required this.id,
    required this.request,
    required this.isSuccessull,
    required this.dataSetCount,
    required this.result,
  });

  String id;
  Request request;
  bool isSuccessull;
  int dataSetCount;
  List<ResultElement> result;

  factory BlocksModelResult.fromJson(Map<String, dynamic> json) =>
      BlocksModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(
            json["Result"].map((x) => ResultElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result.map((x) => x.toJson())),
      };
}

class Request {
  Request({
    required this.id,
    required this.take,
    required this.skip,
    required this.page,
    required this.pageSize,
    required this.filter,
    this.sort,
  });

  String id;
  int take;
  int skip;
  int page;
  int pageSize;
  RequestFilter filter;
  dynamic sort;

  factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter.toJson(),
        "sort": sort,
      };
}

class RequestFilter {
  RequestFilter({
    required this.id,
    required this.logic,
    required this.filters,
  });

  String id;
  String logic;
  List<FilterElement> filters;

  factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(
            json["filters"].map((x) => FilterElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters.map((x) => x.toJson())),
      };
}

class FilterElement {
  FilterElement({
    required this.id,
    required this.field,
    required this.value,
    required this.filterOperator,
  });

  String id;
  String field;
  int value;
  int filterOperator;

  factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
      };
}

class ResultElement {
  ResultElement({
    ////required this.id,
    required this.resultId,
    required this.name,
    required this.datecomparer,
    required this.code,
    this.mainDivisionId,
    required this.blockTblDivisionId,
    this.categorySeparator,
    this.plantedYear,
    required this.typeOfPlantingId,
    required this.spacing,
    required this.rubberClonesId,
    required this.areaAcre,
    this.yearOfOpening,
    this.noTappingTasks,
    required this.typeOfPlantingname,
    required this.rubberClonesName,
    required this.tappingSystemId,
    required this.tappingSystemName,
    this.panelPositionName,
    this.panelPositionId,
    this.standPerAcre,
    this.noOfTreesInTheBlock,
  });

  String? id;
  int? resultId;
  String? name;
  String? datecomparer;
  String? code;
  dynamic mainDivisionId;
  int? blockTblDivisionId;
  dynamic categorySeparator;
  dynamic plantedYear;
  int? typeOfPlantingId;
  String? spacing;
  int? rubberClonesId;
  String? areaAcre;
  dynamic yearOfOpening;
  dynamic noTappingTasks;
  String? typeOfPlantingname;
  String? rubberClonesName;
  int? tappingSystemId;
  String? tappingSystemName;
  dynamic panelPositionName;
  dynamic panelPositionId;
  dynamic standPerAcre;
  dynamic noOfTreesInTheBlock;

  factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        datecomparer: json["datecomparer"].toString(),
        code: json["code"],
        mainDivisionId: json["main_division_id"],
        blockTblDivisionId: json["block_tbl_division_id"],
        categorySeparator: json["category_separator"],
        plantedYear: json["planted_year"],
        typeOfPlantingId: json["type_of_planting_id"],
        spacing: json["spacing"],
        rubberClonesId: json["rubber_clones_id"],
        areaAcre: json["area_acre"].toString(),
        yearOfOpening: json["year_of_opening"],
        noTappingTasks: json["no_tapping_tasks"],
        typeOfPlantingname: json["type_of_plantingname"],
        rubberClonesName: json["rubber_clones_Name"],
        tappingSystemId: json["tapping_system_id"],
        tappingSystemName: json["tapping_system_Name"],
        panelPositionName: json["panel_position_Name"],
        panelPositionId: json["panel_position_id"],
        standPerAcre: json["stand_per_acre"],
        noOfTreesInTheBlock: json["no_of_Trees_in_the_block"],
      );

  Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "name": name,
        "datecomparer": datecomparer,
        "code": code,
        "main_division_id": mainDivisionId,
        "block_tbl_division_id": blockTblDivisionId,
        "category_separator": categorySeparator,
        "planted_year": plantedYear,
        "type_of_planting_id": typeOfPlantingId,
        "spacing": spacing,
        "rubber_clones_id": rubberClonesId,
        "area_acre": areaAcre,
        "year_of_opening": yearOfOpening,
        "no_tapping_tasks": noTappingTasks,
        "type_of_plantingname": typeOfPlantingname,
        "rubber_clones_Name": rubberClonesName,
        "tapping_system_id": tappingSystemId,
        "tapping_system_Name": tappingSystemName,
        "panel_position_Name": panelPositionName,
        "panel_position_id": panelPositionId,
        "stand_per_acre": standPerAcre,
        "no_of_Trees_in_the_block": noOfTreesInTheBlock,
      };
}

class BlocksModelResultElement {
  BlocksModelResultElement({
    ////required this.id,
     this.resultId,
     this.name,
     this.datecomparer,
     this.code,
    this.mainDivisionId,
     this.blockTblDivisionId,
    this.categorySeparator,
    this.plantedYear,
     this.typeOfPlantingId,
     this.spacing,
     this.rubberClonesId,
     this.areaAcre,
    this.yearOfOpening,
    this.noTappingTasks,
     this.typeOfPlantingname,
     this.rubberClonesName,
     this.tappingSystemId,
     this.tappingSystemName,
    this.panelPositionName,
    this.panelPositionId,
    this.standPerAcre,
    this.noOfTreesInTheBlock,
    this.isPlotted,
  });

  String? id;
  int? resultId;
  String? name;
  String? datecomparer;
  String? code;
  dynamic mainDivisionId;
  int? blockTblDivisionId;
  dynamic categorySeparator;
  dynamic plantedYear;
  int? typeOfPlantingId;
  String? spacing;
  int? rubberClonesId;
  String? areaAcre;
  dynamic yearOfOpening;
  dynamic noTappingTasks;
  String? typeOfPlantingname;
  String? rubberClonesName;
  int? tappingSystemId;
  String? tappingSystemName;
  dynamic panelPositionName;
  dynamic panelPositionId;
  dynamic standPerAcre;
  dynamic noOfTreesInTheBlock;
  String? isPlotted;

  factory BlocksModelResultElement.fromJson(Map<String, dynamic> json) =>
      BlocksModelResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        name: json["name"],
        datecomparer: json["datecomparer"].toString(),
        code: json["code"],
        mainDivisionId: json["main_division_id"],
        blockTblDivisionId: json["block_tbl_division_id"],
        categorySeparator: json["category_separator"],
        plantedYear: json["planted_year"],
        typeOfPlantingId: json["type_of_planting_id"],
        spacing: json["spacing"],
        rubberClonesId: json["rubber_clones_id"],
        areaAcre: json["area_acre"].toString(),
        yearOfOpening: json["year_of_opening"],
        noTappingTasks: json["no_tapping_tasks"],
        typeOfPlantingname: json["type_of_plantingname"],
        rubberClonesName: json["rubber_clones_Name"],
        tappingSystemId: json["tapping_system_id"],
        tappingSystemName: json["tapping_system_Name"],
        panelPositionName: json["panel_position_Name"],
        panelPositionId: json["panel_position_id"],
        standPerAcre: json["stand_per_acre"],
        noOfTreesInTheBlock: json["no_of_Trees_in_the_block"],
        isPlotted: json["is_plotted"],
      );

  Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "name": name,
        "datecomparer": datecomparer,
        "code": code,
        "main_division_id": mainDivisionId,
        "block_tbl_division_id": blockTblDivisionId,
        "category_separator": categorySeparator,
        "planted_year": plantedYear,
        "type_of_planting_id": typeOfPlantingId,
        "spacing": spacing,
        "rubber_clones_id": rubberClonesId,
        "area_acre": areaAcre,
        "year_of_opening": yearOfOpening,
        "no_tapping_tasks": noTappingTasks,
        "type_of_plantingname": typeOfPlantingname,
        "rubber_clones_Name": rubberClonesName,
        "tapping_system_id": tappingSystemId,
        "tapping_system_Name": tappingSystemName,
        "panel_position_Name": panelPositionName,
        "panel_position_id": panelPositionId,
        "stand_per_acre": standPerAcre,
        "is_plotted": noOfTreesInTheBlock,
      };
}

// BlocksModel blocksModelFromJson(String str) => BlocksModel.fromJson(json.decode(str));

// String blocksModelToJson(BlocksModel data) => json.encode(data.toJson());

// class BlocksModel {
//     BlocksModel({
//         required this.id,
//         required this.isOkay,
//         required this.message,
//         required this.statusCode,
//         required this.result,
//     });

//     String? id;
//     bool? isOkay;
//     String? message;
//     String? statusCode;
//     BlocksModelResult? result;

//     factory BlocksModel.fromJson(Map<String, dynamic> json) => BlocksModel(
//         id: json["\u0024id"],
//         isOkay: json["IsOkay"],
//         message: json["Message"],
//         statusCode: json["statusCode"],
//         result: BlocksModelResult.fromJson(json["Result"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024id": id,
//         "IsOkay": isOkay,
//         "Message": message,
//         "statusCode": statusCode,
//         "Result": result!.toJson(),
//     };
// }

// class BlocksModelResult {
//     BlocksModelResult({
//         required this.id,
//         required this.request,
//         required this.isSuccessull,
//         required this.dataSetCount,
//         required this.result,
//     });

//     String? id;
//     Request? request;
//     bool? isSuccessull;
//     int? dataSetCount;
//     List<ResultElement>? result;

//     factory BlocksModelResult.fromJson(Map<String, dynamic> json) => BlocksModelResult(
//         id: json["\u0024id"],
//         request: Request.fromJson(json["request"]),
//         isSuccessull: json["isSuccessull"],
//         dataSetCount: json["DataSetCount"],
//         result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024id": id,
//         "request": request!.toJson(),
//         "isSuccessull": isSuccessull,
//         "DataSetCount": dataSetCount,
//         "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
//     };
// }

// class Request {
//     Request({
//         required this.id,
//         required this.take,
//         required this.skip,
//         required this.page,
//         required this.pageSize,
//         required this.filter,
//         this.sort,
//     });

//     String? id;
//     int? take;
//     int? skip;
//     int? page;
//     int? pageSize;
//     RequestFilter? filter;
//     dynamic sort;

//     factory Request.fromJson(Map<String, dynamic> json) => Request(
//         id: json["\u0024id"],
//         take: json["take"],
//         skip: json["skip"],
//         page: json["page"],
//         pageSize: json["pageSize"],
//         filter: RequestFilter.fromJson(json["filter"]),
//         sort: json["sort"],
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024id": id,
//         "take": take,
//         "skip": skip,
//         "page": page,
//         "pageSize": pageSize,
//         "filter": filter!.toJson(),
//         "sort": sort,
//     };
// }

// class RequestFilter {
//     RequestFilter({
//         required this.id,
//         required this.logic,
//         required this.filters,
//     });

//     String? id;
//     String? logic;
//     List<FilterElement>? filters;

//     factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
//         id: json["\u0024id"],
//         logic: json["logic"],
//         filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024id": id,
//         "logic": logic,
//         "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
//     };
// }

// class FilterElement {
//     FilterElement({
//         required this.id,
//         required this.field,
//         required this.value,
//         required this.filterOperator,
//     });

//     String? id;
//     String? field;
//     int? value;
//     int? filterOperator;

//     factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
//         id: json["\u0024id"],
//         field: json["field"],
//         value: json["value"],
//         filterOperator: json["operator"],
//     );

//     Map<String, dynamic> toJson() => {
//         "\u0024id": id,
//         "field": field,
//         "value": value,
//         "operator": filterOperator,
//     };
// }

// class ResultElement {
//     ResultElement({
//         this.id,
//         this.resultId,
//         this.name,
//         this.datecomparer,
//         this.code,
//         this.mainDivisionId,
//         this.blockTblDivisionId,
//         this.categorySeparator,
//         this.plantedYear,
//         this.typeOfPlantingId,
//         this.spacing,
//         this.rubberClonesId,
//         this.areaAcre,
//         this.yearOfOpening,
//         this.noTappingTasks,
//         this.typeOfPlantingname,
//         this.rubberClonesName,
//         this.tappingSystemId,
//         this.tappingSystemName,
//         this.panelPositionName,
//         this.panelPositionId,
//         this.standPerAcre,
//         this.noOfTreesInTheBlock,
//     });

//     String? id;
//     int? resultId;
//     String? name;
//     String? datecomparer;
//     String? code;
//     int? mainDivisionId;
//     int? blockTblDivisionId;
//     String? categorySeparator;
//     String? plantedYear;
//     int? typeOfPlantingId;
//     String? spacing;
//     int? rubberClonesId;
//     String? areaAcre;
//     String? yearOfOpening;
//     String? noTappingTasks;
//     String? typeOfPlantingname;
//     String? rubberClonesName;
//     int? tappingSystemId;
//     String? tappingSystemName;
//     String? panelPositionName;
//     int? panelPositionId;
//     String? standPerAcre;
//     String? noOfTreesInTheBlock;

//     factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
//         //id: json["\u0024id"],
//         resultId: json["id"],
//         name: json["name"],
//         datecomparer: json["datecomparer"].toString(),
//         code: json["code"],
//         mainDivisionId: json["main_division_id"],
//         blockTblDivisionId: json["block_tbl_division_id"],
//         categorySeparator: json["category_separator"],
//         plantedYear: json["planted_year"],
//         typeOfPlantingId: json["type_of_planting_id"],
//         spacing: json["spacing"].toString(),
//         rubberClonesId: json["rubber_clones_id"],
//         areaAcre: json["area_acre"].toString(),
//         yearOfOpening: json["year_of_opening"],
//         noTappingTasks: json["no_tapping_tasks"],
//         typeOfPlantingname: json["type_of_plantingname"],
//         rubberClonesName: json["rubber_clones_Name"],
//         tappingSystemId: json["tapping_system_id"],
//         tappingSystemName: json["tapping_system_Name"],
//         panelPositionName: json["panel_position_Name"],
//         panelPositionId: json["panel_position_id"],
//         standPerAcre: json["stand_per_acre"].toString(),
//         noOfTreesInTheBlock: json["no_of_Trees_in_the_block"],
//     );

//     Map<String, dynamic> toJson() => {
//         //"\u0024id": id,
//         "id": resultId,
//         "name": name,
//         "datecomparer": datecomparer,
//         "code": code,
//         "main_division_id": mainDivisionId,
//         "block_tbl_division_id": blockTblDivisionId,
//         "category_separator": categorySeparator,
//         "planted_year": plantedYear,
//         "type_of_planting_id": typeOfPlantingId,
//         "spacing": spacing,
//         "rubber_clones_id": rubberClonesId,
//         "area_acre": areaAcre,
//         "year_of_opening": yearOfOpening,
//         "no_tapping_tasks": noTappingTasks,
//         "type_of_plantingname": typeOfPlantingname,
//         "rubber_clones_Name": rubberClonesName,
//         "tapping_system_id": tappingSystemId,
//         "tapping_system_Name": tappingSystemName,
//         "panel_position_Name": panelPositionName,
//         "panel_position_id": panelPositionId,
//         "stand_per_acre": standPerAcre,
//         "no_of_Trees_in_the_block": noOfTreesInTheBlock,
//     };
// }
