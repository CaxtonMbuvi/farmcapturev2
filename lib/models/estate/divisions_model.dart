// To parse this JSON data, do
//
//     final divisionsModel = divisionsModelFromJson(jsonString);

import 'dart:convert';

const String divisionsTable = 'divisions';

DivisionsModel divisionsModelFromJson(String str) => DivisionsModel.fromJson(json.decode(str));

String divisionsModelToJson(DivisionsModel data) => json.encode(data.toJson());

class DivisionsModel {
    DivisionsModel({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
    });

    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    DivisionsModelResult? result;

    factory DivisionsModel.fromJson(Map<String, dynamic> json) => DivisionsModel(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: DivisionsModelResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class DivisionsModelResult {
    DivisionsModelResult({
        required this.id,
        required this.request,
        required this.isSuccessull,
        required this.dataSetCount,
        required this.result,
    });

    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    factory DivisionsModelResult.fromJson(Map<String, dynamic> json) => DivisionsModelResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    Request({
        required this.id,
        required this.take,
        required this.skip,
        required this.page,
        required this.pageSize,
        required this.filter,
        this.sort,
    });

    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    RequestFilter({
        required this.id,
        required this.logic,
        required this.filters,
    });

    String? id;
    String? logic;
    List<FilterElement>? filters;

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    FilterElement({
        required this.id,
        required this.field,
        required this.value,
        required this.filterOperator,
    });

    String? id;
    String? field;
    int? value;
    int? filterOperator;

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"],
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    ResultElement({
        //this.id,
        this.resultId,
        this.code,
        this.name,
        this.mobiCode,
        this.datecomparer,
        this.divisionHeadUserId,
        this.rubberSourceId,
        this.mainDivisionId,
    });

    String? id;
    int? resultId;
    String? code;
    String? name;
    String? mobiCode;
    String? datecomparer;
    int? divisionHeadUserId;
    int? rubberSourceId;
    int? mainDivisionId;

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        code: json["code"],
        name: json["name"],
        mobiCode: json["mobi_code"],
        datecomparer: json["datecomparer"].toString(),
        divisionHeadUserId: json["division_head_user_id"],
        rubberSourceId: json["rubber_source_id"],
        mainDivisionId: json["main_division_id"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "code": code,
        "name": name,
        "mobi_code": mobiCode,
        "datecomparer": datecomparer,
        "division_head_user_id": divisionHeadUserId,
        "rubber_source_id": rubberSourceId,
        "main_division_id": mainDivisionId,
    };
}

class DivisionsResultElement {
    DivisionsResultElement({
        //this.id,
        this.resultId,
        this.code,
        this.name,
        this.mobiCode,
        this.datecomparer,
        this.divisionHeadUserId,
        this.rubberSourceId,
        this.mainDivisionId,
    });

    String? id;
    int? resultId;
    String? code;
    String? name;
    String? mobiCode;
    String? datecomparer;
    int? divisionHeadUserId;
    int? rubberSourceId;
    int? mainDivisionId;

    factory DivisionsResultElement.fromJson(Map<String, dynamic> json) => DivisionsResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        code: json["code"],
        name: json["name"],
        mobiCode: json["mobi_code"],
        datecomparer: json["datecomparer"].toString(),
        divisionHeadUserId: json["division_head_user_id"],
        rubberSourceId: json["rubber_source_id"],
        mainDivisionId: json["main_division_id"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "code": code,
        "name": name,
        "mobi_code": mobiCode,
        "datecomparer": datecomparer,
        "division_head_user_id": divisionHeadUserId,
        "rubber_source_id": rubberSourceId,
        "main_division_id": mainDivisionId,
    };
}