// To parse this JSON data, do
//
//     final allDivisionsModelApi = allDivisionsModelApiFromJson(jsonString);

import 'dart:convert';

const String allDivisionPolygonTable = 'alldivisionspolygon';

AllDivisionsModelApi allDivisionsModelApiFromJson(String str) => AllDivisionsModelApi.fromJson(json.decode(str));

String allDivisionsModelApiToJson(AllDivisionsModelApi data) => json.encode(data.toJson());

class AllDivisionsModelApi {
    String? id;
    bool? isOkay;
    String? message;
    String? statusCode;
    AllDivisionsModelApiResult? result;

    AllDivisionsModelApi({
         this.id,
         this.isOkay,
         this.message,
         this.statusCode,
         this.result,
    });

    factory AllDivisionsModelApi.fromJson(Map<String, dynamic> json) => AllDivisionsModelApi(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: AllDivisionsModelApiResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result!.toJson(),
    };
}

class AllDivisionsModelApiResult {
    String? id;
    Request? request;
    bool? isSuccessull;
    int? dataSetCount;
    List<ResultElement>? result;

    AllDivisionsModelApiResult({
         this.id,
         this.request,
         this.isSuccessull,
         this.dataSetCount,
         this.result,
    });

    factory AllDivisionsModelApiResult.fromJson(Map<String, dynamic> json) => AllDivisionsModelApiResult(
        id: json["\u0024id"],
        request: Request.fromJson(json["request"]),
        isSuccessull: json["isSuccessull"],
        dataSetCount: json["DataSetCount"],
        result: List<ResultElement>.from(json["Result"].map((x) => ResultElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "request": request!.toJson(),
        "isSuccessull": isSuccessull,
        "DataSetCount": dataSetCount,
        "Result": List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Request {
    String? id;
    int? take;
    int? skip;
    int? page;
    int? pageSize;
    RequestFilter? filter;
    dynamic sort;

    Request({
         this.id,
         this.take,
         this.skip,
         this.page,
         this.pageSize,
         this.filter,
        this.sort,
    });

    factory Request.fromJson(Map<String, dynamic> json) => Request(
        id: json["\u0024id"],
        take: json["take"],
        skip: json["skip"],
        page: json["page"],
        pageSize: json["pageSize"],
        filter: RequestFilter.fromJson(json["filter"]),
        sort: json["sort"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "take": take,
        "skip": skip,
        "page": page,
        "pageSize": pageSize,
        "filter": filter!.toJson(),
        "sort": sort,
    };
}

class RequestFilter {
    String? id;
    String? logic;
    List<FilterElement>? filters;

    RequestFilter({
         this.id,
         this.logic,
         this.filters,
    });

    factory RequestFilter.fromJson(Map<String, dynamic> json) => RequestFilter(
        id: json["\u0024id"],
        logic: json["logic"],
        filters: List<FilterElement>.from(json["filters"].map((x) => FilterElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "logic": logic,
        "filters": List<dynamic>.from(filters!.map((x) => x.toJson())),
    };
}

class FilterElement {
    String? id;
    String? field;
    double? value;
    int? filterOperator;

    FilterElement({
         this.id,
         this.field,
         this.value,
         this.filterOperator,
    });

    factory FilterElement.fromJson(Map<String, dynamic> json) => FilterElement(
        id: json["\u0024id"],
        field: json["field"],
        value: json["value"]?.toDouble(),
        filterOperator: json["operator"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "field": field,
        "value": value,
        "operator": filterOperator,
    };
}

class ResultElement {
    String? id;
    int? resultId;
    int? mainDivisionId;
    String? divisionId;
    String? polygonId;
    String? longi;
    String? latitude;
    String? newDevBlockId;
    String? transactionNo;
    String? datecomparer;
    String? isactive;

    ResultElement({
         this.id,
         this.resultId,
         this.mainDivisionId,
        this.divisionId,
         this.polygonId,
         this.longi,
         this.latitude,
        this.newDevBlockId,
         this.transactionNo,
         this.datecomparer,
         this.isactive,
    });

    factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
        //id: json["\u0024id"],
        resultId: json["id"],
        mainDivisionId: json["main_division_id"],
        divisionId: json["division_id"],
        polygonId: json["polygon_id"],
        longi: json["longi"].toString(),
        latitude: json["latitude"].toString(),
        newDevBlockId: json["new_dev_block_id"],
        transactionNo: json["transaction_no"],
        datecomparer: json["datecomparer"].toString(),
        isactive: json["isactive"].toString(),
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "main_division_id": mainDivisionId,
        "division_id": divisionId,
        "polygon_id": polygonId,
        "longi": longi,
        "latitude": latitude,
        "new_dev_block_id": newDevBlockId,
        "transaction_no": transactionNo,
        "datecomparer": datecomparer,
        "isactive": isactive,
    };
}
