
const String farmerBiodiversityAnswSurveysTable = 'farmers_bio_surveys_answers';

class FarmersBiodiversityAnswersModel {
  FarmersBiodiversityAnswersModel({
    this.id,
    this.farmVisitTransactionNo,
    this.farmInspectionsQuizId,
    this.quizTransactionNo,
    this.category,
    this.memberNo,
    this.userId,
    this.blockId,
    this.mainDivId,
    this.comments,
    this.answerOption,
    this.activityStatus,
    this.estimatedAcreage,
    this.datetime,
    this.syncId
  });

  String? id;
  String? farmVisitTransactionNo;
  String? farmInspectionsQuizId;
  String? quizTransactionNo;
  String? category;
  String? memberNo;
  String? userId;
  String? blockId;
  String? mainDivId;
  String? comments;
  String? answerOption;
  String? activityStatus;
  String? estimatedAcreage;
  String? datetime;
  String? syncId;

  factory FarmersBiodiversityAnswersModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersBiodiversityAnswersModel(
        id: json["Id"],
        farmVisitTransactionNo: json["farm_visit_transactionNo"],
        farmInspectionsQuizId: json["farm_inspections_quiz_id"],
        quizTransactionNo: json["quiz_transaction_no"],
        category: json["category"],
        memberNo: json["member_no"],
        userId: json["user_id"],
        blockId: json["block_id"],
        mainDivId: json["main_div_id"],
        comments: json["comments"],
        answerOption: json["answ_option"],
        activityStatus: json["activity_status"],
        estimatedAcreage: json["estimated_acreage"],
        datetime: json["datetime"],
        syncId: json["sync_status"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id,
        "farm_visit_transactionNo": farmVisitTransactionNo,
        "farm_inspections_quiz_id": farmInspectionsQuizId,
        "quiz_transaction_no": quizTransactionNo,
        "category": category,
        "member_no": memberNo,
        "user_id": userId,
        "block_id": blockId,
        "main_div_id": mainDivId,
        "comments": comments,
        "answ_option": answerOption,
        "activity_status": activityStatus,
        "estimated_acreage": estimatedAcreage,
        "datetime": datetime,
        "sync_status": syncId
      };
}