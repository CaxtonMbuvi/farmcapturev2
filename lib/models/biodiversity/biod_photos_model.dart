
const String farmerBiodiversitySurveysPhotosTable = 'farmers_biod_surveys_photos';

class FarmersBiodiversityPhotosModel {
  FarmersBiodiversityPhotosModel({
    this.id,
    this.farmVisitTransactionNo,
    this.farmInspectionsQuizId,
    this.quizTransactionNo,
    this.memberNo,
    this.userId,
    this.blockId,
    this.mainDivId,
    this.photoUrl,
    this.photoUniqueNo,
    this.category,
    this.datetime,
    this.syncId
  });

  String? id;
  String? farmVisitTransactionNo;
  String? farmInspectionsQuizId;
  String? quizTransactionNo;
  String? memberNo;
  String? userId;
  String? blockId;
  String? mainDivId;
  String? photoUrl;
  String? photoUniqueNo;
  String? category;
  String? datetime;
  String? syncId;

  factory FarmersBiodiversityPhotosModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersBiodiversityPhotosModel(
        id: json["Id"],
        farmVisitTransactionNo: json["farm_visit_transactionNo"],
        farmInspectionsQuizId: json["farm_inspections_quiz_id"],
        quizTransactionNo: json["quiz_transaction_no"],
        memberNo: json["member_no"],
        userId: json["user_id"],
        blockId: json["block_id"],
        mainDivId: json["main_div_id"],
        photoUrl: json["photo_url"],
        photoUniqueNo: json["photo_uniqueNo"],
        category: json["category"],
        datetime: json["datetime"],
        syncId: json["sync_status"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id,
        "farm_visit_transactionNo": farmVisitTransactionNo,
        "farm_inspections_quiz_id": farmInspectionsQuizId,
        "quiz_transaction_no": quizTransactionNo,
        "member_no": memberNo,
        "user_id": userId,
        "block_id": blockId,
        "main_div_id": mainDivId,
        "photo_url": photoUrl,
        "photo_uniqueNo": photoUniqueNo,
        "category": category,
        "datetime": datetime,
        "sync_status": syncId
      };
}