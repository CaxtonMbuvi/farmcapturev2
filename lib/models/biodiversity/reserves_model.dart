const String farmerReservesPolygonTable = 'farmer_reserve_polygons';

class ReservePolygonResultElement {
  //String? id;
  String? resultId;
  String? polygonId;
  String? longi;
  String? latitude;
  String? reservePolygonUniqueID;
  String? isActive;
  String? landSize;
  String? reserveAreaId;
  String? userId;
  String? reserveName;
  String? mapCategory;
  String? landDetailsId;
  String? isFarmerOrEstate;
  String? isComplete;
  String? datecomparer;
  String? syncId;

  ReservePolygonResultElement(
      {
      //this.id,
      this.resultId,
      this.polygonId,
      this.longi,
      this.latitude,
      this.reservePolygonUniqueID,
      this.isActive,
      this.landSize,
      this.reserveAreaId,
      this.userId,
      this.reserveName,
      this.mapCategory,
      this.landDetailsId,
      this.isFarmerOrEstate,
      this.isComplete,
      this.datecomparer,
      this.syncId});

  factory ReservePolygonResultElement.fromJson(Map<String, dynamic> json) =>
      ReservePolygonResultElement(
        //id: json["\u0024id"],
        resultId: json["Id"],
        polygonId: json["polygon_id"],
        longi: json["longi"].toString(),
        latitude: json["latitude"].toString(),
        reservePolygonUniqueID: json["reserve_polygon_unique_id"],
        isActive: json["isActive"],
        landSize: json["landSize"],
        reserveAreaId: json["reserve_area_id"],
        userId: json["user_id"],
        reserveName: json["reserve_name"],
        mapCategory: json["map_category"],
        landDetailsId: json["land_details_id"],
        isFarmerOrEstate: json["is_farmer_or_estate"],
        isComplete: json["is_complete"],
        datecomparer: json["datecomparer"],
        syncId: json["syncId"],
      );

  Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "Id": resultId,
        "polygon_id": polygonId,
        "longi": longi,
        "latitude": latitude,
        "reserve_polygon_unique_id": reservePolygonUniqueID,
        "isActive": isActive,
        "landSize": landSize,
        "reserve_area_id": reserveAreaId,
        "user_id": userId,
        "reserve_name": reserveName,
        "map_category": mapCategory,
        "land_details_id": landDetailsId,
        "is_farmer_or_estate": isFarmerOrEstate,
        "is_complete": isComplete,
        "datecomparer": datecomparer,
        "syncId": syncId,
      };
}
