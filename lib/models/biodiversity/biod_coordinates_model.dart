
const String farmerBiodiversitySurveyCoordinatesTable = 'farmers_biod_surveys_coordinates';

class FarmersBiodiversityCoordinatesModel {
  FarmersBiodiversityCoordinatesModel({
    this.id,
    this.farmVisitTransactionNo,
    this.farmInspectionsQuizId,
    this.quizTransactionNo,
    this.memberNo,
    this.userId,
    this.blockId,
    this.mainDivId,
    this.latitude,
    this.longitude,
    this.coOrdinateUniqueNo,
    this.datetime,
    this.syncId
  });

  String? id;
  String? farmVisitTransactionNo;
  String? farmInspectionsQuizId;
  String? quizTransactionNo;
  String? memberNo;
  String? userId;
  String? blockId;
  String? mainDivId;
  String? latitude;
  String? longitude;
  String? coOrdinateUniqueNo;
  String? datetime;
  String? syncId;

  factory FarmersBiodiversityCoordinatesModel.fromJson(
          Map<String, dynamic> json) =>
      FarmersBiodiversityCoordinatesModel(
        id: json["Id"],
        farmVisitTransactionNo: json["farm_visit_transactionNo"],
        farmInspectionsQuizId: json["farm_inspections_quiz_id"],
        quizTransactionNo: json["quiz_transaction_no"],
        memberNo: json["member_no"],
        userId: json["user_id"],
        blockId: json["block_id"],
        mainDivId: json["main_div_id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        coOrdinateUniqueNo: json["coordinate_unique_no"],
        datetime: json["datetime"],
        syncId: json["sync_status"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id,
        "farm_visit_transactionNo": farmVisitTransactionNo,
        "farm_inspections_quiz_id": farmInspectionsQuizId,
        "quiz_transaction_no": quizTransactionNo,
        "member_no": memberNo,
        "user_id": userId,
        "block_id": blockId,
        "main_div_id": mainDivId,
        "latitude": latitude,
        "longitude": longitude,
        "coordinate_unique_no": coOrdinateUniqueNo,
        "datetime": datetime,
        "sync_status": syncId
      };
}