
// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';
const String usersTable = 'users';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
    UserModel({
        required this.id,
        required this.result,
        required this.moduleName,
    });

    String id;
    UserModelResult result;
    String moduleName;

    factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["\u0024id"],
        result: UserModelResult.fromJson(json["Result"]),
        moduleName: json["ModuleName"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "Result": result.toJson(),
        "ModuleName": moduleName,
    };
}

class UserModelResult {
    UserModelResult({
        required this.id,
        required this.isOkay,
        required this.message,
        required this.statusCode,
        required this.result,
    });

    String id;
    bool isOkay;
    String message;
    String statusCode;
    ResultResult result;

    factory UserModelResult.fromJson(Map<String, dynamic> json) => UserModelResult(
        id: json["\u0024id"],
        isOkay: json["IsOkay"],
        message: json["Message"],
        statusCode: json["statusCode"],
        result: ResultResult.fromJson(json["Result"]),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "IsOkay": isOkay,
        "Message": message,
        "statusCode": statusCode,
        "Result": result.toJson(),
    };
}

class ResultResult {
    ResultResult({
        this.id,
        this.resultId,
        this.username,
        this.password,
        this.accountId,
        this.statusId,
        this.emailAddress,
        this.phoneNumber,
        this.useSms,
        this.useMail,
        this.useCode,
        this.passwordDuration,
        this.accessLevelId,
        this.userImagePath,
    });

    String? id;
    int? resultId;
    String? username;
    String? password;
    int? accountId;
    bool? statusId;
    String? emailAddress;
    String? phoneNumber;
    bool? useSms;
    bool? useMail;
    bool? useCode;
    int? passwordDuration;
    int? accessLevelId;
    dynamic userImagePath;

    factory ResultResult.fromJson(Map<String, dynamic> json) => ResultResult(
        //id: json["\u0024id"],
        resultId: json["id"],
        username: json["username"],
        password: json["password"],
        accountId: json["account_id"],
        statusId: json["status_id"],
        emailAddress: json["email_address"],
        phoneNumber: json["phone_number"],
        useSms: json["use_sms"],
        useMail: json["use_mail"],
        useCode: json["use_code"],
        passwordDuration: json["password_duration"],
        accessLevelId: json["access_level_id"],
        userImagePath: json["user_image_path"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "id": resultId,
        "username": username,
        "password": password,
        "account_id": accountId,
        "status_id": statusId,
        "email_address": emailAddress,
        "phone_number": phoneNumber,
        "use_sms": useSms,
        "use_mail": useMail,
        "use_code": useCode,
        "password_duration": passwordDuration,
        "access_level_id": accessLevelId,
        "user_image_path": userImagePath,
    };
}

class UserModelResultResult {
    UserModelResultResult({
        this.id,
        this.resultId,
        this.username,
        this.password,
        this.accountId,
        this.statusId,
        this.emailAddress,
        this.phoneNumber,
        this.useSms,
        this.useMail,
        this.useCode,
        this.passwordDuration,
        this.accessLevelId,
        this.userImagePath,
    });

    String? id;
    int? resultId;
    String? username;
    String? password;
    int? accountId;
    bool? statusId;
    String? emailAddress;
    String? phoneNumber;
    bool? useSms;
    bool? useMail;
    bool? useCode;
    int? passwordDuration;
    int? accessLevelId;
    dynamic userImagePath;

    factory UserModelResultResult.fromJson(Map<String, dynamic> json) => UserModelResultResult(
        //id: json["\u0024id"],
        resultId: json["resultId"],
        username: json["username"],
        password: json["password"],
        accountId: json["account_id"],
        statusId: json["status_id"],
        emailAddress: json["email_address"],
        phoneNumber: json["phone_number"],
        useSms: json["use_sms"],
        useMail: json["use_mail"],
        useCode: json["use_code"],
        passwordDuration: json["password_duration"],
        accessLevelId: json["access_level_id"],
        userImagePath: json["user_image_path"],
    );

    Map<String, dynamic> toJson() => {
        //"\u0024id": id,
        "resultId": resultId,
        "username": username,
        "password": password,
        "account_id": accountId,
        "status_id": statusId,
        "email_address": emailAddress,
        "phone_number": phoneNumber,
        "use_sms": useSms,
        "use_mail": useMail,
        "use_code": useCode,
        "password_duration": passwordDuration,
        "access_level_id": accessLevelId,
        "user_image_path": userImagePath,
    };
}
