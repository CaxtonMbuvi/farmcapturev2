import 'package:flutter/material.dart';

import '../utils/dimensions.dart';

class ProgressDialog extends StatelessWidget {
  String? message;

  ProgressDialog({super.key, this.message});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      //backgroundColor: Colors.black,
      child: Container(
        margin: EdgeInsets.all(Dimensions.width15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(Dimensions.radius15/2),
        ),
        child: Padding(
          padding: EdgeInsets.all(Dimensions.width15),
          child: Row(
            children: [
             SizedBox(width: Dimensions.width10/2),

              const CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
              ),

             SizedBox(width: Dimensions.width20),

              Text(
                message!,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: Dimensions.font16-2
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
