import 'package:flutter/material.dart';

import '../utils/dimensions.dart';

class BigText extends StatelessWidget {
  final Color? color;
  final String text;
  final double? width;
  double size;
  TextOverflow overFlow;

  BigText({Key? key, this.color = const Color(0xFFFFFFFF), 
  required this.text,
  this.width,
  this.size = 20,
  this.overFlow = TextOverflow.visible
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Text(
         text,
         //maxLines: 1,
         //overflow: overFlow,
         style: TextStyle(
           color: color,
           fontSize: size==0?Dimensions.font28:size,
           fontWeight: FontWeight.w700,
         ),
      ),
    );
  }
}
