import 'package:flutter/material.dart';

import '../utils/colors.dart';
import '../utils/dimensions.dart';

class FormFields extends StatefulWidget {
  final TextEditingController textEditingController;
  final IconData? icon;
  final Color? color;
  final bool isObscure;
  final String? hintText;
  final TextInputType? inputType;
  final int? maxLines;
  final bool? readOnly;

  const FormFields(
      {Key? key,
      required this.textEditingController,
      this.icon,
      this.color,
      this.hintText,
      this.inputType,
      this.readOnly,
      this.maxLines,
      this.isObscure = false})
      : super(key: key);

  @override
  State<FormFields> createState() => _FormFieldStates();
}

class _FormFieldStates extends State<FormFields> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(
            top: Dimensions.height10, bottom: Dimensions.height10),
        child: TextField(
          autocorrect: true,
          maxLines: widget.maxLines,
          controller: widget.textEditingController,
          keyboardType: widget.inputType,
          readOnly: widget.readOnly ?? false,
          decoration: InputDecoration(
            hintText: widget.hintText,
            hintStyle: TextStyle(color: widget.color ?? AppColors.textGrey),
            filled: true,
            fillColor: Colors.white70,
            enabledBorder: OutlineInputBorder(
              borderRadius:
                  BorderRadius.all(Radius.circular(Dimensions.radius15 - 5)),
              borderSide: const BorderSide(color: Colors.lightGreen, width: 2),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius:
                  BorderRadius.all(Radius.circular(Dimensions.radius15)),
              borderSide: const BorderSide(color: Colors.lightGreen, width: 2),
            ),
          ),
        ));
  }
}
