import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? hintText;
  final String? labelText;
  final onChanged;
  final IconButton? suffixIcon;
  final InputBorder? enabledBorder;
  final InputBorder? focusedBorder;
  final TextInputType? keyboardType;
  Function(dynamic value)? onSaved;
  Function(dynamic value)? onTap;
  bool? isObscure = true;
  final bool readOnly;

  CustomTextField({super.key, 
    this.controller,
    this.hintText,
    this.labelText,
    this.onChanged,
    this.suffixIcon,
    this.isObscure,
    this.enabledBorder,
    this.focusedBorder,
    this.keyboardType,
    this.onSaved,
    this.onTap,
    required this.readOnly
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: isObscure!,
      cursorColor: Colors.black,
      keyboardType: keyboardType,
      readOnly: readOnly,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        focusColor: Theme.of(context).primaryColor,
        hintText: hintText,
        contentPadding: const EdgeInsets.only(left: 5),
        suffixIcon: suffixIcon,
        labelText: labelText,
      ),
    );
  }
}
