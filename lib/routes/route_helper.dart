import 'package:get/get.dart';

import '../pages/authentication/login_page.dart';
import '../pages/authentication/splash_page.dart';

class RouteHelper {
  static const String initial = "/";
  static const String splashPage = "/welcome-page";
  static const String logIn = "/sign_in";
  static const String signup = "/sign_up";

  static String getInitial() => initial;
  static String getSplashPage() => splashPage;
  static String getLogInPage() => logIn;
  static String getSignUpPage() => signup;
  static List<GetPage> routes = [
    GetPage(name: splashPage, page: () => const SplashScreen()),
    GetPage(name: logIn, page: () => const LoginScreen()),
  ];
}
