import 'package:farmcapturev2/pages/main_pages/infrastructure_mapping/infrastructure_mapping.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../controllers/location/location_controller.dart';
import '../../../db/db_helper.dart';
import '../../../models/infrastructure/infrastructureApi_model.dart';
import '../../../models/infrastructure/infrastructurepolygons_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../estate/estate_dashboard.dart';

class InfrastructureDashboardScreen extends StatefulWidget {
  const InfrastructureDashboardScreen({super.key});

  @override
  State<InfrastructureDashboardScreen> createState() =>
      _InfrastructureDashboardScreenState();
}

class _InfrastructureDashboardScreenState extends State<InfrastructureDashboardScreen> {
  var _selectedIndex = Get.arguments;
  final List<InfrastructureResultElement> _infrastructure = [];
  List<dynamic> items = [];
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getinfrastructure();
  }

  _getinfrastructure() async {
    setState(() => isLoading = true);
    List<dynamic> resInfrastructure = [];
    var res8 =
        await LocationController().getLocationsLocal(infrastructureTypesTable);
    //print("Hello there"+ res[0].toString());
    if (res8 != null) {
      resInfrastructure = res8;
      for (var i in resInfrastructure) {
        var item = {
          "id": i['Id'],
          "image": '',
          "name": i['name'],
          "polyType": '',
        };
        if (i['name'] == "ROADS") {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'polyLine';
        } else if (i['name'] == "WATER TANKS") {
          item["image"] = 'assets/images/map_watertank.png';
          item["polyType"] = 'polyPoint';
        } else if (i['name'] == "CULVERTS") {
          item["image"] = 'assets/images/map_culvert.png';
          item["polyType"] = 'polyPoint';
        } else if (i['name'] == "BRIDGES ") {
          item["image"] = 'assets/images/map_bridge.png';
          item["polyType"] = 'polyPoint';
        } else if (i['name'] == "DIVISION TANKS") {
          item["image"] = 'assets/images/map_farmtank.png';
          item["polyType"] = 'polyPoint';
        } else if (i['name'] == "DIVISION CAMPS") {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'polyGon';
        } else if (i['name'] == "SCHOOLS") {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'polyGon';
        } else if (i['name'] == "HAND PUMPS - Harbel Area") {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'polyPoint';
        } else if (i['name'] == "SEPTIC TANKS - Harbel Area") {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'polyPoint';
        } else if (i['name'] == "PUMP HOUSES") {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'polyPoint';
        } else {
          item["image"] = 'assets/images/map_road.png';
          item["polyType"] = 'null';
        }
        items.add(item);
      }
    }

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        //height: Dimensions.screenHeight,
        width: Dimensions.screenWidth,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? const CustomLoader()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: Dimensions.height30 * Dimensions.height10 / 1.5,
                    child: Stack(
                      children: [
                        Container(
                          height: Dimensions.height15 * 10,
                          padding: EdgeInsets.only(
                            top: Dimensions.height30 * 2,
                            left: Dimensions.width20,
                            right: Dimensions.width20,
                          ),
                          decoration: BoxDecoration(
                            color: AppColors.mainColor,
                            borderRadius: BorderRadius.only(
                              bottomLeft:
                                  Radius.circular(Dimensions.radius30 * 4),
                              bottomRight:
                                  Radius.circular(Dimensions.radius30 * 4),
                            ),
                          ),
                          child: SizedBox(
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.white,
                                        size: Dimensions.iconSize24,
                                      ),
                                      onPressed: () {
                                        Get.to(
                                            () => const EstateDashboardScreen(),
                                            arguments: 3);
                                      },
                                    ),
                                    BigText(
                                      text: "INFRASTRUCTURE DASHBOARD",
                                      color: AppColors.textWhite,
                                      size: Dimensions.font20,
                                    ),
                                    SizedBox(
                                      width: Dimensions.width30,
                                    ),
                                  ],
                                ),
                                InkWell(
                                    onTap: () async {
                                      int x =
                                          await DatabaseHelper.deleteAllItems(
                                              farmerInfrastructurePolygonTable);
                                          await DatabaseHelper.deleteAllItems(
                                              farmerInfrastructureCategoryTable); 
                                      debugPrint(x.toString());
                                    },
                                    child: BigText(text: "Clear"))
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      //height: Dimensions.screenHeight,
                      padding: EdgeInsets.only(
                        left: Dimensions.height30,
                        right: Dimensions.height30,
                        top: Dimensions.height20,
                      ),
                      child: ListView.builder(
                        itemCount: items.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          var item = items[index];
                          return GestureDetector(
                            onTap: () async {
                              setState(() {
                                _selectedIndex = index;
                              });
                              print(item["polyType"].toString());
                              if (item["polyType"].toString() == "null") {
                                print("No Type Selected");
                              } else {
                                Get.to(
                                    () => const InfrastructureMappingScreen(),
                                    arguments: [
                                      _selectedIndex,
                                      item["polyType"].toString(),
                                      item["image"].toString(),
                                      item["id"].toString(),
                                    ]);
                              }
                            },
                            child: Center(
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                margin: const EdgeInsets.only(bottom: 20),
                                padding: EdgeInsets.only(
                                  left: Dimensions.width30,
                                ),
                                decoration: _selectedIndex == index
                                    ? BoxDecoration(
                                        color: AppColors.mainColor,
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20),
                                      )
                                    : BoxDecoration(
                                        color: AppColors.textWhite,
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20),
                                        border: Border.all(
                                          width: 2,
                                          color: AppColors.mainColor,
                                        )),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      item["image"].toString(),
                                      width: 60,
                                      height: 60,
                                      // color: _selectedIndex == index
                                      //     ? AppColors.textWhite
                                      //     : AppColors.mainColor,
                                    ),
                                    SizedBox(
                                      width: Dimensions.width30 * 2,
                                    ),
                                    BigText(
                                      text: item["name"].toString(),
                                      color: AppColors.black,
                                      size: Dimensions.font16 - 4,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
