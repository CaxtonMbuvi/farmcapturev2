import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/dimensions.dart';

class InfrastructureMappingScreen extends StatefulWidget {
  const InfrastructureMappingScreen({super.key});

  @override
  State<InfrastructureMappingScreen> createState() =>
      _InfrastructureMappingScreenState();
}

class _InfrastructureMappingScreenState
    extends State<InfrastructureMappingScreen> {
  final _selectedIndex = Get.arguments[0];
  final _selectedType = Get.arguments[1];
  final _selectedImage = Get.arguments[2];
  final _selectedTypeId = Get.arguments[3];

  bool isLoading = false;
  var mainDivId = "";
  var blockId = "";
  double area = 0.0;
  String selectedDivisionName = '';
  bool onMap = false;

  // FOR LOADING MAP

  void _onMapCreated(GoogleMapController controller) async {
    _controller = controller;
    _controllerCompleter.complete(controller);
  }

  TextEditingController infrastructureNameController = TextEditingController();
  late GoogleMapController _controller;
  final Completer<GoogleMapController> _controllerCompleter = Completer();
  LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);
  final Set<Marker> _markers = <Marker>{};
  final List<Set<Marker>> _markersList = [];
  final Set<Polyline> _polyline = <Polyline>{};
  final List<List<Polygon>> _listInfrastructurePolygons =
      []; //For Mapping Division And Blocks

  _setPolygon(List<LatLng> points) async {
    Polygon pol = Polygon(
      polygonId: PolygonId('$selectedDivisionName'),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listInfrastructurePolygons.add(lispol);
  }

  _setPolygon2(List<LatLng> points) async {
    Polygon pol = Polygon(
      polygonId: PolygonId(''),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.blue,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listInfrastructurePolygons.add(lispol);
  }

  LatLng calculatePolygonCenter(List<LatLng> polygonCoordinates) {
    double latitudeSum = 0.0;
    double longitudeSum = 0.0;

    for (LatLng coordinate in polygonCoordinates) {
      latitudeSum += coordinate.latitude;
      longitudeSum += coordinate.longitude;
    }

    double latitudeAverage = latitudeSum / polygonCoordinates.length;
    double longitudeAverage = longitudeSum / polygonCoordinates.length;

    return LatLng(latitudeAverage, longitudeAverage);
  }

  //END

  @override
  void initState() {
    super.initState();

    _getDivision();
  }

  // FOR INITSTATE
  List<dynamic> _divisionsPolygon = [];
  List<LatLng> points = [];

  _getDivision() async {
    setState(() => isLoading = true);
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var data = jsonDecode(result.toString());

    if (data != null) {
      setState(() {
        mainDivId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();
      });
      //setCustomMapPin();
      print(mainDivId);

      await _getDivisionLatLngFromDatabase(mainDivId);
    } else {
      Get.back();
    }
  }

  _getDivisionLatLngFromDatabase(var id) async {
    var res = await EstateController().getlatlongById(id.toString());
    if (res != null) {
      _divisionsPolygon = res;
      for (int i = 0; i < _divisionsPolygon.length; i++) {
        final lat = double.parse(_divisionsPolygon[i]["latitude"]);
        final lng = double.parse(_divisionsPolygon[i]["longi"]);

        points.add(LatLng(lat, lng));
      }

      await _setPolygon(points);
      LatLng center = calculatePolygonCenter(points);
      _cameraPosition = center;

      await _getInfrastructureCategories();
      // if (_selectedType == "polyGon") {
      //   //await _getAllPolyGonInfrustructure(mainDivId);
      //   await _getInfrastructureCategories();
      // }
      // if (_selectedType == "polyPoint") {

      // }
    } else {}

    setState(() => isLoading = false);
  }

  // FOR POLYGON INFRASTRUCTURE
  final List<LatLng> _polygonType = [
    LatLng(6.339974152575659, 6.339974152575659),
    LatLng(6.331192249588412, 6.331192249588412),
    LatLng(6.335307972976783, 6.335307972976783),
    LatLng(6.340875527116253, 6.340875527116253),
    LatLng(6.338770873422983, 6.338770873422983),
    LatLng(6.340722243571742, 6.340722243571742),
    LatLng(6.340358694808992, 6.340358694808992)
  ];

  final List<List<LatLng>> _listInfrastructurePolygonLatLng = [];
  List<dynamic> transactionNos = [];

  _getInfrastructureCategories() async {
    setState(() {
      _listInfrastructurePolygonLatLng.add(_polygonType);
    });
    await _buildAllPolygons();
    //await _setPolygon2(_polygonType);
    // List<dynamic> infraList = [];
    // var polyRes = await EstateController()
    //     .getInfrastructureCategoryById(mainDivId, _selectedTypeId);
    // if (polyRes != null) {
    //   infraList = polyRes;

    //   for (int i = 0; i < infraList.length; i++) {
    //     final transaction = infraList[i]["transaction_no"];
    //     transactionNos.add(transaction);
    //   }

    //   await _getAllPolyGonInfrustructure();
    // } else {
    //   setState(() => isLoading = false);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? SizedBox(
                height: Dimensions.screenHeight,
                width: Dimensions.screenWidth,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [CustomLoader()],
                ),
              )
            : Stack(
                children: [
                  GoogleMap(
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: CameraPosition(
                      target: _cameraPosition,
                      zoom: 13.0,
                    ),
                    mapType: MapType.satellite,
                    markers: _markersList.expand((element) => element).toSet(),
                    polylines: _polyline,
                    polygons: _listInfrastructurePolygons.expand((i) => i).toSet(),
                    onTap: (point) {},
                  ),
                ],
              ));
  }

  _buildAllPolygons() {
    int j = 0;
    while (j < _listInfrastructurePolygonLatLng.length) {
      List<Polygon> mapPolygonList = [];
      Polygon polygon = Polygon(
        polygonId: const PolygonId('block_Polygon${1}'),
        points: _listInfrastructurePolygonLatLng[j],
        strokeWidth: 2,
        strokeColor: Colors.blue,
        fillColor: Colors.blue.withOpacity(0.3),
      );
       LatLng center = calculatePolygonCenter(_listInfrastructurePolygonLatLng[j]);

      setState(() {
        mapPolygonList.add(polygon);
        _listInfrastructurePolygons.add(mapPolygonList);
      });

      j++;
    }
  }
}
