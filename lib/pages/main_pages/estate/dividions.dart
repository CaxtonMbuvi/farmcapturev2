import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;
import 'dart:math';
import 'package:farmcapturev2/models/estate/divisionpolygon_model.dart';
import 'package:farmcapturev2/pages/main_pages/estate/testbluetooth.dart';
import 'package:farmcapturev2/resources/utils/colors.dart';
import 'package:farmcapturev2/resources/widgets/big_text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/utils/poly_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:latlong2/latlong.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../models/estate/blocks_model.dart';
import '../../../models/estate/maindivisions_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/small_text.dart';

class DivisionsScreen extends StatefulWidget {
  const DivisionsScreen({Key? key}) : super(key: key);

  @override
  _DivisionsScreenState createState() => _DivisionsScreenState();
}

class _DivisionsScreenState extends State<DivisionsScreen> {
  var mainDivId = "";
  var blockId = "";

  final List<List<LatLng>> _listBlocksPolygon = [];
  List<Polygon> mapPolygonList = [];
  final List<List<Polygon>> _listBlocksPolygons = [];
  List<dynamic> _divisionsPolygon = [];
  bool isLoading = false;
  bool onMap = false;
  bool blocksEmpty = true;
  late GoogleMapController _controller;
  final Completer<GoogleMapController> _controllerCompleter = Completer();
  LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);
  //Default camera position
  List<LatLng> points = [];
  List<LatLng> pointsB = [];
  List<LatLng> Blockpoints = [];

  final Set<Polygon> _polygon = <Polygon>{};
  final Set<Marker> _markers = <Marker>{};
  final Set<Polyline> _polyline = <Polyline>{};
  List<LatLng> polygonLatLngs = <LatLng>[];
  List<LatLng> temppolygonLatLngs = <LatLng>[];
  int _markerId = 1;
  final int _polylineIdCounter = 1;
  LatLng? _point;
  double area = 0.0;

  List<dynamic> _indivDivisionPolygon = [];
  List<List<LatLng>> divisions = [];

  List<dynamic> _blockslatlong = [];

  List<dynamic> _alldivisionBlocks = [];
  final List<dynamic> _divisionBlocksId = [];

  Future _getAllDivisionBlocks(var id) async {
    

    try {
      _alldivisionBlocks =
          await EstateController().getBlocksLocalById(blocksTable, id);
      if (_alldivisionBlocks.isNotEmpty) {
        _alldivisionBlocks.reversed;
        for (var element in _alldivisionBlocks) {
          if (element['Id'].toString() != blockId) {
            _divisionBlocksId.add(element['Id'].toString());
          }
        }

        setState(() {});
        await _getAllBlocskLatLngFromDatabase();
      } else {
        print("Did not Get");
      }
    } catch (e) {
      //setState(() => isLoading = false);
    }
  }

  _getAllBlocskLatLngFromDatabase() async {
    try {
      int i = 0;
      while (i < _divisionBlocksId.length) {
        List<dynamic> allblockslatlong = [];
        List<LatLng> allBlockspoints = [];
        var res = await EstateController().getBlockslatlongById(_divisionBlocksId[i].toString());
        
        
        if (res != null) {
          setState(() {
            allblockslatlong = res;
          });
          if (allblockslatlong.isNotEmpty) {
            for (int j = 0; j < allblockslatlong.length; j++) {
              final lat = double.parse(allblockslatlong[j]["latitude"]);
              final lng = double.parse(allblockslatlong[j]["longi"]);
              allBlockspoints.add(LatLng(lat, lng));
              pointsB.add(LatLng(lat, lng));
            }
            
          }
          i++;
          _listBlocksPolygon.add(allBlockspoints);
        } else {
          i++;
        }

      }
      await _buildAllPolygons();
    } catch (e) {}
  }

  _getBlockLatLngFromDatabase() async {
    try {
      print("BlockId is: " + blockId.toString());
      //List<dynamic> _blockslatlong = [];
      var res = await EstateController().getBlockslatlongById(
        blockId,
      );
      if (res != null) {
        print("Block Polygon is: " + Blockpoints.toString());

        _blockslatlong = res;
        for (int j = 0; j < _blockslatlong.length; j++) {
          final lat = double.parse(_blockslatlong[j]["latitude"]);
          final lng = double.parse(_blockslatlong[j]["longi"]);
          Blockpoints.add(LatLng(lat, lng));
        }
      }
     
      await _getPolyline();
      area = calculateArea(polygonLatLngs);
      
    } catch (e) {}
  }

  _getPolyline() {
    setState(() {
      blocksEmpty = false;
    });
    for (var element in Blockpoints) {
      polygonLatLngs.add(element);
      //_setMarker(element);
      _setPolyline(element);
    }
  }

  _getDivisionLatLngFromDatabase(var id) async {
    setState(() => isLoading = true);
    print("At Function ${id.toString()}");

    try {
      // _divisionsPolygon = await EstateController().getlatlongById(id.toString());
      //_divisionsPolygon = await EstateController().getDivisionlatlong();
      var res = await EstateController().getlatlongById(id.toString());
       print(res.toString());
      if (res != null) {
        _divisionsPolygon = res;
        //print(_divisionsPolygon[0]["latitude"].toString());
        for (int i = 0; i < _divisionsPolygon.length; i++) {
          final lat = double.parse(_divisionsPolygon[i]["latitude"]);
          final lng = double.parse(_divisionsPolygon[i]["longi"]);

          points.add(LatLng(lat, lng));
        }

        //print("Division Polygon is: " + points.toString());
        await _setPolygon();
        await _getBlockLatLngFromDatabase();
        await _getAllDivisionBlocks(mainDivId);
        _getLatLngCameraPosition().then((latLng) {
          setState(() {
            _cameraPosition = latLng;
          });
        });
        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    } catch (e) {
      print("Did not Get ${e.toString()}");
    }
  }

  // List<dynamic> _mainDivisions = [];

  // Future _getAllMainDivisions() async {
  //   print("At Function");

  //   try {
  //     _mainDivisions =
  //         await EstateController().getMainDivisions(mainDivisionsTable);
  //     if (_mainDivisions.isNotEmpty) {
  //       _mainDivisions.reversed;
  //       print(_mainDivisions[0]["division_name"]!.toString());
  //       print(_mainDivisions.length);

  //       //print("List of Users: " + userModelList.length.toString());
  //     } else {
  //       print("Did not Get");
  //     }
  //   } catch (e) {}
  // }

  // _getAllDivisionsLatLng() async {
  //   setState(() => isLoading = true);
  //   await _getAllMainDivisions();
  //   try {
  //     for (var element in _mainDivisions) {
  //       var id = element["Id"].toString();
  //       _indivDivisionPolygon =
  //           await EstateController().getlatlongById(id.toString());

  //       for (int i = 0; i < _indivDivisionPolygon.length; i++) {
  //         final lat = double.parse(_indivDivisionPolygon[i]["latitude"]);
  //         final lng = double.parse(_indivDivisionPolygon[i]["longi"]);
  //         List<LatLng> myPoints = [];
  //         myPoints.add(LatLng(lat, lng));
  //         divisions.add(myPoints);
  //       }
  //     }
  //     //_indivDivisionPolygon = await EstateController().getDivisionlatlong();
  //     if (_indivDivisionPolygon.isNotEmpty) {
  //       //print(_divisionsPolygon[0]["latitude"].toString());

  //       //print("Division Polygon is: " + points.toString());
  //       await _setPolygon();
  //       await _getBlockLatLngFromDatabase();
  //       await _getAllDivisionBlocks(mainDivId);
  //       _getLatLngCameraPosition().then((latLng) {
  //         setState(() {
  //           _cameraPosition = latLng;
  //         });
  //       });
  //       setState(() => isLoading = false);
  //     } else {
  //       print("Did not Get");
  //     }
  //   } catch (e) {
  //     print("Did not Get ${e.toString()}");
  //   }
  // }

  _setPolygon() async {
    //List<LatLng> flattenedList = divisions.expand((i) => i).toList();
    Polygon pol = Polygon(
      polygonId: const PolygonId('Division_Polygon'),
      points: points,
      // points: flattenedList,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listBlocksPolygons.add(lispol);
  }

  Future<LatLng> _getLatLngCameraPosition() async {
    final lat = double.parse(_divisionsPolygon.first['latitude']);
    final long = double.parse(_divisionsPolygon.first['longi']);
    return LatLng(lat, long);
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
    _controllerCompleter.complete(controller);
  }

  // Function to check if a point is inside a polygon
  // bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
  //   bool inside = false;
  //   //bool inside2 = false;
  //   //double tolerance = 1;
  //   for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
  //     if (((polygon[i].latitude > point.latitude) !=
  //             (polygon[j].latitude > point.latitude)) &&
  //         (point.longitude <
  //             (polygon[j].longitude - polygon[i].longitude) *
  //                     (point.latitude - polygon[i].latitude) /
  //                     (polygon[j].latitude - polygon[i].latitude) +
  //                 polygon[i].longitude)) {
  //       inside = !inside;
  //     }
  //   }

  //   print("Is In 1: $inside");

  //   return inside;
  // }

  bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
    print("Point is****" + point.toString());
    print("Polygon is****" + polygon.toString());
    bool isInside = false;
    for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
      if ((polygon[i].longitude > point.longitude) !=
              (polygon[j].longitude > point.longitude) &&
          (point.latitude <
              (polygon[j].latitude - polygon[i].latitude) *
                      (point.longitude - polygon[i].longitude) /
                      (polygon[j].longitude - polygon[i].longitude) +
                  polygon[i].latitude)) {
        isInside = !isInside;
      }
    }
    return isInside;
  }

  bool _isPointInsidePolygonBlock(LatLng point) {
    bool inside2 = false;
    for (int i = 0, j = pointsB.length - 1; i < pointsB.length; j = i++) {
      if (((pointsB[i].latitude > point.latitude) !=
              (pointsB[j].latitude > point.latitude)) &&
          (point.longitude <
              (pointsB[j].longitude - pointsB[i].longitude) *
                      (point.latitude - pointsB[i].latitude) /
                      (pointsB[j].latitude - pointsB[i].latitude) +
                  pointsB[i].longitude)) {
        inside2 = !inside2;
      }
    }

    return inside2;
  }

  math.Point<double> convertLatLngToPoint(LatLng latLng, double zoom) {
    final double scale = double.parse(math.pow(2, zoom).toString());
    final double worldCoordinateX = latLng.longitude / 360 * scale;
    final double worldCoordinateY = 0.5 -
        math.log(
                math.tan(math.pi / 4 + latLng.latitude * (math.pi / 180) / 2)) /
            (2 * math.pi) *
            scale;
    return math.Point(worldCoordinateX, worldCoordinateY);
  }

  bool _validPolyline(LatLng location) {
    bool hasCrossed = false;
    for (int i = 1; i < polygonLatLngs.length; i++) {
      //if (_polyline.isNotEmpty) LatLng start = _polyline[i].points.last;
      Point start = convert(polygonLatLngs[i - 1], 1);
      Point end = convert(polygonLatLngs[i], 1);
      for (int j = 0; j < _listBlocksPolygon.length; j++) {
        for (int i = 0; i < _listBlocksPolygon[j].length; i++) {
          Point point = convert(_listBlocksPolygon[j][i], 1);
          if (PolyUtils.containsLocationPoly(point, [start, end])) {
            hasCrossed = true;
          }
        }
      }
    }
    print('Has crossed: $hasCrossed');
    return hasCrossed;
  }

  Point<double> convert(LatLng latLng, double zoom) {
    final size = math.pow(2, zoom + 8);
    final x = ((latLng.longitude + 180) / 360) * size;
    final y = ((1 -
                math.log(math.tan(latLng.latitude * math.pi / 180) +
                        1 / math.cos(latLng.latitude * math.pi / 180)) /
                    math.pi) /
            2) *
        size;
    return Point<double>(x, y);
  }

  void _setMarker(LatLng point) async {
    setState(() {
      _markers.add(
          Marker(markerId: MarkerId(_markerId.toString()), position: point));
      _markerId++;
    });
    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  void _setDrawnMarker(LatLng point) async {
    BitmapDescriptor customIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(20, 20)),
      'assets/images/dot.png',
    );
    Marker marker = new Marker(
        markerId: MarkerId(_markerId.toString()),
        position: point,
        icon: customIcon);

    setState(() {
      _markers.add(marker);
      _markerId++;
    });
    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  void _removeMarker() {
    if (_markers.isNotEmpty && _polyline.isNotEmpty) {
      setState(() {
        _markers.remove(_markers.last);
        _polyline.remove(_polyline.last);
        polygonLatLngs.remove(polygonLatLngs.last);
      });
    }

    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  void _setPolyline(LatLng location) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polyline.length.toString()),
      points: [if (_polyline.isNotEmpty) _polyline.last.points.last, location],
      color: Colors.blue,
      width: 2,
    );

    setState(() {
      _polyline.add(polyline);
    });
  }

  void _deletePoint() {
    if (polygonLatLngs.isEmpty || _markers.isEmpty || _polyline.isEmpty) {
      Fluttertoast.showToast(msg: "You did not select any points");
    } else {
      setState(() {
        _removeMarker();
        //_removePolyline(_point!);
      });
    }
  }

  double calculateArea(List<LatLng> coords) {
    int len = coords.length;
    double sum = 0;
    for (int i = 0; i < len; i++) {
      LatLng p1 = coords[i];
      LatLng p2 = coords[(i + 1) % len];
      sum += (p2.longitude - p1.longitude) * (p2.latitude + p1.latitude);
    }
    return (sum.abs() / 2) *
        111319.9 *
        111319.9; // Multiply by Earth's radius to get the area in square meters
  }

  _restart(String id) async {
    await EstateController().deleteBlockById(id);

    setState(() {
      _markers.clear();
      _polyline.clear();
      polygonLatLngs.clear();
      area = 0.0;
    });
    Get.back();
  }

  _saveBlocks() async {
    List<dynamic> divBlocks = [];
    setState(() {
      area = calculateArea(polygonLatLngs);
    });
    for (var element in polygonLatLngs) {
      DivisionPolygonResultElement divPoly = DivisionPolygonResultElement();
      print(element.latitude);
      divPoly.mainDivisionId = int.parse(mainDivId);
      divPoly.latitude = element.latitude.toString();
      divPoly.longi = element.longitude.toString();
      divPoly.newDevBlockId = blockId.toString();
      divPoly.blockArea = area.toString();
      divPoly.syncId = 'p';
      divPoly.polygonId = mainDivId;
      divPoly.isDivision = 0;

      divBlocks.add(divPoly);
    }

    print("The Block is: $divBlocks");

    int? i = await EstateController().saveAll(divBlocks, divisionPolygonTable);

    if (i != null) {
      BlocksModelResultElement blocksModel = BlocksModelResultElement();
      blocksModel.isPlotted = "Yes";
      int j = await EstateController()
          .updateBlockLocal(blocksTable, "is_plotted", "Yes", 'Id', blockId);
      if (j >= 0) {
        print("Success here AT BLOCKS");
      }

      // showCustomSnackBar(
      //     "Block saved successfully", context, ContentType.success,
      //     title: "Success");
      Get.back();
    }
  }

  bool willPolylineCrossPolygon1(
      LatLng point1, List<LatLng> polylinePoints, List<LatLng> polygonPoints) {
    polylinePoints.add(point1);
    for (int i = 0; i < polylinePoints.length - 1; i++) {
      LatLng point1 = polylinePoints[i];
      LatLng point2 = polylinePoints[i + 1];
      for (int j = 0; j < polygonPoints.length - 1; j++) {
        LatLng vertex1 = polygonPoints[j];
        LatLng vertex2 = polygonPoints[j + 1];
        if (doLineSegmentsIntersect(point1, point2, vertex1, vertex2)) {
          return true;
        }
      }
    }

    return false;
  }

  bool willPolylineCrossPolygon(
      LatLng point1, LatLng point2, List<LatLng> polygonPoints) {
    for (int j = 0; j < polygonPoints.length - 1; j++) {
      LatLng vertex1 = polygonPoints[j];
      LatLng vertex2 = polygonPoints[j + 1];
      if (doLineSegmentsIntersect(point1, point2, vertex1, vertex2)) {
        return true;
      }
    }

    return false;
  }

  bool doLineSegmentsIntersect(LatLng p1, LatLng q1, LatLng p2, LatLng q2) {
    double dx1 = q1.latitude - p1.latitude;
    double dy1 = q1.longitude - p1.longitude;
    double dx2 = q2.latitude - p2.latitude;
    double dy2 = q2.longitude - p2.longitude;
    double delta = dx2 * dy1 - dy2 * dx1;
    if (delta == 0) return false;

    double ua = ((p2.longitude - p1.longitude) * dy1 -
            (p2.latitude - p1.latitude) * dx1) /
        delta;
    double ub = ((p2.longitude - p1.longitude) * dy2 -
            (p2.latitude - p1.latitude) * dx2) /
        delta;
    return (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1);
  }

  bool isPolylineCrossingPolygon(
      LatLng point1, LatLng point2, List<List<Polygon>> polygonList) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polyline.length.toString()),
      points: [point1, point2],
      color: Colors.blue,
      width: 2,
    );

    for (var element in polygonList) {
      for (var element1 in element) {
        Polygon polygon = element1;
        for (int i = 0; i < polygon.points.length - 1; i++) {
          LatLng p1 = polygon.points[i];
          LatLng p2 = polygon.points[i + 1];

          for (int j = 0; j < polyline.points.length - 1; j++) {
            LatLng q1 = polyline.points[j];
            LatLng q2 = polyline.points[j + 1];

            if (doSegmentsIntersect(p1, p2, q1, q2)) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  bool doSegmentsIntersect(LatLng p1, LatLng p2, LatLng q1, LatLng q2) {
    double crossProduct(LatLng a, LatLng b, LatLng c) {
      return (b.latitude - a.latitude) * (c.longitude - a.longitude) -
          (b.longitude - a.longitude) * (c.latitude - a.latitude);
    }

    double direction(LatLng a, LatLng b, LatLng c) {
      return crossProduct(a, b, c);
    }

    bool onSegment(LatLng a, LatLng b, LatLng c) {
      return (c.latitude >= a.latitude && c.latitude <= b.latitude) ||
          (c.latitude <= a.latitude && c.latitude >= b.latitude);
    }

    double d1 = direction(q1, q2, p1);
    double d2 = direction(q1, q2, p2);
    double d3 = direction(p1, p2, q1);
    double d4 = direction(p1, p2, q2);

    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
        ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
      return true;
    } else if (d1 == 0 && onSegment(q1, q2, p1)) {
      return true;
    } else if (d2 == 0 && onSegment(q1, q2, p2)) {
      return true;
    } else if (d3 == 0 && onSegment(p1, p2, q1)) {
      return true;
    } else if (d4 == 0 && onSegment(p1, p2, q2)) {
      return true;
    }

    return false;
  }

// CHECK IF POLYGON INTERSECTS ITSELF
  bool doesPolylineIntersectPolygon(
      LatLng newPoint, List<LatLng> polygonPoints) {
    LatLng lastPointOfPolygon = polygonPoints.last;

    for (int i = 0; i < polygonPoints.length - 2; i++) {
      LatLng p1 = polygonPoints[i];
      LatLng p2 = polygonPoints[i + 1];

      if (doSegmentsIntersect1(p1, p2, lastPointOfPolygon, newPoint)) {
        return true;
      }
    }

    return false;
  }

  bool doSegmentsIntersect1(LatLng p1, LatLng p2, LatLng q1, LatLng q2) {
    double crossProduct(LatLng a, LatLng b, LatLng c) {
      return (b.latitude - a.latitude) * (c.longitude - a.longitude) -
          (b.longitude - a.longitude) * (c.latitude - a.latitude);
    }

    double direction(LatLng a, LatLng b, LatLng c) {
      return crossProduct(a, b, c);
    }

    bool onSegment(LatLng a, LatLng b, LatLng c) {
      return (c.latitude >= a.latitude && c.latitude <= b.latitude) ||
          (c.latitude <= a.latitude && c.latitude >= b.latitude);
    }

    double d1 = direction(q1, q2, p1);
    double d2 = direction(q1, q2, p2);
    double d3 = direction(p1, p2, q1);
    double d4 = direction(p1, p2, q2);

    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
        ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
      return true;
    } else if (d1 == 0 && onSegment(q1, q2, p1)) {
      return true;
    } else if (d2 == 0 && onSegment(q1, q2, p2)) {
      return true;
    } else if (d3 == 0 && onSegment(p1, p2, q1)) {
      return true;
    } else if (d4 == 0 && onSegment(p1, p2, q2)) {
      return true;
    }

    return false;
  }

  @override
  void initState() {
    super.initState();

    _getDivision();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        mainDivId = data["Id"].toString();
        //selectedDivisionName = data["division_name"].toString();

        blockId = data2["Id"].toString();
        //selectedBlockName = data2["name"].toString();
      });

      await _getDivisionLatLngFromDatabase(mainDivId);
    } else {
      Get.back();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _getDivisionLatLngFromDatabase(mainDivId);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _controller;
    _controllerCompleter;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: isLoading
          ? SizedBox(
              height: Dimensions.screenHeight,
              width: Dimensions.screenWidth,
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [CustomLoader()],
              ),
            )
          : Stack(
              children: [
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: _cameraPosition,
                    zoom: 13.0,
                  ),
                  mapType: MapType.satellite,
                  markers: _markers,
                  polylines: _polyline,
                  polygons: _listBlocksPolygons.expand((i) => i).toSet(),
                  // Set<Polygon>.of([_buildPolygon1(), _buildPolygon2()]),
                  onTap: (point) {
                    onMap
                        ? blocksEmpty
                            ? _isPointInsidePolygon(point, points)
                                ? !_isPointInsidePolygonBlock(point)
                                    ? polygonLatLngs.isEmpty
                                        ? setState(() {
                                            print("The Point is: $point");
                                            _point = point;
                                            polygonLatLngs.add(point);
                                            _setMarker(point);
                                            _setPolyline(point);
                                          })
                                        //: willPolylineCrossPolygon1(point, polygonLatLngs, pointsB)
                                        : isPolylineCrossingPolygon(
                                                point,
                                                polygonLatLngs.last,
                                                _listBlocksPolygons)
                                            ? Fluttertoast.showToast(
                                                msg:
                                                    "The Point is Crossing a Polygon")
                                            : polygonLatLngs.length >= 3
                                                ? doesPolylineIntersectPolygon(
                                                        point, polygonLatLngs)
                                                    ? Fluttertoast.showToast(
                                                        msg:
                                                            "The Polygon is Crossing Itself")
                                                    : setState(() {
                                                        print(
                                                            "The Point is: $point");
                                                        _point = point;
                                                        polygonLatLngs
                                                            .add(point);
                                                        _setMarker(point);
                                                        _setPolyline(point);
                                                      })
                                                : setState(() {
                                                    print(
                                                        "The Point is: $point");
                                                    _point = point;
                                                    polygonLatLngs.add(point);
                                                    _setMarker(point);
                                                    _setPolyline(point);
                                                  })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "The Point is Inside Another Polygon")
                                : Fluttertoast.showToast(
                                    msg: "The Point is Outside Mapping Area")
                            : showDialog(
                                barrierDismissible: false,
                                context: context,
                                builder: (BuildContext context) {
                                  return Dialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    elevation: 0,
                                    backgroundColor: Colors.transparent,
                                    child: confirm(context),
                                  );
                                })
                        : Fluttertoast.showToast(
                            msg: "You did not select START MAPPING");
                  },
                ),
                Positioned(
                  top: 50,
                  child: Container(
                    padding: EdgeInsets.all(Dimensions.height15),
                    height: Dimensions.height15 * Dimensions.height15,
                    width: (Dimensions.screenWidth / 2) +
                        Dimensions.width10 * Dimensions.width15,
                    margin: EdgeInsets.only(
                        top: Dimensions.height10, left: Dimensions.width10),
                    // decoration: BoxDecoration(
                    //   color: Colors.grey[700],
                    //   borderRadius: BorderRadius.circular(Dimensions.radius15),
                    // ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                            child: Text(
                          "Tap Mark to get Location Co-ordinates",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: Dimensions.font16 - 2,
                              fontWeight: FontWeight.bold),
                        )),
                        SizedBox(
                          height: Dimensions.height10 / 2,
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                //print("Block Polygon is: " + Blockpoints.toString());
                                setState(() {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Dialog(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          elevation: 0,
                                          backgroundColor: Colors.transparent,
                                          child: mapWith(context),
                                        );
                                      });
                                });
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "START MAPPING",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () async {
                                //dialog to choose mode of mapping
                                onMap
                                    ? showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            elevation: 0,
                                            backgroundColor: Colors.transparent,
                                            child: contentBox(context),
                                          );
                                        })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "RESTART",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  onMap
                                      ? _deletePoint()
                                      : Fluttertoast.showToast(
                                          msg:
                                              "You did not select START MAPPING");
                                });
                              },
                              child: Icon(
                                Icons.delete_outline_sharp,
                                color: AppColors.buttonRed,
                                size: Dimensions.iconSize24 * 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                onMap
                                    ? _saveBlocks()
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width10 * Dimensions.width10,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "COMPLETE",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            Column(
                              children: [
                                BigText(
                                  text: "Total Acreage (m2)",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                ),
                                BigText(
                                  text: area.toString(),
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Confirm Restart",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to restart mapping?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: BigText(
                          text: "Cancel",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _restart(blockId);
                        },
                        child: BigText(
                          text: "Ok",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  confirm(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Block Already Drawn",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to Re-Map or Continue?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          // for (var element in polygonLatLngs) {
                          //   if (element != polygonLatLngs.last) {

                          //   }
                          //   _setMarker(element);
                          // }

                          for (int i = 0; i < polygonLatLngs.length; i++) {
                            if (i >= polygonLatLngs.length - 5) {
                              _setMarker(polygonLatLngs[i]);
                            }
                            // else{
                            //   _setDrawnMarker(polygonLatLngs[i]);
                            // }
                          }

                          //_setMarker(polygonLatLngs.last);

                          setState(() {
                            blocksEmpty = true;
                            onMap = true;
                          });
                          Get.back();
                        },
                        child: BigText(
                          text: "Continue",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.back();
                          setState(() {
                            blocksEmpty = true;
                            onMap = true;
                          });
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  elevation: 0,
                                  backgroundColor: Colors.transparent,
                                  child: contentBox(context),
                                );
                              });
                        },
                        child: BigText(
                          text: "Restart",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      )
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  mapWith(context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(
            left: Dimensions.width10,
            top: Dimensions.height20,
            right: Dimensions.width10,
            bottom: Dimensions.height20,
          ),
          margin: EdgeInsets.only(top: Dimensions.height20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(Dimensions.radius30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Type Select",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              SmallText(
                text: "Would you like to ?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {
                        onMap = true;
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Pick Points",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: Dimensions.width30 * 2,
                  ),
                  GestureDetector(
                      onTap: () {
                        Get.back();
                        Get.to(() => const LivePick());
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Walk",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: Dimensions.height20,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 3,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Cancel",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))),
            ],
          ),
        ),
        Positioned(
          left: Dimensions.width10 / 2,
          right: Dimensions.width10 / 2,
          child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: Dimensions.radius20,
              child: FaIcon(
                FontAwesomeIcons.circleQuestion,
                size: Dimensions.height45,
                color: Colors.green,
              )),
        ),
      ],
    );
  }

  Polygon _buildPolygon1() {
    return Polygon(
      polygonId: const PolygonId('Division_Polygon'),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );
  }

  Polygon _buildPolygon2() {
    return Polygon(
      polygonId: const PolygonId('block_Polygon'),
      points: Blockpoints,
      strokeWidth: 2,
      strokeColor: Colors.blue,
      fillColor: Colors.blue.withOpacity(0.3),
    );
  }

  Polygon? _buildPolygon3() {
    Polygon? polygon;
    for (var element in mapPolygonList) {
      polygon = element;
    }
    return polygon;
  }

  _buildAllPolygons() {
    
    int j = 0;
    while (j < _listBlocksPolygon.length) {
      Polygon polygon = Polygon(
        polygonId: const PolygonId('block_Polygon${1}'),
        points: _listBlocksPolygon[j],
        strokeWidth: 2,
        strokeColor: Colors.blue,
        fillColor: Colors.blue.withOpacity(0.3),
      );

      setState(() {
        mapPolygonList.add(polygon);
        _listBlocksPolygons.add(mapPolygonList);
      });

      j++;
    }
  }
}



// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

// import '../../../resources/base/custom_loader.dart';
// import '../../../resources/utils/dimensions.dart';

// List<LatLng> _allLocations = [];

// class DivisionsScreen extends StatefulWidget {
//   const DivisionsScreen({super.key});

//   @override
//   State<DivisionsScreen> createState() => _DivisionsScreenState();
// }

// class _DivisionsScreenState extends State<DivisionsScreen> {
//   Geolocator _geolocator = Geolocator();
//   StreamSubscription<Position>? _positionStream;
//   List<LatLng> _locations = [];
//   GoogleMapController? _controller;
//   final Completer<GoogleMapController> _controllerCompleter = Completer();
//   LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);

//   void _onMapCreated(GoogleMapController controller) {
//     _controller = controller;
//     _controllerCompleter.complete(controller);

//     // _cameraPosition =
//   }

//   // @override
//   // void initState() {
//   //   // TODO: implement initState
//   //   super.initState();
//   //   if (mounted) {
//   //     _controller =
//   //   }
//   // }

//   @override
//   void dispose() {
//     // TODO: implement dispose
//     super.dispose();

//     _controller!.dispose();
//     _positionStream?.cancel();
//   }

//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();

//     _controller;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body:
//           // _controller == null
//           //     ? SizedBox(
//           //         height: Dimensions.screenHeight,
//           //         width: Dimensions.screenWidth,
//           //         child: const Column(
//           //           mainAxisAlignment: MainAxisAlignment.center,
//           //           children: [CustomLoader()],
//           //         ),
//           //       )
//           //     :
//           Stack(
//         children: [
//           GoogleMap(
//             onMapCreated: _onMapCreated,
//             // Map configuration
//             initialCameraPosition: CameraPosition(
//               target: _cameraPosition,
//               zoom: 10,
//             ),
//             mapType: MapType.satellite,
//             // Markers for the locations
//             markers: _locations.map((location) {
//               return Marker(
//                 markerId: MarkerId(location.toString()),
//                 position: location,
//               );
//             }).toSet(),
//           ),
//           Positioned(
//             top: 50,
//             child: ElevatedButton(
//               onPressed: () {
//                 if (_positionStream == null) {
//                   // Start location updates
//                   _positionStream =
//                       Geolocator.getPositionStream().listen((position) {
//                     if (mounted) {
//                       setState(() {
//                         _controller?.animateCamera(
//                           CameraUpdate.newLatLng(
//                               LatLng(position.latitude, position.longitude)),
//                         );
//                         _locations
//                             .add(LatLng(position.latitude, position.longitude));
//                             _allLocations.add(LatLng(position.latitude, position.longitude));
//                       });
//                     }

//                     Fluttertoast.showToast(
//                         msg: "MAPPING${_allLocations.toString().length}");
//                   });
//                 } else {
//                   // Stop location updates
//                   setState(() {
//                     _positionStream?.cancel();
//                     _positionStream = null;
//                   });
//                   debugPrint(_locations.toString());
//                   Fluttertoast.showToast(
//                       msg: "MAPPING${_locations.toString()}");
//                 }
//               },
//               child: Text(_positionStream == null ? 'Start' : 'Stop'),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
