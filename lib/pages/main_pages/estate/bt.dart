
// import 'dart:async';

// import 'package:flutter_blue/flutter_blue.dart';

// // ...

// // Define the UUIDs for the SkyPro GPS service and characteristic
// const String skyProServiceUuid = '49535343-fe7d-4ae5-8fa9-9fafd205e455';
// const String skyProCharacteristicUuid = '49535343-1e4d-4bd9-ba61-23c647249616';

// // ...

// FlutterBlue flutterBlue = FlutterBlue.instance;

// // Start scanning for nearby Bluetooth devices
// StreamSubscription scanSubscription = flutterBlue.scan().listen((scanResult) {
//   // Check if the device is a SkyPro GPS device
//   if (scanResult.device.name == 'SkyPro') {
//     // Connect to the SkyPro GPS device
//     scanSubscription.cancel(); // Stop scanning for devices
//     scanResult.device.connect().then((device) {
//       // Discover the SkyPro GPS service and characteristic
//       device.discoverServices().then((services) {
//         BluetoothCharacteristic characteristic = services
//             .where((service) => service.uuid.toString() == skyProServiceUuid)
//             .map((service) => service.characteristics)
//             .expand((characteristics) => characteristics)
//             .firstWhere((characteristic) => characteristic.uuid.toString() == skyProCharacteristicUuid);
//         // Enable notifications for the SkyPro GPS characteristic
//         characteristic.setNotifyValue(true);
//         // Listen for updates to the SkyPro GPS characteristic value
//         characteristic.value.listen((value) {
//           // Parse the coordinate data from the characteristic value
//           String coordinateString = String.fromCharCodes(value);
//           List<String> coordinateValues = coordinateString.split(',');
//           double latitude = double.parse(coordinateValues[0]);
//           double longitude = double.parse(coordinateValues[1]);
//           // Do something with the coordinate data
//           print('Latitude: $latitude, Longitude: $longitude');
//         });
//       });
//     });
//   }
// });
