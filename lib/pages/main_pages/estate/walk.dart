import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart' as loc;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';

import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/dimensions.dart';

class IntervalWalking extends StatefulWidget {
  const IntervalWalking({super.key});

  @override
  _IntervalWalkingState createState() => _IntervalWalkingState();
}

class _IntervalWalkingState extends State<IntervalWalking> {
  double? latPos;
  double? longPos;
  bool isLoading = false;

  loc.LocationData currentPosition = loc.LocationData.fromMap({
    "latitude": 37.7749, // default latitude
    "longitude": -122.4194, // default longitude
  });
  GoogleMapController? mapController;
  StreamSubscription<loc.LocationData>? locationSubscription;
  Set<Marker> markers = {};
  final Set<Polyline> _polyline = <Polyline>{};

  @override
  void initState() {
    super.initState();
    _getCurrentPosition();
  }

  @override
  void dispose() {
    locationSubscription!.cancel();
    super.dispose();
  }

  Future<void> _getCurrentPosition() async {
    setState(() => isLoading = true);
    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((Position position) {
      setState(() {
        latPos = position.latitude;
        longPos = position.longitude;

        loc.LocationData currentPos = loc.LocationData.fromMap({
          "latitude": latPos, // default latitude
          "longitude": longPos, // default longitude
        });

        currentPosition = currentPos;
      });
      startTracking();
    }).catchError((e) {
      debugPrint(e);
    });
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  void _setPolyline(LatLng location) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polyline.length.toString()),
      points: [if (_polyline.isNotEmpty) _polyline.last.points.last, location],
      color: Colors.blue,
      width: 2,
    );

    setState(() {
      _polyline.add(polyline);
    });
  }

  void startTracking() async {
    loc.Location location = loc.Location();
    currentPosition = await location.getLocation();
    locationSubscription = location.onLocationChanged.listen((loc.LocationData locationData) {
      setState(() {
        currentPosition = locationData;
        markers.clear();
        markers.add(
          Marker(
            markerId: const MarkerId("currentLocation"),
            position: LatLng(double.parse(currentPosition.latitude.toString()),
                double.parse(currentPosition.longitude.toString())),
            infoWindow: const InfoWindow(title: "Current Location"),
          ),
        );

        
        _polyline.add(
          Polyline(
            polylineId: PolylineId(_polyline.length.toString()),
            points: [LatLng(double.parse(currentPosition.latitude.toString()),
                double.parse(currentPosition.longitude.toString()))],
            color: Colors.blue,
            width: 2,
          )
        );
        
      });
    });
    setState(() => isLoading = false);
  }

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: isLoading
          ? SizedBox(
              height: Dimensions.screenHeight,
              width: Dimensions.screenWidth,
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [CustomLoader()],
              ),
            )
          : GoogleMap(
              initialCameraPosition: CameraPosition(
                target: LatLng(
                    double.parse(currentPosition.latitude.toString()),
                    double.parse(currentPosition.longitude.toString())),
                zoom: 15.0,
              ),
              onMapCreated: onMapCreated,
              markers: markers,
              polylines: _polyline,
            ),
    );
  }
}
// class WalkingScreen extends StatefulWidget {
//   const WalkingScreen({super.key});

//   @override
//   State<WalkingScreen> createState() => _WalkingScreenState();
// }

// class _WalkingScreenState extends State<WalkingScreen> {
//   double latPos = 37.42796133580664;
//   double longPos = -122.085749655962;
//   loc.LocationData currentPosition = loc.LocationData.fromMap({
//     "latitude": 37.7749, // default latitude
//     "longitude": -122.4194, // default longitude
//   });

//   loc.Location location = loc.Location();
//   // LocationData? currentPosition;

//   // getCurrentPosition() async {
//   //   LocationData currentPos = await location.getLocation();

//   //   setState(() {
//   //     currentPosition = currentPos;
//   //   });
//   // }

//   Future<void> _getCurrentPosition() async {
//     loc.LocationData currentPos = await location.getLocation();

//     setState(() {
//       currentPosition = currentPos;
//     });

//     // final hasPermission = await _handleLocationPermission();

//     // if (!hasPermission) return;
//     // await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
//     //     .then((Position position) {
//     //   setState(() {
//     //     currentPosition = position.toString() as loc.LocationData;
//     //   });
//     // }).catchError((e) {
//     //   debugPrint(e);
//     // });
//   }

//   _startMapping() {
//     StreamSubscription<loc.LocationData> locationSubscription;
//     locationSubscription =
//         location.onLocationChanged.listen((loc.LocationData locationData) {
//       setState(() {
//         currentPosition = locationData;
//       });
//     });

//     Marker currentLocationMarker = Marker(
//       markerId: MarkerId("currentLocation"),
//       position: LatLng(double.parse(currentPosition.latitude.toString()),
//           double.parse(currentPosition.longitude.toString())),
//       infoWindow: InfoWindow(title: "Current Location"),
//     );

//     Set<Marker> markers = Set();
//     markers.add(currentLocationMarker);
//   }

//   Future<bool> _handleLocationPermission() async {
//     bool serviceEnabled;
//     LocationPermission permission;

//     serviceEnabled = await Geolocator.isLocationServiceEnabled();
//     if (!serviceEnabled) {
//       ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
//           content: Text(
//               'Location services are disabled. Please enable the services')));
//       return false;
//     }
//     permission = await Geolocator.checkPermission();
//     if (permission == LocationPermission.denied) {
//       permission = await Geolocator.requestPermission();
//       if (permission == LocationPermission.denied) {
//         ScaffoldMessenger.of(context).showSnackBar(
//             const SnackBar(content: Text('Location permissions are denied')));
//         return false;
//       }
//     }
//     if (permission == LocationPermission.deniedForever) {
//       ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
//           content: Text(
//               'Location permissions are permanently denied, we cannot request permissions.')));
//       return false;
//     }
//     return true;
//   }

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _getCurrentPosition();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(Dimensions.height30 * 2),
//         child: AppBar(
//           backgroundColor: Colors.green,
//           actions: [
//             Container(
//               width: Dimensions.screenWidth,
//               height: Dimensions.height30 * 2,
//               padding: EdgeInsets.only(
//                 left: Dimensions.width15,
//                 right: Dimensions.width15,
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.green,
//                 boxShadow: [
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(5, 5),
//                       color: AppColors.gradientOne.withOpacity(0.1)),
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(-5, -5),
//                       color: AppColors.gradientOne.withOpacity(0.1))
//                 ],
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   IconButton(
//                     icon: Icon(
//                       Icons.arrow_back_ios,
//                       color: Colors.white,
//                       size: Dimensions.iconSize24,
//                     ),
//                     onPressed: () {
//                       Get.back();
//                     },
//                   ),
//                   BigText(
//                     text: "Map Farm",
//                     color: Colors.black,
//                     size: Dimensions.font16,
//                   ),
//                   SizedBox()
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//       body: Stack(
//         children: [
//           GoogleMap(
//             initialCameraPosition: CameraPosition(
//               target: LatLng(double.parse(currentPosition.latitude.toString()),
//                   double.parse(currentPosition.longitude.toString())),
//               zoom: 15.0,
//             ),
//             onMapCreated: (GoogleMapController controller) {},
//           ),
//           GestureDetector(
//             onTap: () {
//               _startMapping();
//             },
//             child: Container(
//               height: Dimensions.height20 + Dimensions.height30,
//               padding: EdgeInsets.only(
//                 left: Dimensions.width10,
//                 right: Dimensions.width10,
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.green,
//                 borderRadius: BorderRadius.circular(Dimensions.radius15 / 2),
//               ),
//               child: Center(
//                   child: SmallText(
//                 text: "START MAPPING",
//                 color: Colors.black,
//                 size: Dimensions.font12,
//               )),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
