// import 'dart:async';

// import 'package:flutter_blue/flutter_blue.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../resources/utils/colors.dart';
// import '../../../resources/utils/dimensions.dart';
// import '../../../resources/widgets/big_text.dart';

// class BlueToothDeviceScreen extends StatefulWidget {
//   const BlueToothDeviceScreen({super.key});

//   @override
//   State<BlueToothDeviceScreen> createState() => _BlueToothDeviceScreenState();
// }

// class _BlueToothDeviceScreenState extends State<BlueToothDeviceScreen> {
//   FlutterBlue flutterBlue = FlutterBlue.instance;
//   List<ScanResult> scanResults = [];
//   BluetoothCharacteristic? _characteristic;
//   String _gpsData = '';
//   bool deviceChosen = false;

//   @override
//   void initState() {
//     super.initState();
//     startScan();
//   }

//   @override
//   void dispose() {
//     stopScan();
//     super.dispose();
//   }

//   void startScan() {
//     scanResults.clear();

//     flutterBlue.startScan(timeout: Duration(seconds: 5));

//     flutterBlue.scanResults.listen((results) {
//       setState(() {
//         scanResults = results;
//       });
//     });
//   }

//   void stopScan() {
//     flutterBlue.stopScan();
//   }

//   void connectToDevice(BluetoothDevice device) async {
//     await device.connect(autoConnect: false);
//     print(device.name + "Is Connected");

//     List<BluetoothService> services = await device.discoverServices();

//     for (BluetoothService service in services) {
//       print("Device is: ${service.uuid.toString()}");
//       if (service.uuid.toString() == '00002a29-0000-1000-8000-00805f9b34fb') {
//         //_characteristic = characteristic;
//         //_subscribeToCharacteristic();
//         setState(() {
//           deviceChosen = true;
//         });
//       }
//       // List<BluetoothCharacteristic> characteristics = service.characteristics;

//       // for (BluetoothCharacteristic characteristic in characteristics) {
//       //   // Do something with the characteristic, like read or write its value
//       //   //print("Device Connected" + characteristic.toString());
//       //   print("Value is: " + characteristic.value.toList().toString());
//       //   if (characteristic.uuid.toString() ==
//       //       '00002a29-0000-1000-8000-00805f9b34fb') {
//       //     _characteristic = characteristic;
//       //     _subscribeToCharacteristic();
//       //     setState(() {
//       //       deviceChosen = true;
//       //     });
//       //   }
//       // }
//     }
//   }

//   void _subscribeToCharacteristic() async {
//     await _characteristic!.setNotifyValue(true);
//     _characteristic!.value.listen((data) {
//       setState(() {
//         _gpsData = String.fromCharCodes(data);
//       });
//       _parseGPSData(_gpsData);
//     });
//   }

//   void _parseGPSData(String gpsData) {
//     // Parse GPS data here
//     // Example: $GPRMC,121154.000,A,3723.4659,N,12202.2135,W,0.03,197.23,130913,,,A*77
//     // Latitude: 37° 23.4659' N
//     // Longitude: 122° 2.2135' W
//     // ...
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(Dimensions.height30 * 2),
//         child: AppBar(
//           backgroundColor: Colors.green,
//           actions: [
//             Container(
//               width: Dimensions.screenWidth,
//               height: Dimensions.height30 * 2,
//               padding: EdgeInsets.only(
//                 left: Dimensions.width15,
//                 right: Dimensions.width15,
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.green,
//                 boxShadow: [
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(5, 5),
//                       color: AppColors.gradientOne.withOpacity(0.1)),
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(-5, -5),
//                       color: AppColors.gradientOne.withOpacity(0.1))
//                 ],
//               ),
//               child: Center(
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     IconButton(
//                       icon: Icon(
//                         Icons.arrow_back_ios,
//                         color: Colors.white,
//                         size: Dimensions.iconSize24,
//                       ),
//                       onPressed: () {
//                         stopScan();
//                         scanResults.clear();
//                         Get.back();
//                       },
//                     ),
//                     BigText(
//                       text: "BlueTooth",
//                       color: AppColors.textWhite,
//                       size: Dimensions.font28,
//                     ),
//                     SizedBox()
//                   ],
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             ElevatedButton(
//               onPressed: startScan,
//               child: Text('Scan for Devices'),
//             ),
//             deviceChosen
//                 ? Center(
//                     child: Text('Here' + _gpsData),
//                   )
//                 : Expanded(
//                     child: ListView.builder(
//                       itemCount: scanResults.length,
//                       itemBuilder: (BuildContext context, int index) {
//                         BluetoothDevice device = scanResults[index].device;
//                         print("Device Name" + device.name);
//                         return ListTile(
//                           title: Text(
//                             device.name.toString(),
//                             style: TextStyle(color: Colors.black),
//                           ),
//                           subtitle: Text(device.id.toString()),
//                           onTap: () {
//                             stopScan();
//                             connectToDevice(device);
//                           },
//                         );
//                       },
//                     ),
//                   ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// // import 'package:flutter/material.dart';
// // import 'package:flutter_blue/flutter_blue.dart';

// // void main() {
// //   runApp(MyApp());
// // }

// // class MyApp extends StatefulWidget {
// //   @override
// //   _MyAppState createState() => _MyAppState();
// // }

// // class _MyAppState extends State<MyApp> {
// //   FlutterBlue flutterBlue = FlutterBlue.instance;
// //   BluetoothDevice? device;
// //   BluetoothCharacteristic? characteristic;

// //   @override
// //   void initState() {
// //     super.initState();

// //     // Start scanning for devices as soon as the app starts
// //     startScan();
// //   }

// //   void startScan() {
// //     flutterBlue.startScan(timeout: Duration(seconds: 5));

// //     // Listen for scan results and look for the Sky Pro GPS device
// //     flutterBlue.scanResults.listen((results) {
// //       for (ScanResult result in results) {
// //         if (result.device.name == 'Sky Pro GPS') {
// //           // We found the device we're looking for, stop scanning and connect to it
// //           stopScan();
// //           connectToDevice(result.device);
// //           break;
// //         }
// //       }
// //     });
// //   }

// //   void stopScan() {
// //     flutterBlue.stopScan();
// //   }

// //   void connectToDevice(BluetoothDevice device) async {
// //     // Connect to the device and discover its services and characteristics
// //     await device.connect();
// //     List<BluetoothService> services = await device.discoverServices();

// //     // Look for the characteristic we're interested in (e.g. the GPS data)
// //     for (BluetoothService service in services) {
// //       List<BluetoothCharacteristic> characteristics = service.characteristics;

// //       for (BluetoothCharacteristic c in characteristics) {
// //         if (c.uuid.toString() == '0000fff6-0000-1000-8000-00805f9b34fb') {
// //           characteristic = c;
// //           break;
// //         }
// //       }
// //     }

// //     // Subscribe to notifications from the characteristic
// //     await characteristic!.setNotifyValue(true);
// //     characteristic!.value.listen((value) {
// //       // Do something with the data received from the GPS
// //       print(value);
// //     });
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return MaterialApp(
// //       home: Scaffold(
// //         appBar: AppBar(
// //           title: Text('Bluetooth Sky Pro GPS'),
// //         ),
// //         body: Center(
// //           child: Text('Waiting for GPS data...'),
// //         ),
// //       ),
// //     );
// //   }
// // }
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../models/estate/blocks_model.dart';
import '../../../models/estate/divisionpolygon_model.dart';
import '../../../resources/base/snackbar.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class LivePick extends StatefulWidget {
  const LivePick({super.key});

  @override
  State<LivePick> createState() => _LivePickState();
}

class _LivePickState extends State<LivePick> {
  var mainDivId = "";
  var blockId = "";
  bool onMap = false;
  double area = 0.0;
  // GoogleMapController? newmapController;
  // GoogleMapController? _newmapController;
  // Position? currentPosition;
  // List<LatLng> polylineCoordinates = [];
  // Set<Marker> markers = {};
  // Set<Polyline> polylines = {};
  // late Timer timer;
  // final Completer<GoogleMapController> mapController = Completer();
  // final Completer<GoogleMapController> _mapController = Completer();
  // Position? _currentPosition;
  // Stream<Position>? _positionStream;
  // bool isMapping = false;
  // double _totalDistance = 0;
  // LatLng? _lastPosition;
  // static const int DISTANCE_INTERVAL = 10; // in meters

  // final LocationSettings locationSettings = LocationSettings(
  //   accuracy: LocationAccuracy.high,
  //   distanceFilter: 100,
  // );

  // @override
  // void initState() {
  //   super.initState();

  //   _positionStream =
  //       Geolocator.getPositionStream(locationSettings: locationSettings);
  //   _positionStream!.listen((Position position) {
  //     _currentPosition = position;
  //     _lastPosition =
  //       LatLng(_currentPosition!.latitude, _currentPosition!.longitude);
  //   });

  //   _getCurrentLocation();
  // }

  // void _startTimer() {
  //   const duration = const Duration(seconds: 5);
  //   timer = Timer.periodic(duration, (Timer _) {
  //     return;
  //   });
  // }

  // void _getCurrentLocation() async {
  //   Position? currpos = await Geolocator.getCurrentPosition(
  //     desiredAccuracy: LocationAccuracy.high,
  //   );
  //   setState(() {
  //     currentPosition = currpos;
  //   });
  //   if (_lastPosition != null) {
  //     double distance = _calculateDistance(
  //         _lastPosition!.latitude,
  //         _lastPosition!.longitude,
  //         _currentPosition!.latitude,
  //         _currentPosition!.longitude);
  //     _totalDistance += distance;

  //     if (_totalDistance >= DISTANCE_INTERVAL) {
  //       setState(() {
  //         polylineCoordinates.add(
  //             LatLng(currentPosition!.latitude, currentPosition!.longitude));
  //         markers.add(
  //           Marker(
  //             markerId: MarkerId('new_position_${polylineCoordinates.length}'),
  //             position:
  //                 LatLng(currentPosition!.latitude, currentPosition!.longitude),
  //           ),
  //         );
  //         polylines.add(
  //           Polyline(
  //             polylineId: PolylineId('user_path'),
  //             points: polylineCoordinates,
  //             color: Colors.blue,
  //             width: 5,
  //           ),
  //         );
  //       });
  //       _totalDistance = 0;
  //     }
  //   }

  //   _lastPosition = LatLng(_currentPosition!.latitude, _currentPosition!.longitude);

  //   GoogleMapController controller = await mapController.future;
  //   controller.animateCamera(
  //     CameraUpdate.newCameraPosition(
  //       CameraPosition(
  //         target:
  //             LatLng(_currentPosition!.latitude, _currentPosition!.longitude),
  //         zoom: 15,
  //       ),
  //     ),
  //   );

  //   // newmapController!.animateCamera(
  //   //   CameraUpdate.newCameraPosition(
  //   //     CameraPosition(
  //   //       target: LatLng(currentPosition!.latitude, currentPosition!.longitude),
  //   //       zoom: 15,
  //   //     ),
  //   //   ),
  //   // );
  // }

  // void _onMapCreated(GoogleMapController controller) {
  //   //mapController = controller;

  //   mapController.complete(controller);
  //   newmapController = controller;
  // }

  // double _calculateDistance(lat1, lon1, lat2, lon2) {
  //   const R = 6371000; // radius of the earth in meters
  //   var latDistance = (lat2 - lat1) * pi / 180;
  //   var lonDistance = (lon2 - lon1) * pi / 180;
  //   var a = sin(latDistance / 2) * sin(latDistance / 2) +
  //       cos(lat1 * pi / 180) *
  //           cos(lat2 * pi / 180) *
  //           sin(lonDistance / 2) *
  //           sin(lonDistance / 2);
  //   var c = 2 * atan2(sqrt(a), sqrt(1 - a));
  //   var distance = R * c;
  //   return distance;
  // }

  // @override
  // void dispose() {
  //   timer.cancel();
  //   super.dispose();
  // }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(),
  // body: currentPosition == null
  //     ? Center(
  //         child: CircularProgressIndicator(),
  //       )
  //         : Stack(
  //             children: [
  //               GoogleMap(
  //                 onMapCreated: isMapping
  //                     ? _onMapCreated
  //                     : (controller) => _newmapController = controller,
  //                 initialCameraPosition: CameraPosition(
  //                   target: LatLng(
  //                       currentPosition!.latitude, currentPosition!.longitude),
  //                   zoom: 15,
  //                 ),
  //                 myLocationEnabled: true,
  //                 myLocationButtonEnabled: true,
  //                 compassEnabled: true,
  //                 markers: markers,
  //                 polylines: polylines,
  //                 onCameraMove: (CameraPosition newPosition) {},
  //                 onCameraIdle: () {},
  //               ),
  //               Positioned(
  //                 top: 0,
  //                 //right: 16,
  //                 child: Container(
  //                   height: Dimensions.height30 * 3,
  //                   width: Dimensions.screenWidth,
  //                   padding: EdgeInsets.all(10),
  //                   decoration: BoxDecoration(
  //                     color: Colors.grey,
  //                   ),
  //                   child: Row(
  //                     children: [
  //                       Container(
  //                         height: Dimensions.height20 * 2,
  //                         width: Dimensions.width30 * 5,
  //                         decoration: BoxDecoration(
  //                             color: AppColors.mainColor,
  //                             borderRadius: BorderRadius.circular(
  //                                 Dimensions.radius15 / 3)),
  //                         child: Center(
  //                           child: BigText(text: "Save"),
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //                 // child: FloatingActionButton(
  //                 //   onPressed: () => _getCurrentLocation(),
  //                 //   child: Icon(Icons.add),
  //                 // ),
  //               ),
  //             ],
  //           ),
  //   );
  // }
  late GoogleMapController _mapController;
  StreamSubscription<Position>? _positionStreamSubscription;
  final List<LatLng> _points = [];

  bool isLoading = false;

  final List<Marker> _markers = [];
  final List<Polyline> _polylines = [];
  LatLng? _lastPosition;
  late LocationSettings locationSettings;
  Position? currentPosition;

  List<dynamic> _blockslatlong = [];
  List<LatLng> Blockpoints = [];
  List<LatLng> points = [];
  List<LatLng> pointsB = [];
  List<Polygon> mapPolygonList = [];
  final List<List<LatLng>> _listBlocksPolygon = [];
  final List<List<Polygon>> _listBlocksPolygons = [];
  bool blocksEmpty = true;
  List<LatLng> polygonLatLngs = <LatLng>[];
  Timer? timer;
  LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);

  List<LatLng> pathPoints = [
    LatLng(6.326004478650377, -10.383305847644806),
    LatLng(6.322611472340659, -10.382015705108643),
    LatLng(6.323606849805648, -10.379059575498104),
    LatLng(6.326972857626811, -10.380124412477016),
    LatLng(6.326003145711793, -10.383275672793388)
  ];

  List<LatLng> finalPathPoints = [];

  List<dynamic> _divisionsPolygon = [];

  //[  , {longi:, latitude: }, {longi: , latitude: }, {longi: -10.3531777807733, latitude: 6.3493276011563}, {longi: -10.3532391621885, latitude: 6.349559065961559}, {longi: -10.3532097025991, latitude: 6.34965831088691}, {longi: -10.3530860090892, latitude: 6.34977663279433}, {longi: -10.353021341404, latitude: 6.34982664803866}, {longi: -10.3528990049737, latitude: 6.349884451166251}, {longi: -10.3525533541093, latitude: 6.34995313794123}, {longi: -10.3521355598199, latitude: 6.35004930806661}, {longi: -10.3519838697196, latitude: 6.35033653918075}, {longi: -10.3520120786899, latitude: 6.35042799629275}, {longi: -10.3520614976228, latitude: 6.350653137853469}, {longi: -10.3521509996088, latitude: 6.35099631614466}, {longi: -10.3522741760287, latitude: 6.35129938704022}, {longi: -10.3522742825286, latitude: 6.35142957027359}, {longi: -10.3521914383859, latitude: 6.3516154132291}, {longi: -10.3521710580113, latitude: 6.35220934734841}, {longi: -10.3521771259875, latitude: 6.35274837169428}, {longi: -10.3520789264324, latitude: 6.353079188144649}, {longi: -10.351967872846, latitude: 6.35317357405727}, {longi: -10.3517836882554, latitude: 6.353290538029841}, {longi: -10.3514236751495, latitude: 6.353421719696559}, {longi: -10.3511268665127, latitude: 6.35343322151977}, {longi: -10.3529226019644, latitude: 6.35412102247318}, {longi: -10.3719943497247, latitude: 6.34870200460274}, {longi: -10.391393341185, latitude: 6.34342378099707}, {longi: -10.3945209579924, latitude: 6.35386705040947}, {longi: -10.4015180548313, latitude: 6.351780785039041}, {longi: -10.4086842983249, latitude: 6.34990393142069}, {longi: -10.4093379446285, latitude: 6.34944855248912}, {longi: -10.4099255864983, latitude: 6.349039155952}, {longi: -10.4116266595253, latitude: 6.34776925812}, {longi: -10.4112085994004, latitude: 6.347864962409339}, {longi: -10.409571857663, latitude: 6.34815763041146}, {longi: -10.4090822303986, latitude: 6.34823372995665}, {longi: -10.4080594692731, latitude: 6.34783342002668}, {longi: -10.4063727657152, latitude: 6.346875546695071}, {longi: -10.4059006947664, latitude: 6.34646590714615}, {longi: -10.4057293823738, latitude: 6.344314092938571}, {longi: -10.4062146045751, latitude: 6.34399585657377}, {longi: -10.406546132763, latitude: 6.343512828406121}, {longi: -10.4066934955276, latitude: 6.34314678024334}, {longi: -10.4066591560053, latitude: 6.34251631643942}, {longi: -10.4065489892332, latitude: 6.34202947265381}, {longi: -10.4062979763755, latitude: 6.34134572840585}, {longi: -10.4063128384794, latitude: 6.34068003529428}, {longi: -10.4058776627734, latitude: 6.34011327029953}, {longi: -10.4053725421681, latitude: 6.33996877585503}, {longi: -10.4048604271082, latitude: 6.33986650797162}, {longi: -10.4043905181097, latitude: 6.33977123772083}, {longi: -10.4039348363001, latitude: 6.339851873708109}, {longi: -10.4034638133017, latitude: 6.340077481194221}, {longi: -10.4031659057056, latitude: 6.34040988960555}, {longi: -10.402939771761, latitude: 6.340780231122151}, {longi: -10.4027727735079, latitude: 6.34121244243156}, {longi: -10.4025297091355, latitude: 6.34152791236622}, {longi: -10.4020954837335, latitude: 6.34200258797797}, {longi: -10.401772061043, latitude: 6.34212250807575}, {longi: -10.4010647274298, latitude: 6.342360995412171}, {longi: -10.4003700311832, latitude: 6.342394186012339}, {longi: -10.3990343520025, latitude: 6.34106503669948}, {longi: -10.3990164326035, latitude: 6.340589579820979}, {longi: -10.3988762354308, latitude: 6.3395510744988}, {longi: -10.3988461975994, latitude: 6.33899941626647}, {longi: -10.3987362742143, latitude: 6.33877293050567}, {longi: -10.3982577841515, latitude: 6.33852003753889}, {longi: -10.3976980558928, latitude: 6.338652834261751}, {longi: -10.3973577042821, latitude: 6.33871787954086}, {longi: -10.3970776057653, latitude: 6.33852391581827}, {longi: -10.3968311317747, latitude: 6.33817933392759}, {longi: -10.3964117777921, latitude: 6.33798830967882}, {longi: -10.3959305844178, latitude: 6.33785644975227}, {longi: -10.3954241143345, latitude: 6.33777246232119}, {longi: -10.3944450361167, latitude: 6.33752772836997}, {longi: -10.3937965597413, latitude: 6.3361315253927}, {longi: -10.3936047012068, latitude: 6.335509641760521}, {longi: -10.3934649818855, latitude: 6.334991856403411}, {longi: -10.3932747089659, latitude: 6.33456981675114}, {longi: -10.3928650602025, latitude: 6.33421552575876}, {longi: -10.3924032737176, latitude: 6.333757135679421}, {longi: -10.392011907504, latitude: 6.33339719802158}, {longi: -10.3917528009739, latitude: 6.333076548811251}, {longi: -10.3916130845756, latitude: 6.33255876236036}, {longi: -10.3914873601582, latitude: 6.33195652147222}, {longi: -10.3909763758942, latitude: 6.33153335712682}, {longi: -10.3906958233079, latitude: 6.33081866354766}, {longi: -10.3900370076723, latitude: 6.33024081828561}, {longi: -10.3895373115704, latitude: 6.3298542334391}, {longi: -10.3891868022001, latitude: 6.329561810109941}, {longi: -10.3890360337171, latitude: 6.32926780322367}, {longi: -10.3889245347724, latitude: 6.32884146917917}, {longi: -10.38882138556, latitude: 6.32831238991369}, {longi: -10.3882672514025, latitude: 6.32782196668079}, {longi: -10.3882476234369, latitude: 6.32746691060445}, {longi: -10.3882691259181, latitude: 6.326840833866701}, {longi: -10.3886550252491, latitude: 6.32628029783603}, {longi: -10.3888838736797, latitude: 6.32578892553064}, {longi: -10.389042304196, latitude: 6.32519909940758}, {longi: -10.3891587583454, latitude: 6.32486263628746}, {longi: -10.3893903128509, latitude: 6.32425022802852}, {longi: -10.3895390397543, latitude: 6.32382366504806}, {longi: -10.3896189313196, latitude: 6.323498493025109}, {longi: -10.3898042200326, latitude: 6.32306063896411}, {longi: -10.3898601878479, latitude: 6.32272282169282}, {longi: -10.389772614382, latitude: 6.32230913283248}, {longi: -10.3894911744848, latitude: 6.32217568031125}, {longi: -10.389011578797, latitude: 6.32224365507653}, {longi: -10.3883462372141, latitude: 6.32222875755078}, {longi: -10.3881013596525, latitude: 6.32208401320216}, {longi: -10.3872264244147, latitude: 6.3217155687984}, {longi: -10.3867196675097, latitude: 6.32162923403815}, {longi: -10.3864073087195, latitude: 6.32152536132995}, {longi: -10.3862987494397, latitude: 6.32123835273969}, {longi: -10.3861689747311, latitude: 6.32081766252368}, {longi: -10.3862558647533, latitude: 6.320450263451549}, {longi: -10.3864908820924, latitude: 6.31975613537111}, {longi: -10.3865262385065, latitude: 6.319184803016221}, {longi: -10.3863384606355, latitude: 6.31838135887378}, {longi: -10.3862987378599, latitude: 6.3179929595708}, {longi: -10.3862098147221, latitude: 6.31763978730105}, {longi: -10.3859985675729, latitude: 6.317344423541899}, {longi: -10.3859351482429, latitude: 6.31720374175615}, {longi: -10.3858459995131, latitude: 6.31659020616366}, {longi: -10.3858906856553, latitude: 6.31621580702741}, {longi: -10.3858017633457, latitude: 6.315862634537651}, {longi: -10.3857692638261, latitude: 6.31569237104811}, {longi: -10.3855862291868, latitude: 6.31548846140256}, {longi: -10.3854413362942, latitude: 6.315473106117109}, {longi: -10.3851305612995, latitude: 6.31556907692639}, {longi: -10.3849520404647, latitude: 6.315704339276609}, {longi: -10.3847595266658, latitude: 6.31592405592732}, {longi: -10.384572654943, latitude: 6.31616206346515}, {longi: -10.3844067724445, latitude: 6.316273389451201}, {longi: -10.3840899700284, latitude: 6.31646527032949}, {longi: -10.3835757795326, latitude: 6.31663017351657}, {longi: -10.3834042544179, latitude: 6.31672320831749}, {longi: -10.3831638932181, latitude: 6.31691763328312}, {longi: -10.3830472108341, latitude: 6.316993732148471}, {longi: -10.3828641764831, latitude: 6.31678982142213}, {longi: -10.3826642157411, latitude: 6.316531037722931}, {longi: -10.3823236496851, latitude: 6.31633570688772}, {longi: -10.3820180671747, latitude: 6.315929239857199}, {longi: -10.3817842544063, latitude: 6.315560709795479}, {longi: -10.3816377829675, latitude: 6.31534550795554}, {longi: -10.3815572149616, latitude: 6.31488958871421}, {longi: -10.3815581221607, latitude: 6.3143083432311}, {longi: -10.3816014570169, latitude: 6.31399446196919}, {longi: -10.3816702931188, latitude: 6.313893071974621}, {longi: -10.3821415325782, latitude: 6.313927850765559}, {longi: -10.3824229665396, latitude: 6.314061308691821}, {longi: -10.3829034584473, latitude: 6.31421495261446}, {longi: -10.3834351968318, latitude: 6.31433670254123}, {longi: -10.3836481921825, latitude: 6.3141144178194}, {longi: -10.3837973599759, latitude: 6.31378663539166}, {longi: -10.3837134336427, latitude: 6.313311720209801}, {longi: -10.3835350003395, latitude: 6.31283468754228}, {longi: -10.3834027628589, latitude: 6.31240599326576}, {longi: -10.3832595935164, latitude: 6.312043278857849}, {longi: -10.3829429255176, latitude: 6.31182804807732}, {longi: -10.3823054006315, latitude: 6.31143268570236}, {longi: -10.382095676345, latitude: 6.31126179974681}, {longi: -10.3814629684599, latitude: 6.31074626014882}, {longi: -10.3813192641488, latitude: 6.31047511242594}, {longi: -10.3811542352142, latitude: 6.30994010054627}, {longi: -10.380609041163, latitude: 6.30895153635368}, {longi: -10.3804917837549, latitude: 6.30826558238319}, {longi: -10.3806904698177, latitude: 6.30794976892282}, {longi: -10.3808527970566, latitude: 6.307794467129829}, {longi: -10.3809939668508, latitude: 6.307570573994101}, {longi: -10.3810865581601, latitude: 6.30728655725213}, {longi: -10.3810916387524, latitude: 6.30705961403496}, {longi: -10.3810818257736, latitude: 6.30668702047599}, {longi: -10.3809897498968, latitude: 6.306339830011}, {longi: -10.3807204539826, latitude: 6.30600229080948}, {longi: -10.3804281355191, latitude: 6.30588221022807}, {longi: -10.3800440751192, latitude: 6.30580548450207}, {longi: -10.3796566631348, latitude: 6.30576085606235}, {longi: -10.3792629976821, latitude: 6.3057155069355}, {longi: -10.3786282556058, latitude: 6.30568614066079}, {longi: -10.3781017006061, latitude: 6.30572353196986}, {longi: -10.3775652761064, latitude: 6.30552607544286}, {longi: -10.3772645507189, latitude: 6.305443647288359}, {longi: -10.3769031720756, latitude: 6.305367427604621}, {longi: -10.37653135278, latitude: 6.30541963950852}, {longi: -10.3762420529387, latitude: 6.305333682602049}, {longi: -10.3760529032587, latitude: 6.30516671671691}, {longi: -10.3758760263177, latitude: 6.30495839869305}, {longi: -10.3757835008818, latitude: 6.30469899192925}, {longi: -10.3756575464548, latitude: 6.30441234544826}, {longi: -10.3754895537531, latitude: 6.30431397096479}, {longi: -10.3752548400977, latitude: 6.304323844770331}, {longi: -10.3749789350243, latitude: 6.30448416657542}, {longi: -10.3747514103627, latitude: 6.30467963190801}, {longi: -10.3745862334183, latitude: 6.30479324086646}, {longi: -10.374361250325, latitude: 6.304875234184991}, {longi: -10.374146708464, latitude: 6.30482879547975}, {longi: -10.3737790563069, latitude: 6.30456596477475}, {longi: -10.3736329998416, latitude: 6.30446155036306}, {longi: -10.3734815169891, latitude: 6.304132703220611}, {longi: -10.373434558146, latitude: 6.3036964556616}, {longi: -10.3733753266391, latitude: 6.303301560095049}, {longi: -10.3733320305748, latitude: 6.30303947237829}, {longi: -10.3731402039843, latitude: 6.30282324906793}, {longi: -10.3728052040899, latitude: 6.30275140193379}, {longi: -10.3724914963894, latitude: 6.302910874497889}, {longi: -10.3723536797762, latitude: 6.30315376268179}, {longi: -10.3723811764833, latitude: 6.30344577060574}, {longi: -10.372499365735, latitude: 6.303910106312751}, {longi: -10.3726508479354, latitude: 6.304238953891771}, {longi: -10.3728508472873, latitude: 6.30476567316493}, {longi: -10.3730234862477, latitude: 6.305163112423129}, {longi: -10.3731336481903, latitude: 6.30539233063402}, {longi: -10.3736135931971, latitude: 6.30595250700194}, {longi: -10.3737593257585, latitude: 6.30613288050132}, {longi: -10.3737891088544, latitude: 6.30642418250167}, {longi: -10.3738021311277, latitude: 6.30685588446855}, {longi: -10.3736948082138, latitude: 6.30718952321121}, {longi: -10.3733141252853, latitude: 6.30736717313626}, {longi: -10.3728890956983, latitude: 6.30749841665474}, {longi: -10.3726509305997, latitude: 6.30779733270931}, {longi: -10.3721553133458, latitude: 6.30804052159668}, {longi: -10.3719549015713, latitude: 6.30807235578091}, {longi: -10.3717228371731, latitude: 6.30809893876795}, {longi: -10.3715351190478, latitude: 6.308171927973589}, {longi: -10.3713231937921, latitude: 6.30831249157153}, {longi: -10.3710501604871, latitude: 6.308547048805431}, {longi: -10.3704676817988, latitude: 6.30962840328599}, {longi: -10.3700978183985, latitude: 6.3101332580519}, {longi: -10.3693926306868, latitude: 6.31081677819109}, {longi: -10.3690965677262, latitude: 6.311268794236069}, {longi: -10.3687717401114, latitude: 6.31138411860254}, {longi: -10.368447766867, latitude: 6.311258780106949}, {longi: -10.3683717676557, latitude: 6.31120712223488}, {longi: -10.3681386841866, latitude: 6.31127909282131}, {longi: -10.3679763514408, latitude: 6.31143439168869}, {longi: -10.3680071495071, latitude: 6.31168030573707}, {longi: -10.3680548724726, latitude: 6.31198109350282}, {longi: -10.3682481661549, latitude: 6.31226698243128}, {longi: -10.3684499225268, latitude: 6.31258030808913}, {longi: -10.3684649759142, latitude: 6.312921233314599}, {longi: -10.3685445151132, latitude: 6.31342254601741}, {longi: -10.36845395035, latitude: 6.313615784485691}, {longi: -10.3682284739616, latitude: 6.31395585487408}, {longi: -10.3680830655558, latitude: 6.31416602763469}, {longi: -10.3679723614679, latitude: 6.314245285088111}, {longi: -10.3678310215156, latitude: 6.31427390237828}, {longi: -10.3674850355656, latitude: 6.31432063423673}, {longi: -10.3671043448561, latitude: 6.314498281159519}, {longi: -10.3666478163714, latitude: 6.314819543771231}, {longi: -10.3664179449302, latitude: 6.31495062130612}, {longi: -10.3662955640209, latitude: 6.314943334262101}, {longi: -10.3657046535503, latitude: 6.314780216852881}, {longi: -10.3655557063909, latitude: 6.31454073371974}, {longi: -10.365531174892, latitude: 6.31421776000485}, {longi: -10.3654245397709, latitude: 6.31411546140207}, {longi: -10.3651991690408, latitude: 6.31416891624362}, {longi: -10.3652545512557, latitude: 6.31441522311028}, {longi: -10.3663170944226, latitude: 6.31833664986107}, {longi: -10.3671122750959, latitude: 6.320702751595331}, {longi: -10.3673776480129, latitude: 6.3214923757941}, {longi: -10.3673778358575, latitude: 6.32156388532924}, {longi: -10.3677984869845, latitude: 6.32292792210678}, {longi: -10.3678831141786, latitude: 6.323202290298921}, {longi: -10.3681957452437, latitude: 6.323631631000651}, {longi: -10.3683186188665, latitude: 6.324224738416431}, {longi: -10.3683328208488, latitude: 6.32480632603039}, {longi: -10.3682855858433, latitude: 6.32509135997872}, {longi: -10.3680653543224, latitude: 6.325399760805249}, {longi: -10.3675106429922, latitude: 6.32564616534423}, {longi: -10.3668777321967, latitude: 6.32573641563092}, {longi: -10.3663906876193, latitude: 6.32563825227148}, {longi: -10.365837639921, latitude: 6.32545322829248}, {longi: -10.3654650104671, latitude: 6.32560835098764}, {longi: -10.3652458482452, latitude: 6.32593645346206}, {longi: -10.3652194291511, latitude: 6.32630520932729}, {longi: -10.365205649901, latitude: 6.32665002917844}, {longi: -10.3649470319247, latitude: 6.3274329506493}, {longi: -10.364761243156, latitude: 6.3280276393589}, {longi: -10.3645020714865, latitude: 6.328453236312079}, {longi: -10.3639581765602, latitude: 6.32902684531776}, {longi: -10.3636546558386, latitude: 6.32940603539449}, {longi: -10.3633076418462, latitude: 6.32949815560941}, {longi: -10.3625470914577, latitude: 6.32961278519408}, {longi: -10.361906843494, latitude: 6.32958779242113}, {longi: -10.360717803695, latitude: 6.32947309238414}, {longi: -10.3601198508604, latitude: 6.32931965345186}, {longi: -10.3595160959693, latitude: 6.32959371149248}, {longi: -10.3589969650529, latitude: 6.330142139297981}, {longi: -10.3582186906981, latitude: 6.3309458683682}, {longi: -10.3578070340727, latitude: 6.33176161032637}, {longi: -10.3576604567231, latitude: 6.332057281606751}, {longi: -10.3576792020659, latitude: 6.332402075568281}, {longi: -10.3576978139086, latitude: 6.332584140726579}, {longi: -10.3578425186826, latitude: 6.33277558818274}, {longi: -10.3583289688256, latitude: 6.33312382292716}, {longi: -10.3586226405641, latitude: 6.33358907356151}, {longi: -10.3586378518873, latitude: 6.334125274523701}, {longi: -10.3586202306494, latitude: 6.33450633839435}, {longi: -10.3582999385647, latitude: 6.33502592721957}, {longi: -10.3579574717989, latitude: 6.335522312382381}, {longi: -10.3574851733153, latitude: 6.33593858332304}, {longi: -10.3568260195669, latitude: 6.33649163572562}, {longi: -10.3563800782516, latitude: 6.337035199592121}, {longi: -10.3560629522975, latitude: 6.3373459643281}, {longi: -10.3558960648003, latitude: 6.337535218702951}, {longi: -10.3558829420986, latitude: 6.33761439492256}, {longi: -10.3559526150624, latitude: 6.33788086146579}, {longi: -10.3560331055491, latitude: 6.33808868807443}, {longi: -10.3562297021476, latitude: 6.33848979748225}, {longi: -10.3566225713417, latitude: 6.338984522511779}, {longi: -10.3566851836428, latitude: 6.33943096402947}, {longi: -10.3565641351044, latitude: 6.33976883607277}, {longi: -10.3563122365575, latitude: 6.34007198290834}, {longi: -10.3562385895159, latitude: 6.34032009514775}, {longi: -10.3562335008295, latitude: 6.340547040249409}, {longi: -10.35652380243, latitude: 6.340757909319871}, {longi: -10.3566349952384, latitude: 6.341264476527311}, {longi: -10.3565542244945, latitude: 6.341830311898319}, {longi: -10.3564130366253, latitude: 6.3420542023815}, {longi: -10.3562571192202, latitude: 6.34232771530697}, {longi: -10.3563534224248, latitude: 6.34268863040018}, {longi: -10.356226106608, latitude: 6.34310356204225}, {longi: -10.3558099864803, latitude: 6.34384805954185}];

  @override
  void initState() {
    super.initState();

    if (defaultTargetPlatform == TargetPlatform.android) {
      locationSettings = AndroidSettings(
          accuracy: LocationAccuracy.high,
          distanceFilter: 100,
          forceLocationManager: true,
          intervalDuration: const Duration(seconds: 10),
          //(Optional) Set foreground notification config to keep the app alive
          //when going to the background
          foregroundNotificationConfig: const ForegroundNotificationConfig(
            notificationText:
                "Example app will continue to receive your location even when you aren't using it",
            notificationTitle: "Running in Background",
            enableWakeLock: true,
          ));
    } else if (defaultTargetPlatform == TargetPlatform.iOS ||
        defaultTargetPlatform == TargetPlatform.macOS) {
      locationSettings = AppleSettings(
        accuracy: LocationAccuracy.high,
        activityType: ActivityType.fitness,
        distanceFilter: 100,
        pauseLocationUpdatesAutomatically: true,
        // Only set to true if our app will be started up in the background.
        showBackgroundLocationIndicator: false,
      );
    } else {
      locationSettings = const LocationSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 100,
      );
    }
    // _getCurrentLocation();

    _getDivision();

    //_initializeLocationUpdates();

    //onMap ? _setTimer() : null;
  }

  @override
  void dispose() {
    //_positionStreamSubscription!.cancel();
    timer?.cancel();
    super.dispose();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        mainDivId = data["Id"].toString();
        //selectedDivisionName = data["division_name"].toString();

        blockId = data2["Id"].toString();
        //selectedBlockName = data2["name"].toString();
      });

      await _getDivisionLatLngFromDatabase(mainDivId);
    } else {
      Get.back();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _getDivisionLatLngFromDatabase(mainDivId);
  }

  _getDivisionLatLngFromDatabase(var id) async {
    setState(() => isLoading = true);
    print("At Function ${id.toString()}");

    var res = await EstateController().getlatlongById(id.toString());
    print(res.toString());

    try {
      _divisionsPolygon =
          await EstateController().getlatlongById(id.toString());
      //_divisionsPolygon = await EstateController().getDivisionlatlong();
      if (_divisionsPolygon.isNotEmpty) {
        //print(_divisionsPolygon[0]["latitude"].toString());
        for (int i = 0; i < _divisionsPolygon.length; i++) {
          final lat = double.parse(_divisionsPolygon[i]["latitude"]);
          final lng = double.parse(_divisionsPolygon[i]["longi"]);

          points.add(LatLng(lat, lng));
        }

        //print("Division Polygon is: " + points.toString());
        await _setPolygon();
        await _getBlockLatLngFromDatabase();
        await _getAllDivisionBlocks(mainDivId);
        _getLatLngCameraPosition().then((latLng) {
          setState(() {
            _cameraPosition = latLng;
          });
        });
        //_getCurrentLocation();
        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    } catch (e) {
      print("Did not Get ${e.toString()}");
    }
  }

  Future<LatLng> _getLatLngCameraPosition() async {
    final lat = double.parse(_divisionsPolygon.first['latitude']);
    final long = double.parse(_divisionsPolygon.first['longi']);
    return LatLng(lat, long);
  }

  _setPolygon() async {
    //List<LatLng> flattenedList = divisions.expand((i) => i).toList();
    Polygon pol = Polygon(
      polygonId: const PolygonId('Division_Polygon'),
      points: points,
      // points: flattenedList,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listBlocksPolygons.add(lispol);
  }

  _getBlockLatLngFromDatabase() async {
    try {
      //print("BlockId is: " + blockId.toString());
      //List<dynamic> _blockslatlong = [];
      _blockslatlong = await EstateController().getBlockslatlongById(
        blockId,
      );
      if (_blockslatlong.isNotEmpty) {
        for (int j = 0; j < _blockslatlong.length; j++) {
          final lat = double.parse(_blockslatlong[j]["latitude"]);
          final lng = double.parse(_blockslatlong[j]["longi"]);
          Blockpoints.add(LatLng(lat, lng));
        }
      }
      await _getPolyline();
      area = calculateArea(polygonLatLngs);
      //print("Block Polygon is: " + Blockpoints.toString());
    } catch (e) {}
  }

  List<dynamic> _alldivisionBlocks = [];
  final List<dynamic> _divisionBlocksId = [];

  Future _getAllDivisionBlocks(var id) async {
    // setState(() => isLoading = true);
    print("At Function");

    try {
      _alldivisionBlocks =
          await EstateController().getBlocksLocalById(blocksTable, id);
      if (_alldivisionBlocks.isNotEmpty) {
        _alldivisionBlocks.reversed;
        print(_alldivisionBlocks[0]["name"]!.toString());
        print(_alldivisionBlocks.length);

        for (var element in _alldivisionBlocks) {
          print("Id are: ${element['Id']}");
          if (element['Id'].toString() != blockId) {
            _divisionBlocksId.add(element['Id'].toString());
          }
        }

        setState(() {});
        await _getAllBlocskLatLngFromDatabase();

        //setState(() => isLoading = false);

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    } catch (e) {
      //setState(() => isLoading = false);
    }
  }

  _getAllBlocskLatLngFromDatabase() async {
    try {
      
      int i = 0;
      while (i < _divisionBlocksId.length) {
        List<dynamic> allblockslatlong = [];
        List<LatLng> allBlockspoints = [];
        var res = await EstateController().getBlockslatlongById(_divisionBlocksId[i].toString());
        print("BlockId is: $res");
        if (res != null) {
          setState(() {
            allblockslatlong = res;
          });
          if (allblockslatlong.isNotEmpty) {
            for (int j = 0; j < allblockslatlong.length; j++) {
              final lat = double.parse(allblockslatlong[j]["latitude"]);
              final lng = double.parse(allblockslatlong[j]["longi"]);
              allBlockspoints.add(LatLng(lat, lng));
              pointsB.add(LatLng(lat, lng));
            }
            _listBlocksPolygon.add(allBlockspoints);
          }
          i++;
        } else {
          i++;
        }
      }
      await _buildAllPolygons();
      print("Blocks  are: $_listBlocksPolygon");
    } catch (e) {}
  }

  Future<LatLng> _getCurrentLocation() async {
    Position? currpos = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    return LatLng(currpos.latitude, currpos.longitude);
  }
  

  int _currentIndex = 0;
  void _pickRandomPoint() async{
    if (_currentIndex < pathPoints.length) {
      //LatLng newPoint = pathPoints[_currentIndex];
      LatLng newPoint = await _getCurrentLocation();
      _currentIndex++;
      _isPointInsidePolygon(newPoint, points)
          ? !_isPointInsidePolygonBlock(newPoint)
              ? polygonLatLngs.isEmpty
                  ? setState(() {
                      polygonLatLngs.add(newPoint);
                      _setMarker(newPoint);
                      _setPolyline(newPoint);

                      _mapController.animateCamera(
                        CameraUpdate.newCameraPosition(
                          CameraPosition(
                            target:
                                LatLng(newPoint.latitude, newPoint.longitude),
                            zoom: 15,
                          ),
                        ),
                      );
                    })
                  : isPolylineCrossingPolygon(
                          newPoint, polygonLatLngs.last, _listBlocksPolygons)
                      ? Fluttertoast.showToast(
                          msg: "The Point is Crossing a Polygon")
                      : polygonLatLngs.length >= 3
                          ? doesPolylineIntersectPolygon(
                                  newPoint, polygonLatLngs)
                              ? Fluttertoast.showToast(
                                  msg: "The Polygon is Crossing Itself")
                              : setState(() {
                                  polygonLatLngs.add(newPoint);
                                  _setMarker(newPoint);
                                  _setPolyline(newPoint);

                                  _mapController.animateCamera(
                                    CameraUpdate.newCameraPosition(
                                      CameraPosition(
                                        target: LatLng(newPoint.latitude,
                                            newPoint.longitude),
                                        zoom: 15,
                                      ),
                                    ),
                                  );
                                })
                          : setState(() {
                              polygonLatLngs.add(newPoint);
                              _setMarker(newPoint);
                              _setPolyline(newPoint);

                              _mapController.animateCamera(
                                CameraUpdate.newCameraPosition(
                                  CameraPosition(
                                    target: LatLng(
                                        newPoint.latitude, newPoint.longitude),
                                    zoom: 15,
                                  ),
                                ),
                              );
                            })
              : Fluttertoast.showToast(msg: "The Point is Crossing a Polygon")
          : Fluttertoast.showToast(msg: "The Point is Outside Mapping Area");
    }
  }

  bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
    print("Point is****" + point.toString());
    print("Polygon is****" + polygon.toString());
    bool isInside = false;
    for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
      if ((polygon[i].longitude > point.longitude) !=
              (polygon[j].longitude > point.longitude) &&
          (point.latitude <
              (polygon[j].latitude - polygon[i].latitude) *
                      (point.longitude - polygon[i].longitude) /
                      (polygon[j].longitude - polygon[i].longitude) +
                  polygon[i].latitude)) {
        isInside = !isInside;
      }
    }
    return isInside;
  }

  bool _isPointInsidePolygonBlock(LatLng point) {
    bool inside2 = false;
    for (int i = 0, j = pointsB.length - 1; i < pointsB.length; j = i++) {
      if (((pointsB[i].latitude > point.latitude) !=
              (pointsB[j].latitude > point.latitude)) &&
          (point.longitude <
              (pointsB[j].longitude - pointsB[i].longitude) *
                      (point.latitude - pointsB[i].latitude) /
                      (pointsB[j].latitude - pointsB[i].latitude) +
                  pointsB[i].longitude)) {
        inside2 = !inside2;
      }
    }

    return inside2;
  }

  bool isPolylineCrossingPolygon(
      LatLng point1, LatLng point2, List<List<Polygon>> polygonList) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polylines.length.toString()),
      points: [point1, point2],
      color: Colors.blue,
      width: 2,
    );

    for (var element in polygonList) {
      for (var element1 in element) {
        Polygon polygon = element1;
        for (int i = 0; i < polygon.points.length - 1; i++) {
          LatLng p1 = polygon.points[i];
          LatLng p2 = polygon.points[i + 1];

          for (int j = 0; j < polyline.points.length - 1; j++) {
            LatLng q1 = polyline.points[j];
            LatLng q2 = polyline.points[j + 1];

            if (doSegmentsIntersect(p1, p2, q1, q2)) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  bool doSegmentsIntersect(LatLng p1, LatLng p2, LatLng q1, LatLng q2) {
    double crossProduct(LatLng a, LatLng b, LatLng c) {
      return (b.latitude - a.latitude) * (c.longitude - a.longitude) -
          (b.longitude - a.longitude) * (c.latitude - a.latitude);
    }

    double direction(LatLng a, LatLng b, LatLng c) {
      return crossProduct(a, b, c);
    }

    bool onSegment(LatLng a, LatLng b, LatLng c) {
      return (c.latitude >= a.latitude && c.latitude <= b.latitude) ||
          (c.latitude <= a.latitude && c.latitude >= b.latitude);
    }

    double d1 = direction(q1, q2, p1);
    double d2 = direction(q1, q2, p2);
    double d3 = direction(p1, p2, q1);
    double d4 = direction(p1, p2, q2);

    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
        ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
      return true;
    } else if (d1 == 0 && onSegment(q1, q2, p1)) {
      return true;
    } else if (d2 == 0 && onSegment(q1, q2, p2)) {
      return true;
    } else if (d3 == 0 && onSegment(p1, p2, q1)) {
      return true;
    } else if (d4 == 0 && onSegment(p1, p2, q2)) {
      return true;
    }

    return false;
  }

  // CHECK IF POLYGON INTERSECTS ITSELF
  bool doesPolylineIntersectPolygon(
      LatLng newPoint, List<LatLng> polygonPoints) {
    LatLng lastPointOfPolygon = polygonPoints.last;

    for (int i = 0; i < polygonPoints.length - 2; i++) {
      LatLng p1 = polygonPoints[i];
      LatLng p2 = polygonPoints[i + 1];

      if (doSegmentsIntersect1(p1, p2, lastPointOfPolygon, newPoint)) {
        return true;
      }
    }

    return false;
  }

  bool doSegmentsIntersect1(LatLng p1, LatLng p2, LatLng q1, LatLng q2) {
    double crossProduct(LatLng a, LatLng b, LatLng c) {
      return (b.latitude - a.latitude) * (c.longitude - a.longitude) -
          (b.longitude - a.longitude) * (c.latitude - a.latitude);
    }

    double direction(LatLng a, LatLng b, LatLng c) {
      return crossProduct(a, b, c);
    }

    bool onSegment(LatLng a, LatLng b, LatLng c) {
      return (c.latitude >= a.latitude && c.latitude <= b.latitude) ||
          (c.latitude <= a.latitude && c.latitude >= b.latitude);
    }

    double d1 = direction(q1, q2, p1);
    double d2 = direction(q1, q2, p2);
    double d3 = direction(p1, p2, q1);
    double d4 = direction(p1, p2, q2);

    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
        ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
      return true;
    } else if (d1 == 0 && onSegment(q1, q2, p1)) {
      return true;
    } else if (d2 == 0 && onSegment(q1, q2, p2)) {
      return true;
    } else if (d3 == 0 && onSegment(p1, p2, q1)) {
      return true;
    } else if (d4 == 0 && onSegment(p1, p2, q2)) {
      return true;
    }

    return false;
  }

  void startTracking() {
    print("At picking");

    timer = Timer.periodic(Duration(seconds: 3), (timer) async {
      _pickRandomPoint();
    });
  }

  int _markerId = 1;

  void _setMarker(LatLng point) async {
    setState(() {
      _markers.add(
          Marker(markerId: MarkerId(_markerId.toString()), position: point));
      _markerId++;
    });
    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  _addMarker(LatLng position) {
    _markers.add(
      Marker(
        markerId: MarkerId(position.toString()),
        position: LatLng(position.latitude, position.longitude),
      ),
    );
  }

  _getPolyline() {
    setState(() {
      blocksEmpty = false;
    });
    for (var element in Blockpoints) {
      polygonLatLngs.add(element);

      //_setMarker(element);
      _setPolyline(element);
    }
  }

  void _setPolyline(LatLng location) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polylines.length.toString()),
      points: [
        if (_polylines.isNotEmpty) _polylines.last.points.last,
        location
      ],
      color: Colors.blue,
      width: 2,
    );

    setState(() {
      _polylines.add(polyline);
    });
  }

  int seconds = 0;
  bool isRunning = false;

  void startTimer() {
    setState(() {
      isRunning = true;
    });
    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        seconds++;
      });
    });
  }

  void stopTimer() {
    setState(() {
      isRunning = false;
      seconds = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              children: [
                GoogleMap(
                  onMapCreated: (GoogleMapController controller) {
                    _mapController = controller;
                  },
                  initialCameraPosition: CameraPosition(
                    //target: _cameraPosition,
                    target: _points.isEmpty
                        ? _cameraPosition
                        : LatLng(_points.last.latitude, _points.last.longitude),
                    zoom: 13,
                  ),
                  markers: Set.from(_markers),
                  polylines: Set.from(_polylines),
                  //polylines: _polyline,
                  polygons: _listBlocksPolygons.expand((i) => i).toSet(),
                ),
                Positioned(
                  top: 50,
                  child: Container(
                    padding: EdgeInsets.all(Dimensions.height15),
                    height: Dimensions.height15 * Dimensions.height15,
                    width: (Dimensions.screenWidth / 2) +
                        Dimensions.width10 * Dimensions.width15,
                    margin: EdgeInsets.only(
                        top: Dimensions.height10, left: Dimensions.width10),
                    // decoration: BoxDecoration(
                    //   color: Colors.grey[700],
                    //   borderRadius: BorderRadius.circular(Dimensions.radius15),
                    // ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                            child: Text(
                          "Tap Mark to get Location Co-ordinates",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: Dimensions.font16 - 2,
                              fontWeight: FontWeight.bold),
                        )),
                        SizedBox(
                          height: Dimensions.height10 / 2,
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                onMap
                                    ? blocksEmpty
                                        ? startTracking()
                                        : showDialog(
                                            barrierDismissible: false,
                                            context: context,
                                            builder: (BuildContext context) {
                                              return Dialog(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                elevation: 0,
                                                backgroundColor:
                                                    Colors.transparent,
                                                child: confirm(context),
                                              );
                                            })
                                    : setState(() {
                                        onMap = true;
                                      });
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "START MAPPING",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () async {
                                //dialog to choose mode of mapping
                                onMap
                                    ? showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            elevation: 0,
                                            backgroundColor: Colors.transparent,
                                            child: contentBox(context),
                                          );
                                        })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "RESTART",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  onMap
                                      ? _deletePoint()
                                      : Fluttertoast.showToast(
                                          msg:
                                              "You did not select START MAPPING");
                                });
                              },
                              child: Icon(
                                Icons.delete_outline_sharp,
                                color: AppColors.buttonRed,
                                size: Dimensions.iconSize24 * 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                onMap
                                    ? _saveBlocks()
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width10 * Dimensions.width10,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "COMPLETE",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            Column(
                              children: [
                                BigText(
                                  text: "Total Acreage (m2)",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                ),
                                BigText(
                                  text: area.toString(),
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Polygon _buildPolygon1() {
    return Polygon(
      polygonId: const PolygonId('Division_Polygon'),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );
  }

  Polygon _buildPolygon2() {
    return Polygon(
      polygonId: const PolygonId('block_Polygon'),
      points: Blockpoints,
      strokeWidth: 2,
      strokeColor: Colors.blue,
      fillColor: Colors.blue.withOpacity(0.3),
    );
  }

  Polygon? _buildPolygon3() {
    Polygon? polygon;
    for (var element in mapPolygonList) {
      polygon = element;
    }
    return polygon;
  }

  _buildAllPolygons() {
    int j = 0;
    while (j < _listBlocksPolygon.length) {
      Polygon polygon = Polygon(
        polygonId: const PolygonId('block_Polygon${1}'),
        points: _listBlocksPolygon[j],
        strokeWidth: 2,
        strokeColor: Colors.blue,
        fillColor: Colors.blue.withOpacity(0.3),
      );

      setState(() {
        mapPolygonList.add(polygon);
        _listBlocksPolygons.add(mapPolygonList);
      });

      j++;
    }
  }

  _restart(String id) async {
    setState(() {
      _markers.clear();
      _polylines.clear();
      polygonLatLngs.clear();
      blocksEmpty = true;
      area = 0.0;
    });
    await EstateController().deleteBlockById(id);
    Get.back();
  }

  void _deletePoint() {
    if (polygonLatLngs.isEmpty || _markers.isEmpty || _polylines.isEmpty) {
      Fluttertoast.showToast(msg: "You did not select any points");
    } else {
      setState(() {
        _removeMarker();
        //_removePolyline(_point!);
      });
    }
  }

  void _removeMarker() {
    if (_markers.isNotEmpty && _polylines.isNotEmpty) {
      setState(() {
        _markers.remove(_markers.last);
        _polylines.remove(_polylines.last);
        polygonLatLngs.remove(polygonLatLngs.last);
      });
    }

    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  _saveBlocks() async {
    List<dynamic> divBlocks = [];
    setState(() {
      area = calculateArea(polygonLatLngs);
    });
    for (var element in polygonLatLngs) {
      DivisionPolygonResultElement divPoly = DivisionPolygonResultElement();
      print(element.latitude);
      divPoly.mainDivisionId = int.parse(mainDivId);
      divPoly.latitude = element.latitude.toString();
      divPoly.longi = element.longitude.toString();
      divPoly.newDevBlockId = blockId;
      divPoly.blockArea = area.toString();
      divPoly.syncId = 'p';
      divPoly.polygonId = mainDivId;
      divPoly.isDivision = 0;

      divBlocks.add(divPoly);
    }

    print("The Block is: $divBlocks");

    int? i = await EstateController().saveAll(divBlocks, divisionPolygonTable);

    if (i != null) {
      BlocksModelResultElement blocksModel = BlocksModelResultElement();
      blocksModel.isPlotted = "Yes";
      int j = await EstateController()
          .updateBlockLocal(blocksTable, "is_plotted", "Yes", 'Id', blockId);
      if (j >= 0) {
        print("Success here AT BLOCKS");
      }

      // showCustomSnackBar(
      //     "Block saved successfully", context, ContentType.success,
      //     title: "Success");
      Get.back();
      Get.to(() => const LivePick());
    } else {
      showCustomSnackBar("Can Not Save Empty", context, ContentType.failure,
          title: "Failed");
      Get.back();
      Get.to(() => const LivePick());
    }
  }

  double calculateArea(List<LatLng> coords) {
    int len = coords.length;
    double sum = 0;
    for (int i = 0; i < len; i++) {
      LatLng p1 = coords[i];
      LatLng p2 = coords[(i + 1) % len];
      sum += (p2.longitude - p1.longitude) * (p2.latitude + p1.latitude);
    }
    return (sum.abs() / 2) *
        111319.9 *
        111319.9; // Multiply by Earth's radius to get the area in square meters
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Confirm Restart",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to restart mapping?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: BigText(
                        text: "Cancel",
                        color: Colors.black,
                        size: Dimensions.font16,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _restart(blockId);
                      },
                      child: BigText(
                        text: "Ok",
                        color: Colors.black,
                        size: Dimensions.font16,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  confirm(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Block Already Drawn",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to Re-Map or Continue?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          // for (var element in polygonLatLngs) {
                          //   if (element != polygonLatLngs.last) {

                          //   }
                          //   _setMarker(element);
                          // }

                          for (int i = 0; i < polygonLatLngs.length; i++) {
                            if (i >= polygonLatLngs.length - 5) {
                              _setMarker(polygonLatLngs[i]);
                            }
                            // else{
                            //   _setDrawnMarker(polygonLatLngs[i]);
                            // }
                          }

                          //_setMarker(polygonLatLngs.last);

                          setState(() {
                            blocksEmpty = true;
                            onMap = true;
                          });
                          Get.back();
                        },
                        child: BigText(
                          text: "Continue",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.back();
                          setState(() {
                            blocksEmpty = true;
                            onMap = true;
                          });
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  elevation: 0,
                                  backgroundColor: Colors.transparent,
                                  child: contentBox(context),
                                );
                              });
                        },
                        child: BigText(
                          text: "Restart",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      )
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }
}
