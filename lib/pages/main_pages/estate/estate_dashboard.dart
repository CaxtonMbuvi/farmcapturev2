// import 'package:farmcapturev2/pages/main_pages/dashboard.dart';
// import 'package:farmcapturev2/pages/main_pages/estate/all_divisions.dart';
// import 'package:farmcapturev2/pages/main_pages/estate/testwalk.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/container.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:get/get.dart';

// import '../../../resources/utils/colors.dart';
// import '../../../resources/utils/dimensions.dart';
// import '../../../resources/widgets/big_text.dart';

// class EstateDashboardScreen extends StatefulWidget {
//   const EstateDashboardScreen({super.key});

//   @override
//   State<EstateDashboardScreen> createState() => _EstateDashboardScreenState();
// }

// class _EstateDashboardScreenState extends State<EstateDashboardScreen> {
//   /**location permission**/
//   LocationPermission? _locationPermission;
//   checkIfLocationPermissionAllowed() async {
//     _locationPermission = await Geolocator.requestPermission();

//     //if permission is denied
//     if (_locationPermission == LocationPermission.denied) {
//       _locationPermission =
//           await Geolocator.requestPermission(); //ask user to allow permission
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(Dimensions.height30 * 2),
//         child: AppBar(
//           backgroundColor: Colors.green,
//           actions: [
//             Container(
//               width: Dimensions.screenWidth,
//               height: Dimensions.height30 * 2,
//               padding: EdgeInsets.only(
//                 left: Dimensions.width15,
//                 right: Dimensions.width15,
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.green,
//                 boxShadow: [
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(5, 5),
//                       color: AppColors.gradientOne.withOpacity(0.1)),
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(-5, -5),
//                       color: AppColors.gradientOne.withOpacity(0.1))
//                 ],
//               ),
//               child: Center(
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     IconButton(
//                       icon: Icon(
//                         Icons.arrow_back_ios,
//                         color: Colors.white,
//                         size: Dimensions.iconSize24,
//                       ),
//                       onPressed: () {
//                         Get.to(() => DashboardScreen());
//                       },
//                     ),
//                     BigText(
//                       text: "Farm Inspection",
//                       color: AppColors.textWhite,
//                       size: Dimensions.font28,
//                     ),
//                     SizedBox()
//                   ],
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: SafeArea(
//           child: Container(
//             height: Dimensions.screenHeight - (Dimensions.height30 * 3),
//             width: Dimensions.screenWidth,
//             decoration: BoxDecoration(
//               image: DecorationImage(
//                 image: AssetImage("assets/images/bg.png"),
//                 fit: BoxFit.cover,
//               ),
//             ),
//             padding: EdgeInsets.only(
//               top: Dimensions.height45,
//               left: Dimensions.width30 * 2.0,
//               right: Dimensions.width30 * 2.0,
//             ),
//             child: Column(
//               children: [
//                 SizedBox(
//                   height: Dimensions.height30 * 2,
//                 ),
//                 GestureDetector(
//                   onTap: () async{
//                     await checkIfLocationPermissionAllowed();
//                     Get.to(() => AllEstateScreen());
//                   },
//                   child: Column(
//                     children: [
//                       Container(
//                         height: Dimensions.height30 * 4,
//                         width: Dimensions.width30 * 4,
//                         decoration: BoxDecoration(
//                           color: Colors.grey[300],
//                           borderRadius:
//                               BorderRadius.circular(Dimensions.radius30 * 5),
//                         ),
//                         child: Center(
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Image.asset(
//                                 'assets/images/ic_1_1.png',
//                                 width: 60,
//                                 height: 60,
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                       SizedBox(
//                         height: Dimensions.height10,
//                       ),
//                       BigText(
//                         text: "Estates",
//                         color: AppColors.black,
//                         size: Dimensions.font16 - 4,
//                       ),
//                     ],
//                   ),
//                 ),
//                 SizedBox(
//                   height: Dimensions.height30 * 3,
//                 ),
//                 Column(
//                   children: [
//                     GestureDetector(
//                       onTap: () async{
//                         await checkIfLocationPermissionAllowed();
//                         Get.to(() => MapScreen());
//                       },
//                       child: Container(
//                         height: Dimensions.height30 * 4,
//                         width: Dimensions.width30 * 4,
//                         decoration: BoxDecoration(
//                           color: Colors.grey[300],
//                           borderRadius:
//                               BorderRadius.circular(Dimensions.radius30 * 5),
//                         ),
//                         child: Center(
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Image.asset(
//                                 'assets/images/ic_1_1.png',
//                                 width: 60,
//                                 height: 60,
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       height: Dimensions.height10,
//                     ),
//                     BigText(
//                       text: "Outgrowers",
//                       color: AppColors.black,
//                       size: Dimensions.font16 - 4,
//                     ),
//                   ],
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

import 'dart:convert';

import 'package:farmcapturev2/pages/main_pages/dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/estate/all_blocks.dart';
import 'package:farmcapturev2/pages/main_pages/infrastructure_mapping/infrastructure_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';

class EstateDashboardScreen extends StatefulWidget {
  const EstateDashboardScreen({super.key});

  @override
  State<EstateDashboardScreen> createState() => _EstateDashboardScreenState();
}

class _EstateDashboardScreenState extends State<EstateDashboardScreen> {
  var selectedDivision = "";
  var _selectedIndex = Get.arguments;
  //var _selectedIndex = -1;

  List<dynamic> items = [
    {"image": 'assets/images/ic_1_1.png', "name": "ESTATE MAPPING"},
    {
      "image": 'assets/images/ic_grouptraining.png',
      "name": "MAP VISUALIZATION"
    },
    {
      "image": 'assets/images/ic_tree_inspection.png',
      "name": "TECHNICAL UPDATE"
    },
    {
      "image": 'assets/images/ic_tree_inspection.png',
      "name": "INFRUSTRUCTURE MAPPING"
    },
  ];

  /// location permission*
  LocationPermission? _locationPermission;
  checkIfLocationPermissionAllowed() async {
    _locationPermission = await Geolocator.requestPermission();

    //if permission is denied
    if (_locationPermission == LocationPermission.denied) {
      _locationPermission =
          await Geolocator.requestPermission(); //ask user to allow permission
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkIfLocationPermissionAllowed();

    _getDivision();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var data = jsonDecode(result.toString());
    print(data);
    if(data != null){
      setState(() {
        selectedDivision = data["Id"].toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          //height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30 * Dimensions.height10 / 1.5,
                child: Stack(
                  children: [
                    Container(
                      height: Dimensions.height15 * 10,
                      padding: EdgeInsets.only(
                        top: Dimensions.height30 * 2,
                        left: Dimensions.width20,
                        right: Dimensions.width20,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(Dimensions.radius30 * 4),
                          bottomRight: Radius.circular(Dimensions.radius30 * 4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: Dimensions.iconSize24,
                                  ),
                                  onPressed: () {
                                    Get.to(() => const DashboardScreen(),
                                        arguments: 1);
                                  },
                                ),
                                BigText(
                                  text: "Estates Dashboard",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font20,
                                ),
                                SizedBox(
                                  width: Dimensions.width30,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Dimensions.screenHeight,
                padding: EdgeInsets.only(
                  left: Dimensions.height30,
                  right: Dimensions.height30,
                  top: Dimensions.height20,
                ),
                child: GridView.builder(
                  itemCount: items.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 30,
                      mainAxisSpacing: 30),
                  itemBuilder: (context, index) {
                    var item = items[index];
                    return GestureDetector(
                      onTap: () async {
                        setState(() {
                          if (_selectedIndex == index) {
                            if (_selectedIndex == 0) {
                              Get.to(() => const AllBlockScreen(), arguments: [_selectedIndex, 0]);
                            } else if (_selectedIndex == 1) {
                            } else if (_selectedIndex == 2) {
                              Get.to(() => const AllBlockScreen(), arguments: [_selectedIndex, 1]);
                              // Get.to(
                              //   () => const TechnicalUpdateDashboardScreen(),
                              // );
                            }
                          } else {
                            _selectedIndex = index;
                          }
                          print(_selectedIndex.toString());
                        });
                        if (_selectedIndex == 0) {
                          Get.to(() => const AllBlockScreen(), arguments: [_selectedIndex, 0]);
                        } else if (_selectedIndex == 1) {
                        } else if (_selectedIndex == 2) {
                          Get.to(() => const AllBlockScreen(), arguments: [_selectedIndex, 1]);
                        } else if (_selectedIndex == 3) {
                          Get.to(() => const InfrastructureDashboardScreen(),);
                        }
                      },
                      child: Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 2,
                        decoration: _selectedIndex == index
                            ? BoxDecoration(
                                color: AppColors.mainColor,
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius20 / 4),
                              )
                            : BoxDecoration(
                                color: AppColors.textWhite,
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius20 / 4),
                                border: Border.all(
                                  width: 2,
                                  color: AppColors.mainColor,
                                )),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              item["image"].toString(),
                              width: 60,
                              height: 60,
                              color: _selectedIndex == index
                                  ? AppColors.textWhite
                                  : AppColors.mainColor,
                            ),
                            BigText(
                              text: item["name"].toString(),
                              color: AppColors.black,
                              size: Dimensions.font16 - 4,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
