// import 'package:farmcapturev2/controllers/estates/estates_controller.dart';
// import 'package:farmcapturev2/models/estate/maindivisions_model.dart';
// import 'package:farmcapturev2/pages/main_pages/estate/all_blocks.dart';
// import 'package:farmcapturev2/pages/main_pages/estate/estate_dashboard.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter/src/widgets/placeholder.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:get/get.dart';

// import '../../../resources/base/custom_loader.dart';
// import '../../../resources/utils/colors.dart';
// import '../../../resources/utils/dimensions.dart';
// import '../../../resources/widgets/big_text.dart';
// import '../../../resources/widgets/small_text.dart';
// import '../dashboard.dart';
// import '../farmers/single_farmer_dashboard.dart';

// class AllEstateScreen extends StatefulWidget {
//   const AllEstateScreen({super.key});

//   @override
//   State<AllEstateScreen> createState() => _AllEstateScreenState();
// }

// class _AllEstateScreenState extends State<AllEstateScreen> {
//   String searchString = "";

//   bool isLoading = false;
//   List<dynamic> _mainDivisions = [];

//   Future _getAllMainDivisions() async {
//     setState(() => isLoading = true);
//     print("At Function");

//     try {
//       _mainDivisions =
//           await EstateController().getMainDivisions(mainDivisionsTable);
//       if (_mainDivisions.isNotEmpty) {
//         _mainDivisions.reversed;
//         print(_mainDivisions[0]["division_name"]!.toString());
//         print(_mainDivisions.length);

//         setState(() => isLoading = false);

//         //print("List of Users: " + userModelList.length.toString());
//       } else {
//         print("Did not Get");
//       }
//     } catch (e) {
//       setState(() => isLoading = false);
//       await openModeOfMappingDialog();
//     }
//   }

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _getAllMainDivisions();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(Dimensions.height30 * 2),
//         child: AppBar(
//           backgroundColor: Colors.green,
//           actions: [
//             Container(
//               width: Dimensions.screenWidth,
//               height: Dimensions.height30 * 2,
//               padding: EdgeInsets.only(
//                 left: Dimensions.width15,
//                 right: Dimensions.width15,
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.green,
//                 boxShadow: [
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(5, 5),
//                       color: AppColors.gradientOne.withOpacity(0.1)),
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(-5, -5),
//                       color: AppColors.gradientOne.withOpacity(0.1))
//                 ],
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   IconButton(
//                     icon: Icon(
//                       Icons.arrow_back_ios,
//                       color: Colors.white,
//                       size: Dimensions.iconSize24,
//                     ),
//                     onPressed: () {
//                       Get.to(() => EstateDashboardScreen());
//                     },
//                   ),
//                   BigText(text: "Estate Divisions"),
//                   SizedBox()
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: SafeArea(
//           child: Container(
//             color: Colors.green,
//             width: Dimensions.screenWidth,
//             child: isLoading
//                 ? Container(
//                     height: Dimensions.screenHeight,
//                     width: Dimensions.screenWidth,
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [CustomLoader()],
//                     ),
//                   )
//                 : Column(
//                     children: [
//                       SizedBox(
//                         height: Dimensions.height30 * 6,
//                         child: Center(
//                           child: Row(
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Container(
//                                   width: Dimensions.screenWidth / 1.3,
//                                   padding: EdgeInsets.only(
//                                     top: Dimensions.height10,
//                                     bottom: Dimensions.height10,
//                                   ),
//                                   child: Center(
//                                     child: TextField(
//                                       onChanged: (value) {
//                                         setState(() {
//                                           searchString =
//                                               value.toString().toLowerCase();
//                                         });
//                                       },
//                                       autocorrect: true,
//                                       keyboardType: TextInputType.text,
//                                       decoration: InputDecoration(
//                                         hintText: "Search Division",
//                                         hintStyle:
//                                             TextStyle(color: Colors.grey),
//                                         filled: true,
//                                         fillColor: Colors.white,
//                                         enabledBorder: OutlineInputBorder(
//                                           borderRadius: BorderRadius.all(
//                                               Radius.circular(
//                                                   Dimensions.radius30)),
//                                           borderSide: BorderSide(
//                                               color: AppColors.textGrey,
//                                               width: 2),
//                                         ),
//                                         focusedBorder: OutlineInputBorder(
//                                           borderRadius: BorderRadius.all(
//                                               Radius.circular(
//                                                   Dimensions.radius30)),
//                                           borderSide: BorderSide(
//                                               color: Colors.lightGreenAccent,
//                                               width: 2),
//                                         ),
//                                       ),
//                                     ),
//                                   )),
//                               Flexible(
//                                 child: Container(
//                                   width: Dimensions.width30 * 2,
//                                   height: Dimensions.height30 * 2,
//                                   padding: EdgeInsets.only(
//                                     left: Dimensions.width15,
//                                     right: Dimensions.width15,
//                                   ),
//                                   child: Center(
//                                     child: FaIcon(
//                                       FontAwesomeIcons.magnifyingGlass,
//                                       size: Dimensions.height20,
//                                       color: Colors.white,
//                                     ),
//                                   ),
//                                 ),
//                               )
//                             ],
//                           ),
//                         ),
//                       ),
//                       Container(
//                         width: Dimensions.screenWidth,
//                         padding: EdgeInsets.only(
//                           top: Dimensions.height45,
//                           left: Dimensions.width15,
//                           right: Dimensions.width15,
//                         ),
//                         decoration: BoxDecoration(
//                             color: AppColors.textWhite,
//                             borderRadius: BorderRadius.only(
//                               topRight: Radius.circular(
//                                 Dimensions.radius20 * 2,
//                               ),
//                               topLeft: Radius.circular(
//                                 Dimensions.radius20 * 2,
//                               ),
//                             )),
//                         child: Column(
//                           children: [
//                             Container(
//                               child: Column(
//                                 children: [
//                                   ListView.separated(
//                                     shrinkWrap: true,
//                                     physics: NeverScrollableScrollPhysics(),
//                                     itemCount: _mainDivisions.length,
//                                     itemBuilder:
//                                         (BuildContext context, int index) {
//                                       return _mainDivisions[index]
//                                                   ["division_name"]!
//                                               .toString()
//                                               .toLowerCase()
//                                               .contains(searchString)
//                                           ? Padding(
//                                               padding: EdgeInsets.only(
//                                                   left: Dimensions.width10,
//                                                   right: Dimensions.width10,
//                                                   top: Dimensions.height10),
//                                               child: GestureDetector(
//                                                   onTap: () {
//                                                     print(_mainDivisions[index]
//                                                             ["Id"]!
//                                                         .toString());
//                                                     Get.to(
//                                                       () =>
//                                                           const AllBlockScreen(),
//                                                       arguments:
//                                                           _mainDivisions[index]
//                                                                   ["Id"]!
//                                                               .toString(),
//                                                       //allEvents
//                                                     );
//                                                   },
//                                                   child: Container(
//                                                     height:
//                                                         Dimensions.height30 * 4,
//                                                     width:
//                                                         Dimensions.screenWidth,
//                                                     decoration: BoxDecoration(
//                                                         color: Colors.grey[300],
//                                                         borderRadius:
//                                                             BorderRadius.circular(
//                                                                 Dimensions
//                                                                     .radius20)),
//                                                     child: Container(
//                                                       padding: EdgeInsets.only(
//                                                         left:
//                                                             Dimensions.width20,
//                                                         right:
//                                                             Dimensions.width15,
//                                                       ),
//                                                       child: Column(
//                                                         mainAxisAlignment:
//                                                             MainAxisAlignment
//                                                                 .center,
//                                                         crossAxisAlignment:
//                                                             CrossAxisAlignment
//                                                                 .start,
//                                                         children: [
//                                                           BigText(
//                                                             text:
//                                                                 "Name: ${_mainDivisions[index]["division_name"].toString()}",
//                                                             color: AppColors
//                                                                 .mainColor,
//                                                             size: Dimensions
//                                                                 .font16,
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     ),
//                                                   )),
//                                             )
//                                           : Container();
//                                     },
//                                     separatorBuilder:
//                                         (BuildContext context, int index) {
//                                       return _mainDivisions[index]
//                                                   ["division_name"]!
//                                               .toString()
//                                               .toLowerCase()
//                                               .contains(searchString)
//                                           ? Container()
//                                           : Container();
//                                     },
//                                   ),
//                                   SizedBox(
//                                     height: Dimensions.height15,
//                                   ),
//                                   Container(
//                                     height: Dimensions.height30 * 2,
//                                     width: Dimensions.screenWidth,
//                                     padding: EdgeInsets.only(
//                                         left: Dimensions.width20,
//                                         right: Dimensions.width20,
//                                         top: Dimensions.height10),
//                                     decoration: BoxDecoration(
//                                         color: Colors.grey[300],
//                                         borderRadius: BorderRadius.circular(
//                                             Dimensions.radius20)),
//                                     child: Center(
//                                       child: BigText(
//                                         text: "No Item",
//                                         color: AppColors.black,
//                                         size: Dimensions.font18,
//                                       ),
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     height: Dimensions.height15,
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ],
//                         ),
//                       )
//                     ],
//                   ),
//           ),
//         ),
//       ),
//     );
//   }

//   //dialog to choose mode of mapping
//   Future<void> openModeOfMappingDialog() async {
//     showDialog(
//         barrierDismissible: false,
//         context: context,
//         builder: (BuildContext context) {
//           return Dialog(
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(20),
//             ),
//             elevation: 0,
//             backgroundColor: Colors.transparent,
//             child: contentBox(context),
//           );
//         });
//   }

//   contentBox(context) {
//     return Stack(
//       children: [
//         Container(
//           padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
//           margin: EdgeInsets.only(top: 20),
//           decoration: BoxDecoration(
//               shape: BoxShape.rectangle,
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(30),
//               boxShadow: [
//                 BoxShadow(
//                     color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
//               ]),
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: [
//               SizedBox(
//                 height: 10,
//               ),
//               BigText(
//                 text: "No Divisions",
//                 color: Colors.black,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               SmallText(
//                 text: "Kindly SYNC to continue",
//                 color: Colors.black,
//                 size: Dimensions.font16,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Align(
//                   alignment: Alignment.bottomRight,
//                   child: GestureDetector(
//                     onTap: () {
//                       Get.back();
//                       Get.to(() => EstateDashboardScreen());
//                     },
//                     child: BigText(
//                       text: "Ok",
//                       color: Colors.black,
//                       size: Dimensions.font16,
//                     ),
//                   )),
//             ],
//           ),
//         ),
//         Positioned(
//           left: 5,
//           right: 5,
//           child: CircleAvatar(
//             backgroundColor: Colors.transparent,
//             radius: 20,
//             child: Icon(
//               Icons.warning,
//               color: Colors.redAccent,
//               size: 45,
//             ),
//             // child: ClipRRect(
//             //   borderRadius: BorderRadius.all(Radius.circular(10)),
//             //     child: Image.asset('assets/images/ic_1_6.png')
//             // ),
//           ),
//         ),
//       ],
//     );
//   }
// }

import 'package:farmcapturev2/pages/main_pages/dashboard.dart';
import 'package:farmcapturev2/resources/utils/dimensions.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../models/estate/maindivisions_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class AllDivisionsScreen extends StatefulWidget {
  const AllDivisionsScreen({super.key});

  @override
  State<AllDivisionsScreen> createState() => _AllDivisionsScreenState();
}

class _AllDivisionsScreenState extends State<AllDivisionsScreen> {
    String searchString = "";

  bool isLoading = false;
  List<dynamic> _mainDivisions = [];

  Future _getAllMainDivisions() async {
    setState(() => isLoading = true);
    print("At Function");

    try {
      _mainDivisions =
          await EstateController().getMainDivisions(mainDivisionsTable);
      if (_mainDivisions.isNotEmpty) {
        _mainDivisions.reversed;
        print(_mainDivisions[0]["division_name"]!.toString());
        print(_mainDivisions.length);

        setState(() => isLoading = false);

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    } catch (e) {
      setState(() => isLoading = false);
      await openModeOfMappingDialog();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getAllMainDivisions();
  }

  
  @override
  Widget build(BuildContext context) {
    return isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    width: Dimensions.screenWidth,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
   : Dialog(
      insetPadding: EdgeInsets.zero,
      child: SingleChildScrollView(
        child: Container(
          width: Dimensions.screenWidth,
          color: Colors.white,
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height30 * 6,
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: Dimensions.screenWidth / 1.3,
                          padding: EdgeInsets.only(
                            top: Dimensions.height10,
                            bottom: Dimensions.height10,
                          ),
                          child: Center(
                            child: TextField(
                              onChanged: (value) {
                                setState(() {
                                  searchString =
                                      value.toString().toLowerCase();
                                });
                              },
                              autocorrect: true,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                hintText: "Search Division",
                                hintStyle:
                                    const TextStyle(color: Colors.grey),
                                filled: true,
                                fillColor: Colors.white,
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          Dimensions.radius30)),
                                  borderSide: BorderSide(
                                      color: AppColors.textGrey,
                                      width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          Dimensions.radius30)),
                                  borderSide: const BorderSide(
                                      color: Colors.lightGreenAccent,
                                      width: 2),
                                ),
                              ),
                            ),
                          )),
                      Flexible(
                        child: Container(
                          width: Dimensions.width30 * 2,
                          height: Dimensions.height30 * 2,
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                          ),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.magnifyingGlass,
                              size: Dimensions.height20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: Dimensions.screenWidth,
                padding: EdgeInsets.only(
                  top: Dimensions.height45,
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                    color: AppColors.textWhite,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(
                        Dimensions.radius20 * 2,
                      ),
                      topLeft: Radius.circular(
                        Dimensions.radius20 * 2,
                      ),
                    )),
                child: Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: _mainDivisions.length,
                            itemBuilder:
                                (BuildContext context, int index) {
                              return _mainDivisions[index]
                                          ["division_name"]!
                                      .toString()
                                      .toLowerCase()
                                      .contains(searchString)
                                  ? Padding(
                                      padding: EdgeInsets.only(
                                          left: Dimensions.width10,
                                          right: Dimensions.width10,
                                          top: Dimensions.height10),
                                      child: GestureDetector(
                                          onTap: () {
                                            print(_mainDivisions[index]
                                                    ["Id"]!
                                                .toString());
                                            Navigator.of(context).pop(_mainDivisions[index]);
                                          },
                                          child: Container(
                                            height:
                                                Dimensions.height30 * 4,
                                            width:
                                                Dimensions.screenWidth,
                                            decoration: BoxDecoration(
                                                color: Colors.grey[300],
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Dimensions
                                                            .radius20)),
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                left:
                                                    Dimensions.width20,
                                                right:
                                                    Dimensions.width15,
                                              ),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment
                                                        .start,
                                                children: [
                                                  BigText(
                                                    text:
                                                        "Name: ${_mainDivisions[index]["division_name"].toString()}",
                                                    color: AppColors
                                                        .mainColor,
                                                    size: Dimensions
                                                        .font16,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )),
                                    )
                                  : Container();
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return _mainDivisions[index]
                                          ["division_name"]!
                                      .toString()
                                      .toLowerCase()
                                      .contains(searchString)
                                  ? Container()
                                  : Container();
                            },
                          ),
                          SizedBox(
                            height: Dimensions.height15,
                          ),
                          Container(
                            height: Dimensions.height30 * 2,
                            width: Dimensions.screenWidth,
                            padding: EdgeInsets.only(
                                left: Dimensions.width20,
                                right: Dimensions.width20,
                                top: Dimensions.height10),
                            decoration: BoxDecoration(
                                color: Colors.grey[300],
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius20)),
                            child: Center(
                              child: BigText(
                                text: "No Item",
                                color: AppColors.black,
                                size: Dimensions.font18,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height15,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


   //dialog to choose mode of mapping
  Future<void> openModeOfMappingDialog() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: contentBox(context),
          );
        });
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "No Divisions",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Kindly SYNC to continue",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                    Get.back();
                    Get.to(() => const DashboardScreen());
                  },
                  child: BigText(
                    text: "Ok",
                    color: Colors.black,
                    size: Dimensions.font16,
                  ),
                )
              ),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }
}