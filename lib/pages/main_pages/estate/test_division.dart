// import 'dart:async';
// import 'dart:ffi';
// import 'dart:math';
// import 'package:farmcapturev2/resources/utils/dimensions.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/container.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:get/get.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:google_maps_utils/google_maps_utils.dart';

// import '../../../controllers/estates/estates_controller.dart';
// import '../../../models/estate/blocks_model.dart';
// import '../../../resources/base/custom_loader.dart';

// class DivisionsScreen extends StatefulWidget {
//   const DivisionsScreen({super.key});

//   @override
//   State<DivisionsScreen> createState() => _DivisionsScreenState();
// }

// class _DivisionsScreenState extends State<DivisionsScreen> {
//   var pageId = Get.arguments;
//   List<dynamic> _divisionsPolygon = [];
//   // created controller to display Google Maps
//   Completer<GoogleMapController> _controller = Completer();

//   // on below line we have set the camera position
//   static final CameraPosition _kGoogle = const CameraPosition(
//     target: LatLng(19.0759837, 72.8776559),
//     zoom: 14,
//   );

//   Set<Polygon> _polygon = Set<Polygon>();
//   Set<Polyline> _polyline = Set<Polyline>();
//   Set<Marker> _markers = Set<Marker>();
//   List<LatLng> polygonLatLngs = <LatLng>[];
//   int _markerId = 1;
//   int _polylineIdCounter = 1;
//   bool onMap = false;

//   void _setMarker(LatLng point) {
//     setState(() {
//       _markers.add(
//           Marker(markerId: MarkerId(_markerId.toString()), position: point));
//       _markerId++;
//     });
//     print('Markers are: ${_markers.length}');
//   }

//   void _setPolyline() {
//     final String polylineIdVal = 'polygon_$_polylineIdCounter';
//     _polylineIdCounter++;

//     _polyline.add(
//       Polyline(
//           polylineId: PolylineId(polylineIdVal),
//           points: polygonLatLngs,
//           width: 2),
//     );
//     print('PolyLines are ${_polyline.length.toString()}');
//   }
  
//   // created list of locations to display polygon
//   List<LatLng> points = [];

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();

//     _getPolygonDivisions(pageId);
//   }

//   bool isLoading = false;
//   var lat = 19.0759837;
//   var long = 72.8776559;

//   _getPolygonDivisions(var id) async {
//     setState(() => isLoading = true);
//     print("At Function");

//     try {
//       _divisionsPolygon = await EstateController().getlatlongById(id);
//       if (_divisionsPolygon.isNotEmpty) {
//         lat = _divisionsPolygon[0]["latitude"];
//         long = _divisionsPolygon[0]["longi"];

//         print(_divisionsPolygon[0]["latitude"].toString());
//         for (int i = 0; i < _divisionsPolygon.length; i++) {
//           final lat = double.parse(_divisionsPolygon[i]["latitude"]);
//           final lng = double.parse(_divisionsPolygon[i]["longi"]);
//           points.add(LatLng(lat, lng));
//         }

//         print("Points are: " + points.toString());
//         //initialize polygon
//         _polygon.add(Polygon(
//           // given polygonId
//           polygonId: PolygonId('1'),
//           // initialize the list of points to display polygon
//           points: points,
//           // given color to polygon
//           fillColor: Colors.green.withOpacity(0.3),
//           // given border color to polygon
//           strokeColor: Colors.green,
//           geodesic: true,
//           // given width of border
//           strokeWidth: 4,
//         ));

//         print("Polygon is: " + _polygon.toString());

//         setState(() => isLoading = false);

//         //print("List of Users: " + userModelList.length.toString());
//       } else {
//         print("Did not Get");
//       }
//     } catch (e) {
//       setState(() => isLoading = false);
//     }
//   }

//   // Define the point to check
//   final LatLng pointToCheck = LatLng(37.76626, -122.4254);

//   // Function to check if a point is inside a polygon
//   bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
//     bool inside = false;
//     double tolerance = 1;
//     for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
//       if (((polygon[i].latitude > point.latitude) !=
//               (polygon[j].latitude > point.latitude)) &&
//           (point.longitude <
//               (polygon[j].longitude - polygon[i].longitude) *
//                       (point.latitude - polygon[i].latitude) /
//                       (polygon[j].latitude - polygon[i].latitude) +
//                   polygon[i].longitude)) {
//         inside = !inside;
//       }
//     }
//     // if (inside == false) {
//     //   inside = isPointOnPolygon(point, polygon, tolerance);
//     // }
//     print("Is In: $inside");
//     return inside ? inside : isPointOnPolygon(point, polygon, tolerance);
//   }

//   bool isPointOnPolygon(
//       LatLng point, List<LatLng> polygonPoints, double tolerance) {
//     int i;
//     print("Is Here: ");
//     bool j = false;
//     for (i = 0; i < polygonPoints.length - 1; i++) {
//       if (_isRayIntersectingSegment(
//           point, polygonPoints[i], polygonPoints[i + 1], tolerance)) {
//         return true;
//       }
//     }
//     if (_isRayIntersectingSegment(
//         point, polygonPoints[i], polygonPoints[0], tolerance)) {
//       return true;
//     }
//     return false;
//   }

// // Function to check if a point is on a line segment
//   bool _isPointOnSegment(LatLng point, LatLng segmentStart, LatLng segmentEnd) {
//     double crossProduct = (point.longitude - segmentStart.longitude) *
//             (segmentEnd.latitude - segmentStart.latitude) -
//         (point.latitude - segmentStart.latitude) *
//             (segmentEnd.longitude - segmentStart.longitude);
//     if (crossProduct.abs() > 1e-100) {
//       return false;
//     }
//     double dotProduct = (point.longitude - segmentStart.longitude) *
//             (segmentEnd.longitude - segmentStart.longitude) +
//         (point.latitude - segmentStart.latitude) *
//             (segmentEnd.latitude - segmentStart.latitude);
//     if (dotProduct < 0) {
//       return false;
//     }
//     double squaredLength = (segmentEnd.longitude - segmentStart.longitude) *
//             (segmentEnd.longitude - segmentStart.longitude) +
//         (segmentEnd.latitude - segmentStart.latitude) *
//             (segmentEnd.latitude - segmentStart.latitude);
//     if (dotProduct > squaredLength) {
//       return false;
//     }
//     return true;
//   }

// // Function to check if a ray from a point intersects with a line segment
//   bool _isRayIntersectingSegment(
//       LatLng point, LatLng segmentStart, LatLng segmentEnd, double tolerance) {
//     double x1 = segmentStart.latitude - point.latitude;
//     double y1 = segmentStart.longitude - point.longitude;
//     double x2 = segmentEnd.latitude - point.latitude;
//     double y2 = segmentEnd.longitude - point.longitude;
//     if (y1 > y2) {
//       double tempX = x1;
//       double tempY = y1;
//       x1 = x2;
//       y1 = y2;
//       x2 = tempX;
//       y2 = tempY;
//     }
//     if (y2 >= 0 && y1 < 0 && x1 * y2 > x2 * y1) {
//       return true;
//     }
//     if (y1 == 0 && y2 >= 0 && x1 * y2 > x2 * y1) {
//       return true;
//     }
//     if (x1 == 0 && x2 >= 0) {
//       return true;
//     }
//     double slope = y2 - y1 == 0 ? 0 : (x2 - x1) / (y2 - y1);
//     if (y1 * y2 < 0 || x1 * x2 < 0 || x1 * y2 > x2 * y1) {
//       return false;
//     }
//     if ((x1 < 0 || x2 < 0) && slope < 0) {
//       return false;
//     }
//     if (x1 * y2 < x2 * y1 && (x1 > 0 || x2 > 0)) {
//       return true;
//     }
//     if ((x1 * x2 < 0 || y1 * y2 < 0) && slope == 0) {
//       return true;
//     }
//     if (x1 * x2 < 0 || y1 * y2 < 0) {
//       return false;
//     }
//     double squaredDistanceToLine =
//         (x1 * x1 + y1 * y1) - slope * slope * y1 * y1;
//     return squaredDistanceToLine < tolerance;
//   }

// // // Function to check if a point is inside a polygon
// // bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
// //   int intersectCount = 0;
// //   double tolerance = 1e-10;

// //   for (int i = 0; i < polygon.length; i++) {
// //     LatLng vertex1 = polygon[i];
// //     LatLng vertex2 = polygon[(i + 1) % polygon.length];

// //     // Check if the point is on the polygon edge
// //     if (_isPointOnSegment(point, vertex1, vertex2)) {
// //       return true;
// //     }

// //     // // Check if the ray from the point intersects with the polygon edge
// //     // if (_isRayIntersectingSegment(point, vertex1, vertex2, tolerance)) {
// //     //   intersectCount++;
// //     // }
// //   }

// //   // If the intersect count is odd, the point is inside the polygon
// //   return intersectCount % 2 == 1;
// // }



//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Color(0xFF0F9D58),
//         // title of app
//         title: Text("GFG"),
//       ),
//       body: SafeArea(
//         child: isLoading
//             ? Container(
//                 height: Dimensions.screenHeight,
//                 width: Dimensions.screenWidth,
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [CustomLoader()],
//                 ),
//               )
//             : Container(
//                 height: Dimensions.screenHeight,
//                 child: GoogleMap(
//                   //given camera position
//                   initialCameraPosition: _kGoogle,
//                   // on below line we have given map type
//                   mapType: MapType.normal,
//                   // on below line we have enabled location
//                   myLocationEnabled: true,
//                   myLocationButtonEnabled: true,
//                   // on below line we have enabled compass location
//                   compassEnabled: true,
//                   // on below line we have added polygon
//                   polygons: _polygon,
//                   markers: _markers,
//                   polylines: _polyline,
//                   // displayed google map
//                   onMapCreated: (GoogleMapController controller) {
//                     _controller.complete(controller);
//                   },
//                   onTap: (point) {
//                     onMap
//                         ?
//                         // Check if the point is inside the polygon
//                         _isPointInsidePolygon(point, points)
//                             ? setState(() {
//                                 print("The Point is: " + point.toString());
//                                 polygonLatLngs.add(point);
//                                 _setMarker(point);
//                                 _setPolyline();
//                               })
//                             : Fluttertoast.showToast(
//                                 msg: "The Point is Outside Mapping Area")
//                         : null;
//                   },
//                 ),
//               ),
//       ),
//     );
//   }
// }



// DRAWING DIVISIONS

// import 'dart:async';
// import 'dart:math' as math;
// import 'dart:math';
// import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
// import 'package:farmcapturev2/models/estate/divisionpolygon_model.dart';
// import 'package:farmcapturev2/pages/main_pages/estate/walk.dart';
// import 'package:farmcapturev2/resources/utils/colors.dart';
// import 'package:farmcapturev2/resources/widgets/big_text.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:get/get.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:google_maps_utils/utils/poly_utils.dart';
// import 'package:sqflite/sqflite.dart';
// import 'package:path/path.dart';
// import 'package:vector_math/vector_math.dart' as vectormath;
// //import 'package:latlong2/latlong.dart';

// import '../../../controllers/estates/estates_controller.dart';
// import '../../../models/estate/blocks_model.dart';
// import '../../../resources/base/custom_loader.dart';
// import '../../../resources/base/snackbar.dart';
// import '../../../resources/utils/dimensions.dart';
// import '../../../resources/widgets/small_text.dart';
// import 'all_blocks.dart';

// class DivisionsScreen extends StatefulWidget {
//   const DivisionsScreen({Key? key}) : super(key: key);

//   @override
//   _DivisionsScreenState createState() => _DivisionsScreenState();
// }

// class _DivisionsScreenState extends State<DivisionsScreen> {
//   var mainDivId = Get.arguments[0];
//   var blockId = Get.arguments[1];

//   List<List<LatLng>> _listBlocksPolygon = [];
//   List<Polygon> mapPolygonList = [];
//   List<List<Polygon>> _listBlocksPolygons = [];
//   List<dynamic> _divisionsPolygon = [];
//   bool isLoading = false;
//   bool onMap = false;
//   bool blocksEmpty = true;
//   late GoogleMapController _controller;
//   final Completer<GoogleMapController> _controllerCompleter = Completer();
//   LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);
//   //Default camera position
//   List<LatLng> points = [];
//   List<LatLng> pointsB = [];
//   List<LatLng> Blockpoints = [];

//   Set<Polygon> _polygon = Set<Polygon>();
//   Set<Marker> _markers = Set<Marker>();
//   Set<Polyline> _polyline = Set<Polyline>();
//   List<LatLng> polygonLatLngs = <LatLng>[];
//   List<LatLng> temppolygonLatLngs = <LatLng>[];
//   int _markerId = 1;
//   int _polylineIdCounter = 1;
//   LatLng? _point;
//   double area = 0.0;

//   List<dynamic> _blockslatlong = [];

//   List<dynamic> _alldivisionBlocks = [];
//   List<dynamic> _divisionBlocksId = [];

//   Future _getAllDivisionBlocks(var id) async {
//     // setState(() => isLoading = true);
//     print("At Function");

//     try {
//       _alldivisionBlocks =
//           await EstateController().getBlocksLocalById(blocksTable, id);
//       if (_alldivisionBlocks.isNotEmpty) {
//         _alldivisionBlocks.reversed;
//         print(_alldivisionBlocks[0]["name"]!.toString());
//         print(_alldivisionBlocks.length);

//         for (var element in _alldivisionBlocks) {
//           print("Id are: " + element['Id'].toString());
//           if (element['Id'].toString() != blockId) {
//             _divisionBlocksId.add(element['Id'].toString());
//           }
//         }

//         setState(() {});
//         await _getAllBlocskLatLngFromDatabase();

//         //setState(() => isLoading = false);

//         //print("List of Users: " + userModelList.length.toString());
//       } else {
//         print("Did not Get");
//       }
//     } catch (e) {
//       //setState(() => isLoading = false);
//     }
//   }

//   _getAllBlocskLatLngFromDatabase() async {
//     try {
//       print("BlockId is: " + blockId.toString());
//       int i = 0;
//       while (i < _divisionBlocksId.length) {
//         List<dynamic> _allblockslatlong = [];
//         List<LatLng> allBlockspoints = [];
//         var res = await EstateController().getAllBlockslatlongById(
//             mainDivId, _divisionBlocksId[i].toString());
//         if (res != null) {
//           setState(() {
//             _allblockslatlong = res;
//           });
//           if (_allblockslatlong.isNotEmpty) {
//             for (int j = 0; j < _allblockslatlong.length; j++) {
//               final lat = double.parse(_allblockslatlong[j]["latitude"]);
//               final lng = double.parse(_allblockslatlong[j]["longi"]);
//               allBlockspoints.add(LatLng(lat, lng));
//               pointsB.add(LatLng(lat, lng));
//               _listBlocksPolygon.add(allBlockspoints);
//             }
//           }
//           i++;
//         } else {
//           i++;
//         }
//       }
//       await _buildAllPolygons();
//       print("Blocks  are: " + _listBlocksPolygon.toString());
//     } catch (e) {}
//   }

//   _getBlockLatLngFromDatabase() async {
//     try {
//       //print("BlockId is: " + blockId.toString());
//       //List<dynamic> _blockslatlong = [];
//       _blockslatlong = await EstateController().getBlockslatlongById(
//         blockId,
//       );
//       if (_blockslatlong.isNotEmpty) {
//         for (int j = 0; j < _blockslatlong.length; j++) {
//           final lat = double.parse(_blockslatlong[j]["latitude"]);
//           final lng = double.parse(_blockslatlong[j]["longi"]);
//           Blockpoints.add(LatLng(lat, lng));
//         }
//       }
//       await _getPolyline();
//       //print("Block Polygon is: " + Blockpoints.toString());
//     } catch (e) {}
//   }

//   _getPolyline() {
//     setState(() {
//       blocksEmpty = false;
//     });
//     for (var element in Blockpoints) {
//       polygonLatLngs.add(element);
//       //_setMarker(element);
//       _setPolyline(element);
//     }
//   }

//   _getDivisionLatLngFromDatabase(var id) async {
//     setState(() => isLoading = true);
//     //print("At Function");

//     try {
//       _divisionsPolygon = await EstateController().getlatlongById(id);
//       if (_divisionsPolygon.isNotEmpty) {
//         //print(_divisionsPolygon[0]["latitude"].toString());
//         for (int i = 0; i < _divisionsPolygon.length; i++) {
//           final lat = double.parse(_divisionsPolygon[i]["latitude"]);
//           final lng = double.parse(_divisionsPolygon[i]["longi"]);

//           points.add(LatLng(lat, lng));
//         }
//         //print("Division Polygon is: " + points.toString());
//         await _setPolygon();
//         await _getBlockLatLngFromDatabase();
//         await _getAllDivisionBlocks(mainDivId);
//         _getLatLngCameraPosition().then((latLng) {
//           setState(() {
//             _cameraPosition = latLng;
//           });
//         });
//         setState(() => isLoading = false);
//       } else {
//         print("Did not Get");
//       }
//     } catch (e) {}
//   }

//   _setPolygon() async {
//     Polygon pol = Polygon(
//       polygonId: PolygonId('Division_Polygon'),
//       points: points,
//       strokeWidth: 2,
//       strokeColor: Colors.green,
//       fillColor: Colors.transparent,
//     );

//     List<Polygon> lispol = [];
//     lispol.add(pol);
//     _listBlocksPolygons.add(lispol);
//   }

//   Future<LatLng> _getLatLngCameraPosition() async {
//     final lat = double.parse(_divisionsPolygon.first['latitude']);
//     final long = double.parse(_divisionsPolygon.first['longi']);
//     return LatLng(lat, long);
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     _controller = controller;
//     _controllerCompleter.complete(controller);
//   }

//   // Function to check if a point is inside a polygon
//   bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
//     bool inside = false;
//     //bool inside2 = false;
//     //double tolerance = 1;
//     for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
//       if (((polygon[i].latitude > point.latitude) !=
//               (polygon[j].latitude > point.latitude)) &&
//           (point.longitude <
//               (polygon[j].longitude - polygon[i].longitude) *
//                       (point.latitude - polygon[i].latitude) /
//                       (polygon[j].latitude - polygon[i].latitude) +
//                   polygon[i].longitude)) {
//         inside = !inside;
//       }
//     }

//     print("Is In 1: ${inside}");

//     return inside;
//   }

//   bool _isPointInsidePolygonBlock(LatLng point) {
//     bool inside2 = false;
//     for (int i = 0, j = pointsB.length - 1; i < pointsB.length; j = i++) {
//       if (((pointsB[i].latitude > point.latitude) !=
//               (pointsB[j].latitude > point.latitude)) &&
//           (point.longitude <
//               (pointsB[j].longitude - pointsB[i].longitude) *
//                       (point.latitude - pointsB[i].latitude) /
//                       (pointsB[j].latitude - pointsB[i].latitude) +
//                   pointsB[i].longitude)) {
//         inside2 = !inside2;
//       }
//     }

//     return inside2;
//   }

//   math.Point<double> convertLatLngToPoint(LatLng latLng, double zoom) {
//     final double scale = double.parse(math.pow(2, zoom).toString());
//     final double worldCoordinateX = latLng.longitude / 360 * scale;
//     final double worldCoordinateY = 0.5 -
//         math.log(
//                 math.tan(math.pi / 4 + latLng.latitude * (math.pi / 180) / 2)) /
//             (2 * math.pi) *
//             scale;
//     return math.Point(worldCoordinateX, worldCoordinateY);
//   }

//   bool _validPolyline(LatLng location) {
//     bool hasCrossed = false;
//     for (int i = 1; i < polygonLatLngs.length; i++) {
//       //if (_polyline.isNotEmpty) LatLng start = _polyline[i].points.last;
//       Point start = convert(polygonLatLngs[i - 1], 1);
//       Point end = convert(polygonLatLngs[i], 1);
//       for (int j = 0; j < _listBlocksPolygon.length; j++) {
//         for (int i = 0; i < _listBlocksPolygon[j].length; i++) {
//           Point point = convert(_listBlocksPolygon[j][i], 1);
//           if (PolyUtils.containsLocationPoly(point, [start, end])) {
//             hasCrossed = true;
//           }
//         }
//       }
//     }
//     print('Has crossed: $hasCrossed');
//     return hasCrossed;
//   }

//   Point<double> convert(LatLng latLng, double zoom) {
//     final size = math.pow(2, zoom + 8);
//     final x = ((latLng.longitude + 180) / 360) * size;
//     final y = ((1 -
//                 math.log(math.tan(latLng.latitude * math.pi / 180) +
//                         1 / math.cos(latLng.latitude * math.pi / 180)) /
//                     math.pi) /
//             2) *
//         size;
//     return Point<double>(x, y);
//   }

//   void _setMarker(LatLng point) {
//     setState(() {
//       _markers.add(
//           Marker(markerId: MarkerId(_markerId.toString()), position: point));
//       _markerId++;
//     });
//     print('Markers are: ${_markers.length}');
//     print('PolyLatLangs are: ${polygonLatLngs.length}');
//   }

//   void _removeMarker() {
//     if (_markers.isNotEmpty && _polyline.isNotEmpty) {
//       setState(() {
//         _markers.remove(_markers.last);
//         _polyline.remove(_polyline.last);
//         polygonLatLngs.remove(polygonLatLngs.last);
//       });
//     }

//     print('Markers are: ${_markers.length}');
//     print('PolyLatLangs are: ${polygonLatLngs.length}');
//   }

//   void _setPolyline(LatLng location) {
//     final Polyline polyline = Polyline(
//       polylineId: PolylineId(_polyline.length.toString()),
//       points: [if (_polyline.isNotEmpty) _polyline.last.points.last, location],
//       color: Colors.blue,
//       width: 2,
//     );

//     setState(() {
//       _polyline.add(polyline);
//     });
//   }

//   void _deletePoint() {
//     if (polygonLatLngs.isEmpty || _markers.isEmpty || _polyline.isEmpty) {
//       Fluttertoast.showToast(msg: "You did not select any points");
//     } else {
//       setState(() {
//         _removeMarker();
//         //_removePolyline(_point!);
//       });
//     }
//   }

//   double calculateArea(List<LatLng> coords) {
//     int len = coords.length;
//     double sum = 0;
//     for (int i = 0; i < len; i++) {
//       LatLng p1 = coords[i];
//       LatLng p2 = coords[(i + 1) % len];
//       sum += (p2.longitude - p1.longitude) * (p2.latitude + p1.latitude);
//     }
//     return (sum.abs() / 2) *
//         111319.9 *
//         111319.9; // Multiply by Earth's radius to get the area in square meters
//   }

//   _restart(String id) async {
//     await EstateController().deleteBlockById(id);

//     setState(() {
//       _markers.clear();
//       _polyline.clear();
//       polygonLatLngs.clear();
//       area = 0.0;
//     });
//     Get.back();
//   }

//   _saveBlocks() async {
//     List<dynamic> divBlocks = [];
//     setState(() {
//       area = calculateArea(polygonLatLngs);
//     });
//     for (var element in polygonLatLngs) {
//       DivisionPolygonResultElement divPoly = DivisionPolygonResultElement();
//       print(element.latitude);
//       divPoly.mainDivisionId = int.parse(mainDivId);
//       divPoly.latitude = element.latitude.toString();
//       divPoly.longi = element.longitude.toString();
//       divPoly.newDevBlockId = blockId;
//       divPoly.blockArea = area.toString();
//       divPoly.syncId = 'p';
//       divPoly.polygonId = mainDivId;
//       divPoly.isDivision = 0;

//       divBlocks.add(divPoly);
//     }

//     print("The Block is: " + divBlocks.toString());

//     int i = await EstateController().saveAll(divBlocks, divisionPolygonTable);

//     if (i >= 0) {
//       BlocksModelResultElement blocksModel = BlocksModelResultElement();
//       blocksModel.isPlotted = "Yes";
//       int j = await EstateController()
//           .updateBlockLocal(blocksTable, "is_plotted", "Yes", 'Id', blockId);
//       if (j >= 0) {
//         print("Success here AT BLOCKS");
//       }

//       // showCustomSnackBar(
//       //     "Block saved successfully", context, ContentType.success,
//       //     title: "Success");
//       Get.back();
//     }
//   }

// //   void _checkPolylineCrossesPolygon() async {
// //   //final GoogleMapController controller = await _controller.future;
// //   for (int i = 1; i < polygonLatLngs.length; i++) {
// //     Point start = convert(polygonLatLngs[i - 1], 1);
// //       Point end = convert(polygonLatLngs[i], 1);
// //     for (int j = 0; j < Blockpoints.length; j++) {
// //       LatLng polygonVertex1 = Blockpoints[j];
// //       LatLng polygonVertex2 = Blockpoints[(j + 1) % Blockpoints.length];
// //       if (poly.isLocationOnEdge(
// //         LatLng(start.latitude, start.longitude),
// //         LatLng(polygonVertex1.latitude, polygonVertex1.longitude),
// //         LatLng(polygonVertex2.latitude, polygonVertex2.longitude),
// //         tolerance: 0.0001,
// //       )) {
// //         print('Polyline intersects with polygon');
// //         return;
// //       }
// //     }
// //   }
// //   print('Polyline does not intersect with polygon');
// // }

//   bool willPolylineCrossPolygon1(
//       LatLng point1, List<LatLng> polylinePoints, List<LatLng> polygonPoints) {
//     print("Point is: $point1");
//     print("Points Here are: $polylinePoints");
//     polylinePoints.add(point1);
//     for (int i = 0; i < polylinePoints.length - 1; i++) {
//       LatLng point1 = polylinePoints[i];
//       LatLng point2 = polylinePoints[i + 1];
//       print("Points Are: $point1 And $point2");
//       for (int j = 0; j < polygonPoints.length - 1; j++) {
//         LatLng vertex1 = polygonPoints[j];
//         LatLng vertex2 = polygonPoints[j + 1];
//         if (doLineSegmentsIntersect(point1, point2, vertex1, vertex2)) {
//           return true;
//         }
//       }
//     }

//     return false;
//   }

//   bool willPolylineCrossPolygon(
//       LatLng point1, LatLng point2, List<LatLng> polygonPoints) {
//     for (int j = 0; j < polygonPoints.length - 1; j++) {
//       LatLng vertex1 = polygonPoints[j];
//       LatLng vertex2 = polygonPoints[j + 1];
//       if (doLineSegmentsIntersect(point1, point2, vertex1, vertex2)) {
//         return true;
//       }
//     }

//     return false;
//   }

//   bool doLineSegmentsIntersect(LatLng p1, LatLng q1, LatLng p2, LatLng q2) {
//     double dx1 = q1.latitude - p1.latitude;
//     double dy1 = q1.longitude - p1.longitude;
//     double dx2 = q2.latitude - p2.latitude;
//     double dy2 = q2.longitude - p2.longitude;
//     double delta = dx2 * dy1 - dy2 * dx1;
//     if (delta == 0) return false;

//     double ua = ((p2.longitude - p1.longitude) * dy1 -
//             (p2.latitude - p1.latitude) * dx1) /
//         delta;
//     double ub = ((p2.longitude - p1.longitude) * dy2 -
//             (p2.latitude - p1.latitude) * dx2) /
//         delta;
//     return (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1);
//   }

//   bool isPolylineCrossingPolygon(
//       LatLng point1, LatLng point2, List<List<Polygon>> polygonList) {
//     final Polyline polyline = Polyline(
//       polylineId: PolylineId(_polyline.length.toString()),
//       points: [point1, point2],
//       color: Colors.blue,
//       width: 2,
//     );

//     for (var element in polygonList) {
//       for (var element1 in element) {
//         Polygon polygon = element1;
//         for (int i = 0; i < polygon.points.length - 1; i++) {
//           LatLng p1 = polygon.points[i];
//           LatLng p2 = polygon.points[i + 1];

//           for (int j = 0; j < polyline.points.length - 1; j++) {
//             LatLng q1 = polyline.points[j];
//             LatLng q2 = polyline.points[j + 1];

//             if (doSegmentsIntersect(p1, p2, q1, q2)) {
//               return true;
//             }
//           }
//         }
//       }
//     }
//     return false;
//   }

//   bool doSegmentsIntersect(LatLng p1, LatLng p2, LatLng q1, LatLng q2) {
//     double crossProduct(LatLng a, LatLng b, LatLng c) {
//       return (b.latitude - a.latitude) * (c.longitude - a.longitude) -
//           (b.longitude - a.longitude) * (c.latitude - a.latitude);
//     }

//     double direction(LatLng a, LatLng b, LatLng c) {
//       return crossProduct(a, b, c);
//     }

//     bool onSegment(LatLng a, LatLng b, LatLng c) {
//       return (c.latitude >= a.latitude && c.latitude <= b.latitude) ||
//           (c.latitude <= a.latitude && c.latitude >= b.latitude);
//     }

//     double d1 = direction(q1, q2, p1);
//     double d2 = direction(q1, q2, p2);
//     double d3 = direction(p1, p2, q1);
//     double d4 = direction(p1, p2, q2);

//     if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
//         ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
//       return true;
//     } else if (d1 == 0 && onSegment(q1, q2, p1)) {
//       return true;
//     } else if (d2 == 0 && onSegment(q1, q2, p2)) {
//       return true;
//     } else if (d3 == 0 && onSegment(p1, p2, q1)) {
//       return true;
//     } else if (d4 == 0 && onSegment(p1, p2, q2)) {
//       return true;
//     }

//     return false;
//   }

//   // bool isPolylineCrossingPolygon(LatLng point1, LatLng point2, List<LatLng> polygon) {
//   //   print(point1.toString());
//   //   print(point2.toString());
//   //   int polygonSides = polygon.length;
//   //   bool inside = false;
//   //   double cross1, cross2;

//   //   for (int i = 0; i < polygonSides; i++) {
//   //     LatLng polygonPoint1 = polygon[i];
//   //     LatLng polygonPoint2 = polygon[(i + 1) % polygonSides];
//   //     cross1 = (polygonPoint2.latitude - polygonPoint1.latitude) *
//   //             (point1.longitude - polygonPoint1.longitude) -
//   //         (polygonPoint2.longitude - polygonPoint1.longitude) *
//   //             (point1.latitude - polygonPoint1.latitude);
//   //     cross2 = (polygonPoint2.latitude - polygonPoint1.latitude) *
//   //             (point2.longitude - polygonPoint1.longitude) -
//   //         (polygonPoint2.longitude - polygonPoint1.longitude) *
//   //             (point2.latitude - polygonPoint1.latitude);
//   //     if (cross2 == 0) {
//   //       continue;
//   //     }
//   //     if (cross1 * cross2 < 0) {
//   //       continue;
//   //     }
//   //     if (cross1 > 0) {
//   //       continue;
//   //     }
//   //     inside = !inside;
//   //   }

//   //   if (inside) {
//   //     return true;
//   //   }

//   //   for (int i = 0; i < polygonSides; i++) {
//   //     LatLng polygonPoint1 = polygon[i];
//   //     LatLng polygonPoint2 = polygon[(i + 1) % polygonSides];
//   //     cross1 = (point2.latitude - point1.latitude) *
//   //             (polygonPoint1.longitude - point1.longitude) -
//   //         (point2.longitude - point1.longitude) *
//   //             (polygonPoint1.latitude - point1.latitude);
//   //     cross2 = (point2.latitude - point1.latitude) *
//   //             (polygonPoint2.longitude - point1.longitude) -
//   //         (point2.longitude - point1.longitude) *
//   //             (polygonPoint2.latitude - point1.latitude);
//   //     if (cross2 == 0) {
//   //       continue;
//   //     }
//   //     if (cross1 * cross2 < 0) {
//   //       continue;
//   //     }
//   //     if (cross1 > 0) {
//   //       continue;
//   //     }
//   //     return true;
//   //   }

//   //   return false;
//   // }

// // CHECK IF POLYGON INTERSECTS ITSELF
//   bool doesPolylineIntersectPolygon(
//       LatLng newPoint, List<LatLng> polygonPoints) {
//     LatLng lastPointOfPolygon = polygonPoints.last;

//     for (int i = 0; i < polygonPoints.length - 2; i++) {
//       LatLng p1 = polygonPoints[i];
//       LatLng p2 = polygonPoints[i + 1];

//       if (doSegmentsIntersect1(p1, p2, lastPointOfPolygon, newPoint)) {
//         return true;
//       }
//     }

//     return false;
//   }

//   bool doSegmentsIntersect1(LatLng p1, LatLng p2, LatLng q1, LatLng q2) {
//     double crossProduct(LatLng a, LatLng b, LatLng c) {
//       return (b.latitude - a.latitude) * (c.longitude - a.longitude) -
//           (b.longitude - a.longitude) * (c.latitude - a.latitude);
//     }

//     double direction(LatLng a, LatLng b, LatLng c) {
//       return crossProduct(a, b, c);
//     }

//     bool onSegment(LatLng a, LatLng b, LatLng c) {
//       return (c.latitude >= a.latitude && c.latitude <= b.latitude) ||
//           (c.latitude <= a.latitude && c.latitude >= b.latitude);
//     }

//     double d1 = direction(q1, q2, p1);
//     double d2 = direction(q1, q2, p2);
//     double d3 = direction(p1, p2, q1);
//     double d4 = direction(p1, p2, q2);

//     if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
//         ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
//       return true;
//     } else if (d1 == 0 && onSegment(q1, q2, p1)) {
//       return true;
//     } else if (d2 == 0 && onSegment(q1, q2, p2)) {
//       return true;
//     } else if (d3 == 0 && onSegment(p1, p2, q1)) {
//       return true;
//     } else if (d4 == 0 && onSegment(p1, p2, q2)) {
//       return true;
//     }

//     return false;
//   }

//   @override
//   void initState() {
//     super.initState();

//   //   Future.delayed(Duration.zero, () {
//   //     this._getCategories();
//   //  });
//     //_getDivisionLatLngFromDatabase(mainDivId);
//   }

//   @override
// void didChangeDependencies() {
//   super.didChangeDependencies();

//   _getDivisionLatLngFromDatabase(mainDivId);
// }

//   @override
//   Widget build(BuildContext context) {
//     // return Scaffold(
//     //   extendBodyBehindAppBar: true,
//     //   appBar: AppBar(
//     //     backgroundColor: Colors.transparent,
//     //     // actions: [
//     //     //   Container(
//     //     //     width: Dimensions.screenWidth,
//     //     //     height: Dimensions.height30 * 2,
//     //     //     padding: EdgeInsets.only(
//     //     //       left: Dimensions.width15,
//     //     //       right: Dimensions.width15,
//     //     //     ),
//     //     //     decoration: BoxDecoration(
//     //     //       color: Colors.green,
//     //     //       boxShadow: [
//     //     //         BoxShadow(
//     //     //             blurRadius: 3,
//     //     //             offset: Offset(5, 5),
//     //     //             color: AppColors.gradientOne.withOpacity(0.1)),
//     //     //         BoxShadow(
//     //     //             blurRadius: 3,
//     //     //             offset: Offset(-5, -5),
//     //     //             color: AppColors.gradientOne.withOpacity(0.1))
//     //     //       ],
//     //     //     ),
//     //     //     child: Row(
//     //     //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     //     //       children: [
//     //     //         IconButton(
//     //     //           icon: Icon(
//     //     //             Icons.arrow_back_ios,
//     //     //             color: Colors.white,
//     //     //             size: Dimensions.iconSize24,
//     //     //           ),
//     //     //           onPressed: () {
//     //     //             Get.to(() => AllBlockScreen(), arguments: mainDivId);
//     //     //           },
//     //     //         ),
//     //     //         BigText(
//     //     //           text: "Map Farm",
//     //     //           color: Colors.black,
//     //     //           size: Dimensions.font16,
//     //     //         ),
//     //     //         SizedBox()
//     //     //       ],
//     //     //     ),
//     //     //   ),
//     //     // ],
//     //   ),
//     //   body: isLoading
//     //       ? Container(
//     //           height: Dimensions.screenHeight,
//     //           width: Dimensions.screenWidth,
//     //           child: Column(
//     //             mainAxisAlignment: MainAxisAlignment.center,
//     //             children: [CustomLoader()],
//     //           ),
//     //         )
//           // : SafeArea(
//           //   child: Stack(
//           //       children: [
//           //         GoogleMap(
//           //           onMapCreated: _onMapCreated,
//           //           initialCameraPosition: CameraPosition(
//           //             target: _cameraPosition,
//           //             zoom: 13.0,
//           //           ),
//           //           mapType: MapType.satellite,
//           //           markers: _markers,
//           //           polylines: _polyline,
//           //           polygons: _listBlocksPolygons.expand((i) => i).toSet(),
//           //           // Set<Polygon>.of([_buildPolygon1(), _buildPolygon2()]),
//           //           onTap: (point) {
//           //             onMap
//           //                 ? blocksEmpty
//           //                     ? _isPointInsidePolygon(point, points)
//           //                         ? !_isPointInsidePolygonBlock(point)
//           //                             ? polygonLatLngs.isEmpty
//           //                                 ? setState(() {
//           //                                     print("The Point is: " +
//           //                                         point.toString());
//           //                                     _point = point;
//           //                                     polygonLatLngs.add(point);
//           //                                     _setMarker(point);
//           //                                     _setPolyline(point);
//           //                                   })
//           //                                 //: willPolylineCrossPolygon1(point, polygonLatLngs, pointsB)
//           //                                 : isPolylineCrossingPolygon(
//           //                                         point,
//           //                                         polygonLatLngs.last,
//           //                                         _listBlocksPolygons)
//           //                                     ? Fluttertoast.showToast(
//           //                                         msg:
//           //                                             "The Point is Crossing a Polygon")
//           //                                     : polygonLatLngs.length >= 3 ?
//           //                                        doesPolylineIntersectPolygon(point, polygonLatLngs)
//           //                                         ? Fluttertoast.showToast(msg: "The Polygon is Crossing Itself")
//           //                                         : setState(() {
//           //                                             print("The Point is: " +
//           //                                                 point.toString());
//           //                                             _point = point;
//           //                                             polygonLatLngs.add(point);
//           //                                             _setMarker(point);
//           //                                             _setPolyline(point);
//           //                                           }): setState(() {
//           //                                             print("The Point is: " +
//           //                                                 point.toString());
//           //                                             _point = point;
//           //                                             polygonLatLngs.add(point);
//           //                                             _setMarker(point);
//           //                                             _setPolyline(point);
//           //                                           })
//           //                             : Fluttertoast.showToast(
//           //                                 msg:
//           //                                     "The Point is Inside Another Polygon")
//           //                         : Fluttertoast.showToast(
//           //                             msg: "The Point is Outside Mapping Area")
//           //                     : showDialog(
//           //                         barrierDismissible: false,
//           //                         context: context,
//           //                         builder: (BuildContext context) {
//           //                           return Dialog(
//           //                             shape: RoundedRectangleBorder(
//           //                               borderRadius: BorderRadius.circular(20),
//           //                             ),
//           //                             elevation: 0,
//           //                             backgroundColor: Colors.transparent,
//           //                             child: confirm(context),
//           //                           );
//           //                         })
//           //                 : Fluttertoast.showToast(
//           //                     msg: "You did not select START MAPPING");
//           //           },
//           //         ),
//           //         Container(
//           //           padding: EdgeInsets.all(Dimensions.height15),
//           //           height: Dimensions.height15 * Dimensions.height15,
//           //           width: (Dimensions.screenWidth / 2) +
//           //               Dimensions.width10 * Dimensions.width15,
//           //           margin: EdgeInsets.only(
//           //               top: Dimensions.height10, left: Dimensions.width10),
//           //           // decoration: BoxDecoration(
//           //           //   color: Colors.grey[700],
//           //           //   borderRadius: BorderRadius.circular(Dimensions.radius15),
//           //           // ),
//           //           child: Column(
//           //             mainAxisAlignment: MainAxisAlignment.start,
//           //             crossAxisAlignment: CrossAxisAlignment.start,
//           //             children: [
//           //               Center(
//           //                   child: Text(
//           //                 "Tap Mark to get Location Co-ordinates",
//           //                 style: TextStyle(
//           //                     color: Colors.black,
//           //                     fontSize: Dimensions.font16 - 2,
//           //                     fontWeight: FontWeight.bold),
//           //               )),
//           //               SizedBox(
//           //                 height: Dimensions.height10 / 2,
//           //               ),
//           //               Row(
//           //                 children: [
//           //                   GestureDetector(
//           //                     onTap: () {
//           //                       setState(() {
//           //                         showDialog(
//           //                             barrierDismissible: false,
//           //                             context: context,
//           //                             builder: (BuildContext context) {
//           //                               return Dialog(
//           //                                 shape: RoundedRectangleBorder(
//           //                                   borderRadius:
//           //                                       BorderRadius.circular(20),
//           //                                 ),
//           //                                 elevation: 0,
//           //                                 backgroundColor: Colors.transparent,
//           //                                 child: mapWith(context),
//           //                               );
//           //                             });
//           //                       });
//           //                     },
//           //                     child: Container(
//           //                       height: Dimensions.height20 + Dimensions.height30,
//           //                       padding: EdgeInsets.only(
//           //                         left: Dimensions.width10,
//           //                         right: Dimensions.width10,
//           //                       ),
//           //                       decoration: BoxDecoration(
//           //                         color: Colors.green,
//           //                         borderRadius: BorderRadius.circular(
//           //                             Dimensions.radius15 / 2),
//           //                       ),
//           //                       child: Center(
//           //                           child: SmallText(
//           //                         text: "START MAPPING",
//           //                         color: Colors.black,
//           //                         size: Dimensions.font12,
//           //                       )),
//           //                     ),
//           //                   ),
//           //                   SizedBox(
//           //                     width: Dimensions.width30,
//           //                   ),
//           //                   GestureDetector(
//           //                     onTap: () async {
//           //                       //dialog to choose mode of mapping
//           //                       onMap
//           //                           ? showDialog(
//           //                               barrierDismissible: false,
//           //                               context: context,
//           //                               builder: (BuildContext context) {
//           //                                 return Dialog(
//           //                                   shape: RoundedRectangleBorder(
//           //                                     borderRadius:
//           //                                         BorderRadius.circular(20),
//           //                                   ),
//           //                                   elevation: 0,
//           //                                   backgroundColor: Colors.transparent,
//           //                                   child: contentBox(context),
//           //                                 );
//           //                               })
//           //                           : Fluttertoast.showToast(
//           //                               msg: "You did not select START MAPPING");
//           //                     },
//           //                     child: Container(
//           //                       height: Dimensions.height20 + Dimensions.height30,
//           //                       padding: EdgeInsets.only(
//           //                         left: Dimensions.width10,
//           //                         right: Dimensions.width10,
//           //                       ),
//           //                       decoration: BoxDecoration(
//           //                         color: Colors.grey,
//           //                         borderRadius: BorderRadius.circular(
//           //                             Dimensions.radius15 / 2),
//           //                       ),
//           //                       child: Center(
//           //                           child: SmallText(
//           //                         text: "RESTART",
//           //                         color: Colors.black,
//           //                         size: Dimensions.font12,
//           //                       )),
//           //                     ),
//           //                   ),
//           //                   SizedBox(
//           //                     width: Dimensions.width30,
//           //                   ),
//           //                   GestureDetector(
//           //                     onTap: () {
//           //                       setState(() {
//           //                         onMap
//           //                             ? _deletePoint()
//           //                             : Fluttertoast.showToast(
//           //                                 msg:
//           //                                     "You did not select START MAPPING");
//           //                       });
//           //                     },
//           //                     child: Icon(
//           //                       Icons.delete_outline_sharp,
//           //                       color: AppColors.buttonRed,
//           //                       size: Dimensions.iconSize24 * 2,
//           //                     ),
//           //                   ),
//           //                 ],
//           //               ),
//           //               SizedBox(
//           //                 height: Dimensions.height10,
//           //               ),
//           //               Row(
//           //                 mainAxisAlignment: MainAxisAlignment.start,
//           //                 children: [
//           //                   GestureDetector(
//           //                     onTap: () {
//           //                       onMap
//           //                           ? _saveBlocks()
//           //                           : Fluttertoast.showToast(msg: "You did not select START MAPPING");
//           //                     },
//           //                     child: Container(
//           //                       height: Dimensions.height20 + Dimensions.height30,
//           //                       width: Dimensions.width10 * Dimensions.width10,
//           //                       decoration: BoxDecoration(
//           //                         color: Colors.grey,
//           //                         borderRadius: BorderRadius.circular(0),
//           //                       ),
//           //                       child: Center(
//           //                           child: SmallText(
//           //                         text: "COMPLETE",
//           //                         color: Colors.black,
//           //                         size: Dimensions.font12,
//           //                       )),
//           //                     ),
//           //                   ),
//           //                   Column(
//           //                     children: [
//           //                       BigText(
//           //                         text: "Total Acreage (m2)",
//           //                         color: Colors.black,
//           //                         size: Dimensions.font12,
//           //                       ),
//           //                       BigText(
//           //                         text: area.toString(),
//           //                         color: Colors.black,
//           //                         size: Dimensions.font12,
//           //                       )
//           //                     ],
//           //                   )
//           //                 ],
//           //               )
//           //             ],
//           //           ),
//           //         ),
//           //       ],
//           //     ),
//           // ),
//     // );
//     return Scaffold(
//       extendBodyBehindAppBar: true,
//       appBar: AppBar(
//         backgroundColor: Colors.transparent,
//       ),
//       // appBar: AppBar(
//       //   actions: [
//       //     Container(
//       //       width: Dimensions.screenWidth,
//       //       height: Dimensions.height30 * 2,
//       //       padding: EdgeInsets.only(
//       //         left: Dimensions.width15,
//       //         right: Dimensions.width15,
//       //       ),
//       //       decoration: BoxDecoration(
//       //         color: Colors.green,
//       //         boxShadow: [
//       //           BoxShadow(
//       //               blurRadius: 3,
//       //               offset: Offset(5, 5),
//       //               color: AppColors.gradientOne.withOpacity(0.1)),
//       //           BoxShadow(
//       //               blurRadius: 3,
//       //               offset: Offset(-5, -5),
//       //               color: AppColors.gradientOne.withOpacity(0.1))
//       //         ],
//       //       ),
//       //       child: Row(
//       //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       //         children: [
//       //           IconButton(
//       //             icon: Icon(
//       //               Icons.arrow_back_ios,
//       //               color: Colors.white,
//       //               size: Dimensions.iconSize24,
//       //             ),
//       //             onPressed: () {
//       //               Get.to(() => AllBlockScreen(), arguments: mainDivId);
//       //             },
//       //           ),
//       //           BigText(
//       //             text: "Map Farm",
//       //             color: Colors.black,
//       //             size: Dimensions.font16,
//       //           ),
//       //           SizedBox()
//       //         ],
//       //       ),
//       //     ),
//       //   ],
//       // ),
//       body: isLoading
//       ? Container(
//           height: Dimensions.screenHeight,
//           width: Dimensions.screenWidth,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [CustomLoader()],
//           ),
//         )
//         :
//       Stack(
//         children: [
//           GoogleMap(
//             onMapCreated: _onMapCreated,
//             initialCameraPosition: CameraPosition(
//               target: _cameraPosition,
//               zoom: 13.0,
//             ),
//             mapType: MapType.satellite,
//             markers: _markers,
//             polylines: _polyline,
//             polygons: _listBlocksPolygons.expand((i) => i).toSet(),
//             // Set<Polygon>.of([_buildPolygon1(), _buildPolygon2()]),
//             onTap: (point) {
//               onMap
//                   ? blocksEmpty
//                       ? _isPointInsidePolygon(point, points)
//                           ? !_isPointInsidePolygonBlock(point)
//                               ? polygonLatLngs.isEmpty
//                                   ? setState(() {
//                                       print("The Point is: " +
//                                           point.toString());
//                                       _point = point;
//                                       polygonLatLngs.add(point);
//                                       _setMarker(point);
//                                       _setPolyline(point);
//                                     })
//                                   //: willPolylineCrossPolygon1(point, polygonLatLngs, pointsB)
//                                   : isPolylineCrossingPolygon(
//                                           point,
//                                           polygonLatLngs.last,
//                                           _listBlocksPolygons)
//                                       ? Fluttertoast.showToast(
//                                           msg:
//                                               "The Point is Crossing a Polygon")
//                                       : polygonLatLngs.length >= 3 ?
//                                           doesPolylineIntersectPolygon(point, polygonLatLngs)
//                                           ? Fluttertoast.showToast(msg: "The Polygon is Crossing Itself")
//                                           : setState(() {
//                                               print("The Point is: " +
//                                                   point.toString());
//                                               _point = point;
//                                               polygonLatLngs.add(point);
//                                               _setMarker(point);
//                                               _setPolyline(point);
//                                             }): setState(() {
//                                               print("The Point is: " +
//                                                   point.toString());
//                                               _point = point;
//                                               polygonLatLngs.add(point);
//                                               _setMarker(point);
//                                               _setPolyline(point);
//                                             })
//                               : Fluttertoast.showToast(
//                                   msg:
//                                       "The Point is Inside Another Polygon")
//                           : Fluttertoast.showToast(
//                               msg: "The Point is Outside Mapping Area")
//                       : showDialog(
//                           barrierDismissible: false,
//                           context: context,
//                           builder: (BuildContext context) {
//                             return Dialog(
//                               shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(20),
//                               ),
//                               elevation: 0,
//                               backgroundColor: Colors.transparent,
//                               child: confirm(context),
//                             );
//                           })
//                   : Fluttertoast.showToast(
//                       msg: "You did not select START MAPPING");
//             },
//           ),
//           Positioned(
//             top: 50,
//             child: Container(
//               padding: EdgeInsets.all(Dimensions.height15),
//               height: Dimensions.height15 * Dimensions.height15,
//               width: (Dimensions.screenWidth / 2) +
//                   Dimensions.width10 * Dimensions.width15,
//               margin: EdgeInsets.only(
//                   top: Dimensions.height10, left: Dimensions.width10),
//               // decoration: BoxDecoration(
//               //   color: Colors.grey[700],
//               //   borderRadius: BorderRadius.circular(Dimensions.radius15),
//               // ),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Center(
//                       child: Text(
//                     "Tap Mark to get Location Co-ordinates",
//                     style: TextStyle(
//                         color: Colors.black,
//                         fontSize: Dimensions.font16 - 2,
//                         fontWeight: FontWeight.bold),
//                   )),
//                   SizedBox(
//                     height: Dimensions.height10 / 2,
//                   ),
//                   Row(
//                     children: [
//                       GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             showDialog(
//                                 barrierDismissible: false,
//                                 context: context,
//                                 builder: (BuildContext context) {
//                                   return Dialog(
//                                     shape: RoundedRectangleBorder(
//                                       borderRadius:
//                                           BorderRadius.circular(20),
//                                     ),
//                                     elevation: 0,
//                                     backgroundColor: Colors.transparent,
//                                     child: mapWith(context),
//                                   );
//                                 });
//                           });
//                         },
//                         child: Container(
//                           height: Dimensions.height20 + Dimensions.height30,
//                           padding: EdgeInsets.only(
//                             left: Dimensions.width10,
//                             right: Dimensions.width10,
//                           ),
//                           decoration: BoxDecoration(
//                             color: Colors.green,
//                             borderRadius: BorderRadius.circular(
//                                 Dimensions.radius15 / 2),
//                           ),
//                           child: Center(
//                               child: SmallText(
//                             text: "START MAPPING",
//                             color: Colors.black,
//                             size: Dimensions.font12,
//                           )),
//                         ),
//                       ),
//                       SizedBox(
//                         width: Dimensions.width30,
//                       ),
//                       GestureDetector(
//                         onTap: () async {
//                           //dialog to choose mode of mapping
//                           onMap
//                               ? showDialog(
//                                   barrierDismissible: false,
//                                   context: context,
//                                   builder: (BuildContext context) {
//                                     return Dialog(
//                                       shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                             BorderRadius.circular(20),
//                                       ),
//                                       elevation: 0,
//                                       backgroundColor: Colors.transparent,
//                                       child: contentBox(context),
//                                     );
//                                   })
//                               : Fluttertoast.showToast(
//                                   msg: "You did not select START MAPPING");
//                         },
//                         child: Container(
//                           height: Dimensions.height20 + Dimensions.height30,
//                           padding: EdgeInsets.only(
//                             left: Dimensions.width10,
//                             right: Dimensions.width10,
//                           ),
//                           decoration: BoxDecoration(
//                             color: Colors.grey,
//                             borderRadius: BorderRadius.circular(
//                                 Dimensions.radius15 / 2),
//                           ),
//                           child: Center(
//                               child: SmallText(
//                             text: "RESTART",
//                             color: Colors.black,
//                             size: Dimensions.font12,
//                           )),
//                         ),
//                       ),
//                       SizedBox(
//                         width: Dimensions.width30,
//                       ),
//                       GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             onMap
//                                 ? _deletePoint()
//                                 : Fluttertoast.showToast(
//                                     msg:
//                                         "You did not select START MAPPING");
//                           });
//                         },
//                         child: Icon(
//                           Icons.delete_outline_sharp,
//                           color: AppColors.buttonRed,
//                           size: Dimensions.iconSize24 * 2,
//                         ),
//                       ),
//                     ],
//                   ),
//                   SizedBox(
//                     height: Dimensions.height10,
//                   ),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     children: [
//                       GestureDetector(
//                         onTap: () {
//                           onMap
//                               ? _saveBlocks()
//                               : Fluttertoast.showToast(msg: "You did not select START MAPPING");
//                         },
//                         child: Container(
//                           height: Dimensions.height20 + Dimensions.height30,
//                           width: Dimensions.width10 * Dimensions.width10,
//                           decoration: BoxDecoration(
//                             color: Colors.grey,
//                             borderRadius: BorderRadius.circular(0),
//                           ),
//                           child: Center(
//                               child: SmallText(
//                             text: "COMPLETE",
//                             color: Colors.black,
//                             size: Dimensions.font12,
//                           )),
//                         ),
//                       ),
//                       Column(
//                         children: [
//                           BigText(
//                             text: "Total Acreage (m2)",
//                             color: Colors.black,
//                             size: Dimensions.font12,
//                           ),
//                           BigText(
//                             text: area.toString(),
//                             color: Colors.black,
//                             size: Dimensions.font12,
//                           )
//                         ],
//                       )
//                     ],
//                   )
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   contentBox(context) {
//     return Stack(
//       children: [
//         Container(
//           padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
//           margin: EdgeInsets.only(top: 20),
//           decoration: BoxDecoration(
//               shape: BoxShape.rectangle,
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(30),
//               boxShadow: [
//                 BoxShadow(
//                     color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
//               ]),
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: [
//               SizedBox(
//                 height: 10,
//               ),
//               BigText(
//                 text: "Confirm Restart",
//                 color: Colors.black,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               SmallText(
//                 text: "Would you like to restart mapping?",
//                 color: Colors.black,
//                 size: Dimensions.font16,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Align(
//                   alignment: Alignment.bottomRight,
//                   child: GestureDetector(
//                     onTap: () {
//                       _restart(blockId);
//                     },
//                     child: BigText(
//                       text: "Ok",
//                       color: Colors.black,
//                       size: Dimensions.font16,
//                     ),
//                   )),
//             ],
//           ),
//         ),
//         Positioned(
//           left: 5,
//           right: 5,
//           child: CircleAvatar(
//             backgroundColor: Colors.transparent,
//             radius: 20,
//             child: Icon(
//               Icons.warning,
//               color: Colors.redAccent,
//               size: 45,
//             ),
//             // child: ClipRRect(
//             //   borderRadius: BorderRadius.all(Radius.circular(10)),
//             //     child: Image.asset('assets/images/ic_1_6.png')
//             // ),
//           ),
//         ),
//       ],
//     );
//   }

//   confirm(context) {
//     return Stack(
//       children: [
//         Container(
//           padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
//           margin: EdgeInsets.only(top: 20),
//           decoration: BoxDecoration(
//               shape: BoxShape.rectangle,
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(30),
//               boxShadow: [
//                 BoxShadow(
//                     color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
//               ]),
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: [
//               SizedBox(
//                 height: 10,
//               ),
//               BigText(
//                 text: "Block Already Drawn",
//                 color: Colors.black,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               SmallText(
//                 text: "Would you like to Re-Map or Continue?",
//                 color: Colors.black,
//                 size: Dimensions.font16,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Align(
//                   alignment: Alignment.bottomRight,
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       GestureDetector(
//                         onTap: () async {
//                           for (var element in polygonLatLngs) {
//                             _setMarker(element);
//                           }
//                           setState(() {
//                             blocksEmpty = true;
//                             onMap = true;
//                           });
//                           Get.back();
//                         },
//                         child: BigText(
//                           text: "Continue",
//                           color: Colors.black,
//                           size: Dimensions.font16,
//                         ),
//                       ),
//                       GestureDetector(
//                         onTap: () {
//                           Get.back();
//                           setState(() {
//                             blocksEmpty = true;
//                             onMap = true;
//                           });
//                           showDialog(
//                               barrierDismissible: false,
//                               context: context,
//                               builder: (BuildContext context) {
//                                 return Dialog(
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(20),
//                                   ),
//                                   elevation: 0,
//                                   backgroundColor: Colors.transparent,
//                                   child: contentBox(context),
//                                 );
//                               });
//                         },
//                         child: BigText(
//                           text: "Restart",
//                           color: Colors.black,
//                           size: Dimensions.font16,
//                         ),
//                       )
//                     ],
//                   )),
//             ],
//           ),
//         ),
//         const Positioned(
//           left: 5,
//           right: 5,
//           child: CircleAvatar(
//             backgroundColor: Colors.transparent,
//             radius: 20,
//             child: Icon(
//               Icons.warning,
//               color: Colors.redAccent,
//               size: 45,
//             ),
//             // child: ClipRRect(
//             //   borderRadius: BorderRadius.all(Radius.circular(10)),
//             //     child: Image.asset('assets/images/ic_1_6.png')
//             // ),
//           ),
//         ),
//       ],
//     );
//   }

//   mapWith(context) {
//     return Stack(
//       children: [
//         Container(
//           padding: EdgeInsets.only(
//             left: Dimensions.width10,
//             top: Dimensions.height20,
//             right: Dimensions.width10,
//             bottom: Dimensions.height20,
//           ),
//           margin: EdgeInsets.only(top: Dimensions.height20),
//           decoration: BoxDecoration(
//               shape: BoxShape.rectangle,
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(Dimensions.radius30),
//               boxShadow: [
//                 BoxShadow(
//                     color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
//               ]),
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: [
//               SizedBox(
//                 height: Dimensions.height10,
//               ),
//               BigText(
//                 text: "Type Select",
//                 color: Colors.black,
//                 size: Dimensions.font16,
//               ),
//               SizedBox(
//                 height: Dimensions.height10,
//               ),
//               SmallText(
//                 text: "Would you like to ?",
//                 color: Colors.black,
//                 size: Dimensions.font16,
//               ),
//               SizedBox(
//                 height: Dimensions.height10,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   GestureDetector(
//                       onTap: () {
//                         onMap = true;
//                         Get.back();
//                       },
//                       child: Container(
//                         height: Dimensions.height45,
//                         width: Dimensions.width30 * 4,
//                         decoration: BoxDecoration(
//                             color: Colors.green,
//                             borderRadius:
//                                 BorderRadius.circular(Dimensions.radius15 / 2)),
//                         child: Center(
//                           child: BigText(
//                             text: "Pick Points",
//                             color: Colors.black,
//                             size: Dimensions.font16,
//                           ),
//                         ),
//                       )),
//                   SizedBox(
//                     width: Dimensions.width30 * 2,
//                   ),
//                   GestureDetector(
//                       onTap: () {
//                         Get.back();
//                         Get.to(() => IntervalWalking());
//                       },
//                       child: Container(
//                         height: Dimensions.height45,
//                         width: Dimensions.width30 * 4,
//                         decoration: BoxDecoration(
//                             color: Colors.green,
//                             borderRadius:
//                                 BorderRadius.circular(Dimensions.radius15 / 2)),
//                         child: Center(
//                           child: BigText(
//                             text: "Walk",
//                             color: Colors.black,
//                             size: Dimensions.font16,
//                           ),
//                         ),
//                       ))
//                 ],
//               ),
//               SizedBox(
//                 height: Dimensions.height20,
//               ),
//               Align(
//                   alignment: Alignment.bottomCenter,
//                   child: GestureDetector(
//                       onTap: () {
//                         Get.back();
//                       },
//                       child: Container(
//                         height: Dimensions.height45,
//                         width: Dimensions.width30 * 3,
//                         decoration: BoxDecoration(
//                             color: Colors.grey,
//                             borderRadius:
//                                 BorderRadius.circular(Dimensions.radius15 / 2)),
//                         child: Center(
//                           child: BigText(
//                             text: "Cancel",
//                             color: Colors.black,
//                             size: Dimensions.font16,
//                           ),
//                         ),
//                       ))),
//             ],
//           ),
//         ),
//         Positioned(
//           left: Dimensions.width10 / 2,
//           right: Dimensions.width10 / 2,
//           child: CircleAvatar(
//               backgroundColor: Colors.transparent,
//               radius: Dimensions.radius20,
//               child: FaIcon(
//                 FontAwesomeIcons.circleQuestion,
//                 size: Dimensions.height45,
//                 color: Colors.green,
//               )),
//         ),
//       ],
//     );
//   }

//   Polygon _buildPolygon1() {
//     return Polygon(
//       polygonId: PolygonId('Division_Polygon'),
//       points: points,
//       strokeWidth: 2,
//       strokeColor: Colors.green,
//       fillColor: Colors.transparent,
//     );
//   }

//   Polygon _buildPolygon2() {
//     return Polygon(
//       polygonId: PolygonId('block_Polygon'),
//       points: Blockpoints,
//       strokeWidth: 2,
//       strokeColor: Colors.blue,
//       fillColor: Colors.blue.withOpacity(0.3),
//     );
//   }

//   Polygon? _buildPolygon3() {
//     Polygon? polygon;
//     for (var element in mapPolygonList) {
//       polygon = element;
//     }
//     return polygon;
//   }

//   _buildAllPolygons() {
//     int j = 0;
//     while (j < _listBlocksPolygon.length) {
//       Polygon polygon = Polygon(
//         polygonId: PolygonId('block_Polygon${1}'),
//         points: _listBlocksPolygon[j],
//         strokeWidth: 2,
//         strokeColor: Colors.blue,
//         fillColor: Colors.blue.withOpacity(0.3),
//       );

//       setState(() {
//         mapPolygonList.add(polygon);
//         _listBlocksPolygons.add(mapPolygonList);
//       });

//       j++;
//     }
//   }
// }
