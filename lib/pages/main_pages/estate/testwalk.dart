import 'dart:async';

import 'package:farmcapturev2/resources/utils/dimensions.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../resources/widgets/small_text.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({super.key});

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  GoogleMapController? _mapController;
  final List<LatLng> _points = [];
  final List<Marker> _markers = [];
  final List<Polyline> _polylines = [];
  Position? currentPosition;
  bool isMapping = false;

  @override
  void initState() {
    super.initState();
    //_initializeLocationUpdates();

    _getCurrentLocation();
  }

  Future<void> _initializeLocationUpdates() async {
    const LocationSettings locationSettings = LocationSettings(
      accuracy: LocationAccuracy.high,
      distanceFilter: 100,
    );

    StreamSubscription<Position> positionStream =
        Geolocator.getPositionStream(locationSettings: locationSettings)
            .listen((Position position) {
      print(position == null
          ? 'Unknown'
          : '${position.latitude.toString()}, ${position.longitude.toString()}');
      _onPositionChanged(position);
    });
  }

  Future<void> _getCurrentLocation() async {
    Position? currpos = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    setState(() {
      currentPosition = currpos;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _mapController = controller;
    });
  }

  void _onPositionChanged(Position position) async {
    LatLng currentLatLng = LatLng(position.latitude, position.longitude);
    double distance1 = 0.0;
    if (_points.isNotEmpty) {
      double distance = Geolocator.distanceBetween(
        _points.last.latitude,
        _points.last.longitude,
        currentLatLng.latitude,
        currentLatLng.longitude,
      );
      setState(() {
        distance1 = distance;
      });

      print(distance);
      //if (distance < 10) return; // Do not update if distance < 10 meters
    }
    // _isPointInsidePolygon(point, points) ?
    // await addValidPoint(currentLatLng, distance1);
  }

  // Function to check if a point is inside a polygon
  bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
    bool inside = false;
    //bool inside2 = false;
    //double tolerance = 1;
    for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
      if (((polygon[i].latitude > point.latitude) !=
              (polygon[j].latitude > point.latitude)) &&
          (point.longitude <
              (polygon[j].longitude - polygon[i].longitude) *
                      (point.latitude - polygon[i].latitude) /
                      (polygon[j].latitude - polygon[i].latitude) +
                  polygon[i].longitude)) {
        inside = !inside;
      }
    }

    print("Is In 1: $inside");

    return inside;
  }

  addValidPoint(LatLng currentLatLng, double distance) {
    setState(() {
      _points.add(currentLatLng);

      if (distance > 10){
          _markers.add(
          Marker(
            markerId: MarkerId(currentLatLng.toString()),
            position: currentLatLng,
          ),
        );
      }

      if (_points.length > 1) {
        Polyline polyline = Polyline(
          polylineId: PolylineId(_points.length.toString()),
          color: Colors.blue,
          width: 5,
          points: _points,
        );

        _polylines.add(polyline);
      }

      if (_mapController != null) {
        _mapController!.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: currentLatLng,
              zoom: 15,
            ),
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: currentPosition == null
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              children: [
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                        currentPosition!.latitude, currentPosition!.longitude),
                    zoom: 15,
                  ),
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                  markers: Set.from(_markers),
                  polylines: Set.from(_polylines),
                ),
                Positioned(
                    child: Container(
                  height: Dimensions.height30 * 3,
                  width: Dimensions.screenWidth,
                  decoration: const BoxDecoration(
                    color: Colors.grey,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _initializeLocationUpdates();
                        },
                        child: Container(
                          height: Dimensions.height20 + Dimensions.height30,
                          width: Dimensions.width45 * 4,
                          padding: EdgeInsets.all(Dimensions.height10 / 2),
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(0),
                          ),
                          child: Center(
                              child: SmallText(
                            text: "START FARM MAPPING NOW",
                            color: Colors.black,
                            size: Dimensions.font12,
                          )),
                        ),
                      ),
                    ],
                  ),
                )),
              ],
            ),
    );
  }
}
