import 'package:farmcapturev2/pages/main_pages/biodiversity/biodiversity_report.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/conduct_survey.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/reserves_list.dart';
import 'package:farmcapturev2/resources/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../dashboard.dart';
import 'biodiversity_mapping.dart';

class BioDiversityDashBoardScreen extends StatefulWidget {
  const BioDiversityDashBoardScreen({super.key});

  @override
  State<BioDiversityDashBoardScreen> createState() => _BioDiversityDashBoardScreenState();
}

class _BioDiversityDashBoardScreenState extends State<BioDiversityDashBoardScreen> {
  var selectedDivision = "";
  var _selectedIndex = Get.arguments;
  List<dynamic> items = [
    {
      "image": 'assets/images/map.jpg',
      "name": "RESERVES MAPPING",
      "descr": "Mapping Conservation Area"
    },
    {
      "image": 'assets/images/outgrowers.png',
      "name": "CONDUCT SURVEY",
      "descr": "Conduct Biodiversity Survey"
    },
    {
      "image": 'assets/images/ic_3_1.png',
      "name": "BIODIVERSITY REPORT",
      "descr": "Biodiversity Reports"
    },
  ];
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          //height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30 * Dimensions.height10 / 1.5,
                child: Stack(
                  children: [
                    Container(
                      height: Dimensions.height15 * 10,
                      padding: EdgeInsets.only(
                        top: Dimensions.height30 * 2,
                        left: Dimensions.width20,
                        right: Dimensions.width20,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(Dimensions.radius30 * 4),
                          bottomRight: Radius.circular(Dimensions.radius30 * 4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: Dimensions.iconSize24,
                                  ),
                                  onPressed: () {
                                    Get.to(() => const DashboardScreen(),
                                        arguments: 3);
                                  },
                                ),
                                BigText(
                                  text: "BioDiversity Dashboard",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font20,
                                ),
                                SizedBox(
                                  width: Dimensions.width30,
                                ),
                                InkWell(
                onTap: () async{
                  await EstateController().deleteReserveById([3].toString());
                },
                child: Text("Clear")
              ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Dimensions.screenHeight,
                padding: EdgeInsets.only(
                  left: Dimensions.height30,
                  right: Dimensions.height30,
                  //top: Dimensions.height20,
                ),
                child: ListView.builder(
                  itemCount: items.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: ((context, index) {
                    var item = items[index];
                    return InkWell(
                      onTap: () async {
                        setState(() {
                          if (_selectedIndex == index) {
                            if (_selectedIndex == 0) {
                              Get.to(() => const BiodiversityMappingScreen());
                            } else if (_selectedIndex == 1) {
                              Get.to(() => const ConductSurveyScreen(), arguments: [_selectedIndex, 0]);
                            } else if (_selectedIndex == 2) {
                              Get.to(() => const BiodiversityReportScreen(), arguments: [_selectedIndex, 0]);
                            }
                          } else {
                            _selectedIndex = index;
                          }
                        });
                        if (_selectedIndex == 0) {
                          // Get.to(() => const BiodiversityMappingScreen());
                          Get.to(() => const ReservesListScreen());
                        } else if (_selectedIndex == 1) {
                          Get.to(() => const ConductSurveyScreen(), arguments: [_selectedIndex, 0]);
                        } else if (_selectedIndex == 2) {
                          Get.to(() => const BiodiversityReportScreen(), arguments: [_selectedIndex, 0]);
                        }
                      },
                      child: Center(
                        child: Container(
                          height: Dimensions.height30 * 5,
                          width: Dimensions.width30 * 10,
                          margin: const EdgeInsets.only(bottom: 20),
                          decoration: 
                          _selectedIndex == index
                              ? BoxDecoration(
                                  color: AppColors.mainColor,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius20 / 4),
                                )
                              : BoxDecoration(
                                  //color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius20 / 4),
                                  border: Border.all(
                                    width: 2,
                                    color: AppColors.mainColor,
                                  )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                item["image"].toString(),
                                width: 60,
                                height: 60,
                                color:
                                index == 0
                                ? null
                                :
                                _selectedIndex == index
                                ? AppColors.textWhite
                                : AppColors.mainColor,
                              ),
                              BigText(
                                text: item["name"].toString(),
                                color: AppColors.black,
                                size: Dimensions.font16 - 4,
                              ),
                              const Divider(
                                color: Colors.black,
                                indent: 20,
                                endIndent: 20,
                              ),
                              SmallText(
                                text: item["descr"].toString(),
                                color: AppColors.black,
                                size: Dimensions.font12,
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  })
                )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
