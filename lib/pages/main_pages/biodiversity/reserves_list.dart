import 'dart:io';

import 'package:farmcapturev2/pages/main_pages/biodiversity/biodiversity_dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/biodiversity_mapping.dart';
import 'package:farmcapturev2/resources/base/custom_loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/location/location_controller.dart';
import '../../../models/biodiversity/reservesTypes_model.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';


class ReservesListScreen extends StatefulWidget {
  const ReservesListScreen({super.key});

  @override
  State<ReservesListScreen> createState() => _ReservesListScreenState();
}

class _ReservesListScreenState extends State<ReservesListScreen> {

  var _selectedIndex = Get.arguments;
  List<dynamic> reservesItems = [];
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getreserves();
  }

  _getreserves() async {
    setState(() => isLoading = true);
    List<dynamic> resReserves = [];

    var response = await LocationController().getLocationsLocal(reservesTypesTable);

    // print("Reserves"+ response[0].toString());

    if (response != null) {
      resReserves = response;

      for(var i in resReserves){
        var item = {
          "Id": i['Id'],
          "name": i['name'],
        };

        reservesItems.add(item);
      }
    }

    setState(() => isLoading = false);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          width: Dimensions.screenWidth,

          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),

          child: isLoading
          ? CustomLoader()
          : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30 * Dimensions.height10 / 1.5,
                  child: Stack(
                    children: [
                      Container(
                        height: Dimensions.height15 * 10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height30 * 2,
                          left: Dimensions.width20,
                          right: Dimensions.width20,
                        ),
                        decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft:
                          Radius.circular(Dimensions.radius30 * 4),
                          bottomRight:
                          Radius.circular(Dimensions.radius30 * 4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: Dimensions.iconSize24,
                                  ),
                                  onPressed: () {
                                    Get.to(
                                            () => const BioDiversityDashBoardScreen(),
                                        arguments: 3);
                                  },
                                ),
                                BigText(
                                  text: "RESERVES LIST",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font20,
                                ),
                                SizedBox(
                                  width: Dimensions.width30,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      ),
                    ],
                  ),
              ),
              Expanded(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: Dimensions.height30,
                      right: Dimensions.height30,
                      top: Dimensions.height20,
                    ),
                      child: ListView.builder(
                        itemCount: reservesItems.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index){
                          var item = reservesItems[index];
                          return GestureDetector(
                            onTap: () async {
                              setState(() {
                                _selectedIndex = index;
                              });

                              Get.to(() => const BiodiversityMappingScreen(), arguments: [item["Id"].toString()]);
                            },

                              child: Center(
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                margin: const EdgeInsets.only(bottom: 20),
                                padding: EdgeInsets.only(
                                  left: Dimensions.width30,
                                ),
                                decoration: _selectedIndex == index
                                    ? BoxDecoration(
                                  color: AppColors.mainColor,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius20 / 4),
                                )
                                    : BoxDecoration(
                                    color: AppColors.textWhite,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20),
                                    border: Border.all(
                                      width: 2,
                                      color: AppColors.mainColor,
                                    )),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      BigText(
                                        text: item["name"].toString(),
                                        color: AppColors.black,
                                        size: Dimensions.font16 - 4,
                                      ),
                                    ],
                                  ),
                              ),
                              ),
                          );
                        },
                      ),
                  ),
              ),
            ],
          ),
        ),
    );
  }
}
