import 'package:farmcapturev2/pages/main_pages/biodiversity/biodiversity_dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/biodiversity_questions.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/farmers/additional_info/inputquestions_model.dart';
import '../../../models/farmers/additional_info/questionChoice_model.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class ConductSurveyScreen extends StatefulWidget {
  const ConductSurveyScreen({super.key});

  @override
  State<ConductSurveyScreen> createState() => _ConductSurveyScreenState();
}

class _ConductSurveyScreenState extends State<ConductSurveyScreen> {
  var selectedDivision = "";
  var _selectedIndex = Get.arguments;
  bool isLoading = false;

  List<dynamic> items = [
    {
      "name": "BioDiversity",
      "descr": "Biodiversity Survey"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const BioDiversityDashBoardScreen(), arguments: 0);
                    },
                  ),
                  BigText(
                    text: "Conduct Survey",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  //physics: const NeverScrollableScrollPhysics(),
                  itemCount: items.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = items[index];
                    return InkWell(
                      onTap: () {
                        if(index == 0){
                          Get.to(() => const BioDiversityQuestionsScreen());
                        }
                      },
                      child: Container(
                        height: Dimensions.height30 * 4,
                        margin: EdgeInsets.only(
                          bottom: 20
                        ),
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(
                                Dimensions.radius20)),
                        child: Container(
                          padding: EdgeInsets.only(
                            left: Dimensions.width20,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Icon(Icons.question_answer, size: 60,),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.center,
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  BigText(
                                    width: Dimensions.width45 * 7,
                                    text: item["name"].toString(),
                                    color: AppColors.mainColor,
                                    size: Dimensions.font20,
                                  ),
                                  SizedBox(
                                    height:
                                        Dimensions.height10 / 2,
                                  ),
                                  SmallText(
                                    text: item["descr"].toString(),
                                    color: AppColors.black,
                                    size: Dimensions.font14,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
