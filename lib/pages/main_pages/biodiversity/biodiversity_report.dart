import 'dart:convert';

import 'package:farmcapturev2/resources/base/custom_loader.dart';
import 'package:farmcapturev2/resources/widgets/small_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/biodiversity/biodiversityanswers_model.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import 'biodiversity_dashboard.dart';

class BiodiversityReportScreen extends StatefulWidget {
  const BiodiversityReportScreen({super.key});

  @override
  State<BiodiversityReportScreen> createState() =>
      _BiodiversityReportScreenState();
}

class _BiodiversityReportScreenState extends State<BiodiversityReportScreen> {
  var selectedDivision = "";
  var _selectedIndex = Get.arguments;
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();
  List<dynamic> records = [];

  getRecords() async {
    //try {
    var res = await FarmersController().getBioSummaryReport(
      'main_div_id',
      selectedDivisionId,
      'block_id',
      selectedBlockId,
      'datetime',
      startDateController.text,
      'datetime',
      endDateController.text,
    );

    if (res != null) {
      records = res;
      setState(() => isLoading = false);
    } else {
      records = [];
      print("Did Not get Reports");
      setState(() => isLoading = false);
    }
    // } catch (e) {
    //   print("Error is: " + e.toString());
    // }
  }

  _getDivision() async {
    setState(() => isLoading = true);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();
      });
      //await getRecords();
      //print("BlockId is: ${selectedBlockId} DivisionId is: ${selectedDivisionId}");

      setState(() => isLoading = false);
    } else {
      Get.back();
    }
  }

  @override
  void initState() {
    super.initState();
    _getDivision();
  }

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedStartTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        startDateController.text = formattedDate;

        print(date.toString());
      });
    }
  }

  Future<void> selectedEndTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        endDateController.text = formattedDate;

        print(date.toString());
        isLoading = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const BioDiversityDashBoardScreen(),
                          arguments: 1);
                    },
                  ),
                  BigText(
                    text: "Conduct Survey",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: Dimensions.screenWidth / 2 - 15,
                    padding: EdgeInsets.only(
                        top: Dimensions.height10, bottom: Dimensions.height10),
                    child: TextField(
                      autocorrect: true,
                      controller: startDateController,
                      keyboardType: TextInputType.text,
                      readOnly: true,
                      onTap: () async {
                        await selectedStartTimePicker(context);
                      },
                      decoration: InputDecoration(
                        hintText: "Start Date",
                        suffixIcon: const Icon(Icons.calendar_month),
                        hintStyle: const TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(Dimensions.radius15 - 5)),
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(Dimensions.radius15)),
                          borderSide: const BorderSide(
                              color: Colors.lightGreen, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: Dimensions.screenWidth / 2 - 15,
                    padding: EdgeInsets.only(
                        top: Dimensions.height10, bottom: Dimensions.height10),
                    child: TextField(
                      autocorrect: true,
                      controller: endDateController,
                      keyboardType: TextInputType.text,
                      readOnly: true,
                      onTap: () async {
                        startDateController.text.isEmpty
                            ? null
                            : await selectedEndTimePicker(context);
                        Future.delayed(Duration(seconds: 1), () async {
                          endDateController.text.isNotEmpty
                              ? await getRecords()
                              : null;
                        });
                      },
                      decoration: InputDecoration(
                        hintText: "End Date",
                        suffixIcon: const Icon(Icons.calendar_month),
                        hintStyle: const TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(Dimensions.radius15 - 5)),
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(Dimensions.radius15)),
                          borderSide: const BorderSide(
                              color: Colors.lightGreen, width: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            isLoading
                ? Center(
                    child: CustomLoader(),
                  )
                : records.isEmpty
                    ? Container(
                        child: Center(
                          child: BigText(
                            text: "No Item!",
                            color: AppColors.black,
                          ),
                        ),
                      )
                    : Expanded(
                        child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: Dimensions.width10,
                            vertical: Dimensions.height20),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: records.length,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, index) {
                            var record = records[index];

                            return Container(
                              margin: EdgeInsets.only(bottom: 15),
                              padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: 10,
                              ),
                              //height: Dimensions.height30 * 4,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius20)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  BigText(
                                    text: "QN: ${record['name']} ?",
                                    color: AppColors.mainColor,
                                    size: 18,
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  BigText(
                                    text: "Lat: ${record['latitude']}",
                                    size: 16,
                                    color: Colors.black,
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  BigText(
                                    text: "Long: ${record['longitude']}",
                                    size: 16,
                                    color: Colors.black,
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  BigText(
                                    text:
                                        "Estimated Area: ${record['estimated_acreage']}",
                                    color: AppColors.black,
                                    size: 16,
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  BigText(
                                    text: "Comment: ${record['comments']}",
                                    size: 16,
                                    color: Colors.black,
                                  ),

                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      BigText(
                                        text: "Date Conducted:  ${record['datetime']} ",
                                        color: AppColors.black,
                                        size: 16,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ))
          ],
        ),
      ),
    );
  }
}
