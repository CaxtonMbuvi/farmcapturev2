import 'dart:convert';
import 'dart:io';

import 'package:farmcapturev2/models/biodiversity/biod_coordinates_model.dart';
import 'package:farmcapturev2/models/biodiversity/biod_photos_model.dart';
import 'package:farmcapturev2/models/biodiversity/biodiversityanswers_model.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/conduct_survey.dart';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/resources/widgets/small_text.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../controllers/location/location_controller.dart';
import '../../../models/farmers/additional_info/questionChoice_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/base/snackbar.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/form_field.dart';

class AnswerQuestionBioDiversityScreen extends StatefulWidget {
  AnswerQuestionBioDiversityScreen({super.key});

  @override
  State<AnswerQuestionBioDiversityScreen> createState() =>
      _AnswerQuestionBioDiversityScreenState();
}

class _AnswerQuestionBioDiversityScreenState
    extends State<AnswerQuestionBioDiversityScreen> {
  QuestionChoicesModel receivedModel = Get.arguments;
  List<String> _items = [
    "> 5 Acres"
        "< 5 Acres",
    "> 10 Acres - < 15 Acres",
    "> 15 Acres - < 20Acres"
  ];

  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController latitudeController = TextEditingController();
  TextEditingController longitudeController = TextEditingController();
  TextEditingController commentsController = TextEditingController();
  TextEditingController userController = TextEditingController();
  String? selectedEstimatedArea;
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;
  List<dynamic> imageList = [];

  File? image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getDivision();
  }

  _getDate() {
    DateTime dateTime = DateTime.now();
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var userId = localStorage.getString('UserId');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    //print(data);
    //print(receivedModel.selectedChoice!.choiceId.toString());
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;

        userController.text = userId.toString();
      });

      await _getCurrentPosition();
    } else {
      Get.back();
    }
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() {
        latitudeController.text = position.latitude.toString();
        longitudeController.text = position.longitude.toString();
      });
    }).catchError((e) {
      debugPrint(e.toString());
    });
  }

  Future _getImage() async {
    final picker = ImagePicker();

    var pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (pickedFile != null) {
      final bytes = File(pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

      setState(() {
        image = File(pickedFile.path);
        imageList.add(image);
        image.toString();
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    //return image;
  }

  _save() async {
    if (imageList.isEmpty) {
      showCustomSnackBar("Please add An Image", context, ContentType.failure,
          title: "Failed");
    } else if (selectedEstimatedArea == null) {
      showCustomSnackBar(
          "Please add An Estimated Area", context, ContentType.failure,
          title: "Failed");
    } else {
      var surveyTransactionNo =
          'BIO_${DateTime.now().millisecondsSinceEpoch.toString()}';

      FarmersBiodiversityAnswersModel model = FarmersBiodiversityAnswersModel();
      model.estimatedAcreage = selectedEstimatedArea.toString();
      model.comments = commentsController.text;
      model.blockId = selectedBlockId;
      model.mainDivId = selectedDivisionId;
      model.quizTransactionNo = surveyTransactionNo;
      model.datetime = await _getDate();
      model.answerOption = "yes";
      model.category = receivedModel.categoryId.toString();
      model.farmInspectionsQuizId = receivedModel.questionId.toString();

      List<dynamic> bio = [];
      bio.add(model);

      int i = await FarmersController()
          .saveAll(bio, farmerBiodiversityAnswSurveysTable);

      if (i > 0) {
        print("Success here");
        int j = 0;

        for (var element in imageList) {
          var photoUniqueNo =
              'BIOPHOTO_${DateTime.now().millisecondsSinceEpoch.toString()}';
          FarmersBiodiversityPhotosModel photoModel =
              FarmersBiodiversityPhotosModel();
          photoModel.blockId = selectedBlockId;
          photoModel.mainDivId = selectedDivisionId;
          photoModel.quizTransactionNo = surveyTransactionNo;
          photoModel.photoUrl = element.toString();
          photoModel.photoUniqueNo = photoUniqueNo;
          photoModel.userId = userController.text;

          photoModel.datetime = await _getDate();

          List<dynamic> bIOphoto = [];
          bIOphoto.add(photoModel);

          j = await FarmersController()
              .saveAll(bIOphoto, farmerBiodiversitySurveysPhotosTable);
        }

        if (j > 0) {
          int k = 0;
          var coOrdinateUniqueNo =
              'BIOLATLNG_${DateTime.now().millisecondsSinceEpoch.toString()}';
          FarmersBiodiversityCoordinatesModel latlngModel =
              FarmersBiodiversityCoordinatesModel();
          latlngModel.blockId = selectedBlockId;
          latlngModel.mainDivId = selectedDivisionId;
          latlngModel.coOrdinateUniqueNo = coOrdinateUniqueNo;
          latlngModel.latitude = latitudeController.text;
          latlngModel.longitude = longitudeController.text;
          latlngModel.quizTransactionNo = surveyTransactionNo;
          latlngModel.userId = userController.text;
          latlngModel.datetime = await _getDate();
          List<dynamic> bIOlatlng = [];
          bIOlatlng.add(latlngModel);

          k = await FarmersController()
              .saveAll(bIOlatlng, farmerBiodiversitySurveyCoordinatesTable);

          if (k > 0) {
            setState(() => isLoading = false);
            Get.to(() => const ConductSurveyScreen());
          }
        } else {
          setState(() => isLoading = false);
        }
      } else {
        setState(() => isLoading = false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const ConductSurveyScreen(), arguments: 1);
                    },
                  ),
                  // BigText(
                  //   text: "IPM DAMAGE FORM",
                  //   color: AppColors.textWhite,
                  //   size: Dimensions.font20,
                  // ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: isLoading
            ? const CustomLoader()
            : Container(
                padding: EdgeInsets.symmetric(horizontal: Dimensions.width15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: Dimensions.height20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: Dimensions.screenWidth / 2 - 15,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Latitude",
                                color: Colors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              FormFields(
                                textEditingController: latitudeController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: Dimensions.screenWidth / 2 - 15,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Longitude",
                                color: Colors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              FormFields(
                                textEditingController: longitudeController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Estimated Hectares",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    SizedBox(
                      height: 60.0,
                      child: FormField<String>(
                        builder: (FormFieldState<String> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                  color: Colors.lightGreen,
                                  width: 2,
                                ),
                              ),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                value: selectedEstimatedArea,
                                isExpanded: true,
                                onChanged: (String? newValue) async {
                                  setState(() {
                                    selectedEstimatedArea = newValue;
                                  });
                                },
                                items: _items.map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Comments",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    FormFields(
                      maxLines: 3,
                      textEditingController: commentsController,
                      inputType: TextInputType.text,
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ElevatedButton(
                        onPressed: _getImage,
                        child: Text('Add Image'),
                      ),
                    ),
                    imageList.isEmpty
                        ? Row(
                            children: [
                              const Icon(
                                Icons.info,
                                color: Colors.blue,
                                size: 16,
                              ),
                              SizedBox(
                                width: Dimensions.width10 / 2,
                              ),
                              SmallText(
                                text: "Kindly Add An Image / Images",
                                color: Colors.black,
                              ),
                            ],
                          )
                        : GridView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: imageList.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 4.0,
                              mainAxisSpacing: 4.0,
                            ),
                            itemBuilder: (BuildContext context, int index) {
                              File image1 = imageList[index];

                              return ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius15),
                                child: Image.file(image1, fit: BoxFit.cover),
                              );
                            },
                          ),
                    SizedBox(
                      height: Dimensions.height30,
                    ),
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          _save();
                        },
                        child: Container(
                          height: Dimensions.height30 * 2,
                          width: Dimensions.width30 * Dimensions.width10,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius30),
                          ),
                          child: Center(
                            child: BigText(
                              text: "Submit",
                              color: AppColors.textWhite,
                              size: Dimensions.font16,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height30,
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
