import 'package:farmcapturev2/pages/main_pages/biodiversity/conduct_survey.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/answer_question.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/farmers/additional_info/inputquestions_model.dart';
import '../../../models/farmers/additional_info/questionChoice_model.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class BioDiversityQuestionsScreen extends StatefulWidget {
  const BioDiversityQuestionsScreen({super.key});

  @override
  State<BioDiversityQuestionsScreen> createState() =>
      _BioDiversityQuestionsScreenState();
}

class _BioDiversityQuestionsScreenState
    extends State<BioDiversityQuestionsScreen> {
  var selectedDivision = "";
  bool isLoading = false;

  final List<QuestionChoicesModel> _surverQuestionsChoices = [];
  final List<InpQuestionsModelResultElement> _surveyQuestion = [];

  List<ChoiceModel?> selectedAnswers = [];
  List<String> selectedQuestions = [];

  _goToScreen() {
    //Navigator.pop(context);
    Get.to(() => AnswerQuestionBioDiversityScreen());
  }

  _confirmQuestion(QuestionChoicesModel questionChoices) async {
    await showDialog(
        context: context,
        builder: (BuildContext dialoguecontext) {
          return SizedBox(
            width: Dimensions.screenWidth - 40,
            child: Dialog(
              insetPadding: EdgeInsets.zero,
              //title: BigText(text: "Confirm Choice"),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Container(
                  width: Dimensions.screenWidth - 40,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          BigText(
                            text: "Question : ",
                            color: AppColors.black,
                            size: Dimensions.font16 - 2,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          BigText(
                            width: Dimensions.screenWidth / 2,
                            text: questionChoices.questionName.toString(),
                            color: AppColors.black,
                            size: Dimensions.font16 - 2,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Dimensions.height20,
                      ),
                      // BigText(
                      //   text: questionChoices.questionId
                      //       .toString(),
                      //   color: AppColors.black,
                      //   size: Dimensions.font16 - 2,
                      // ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: 120,
                            child: ElevatedButton(
                              onPressed: () {
                                //Navigator.pop(dialoguecontext);
                                Get.to(() => AnswerQuestionBioDiversityScreen(),
                                    arguments: questionChoices);
                              },
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.green),
                                elevation: MaterialStateProperty.all(0.0),
                                minimumSize: MaterialStateProperty.all(
                                    Size.fromHeight(50)),
                                //minimumSize: MaterialStateProperty.all(const Size(200, 50)),
                                //maximumSize: MaterialStatePropertyAll(Size(ScreenUtil().screenWidth, 50)),
                              ),
                              child: BigText(text: "Confirm"),
                            ),
                          ),
                          SizedBox(
                            width: 100,
                            child: ElevatedButton(
                              onPressed: () {},
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.grey),
                                elevation: MaterialStateProperty.all(0.0),
                                minimumSize: MaterialStateProperty.all(
                                    Size.fromHeight(50)),
                                //minimumSize: MaterialStateProperty.all(const Size(200, 50)),
                                //maximumSize: MaterialStatePropertyAll(Size(ScreenUtil().screenWidth, 50)),
                              ),
                              child: BigText(text: "Cancel"),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  _getLivingConditionsFarmQuestions() async {
    setState(() => isLoading = true);
    List<dynamic> resSurveyQuestion = [];
    //var res = await FarmersController().getFarmQuestions('LC');
    var res =
        await FarmersController().getFarmLivingConditionsQuestions('SRVY2');
    if (res != null) {
      resSurveyQuestion = res;
      for (Map<String, dynamic> row in resSurveyQuestion) {
        List<dynamic> choices = row['choices_text'].split(':');

        QuestionChoicesModel questionChoices = QuestionChoicesModel();
        questionChoices.questionId = row['Id'].toString();
        questionChoices.questionName = row['name'];
        questionChoices.categoryId = row['main_category_id'].toString();

        // print("Hello there" + questionChoices.questionId.toString());
        // var data = {
        //   "questionName": row['name'],
        //   "questionId": row['id'],
        //   "choices": [],
        // };
        // do something with the question and choices
        List<ChoiceModel> choiseList = [];
        for (var element in choices) {
          ChoiceModel choices = ChoiceModel();
          List<dynamic> choice = element.split('|');

          var choicesdata = {
            "choiceName": choice[0].toString(),
            "choiceId": choice[1].toString()
          };
          choices.choiceId = choice[1].toString();
          choices.choiceName = choice[0].toString();
          choices.questionName = row['name'];
          choices.questionId = row['Id'].toString();

          choiseList.add(choices);
        }
        questionChoices.choices = choiseList;

        print(questionChoices.toJson().toString());

        _surverQuestionsChoices.add(questionChoices);

        var result = {};
      }

      //_livingConditionsQuestionChoices = resLivingConditionsQuestion;
    }

    setState(() => isLoading = false);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getLivingConditionsFarmQuestions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const ConductSurveyScreen());
                    },
                  ),
                  BigText(
                    text: "Biodiversity",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                //physics: const NeverScrollableScrollPhysics(),
                itemCount: _surverQuestionsChoices.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if (index == _surverQuestionsChoices.length) {
                    return Column(
                      children: [
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              selectedAnswers = _surverQuestionsChoices
                                  .map((QuestionChoicesModel question) {
                                return question.selectedChoice;
                              }).toList();
                            });
                          },
                          child: Container(
                            height: Dimensions.height30 * 2,
                            width: Dimensions.width30 * Dimensions.width10,
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius:
                                  BorderRadius.circular(Dimensions.radius30),
                            ),
                            child: Center(
                              child: BigText(
                                text: "Submit",
                                color: AppColors.textWhite,
                                size: Dimensions.font16,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                      ],
                    );
                  }
                  QuestionChoicesModel questionChoices =
                      _surverQuestionsChoices[index];

                  return InkWell(
                    onTap: () {
                      print("Answer is: " +
                          questionChoices.choices![0].toJson().toString());

                      setState(() {
                        questionChoices.selectedChoice = questionChoices.choices![0];
                      });
                      _confirmQuestion(questionChoices);
                      //questionChoices.selectedChoice = selectedAnswers[0];
                      // if (questionChoices.categoryId.toString().isEmpty) {
                      //   setState(() {
                      //     questionChoices = _surverQuestionsChoices[index];
                      //   });
                      //   _confirmQuestion(questionChoices);
                      // } else {
                      //   _confirmQuestion(questionChoices);
                      // }
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                        left: Dimensions.width15,
                        right: Dimensions.width15,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: Dimensions.height30,
                          ),
                          Container(
                            padding: EdgeInsets.all(20.0),
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                BigText(
                                  width: Dimensions.screenWidth / 2,
                                  text: questionChoices.questionName.toString(),
                                  color: AppColors.black,
                                  size: Dimensions.font16 - 2,
                                ),
                                Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromARGB(
                                              255, 103, 103, 103),
                                          width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              ],
                            ),
                          )
                          // FormField<ChoiceModel>(
                          //   builder: (FormFieldState<ChoiceModel> state) {
                          //     return InputDecorator(
                          //       decoration: InputDecoration(
                          //         hintStyle: TextStyle(color: AppColors.black),
                          //         filled: true,
                          //         fillColor: Colors.white70,
                          //         enabledBorder: OutlineInputBorder(
                          //           borderRadius: BorderRadius.all(
                          //               Radius.circular(Dimensions.radius15 - 5)),
                          //           borderSide: const BorderSide(
                          //               color: Colors.grey, width: 2),
                          //         ),
                          //         focusedBorder: OutlineInputBorder(
                          //           borderRadius: BorderRadius.all(
                          //               Radius.circular(Dimensions.radius15)),
                          //           borderSide: const BorderSide(
                          //               color: Colors.lightGreen, width: 2),
                          //         ),
                          //       ),
                          //       //isEmpty: _currentSelectedValue == '',
                          //       child: DropdownButtonHideUnderline(
                          //         child: DropdownButton<ChoiceModel>(
                          //           hint: Text("Select Here"),
                          //           value: questionChoices.selectedChoice,
                          //           isDense: true,
                          //           onChanged: (ChoiceModel? newValue) async {
                          //             setState(() {
                          //               questionChoices.selectedChoice = newValue;
                          //             });
                          //             print("Answer: $selectedAnswers");
                          //           },
                          //           items: questionChoices.choices!
                          //               .map((ChoiceModel value) {
                          //             return DropdownMenuItem<ChoiceModel>(
                          //               value: value,
                          //               child: Text(value.choiceName.toString(),
                          //                   style: const TextStyle(
                          //                       color: Colors.black)),
                          //             );
                          //           }).toList(),
                          //         ),
                          //       ),
                          //     );
                          //   },
                          // ),
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
