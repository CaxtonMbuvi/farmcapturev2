import 'dart:async';

import 'package:farmcapturev2/models/biodiversity/reservesTypes_model.dart';
import 'package:farmcapturev2/pages/main_pages/biodiversity/biodiversity_dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../models/biodiversity/reserves_model.dart';
import '../../../models/estate/blocks_model.dart';
import '../../../models/estate/maindivisions_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../estate/testbluetooth.dart';

class BiodiversityMappingScreen extends StatefulWidget {
  const BiodiversityMappingScreen({super.key});

  @override
  State<BiodiversityMappingScreen> createState() =>
      _BiodiversityMappingScreenState();
}

class _BiodiversityMappingScreenState extends State<BiodiversityMappingScreen> {
  var resId = Get.arguments[0];
  bool isLoading = false;
  double area = 0.0;
  String selectedDivisionName = '';
  bool onMap = false;
  bool reserveEmpty = true;

  // FOR LOADING GOOGLE MAP
  late GoogleMapController _controller;
  final Completer<GoogleMapController> _controllerCompleter = Completer();
  LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);
  final Set<Marker> _markers = <Marker>{};
  final Set<Polyline> _polyline = <Polyline>{};
  //END

  final List<List<Polygon>> _listBlocksPolygons = []; //For Mapping Division And Blocks
  List<LatLng> pointsB = []; //For Verifying If Drawn Point is Valid

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
    _controllerCompleter.complete(controller);
  }

  Future<LatLng> _getLatLngCameraPosition() async {
    final lat = double.parse(_divisionsPolygon.first['latitude']);
    final long = double.parse(_divisionsPolygon.first['longi']);
    return LatLng(lat, long);
  }

  // FROM THE LOCAL DP
  List<dynamic> _divisionsPolygon = []; //FOR DIVISION DATA

  List<dynamic> _reserveslatlong = []; //FOR RESERVES DATA
  List<LatLng> Reservespoints = []; //LATLNG FROM RESERVES

  // SETTING THE MARKERS, POLYLINE, AREA ETC
  _setPolygon(List<LatLng> points) async {
    Polygon pol = Polygon(
      polygonId: PolygonId('$selectedDivisionName'),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listBlocksPolygons.add(lispol);

    //LatLng center = getPolygonCenter(points);
  }

  // LatLng getPolygonCenter(List<LatLng> polygonLatLngs) {
  //   double lat = 0;
  //   double lng = 0;

  //   for (LatLng latLng in polygonLatLngs) {
  //     lat += latLng.latitude;
  //     lng += latLng.longitude;
  //   }

  //   return LatLng(lat / polygonLatLngs.length, lng / polygonLatLngs.length);
  // }

  void _setPolyline(LatLng location) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polyline.length.toString()),
      points: [if (_polyline.isNotEmpty) _polyline.last.points.last, location],
      color: Colors.blue,
      width: 2,
    );

    setState(() {
      _polyline.add(polyline);
    });
  }

  double calculateArea(List<LatLng> coords) {
    int len = coords.length;
    double sum = 0;
    for (int i = 0; i < len; i++) {
      LatLng p1 = coords[i];
      LatLng p2 = coords[(i + 1) % len];
      sum += (p2.longitude - p1.longitude) * (p2.latitude + p1.latitude);
    }
    return (sum.abs() / 2) *
        111319.9 *
        111319.9; // Multiply by Earth's radius to get the area in square meters
  }

  int _markerId = 1;
  BitmapDescriptor markerIcon = BitmapDescriptor.defaultMarker;
  void addCustomIcon() {
    BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(5, 5)),
      "assets/images/dot.png",
    ).then(
      (icon) {
        setState(() {
          markerIcon = icon;
        });
      },
    );
  }

  void _setMarker(LatLng point) async {
    setState(() {
      _markers.add(
          Marker(markerId: MarkerId(_markerId.toString()), position: point));
      _markerId++;
    });
    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  // END

  // FOR GETTING RESERVELATLNG
  // List<dynamic> _allReserves = [];
  Future _getAllReserves(var id) async {
    // Getting blocks Related a division
    //try {
    var res =
        await EstateController().getReserveLocalById(reservesTypesTable, id);

    if (res != null) {
      _reserveslatlong = res;
      if (_reserveslatlong.isNotEmpty) {
        for (int j = 0; j < _reserveslatlong.length; j++) {
          final lat = double.parse(_reserveslatlong[j]["latitude"]);
          final lng = double.parse(_reserveslatlong[j]["longi"]);
          Reservespoints.add(LatLng(lat, lng));
        }
      }
      await _getPolyline();
      //area = calculateArea(polygonLatLngs);
    } else {
      print("Did not Get Blocks");
    }
    // } catch (e) {
    //   print("Did not Get Blocks ${e.toString()}");
    // }
  }

  final List<List<LatLng>> _listBlocksPolygon = [];
  List<Polygon> mapPolygonList = [];
  _buildAllPolygons() {
    int j = 0;
    while (j < _listBlocksPolygon.length) {
      Polygon polygon = Polygon(
        polygonId: const PolygonId('block_Polygon${1}'),
        points: _listBlocksPolygon[j],
        strokeWidth: 2,
        strokeColor: Colors.blue,
        fillColor: Colors.blue.withOpacity(0.3),
      );

      setState(() {
        mapPolygonList.add(polygon);
        _listBlocksPolygons.add(mapPolygonList);
      });

      j++;
    }
  }

  // END

  List<LatLng> polygonLatLngs = <LatLng>[];
  _getPolyline() {
    setState(() {
      reserveEmpty = false;
    });
    for (var element in Reservespoints) {
      polygonLatLngs.add(element);
      _setPolyline(element);
    }
  }

  Future _getDivisionLatLngFromDatabase(String id) async {
    var res = await EstateController().getDivisionlatlong(id);
    if (res != null) {
      // print(res.toString());

      _divisionsPolygon = res;
      if (_divisionsPolygon.isNotEmpty) {
        List<LatLng> points = []; //LATLNG FROM DIVISION
        for (int i = 0; i < _divisionsPolygon.length; i++) {
          final lat = double.parse(_divisionsPolygon[i]["latitude"]);
          final lng = double.parse(_divisionsPolygon[i]["longi"]);

          points.add(LatLng(lat, lng));
        }
        await _setPolygon(points);
        _getLatLngCameraPosition().then((latLng) {
          setState(() {
            _cameraPosition = latLng;
          });
        });
      } else {
        print("Did not Get");
      }
    } else {
      print("Did not Get All Data");
    }
  }

  List<dynamic> _mainDivisions = [];

  Future _getAllMainDivisions() async {
    setState(() => isLoading = true);

    try {
      addCustomIcon();
      var res = await EstateController().getMainDivisions(mainDivisionsTable);
      if (res != null) {
        //print(res.toString());
        _mainDivisions = res;
        //print(_mainDivisions[0]["Id"]!.toString());

        for (var item in _mainDivisions) {
          var itemId = item["Id"].toString();
          await _getDivisionLatLngFromDatabase(itemId);
        }

        await _getAllReserves(resId);

        setState(() => isLoading = false);

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    } catch (e) {
      setState(() => isLoading = false);
    }
  }

  Future _saveBlocks(String isComplete) async {
    setState(() => isLoading = true);
    List<dynamic> resData = [];
    area = calculateArea(polygonLatLngs);
    var transactionNo =
        'RESPOL_${DateTime.now().millisecondsSinceEpoch.toString()}';

    for (var element in polygonLatLngs) {
      var polygonId =
          'RESPOL_${DateTime.now().millisecondsSinceEpoch.toString()}';
      ReservePolygonResultElement resPoly = ReservePolygonResultElement();
      print(element.latitude);
      resPoly.latitude = element.latitude.toString();
      resPoly.longi = element.longitude.toString();
      resPoly.landSize = area.toString();
      resPoly.isComplete = isComplete;
      resPoly.reserveAreaId = resId.toString();
      resPoly.polygonId = polygonId.toString();
      resPoly.reservePolygonUniqueID = transactionNo;

      resData.add(resPoly);
    }

    print("The Block is: $resData");

    int? i =
        await EstateController().saveAll(resData, farmerReservesPolygonTable);

    if (i != null) {
      print("Done Here");
    }

    setState(() => isLoading = false);
  }

  @override
  void initState() {
    super.initState();

    //_getDivisionLatLngFromDatabase();
    _getAllMainDivisions();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            Get.to(() => const BioDiversityDashBoardScreen());
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
            size: Dimensions.iconSize24,
          ),
        ),
      ),
      body: isLoading
          ? SizedBox(
              height: Dimensions.screenHeight,
              width: Dimensions.screenWidth,
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [CustomLoader()],
              ),
            )
          : //Container()
          Stack(
              children: [
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: _cameraPosition,
                    zoom: 13.0,
                  ),
                  mapType: MapType.satellite,
                  markers: _markers,
                  polylines: _polyline,
                  polygons: _listBlocksPolygons.expand((i) => i).toSet(),
                  onTap: (point) {
                    onMap
                        ? reserveEmpty
                            ? setState(() {
                                polygonLatLngs.add(point);
                                _setMarker(point);
                                _setPolyline(point);
                              })
                            : Fluttertoast.showToast(
                                msg: "You did not select START MAPPING")
                        : showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) {
                              return Dialog(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                elevation: 0,
                                backgroundColor: Colors.transparent,
                                child: confirmRedraw(context),
                              );
                            });
                  },
                ),

                Positioned(
                  top: 50,
                  child: Container(
                    padding: EdgeInsets.all(Dimensions.height15),
                    height: Dimensions.height15 * Dimensions.height15,
                    width:
                        (Dimensions.screenWidth / 2) + Dimensions.width45 * 3,
                    margin: EdgeInsets.only(
                        top: Dimensions.height10, left: Dimensions.width10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                            child: Text(
                          "Tap START to get Location Co-ordinates",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: Dimensions.font16 - 2,
                              fontWeight: FontWeight.bold),
                        )),
                        SizedBox(
                          height: Dimensions.height10 / 2,
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Dialog(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          elevation: 0,
                                          backgroundColor: Colors.transparent,
                                          child: mapWith(context),
                                        );
                                      });
                                });
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "START MAPPING",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () async {
                                //dialog to choose mode of mapping
                                onMap
                                    ? showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            elevation: 0,
                                            backgroundColor: Colors.transparent,
                                            child: restartBox(context),
                                          );
                                        })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "RESTART",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  onMap
                                      ? _deletePoint()
                                      : Fluttertoast.showToast(
                                          msg:
                                              "You did not select START MAPPING");
                                });
                              },
                              child: Icon(
                                Icons.delete_outline_sharp,
                                color: AppColors.buttonRed,
                                size: Dimensions.iconSize24 * 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                onMap
                                    ? _saveBlocks("YES")
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width10 * Dimensions.width10,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "COMPLETE",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                onMap
                                    ? _saveBlocks("NO")
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width10 * Dimensions.width10,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "PAUSE",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            Column(
                              children: [
                                BigText(
                                  text: "Total Acreage (m2)",
                                  color: Colors.white,
                                  size: Dimensions.font12,
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                BigText(
                                  text: area.toString(),
                                  color: Colors.white,
                                  size: Dimensions.font12,
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),

                // Positioned(
                //   top: Dimensions.height45,
                //   child: Container(
                //     padding: EdgeInsets.all(Dimensions.height15),
                //     height: Dimensions.height15 * Dimensions.height15,
                //     width: (Dimensions.screenWidth / 2) + Dimensions.width10 * Dimensions.width15,
                //     margin: EdgeInsets.only( top: Dimensions.height10, left: Dimensions.width10),
                //     child: Column(
                //       children: [
                //         Center(
                //           child: Text(
                //             "Tap Start to begin mapping",
                //             style: TextStyle(
                //               color: Colors.white,
                //               fontSize: Dimensions.font16 - 2,
                //               fontWeight: FontWeight.bold
                //             ),
                //           )
                //         ),

                //       ],
                //     ),
                //   ),
                // )
              ],
            ),
    );
  }

  void _removeMarker() {
    if (_markers.isNotEmpty && _polyline.isNotEmpty) {
      setState(() {
        _markers.remove(_markers.last);
        _polyline.remove(_polyline.last);
        polygonLatLngs.remove(polygonLatLngs.last);
      });
    }

    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${polygonLatLngs.length}');
  }

  _restart(String id) async {
    await EstateController().deleteReserveById(id);

    setState(() {
      _markers.clear();
      _polyline.clear();
      polygonLatLngs.clear();
      area = 0.0;
    });
    Get.back();
  }

  void _deletePoint() {
    if (polygonLatLngs.isEmpty || _markers.isEmpty || _polyline.isEmpty) {
      Fluttertoast.showToast(msg: "You did not select any points");
    } else {
      setState(() {
        _removeMarker();
        //_removePolyline(_point!);
      });
    }
  }

  mapWith(context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(
            left: Dimensions.width10,
            top: Dimensions.height20,
            right: Dimensions.width10,
            bottom: Dimensions.height20,
          ),
          margin: EdgeInsets.only(top: Dimensions.height20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(Dimensions.radius30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Type Select",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              SmallText(
                text: "Would you like to ?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {
                        onMap = true;
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Pick Points",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: Dimensions.width30 * 2,
                  ),
                  GestureDetector(
                      onTap: () {
                        // Get.back();
                        // Get.to(() => const LivePick());
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Walk",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: Dimensions.height20,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 3,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Cancel",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))),
            ],
          ),
        ),
        Positioned(
          left: Dimensions.width10 / 2,
          right: Dimensions.width10 / 2,
          child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: Dimensions.radius20,
              child: FaIcon(
                FontAwesomeIcons.circleQuestion,
                size: Dimensions.height45,
                color: Colors.green,
              )),
        ),
      ],
    );
  }

  restartBox(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Confirm Restart",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to restart mapping?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: BigText(
                          text: "Cancel",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _restart(resId);
                        },
                        child: BigText(
                          text: "Ok",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  confirmRedraw(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Block Already Drawn",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to Re-Map or Continue?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          // for (var element in polygonLatLngs) {
                          //   if (element != polygonLatLngs.last) {

                          //   }
                          //   _setMarker(element);
                          // }

                          for (int i = 0; i < polygonLatLngs.length; i++) {
                            if (i >= polygonLatLngs.length - 5) {
                              _setMarker(polygonLatLngs[i]);
                            }
                            // else{
                            //   _setDrawnMarker(polygonLatLngs[i]);
                            // }
                          }

                          //_setMarker(polygonLatLngs.last);

                          setState(() {
                            reserveEmpty = true;
                            onMap = true;
                          });
                          Get.back();
                        },
                        child: BigText(
                          text: "Continue",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.back();
                          setState(() {
                            reserveEmpty = true;
                            onMap = true;
                          });
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  elevation: 0,
                                  backgroundColor: Colors.transparent,
                                  child: restartContentBox(context),
                                );
                              });
                        },
                        child: BigText(
                          text: "Restart",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      )
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  restartContentBox(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Confirm Restart",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to restart mapping?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: BigText(
                          text: "Cancel",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _restart(resId);
                        },
                        child: BigText(
                          text: "Ok",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }
}
