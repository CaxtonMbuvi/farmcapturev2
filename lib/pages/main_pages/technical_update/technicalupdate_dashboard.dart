import 'dart:convert';

import 'package:farmcapturev2/pages/main_pages/estate/estate_dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/intervention_screen.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/ipm_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../biodiversity/biodiversity_dashboard.dart';

class TechnicalUpdateDashboardScreen extends StatefulWidget {
  const TechnicalUpdateDashboardScreen({super.key});

  @override
  State<TechnicalUpdateDashboardScreen> createState() => _TechnicalUpdateDashboardScreenState();
}

class _TechnicalUpdateDashboardScreenState extends State<TechnicalUpdateDashboardScreen> {
  var _selectedIndex = Get.arguments;

  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


    @override
    void didChangeDependencies() {
      super.didChangeDependencies();

      _getDivision();
    }
  //int _selectedIndex = -1;

  List<dynamic> items = [
    {"image": 'assets/images/ic_3_2.png', "name": "Tree Survey" , "descr": "Trees Survey Module"},
    {"image": 'assets/images/ic_ipm.png', "name": "Integrated Pest Management" , "descr": "IPM Module"},
    {"image": 'assets/images/ic_intervention.png', "name": "Interventions" , "descr": "Interventions Module"},
  ];

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data2);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();
      });
    } else {
      Get.back();
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          //height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30 * Dimensions.height10 / 1.5,
                child: Stack(
                  children: [
                    Container(
                      height: Dimensions.height15 * 10,
                      padding: EdgeInsets.only(
                        top: Dimensions.height30 * 2,
                        left: Dimensions.width20,
                        right: Dimensions.width20,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(Dimensions.radius30 * 4),
                          bottomRight: Radius.circular(Dimensions.radius30 * 4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: Dimensions.iconSize24,
                                  ),
                                  onPressed: () {
                                    Get.to(() => const EstateDashboardScreen(), arguments: 2);
                                  },
                                ),
                                BigText(
                                  text: "TECHNICAL UPDATE DASHBOARD",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font20,
                                ),
                                SizedBox(
                                  width: Dimensions.width30,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                height: Dimensions.screenHeight,
                padding: EdgeInsets.only(
                  left: Dimensions.height30,
                  right: Dimensions.height30,
                  top: Dimensions.height20,
                ),
                child: ListView.builder(
                  itemCount: items.length,
                  physics: const NeverScrollableScrollPhysics(),
                  // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //     crossAxisCount: 2,
                  //     childAspectRatio: 3 / 2,
                  //     crossAxisSpacing: 30,
                  //     mainAxisSpacing: 30),
                  itemBuilder: (context, index) {
                    var item = items[index];
                    return GestureDetector(
                      onTap: () async{
                        setState(() {
                          if (_selectedIndex == index) {
                            if (_selectedIndex == 0) {
                              Get.to(() => const TreeSurveyDashboardScreen(),);
                            } else if (_selectedIndex == 1) {
                              Get.to(() => const IPMManagementScreen(),);
                            } else if (_selectedIndex == 2) {
                              Get.to(() => const InterventionScreen(),);
                            }
                          } else {
                            _selectedIndex = index;
                          }
                          print(_selectedIndex.toString());
                        });
                        if (_selectedIndex == 0) {
                          Get.to(() => const TreeSurveyDashboardScreen(),);
                        } else if (_selectedIndex == 1) {
                          Get.to(() => const IPMManagementScreen(),);
                        } else if (_selectedIndex == 2) {
                          Get.to(() => const InterventionScreen(),);
                        }
                      },
                      child: Center(
                        child: Container(
                          height: Dimensions.height30 * 5,
                          width: Dimensions.width30 * 10,
                          margin: const EdgeInsets.only(bottom: 20),
                          decoration: _selectedIndex == index ?
                          BoxDecoration(
                            color: AppColors.mainColor,
                            borderRadius: BorderRadius.circular(Dimensions.radius20 / 4),
                          ):
                          BoxDecoration(
                              color: AppColors.textWhite,
                              borderRadius: BorderRadius.circular(
                                  Dimensions.radius20 / 4),
                              border: Border.all(
                                width: 2,
                                color: AppColors.mainColor,
                              )
                            ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                item["image"].toString(),
                                width: 60,
                                height: 60,
                                color: _selectedIndex == index ? AppColors.textWhite : AppColors.mainColor,
                              ),
                              BigText(
                                text: item["name"].toString(),
                                color: AppColors.black,
                                size: Dimensions.font16 - 4,
                              ),
                              const Divider(color: Colors.black, indent: 20, endIndent: 20,),
                              SmallText(
                                text: item["descr"].toString(),
                                color: AppColors.black,
                                size: Dimensions.font12,
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}