import 'dart:convert';
import 'dart:io';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/models/technical_update/weather_type_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../controllers/location/location_controller.dart';
import '../../../../models/technical_update/cpa_types_model.dart';
import '../../../../models/technical_update/farmers/intervention_model.dart';
import '../../../../models/technical_update/intervention_apllication_method_model.dart';
import '../../../../models/technical_update/intervention_type_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/base/snackbar.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';
import '../technicalupdate_dashboard.dart';

class InterventionScreen extends StatefulWidget {
  const InterventionScreen({super.key});

  @override
  State<InterventionScreen> createState() => _InterventionScreenState();
}

class _InterventionScreenState extends State<InterventionScreen> {
  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController selectedinterventionController =
      TextEditingController();
  TextEditingController selectedinterventionCategoryController =
      TextEditingController();
  TextEditingController selectedWeatherTypeController =
      TextEditingController();
  TextEditingController applicationTypeController = TextEditingController();
  TextEditingController amountAppliedController = TextEditingController();
  TextEditingController interventionController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;
  final List<InterventionTypeModelResultElement> _interventionType = [];
  InterventionTypeModelResultElement? selectedinterventionType;
  
  final List<CPATypesResultElement> _categoryType = [];
  CPATypesResultElement? selectedCategoryType;

  final List<WeatherTypeModelResultElement> _weatherType = [];
  WeatherTypeModelResultElement? selectedweatherType;

  final List<InterventionApplicationMethodModelResultElement> _applicationType =
      [];
  InterventionApplicationMethodModelResultElement? selectedapplicationType;

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        dateController.text = formattedDate;

        print(date.toString());
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getDivision();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;
      });

      await _getData();
    } else {
      Get.back();
    }
  }

  _getData() async {
    List<dynamic> resInterventionType = [];
    var res8 =
        await LocationController().getLocationsLocal(interventionTypeTable);
    //print("Hello there"+ res[0].toString());
    if (res8 != null) {
      resInterventionType = res8;
      for (var i in resInterventionType) {
        InterventionTypeModelResultElement model =
            InterventionTypeModelResultElement(
                resultId: i['Id'],
                code: i['code'],
                name: i['name'],
                datecomparer: i['datecomparer']);

        _interventionType.add(model);
      }
    }

    List<dynamic> resCategoryType = [];
    var resCategory =
        await LocationController().getLocationsLocal(cpaTypeTable);
    //print("Hello there"+ res[0].toString());
    if (resCategory != null) {
      resCategoryType = resCategory;
      for (var i in resCategoryType) {
        CPATypesResultElement model =
            CPATypesResultElement(
                resultId: i['Id'],
                code: i['code'],
                name: i['name'],
                datecomparer: i['datecomparer']);

        _categoryType.add(model);
      }
    }

    List<dynamic> resWeatherType = [];
    var resWeather =
        await LocationController().getLocationsLocal(weatherTypeTable);
    //print("Hello there"+ res[0].toString());
    if (resWeather != null) {
      resWeatherType = resWeather;
      for (var i in resWeatherType) {
        WeatherTypeModelResultElement model = WeatherTypeModelResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _weatherType.add(model);
      }
    }

    List<dynamic> resApplicationType = [];
    var resApplication = await LocationController()
        .getLocationsLocal(interventionApplicationMethodTable);
    //print("Hello there"+ res[0].toString());
    if (resApplication != null) {
      resApplicationType = resApplication;
      for (var i in resApplicationType) {
        InterventionApplicationMethodModelResultElement model =
            InterventionApplicationMethodModelResultElement(
                resultId: i['Id'],
                code: i['code'],
                name: i['name'],
                datecomparer: i['datecomparer']);

        _applicationType.add(model);
      }
    }
  }

  _submit() async{
    if (divisionController.text == '') {
      showCustomSnackBar("Please add A Division", context, ContentType.failure,
          title: "Failed");
    } else if (blockController.text.isEmpty) {
      showCustomSnackBar("Please add A Block", context, ContentType.failure,
          title: "Failed");
    } else {
      InterventionResultElement model = InterventionResultElement();
      model.main_division_id = selectedDivisionId;
      model.block_id = selectedBlockId;
      model.intervention_type = selectedinterventionController.text;
      model.category_used = selectedinterventionCategoryController.text;
      model.amount_applied = amountAppliedController.text;
      model.date_applied = dateController.text;
      model.cpa_name = selectedinterventionController.text;
      model.cpa_type_used = selectedinterventionCategoryController.text;
      model.application_methd = applicationTypeController.text;
      model.weather_type = selectedWeatherTypeController.text;

      List<dynamic> interventions = [];
        interventions.add(model);

        int i = await FarmersController().saveAll(interventions, farmerInterventionTable);

        if (i >= 0) {
          print("Success here");

          setState(() => isLoading = false);
          Get.back();
          Get.to(() => TechnicalUpdateDashboardScreen(), arguments: 2);
        }else{
          setState(() => isLoading = false);
        }
 
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const TechnicalUpdateDashboardScreen(),
                          arguments: 1);
                    },
                  ),
                  BigText(
                    text: "INTERVENTIONS FORM",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: isLoading
            ? const CustomLoader()
            : Container(
                padding: EdgeInsets.symmetric(horizontal: Dimensions.width15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: Dimensions.height20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: Dimensions.screenWidth / 2 - 20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Division",
                                color: Colors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              FormFields(
                                textEditingController: divisionController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: Dimensions.screenWidth / 2 - 20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Block",
                                color: Colors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              FormFields(
                                textEditingController: blockController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Type of Intervention",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    SizedBox(
                      height: 60.0,
                      child: FormField<InterventionTypeModelResultElement>(
                        builder: (FormFieldState<InterventionTypeModelResultElement> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                  color: Colors.lightGreen,
                                  width: 2,
                                ),
                              ),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<InterventionTypeModelResultElement>(
                                value: selectedinterventionType,
                                isExpanded: true,
                                onChanged: (InterventionTypeModelResultElement?
                                    newValue) async {
                                  setState(() {
                                    selectedinterventionType = newValue;
                                    selectedinterventionController.text =
                                        selectedinterventionType!.resultId
                                            .toString();
                                    print(
                                        "Here is Intervention Type ==*** ${selectedinterventionType!.name.toString()} and id === ${selectedinterventionType!.resultId.toString()}");
                                  });
                                },
                                items: _interventionType.map(
                                    (InterventionTypeModelResultElement value) {
                                  return DropdownMenuItem<
                                      InterventionTypeModelResultElement>(
                                    value: value,
                                    child: Text(
                                      value.name.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Category Used",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    SizedBox(
                      height: 60.0,
                      child: FormField<CPATypesResultElement>(
                        builder:
                            (FormFieldState<CPATypesResultElement>
                                state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                  color: Colors.lightGreen,
                                  width: 2,
                                ),
                              ),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<CPATypesResultElement>(
                                value: selectedCategoryType,
                                isExpanded: true,
                                onChanged: (CPATypesResultElement?
                                    newValue) async {
                                  setState(() {
                                    selectedCategoryType = newValue;
                                    selectedinterventionCategoryController.text =
                                        (selectedCategoryType!.resultId)
                                            .toString();
                                    print(
                                        "Here is Category Type ==*** ${selectedCategoryType!.name.toString()} and id === ${selectedCategoryType!.resultId.toString()}");
                                  });
                                },
                                items: _categoryType.map(
                                    (CPATypesResultElement value) {
                                  return DropdownMenuItem<
                                      CPATypesResultElement>(
                                    value: value,
                                    child: Text(
                                      value.name.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Application Method",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    SizedBox(
                      height: 60.0,
                      child: FormField<
                          InterventionApplicationMethodModelResultElement>(
                        builder: (FormFieldState<
                                InterventionApplicationMethodModelResultElement>
                            state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                  color: Colors.lightGreen,
                                  width: 2,
                                ),
                              ),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<
                                  InterventionApplicationMethodModelResultElement>(
                                value: selectedapplicationType,
                                isExpanded: true,
                                onChanged:
                                    (InterventionApplicationMethodModelResultElement?
                                        newValue) async {
                                  setState(() {
                                    selectedapplicationType = newValue;
                                    applicationTypeController.text =
                                        selectedapplicationType!.resultId
                                            .toString();
                                    print(
                                        "Here is Application Type ==*** ${selectedapplicationType!.name.toString()} and id === ${selectedapplicationType!.resultId.toString()}");
                                  });
                                },
                                items: _applicationType.map(
                                    (InterventionApplicationMethodModelResultElement
                                        value) {
                                  return DropdownMenuItem<
                                      InterventionApplicationMethodModelResultElement>(
                                    value: value,
                                    child: Text(
                                      value.name.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Amount Applied(litres/task)",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    FormFields(
                      textEditingController: amountAppliedController,
                      inputType: TextInputType.number,
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Weather Type",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    SizedBox(
                      height: 60.0,
                      child: FormField<WeatherTypeModelResultElement>(
                        builder: (FormFieldState<WeatherTypeModelResultElement>
                            state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                  color: Colors.lightGreen,
                                  width: 2,
                                ),
                              ),
                            ),
                            child: DropdownButtonHideUnderline(
                              child:
                                  DropdownButton<WeatherTypeModelResultElement>(
                                value: selectedweatherType,
                                isExpanded: true,
                                onChanged: (WeatherTypeModelResultElement?
                                    newValue) async {
                                  setState(() {
                                    selectedweatherType = newValue;
                                    selectedWeatherTypeController.text =
                                        selectedweatherType!.resultId
                                            .toString();
                                    print(
                                        "Here is Weather Type ==*** ${selectedweatherType!.name.toString()} and id === ${selectedweatherType!.resultId.toString()}");
                                  });
                                },
                                items: _weatherType
                                    .map((WeatherTypeModelResultElement value) {
                                  return DropdownMenuItem<
                                      WeatherTypeModelResultElement>(
                                    value: value,
                                    child: Text(
                                      value.name.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Date Done",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: Dimensions.height10,
                          bottom: Dimensions.height10),
                      child: TextField(
                        autocorrect: true,
                        controller: dateController,
                        keyboardType: TextInputType.text,
                        readOnly: false,
                        onTap: () {
                          selectedTimePicker(context);
                        },
                        decoration: InputDecoration(
                          suffixIcon: const Icon(Icons.calendar_month),
                          hintStyle: const TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(Dimensions.radius15 - 5)),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(Dimensions.radius15)),
                            borderSide: const BorderSide(
                                color: Colors.lightGreen, width: 2),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height15,
                    ),
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          _submit();
                        },
                        child: Container(
                          height: Dimensions.height30 * 2,
                          width: Dimensions.width30 * Dimensions.width10,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius30),
                          ),
                          child: Center(
                            child: BigText(
                              text: "Submit",
                              color: AppColors.textWhite,
                              size: Dimensions.font16,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
