import 'dart:convert';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../controllers/location/location_controller.dart';
import '../../../../models/technical_update/farmers/damage_count_model.dart';
import '../../../../models/technical_update/natural_damage_cause_model.dart';
import '../../../../resources/base/snackbar.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';
import 'dashboard.dart';

class DamagedTreeCountFormScreen extends StatefulWidget {
  const DamagedTreeCountFormScreen({super.key});

  @override
  State<DamagedTreeCountFormScreen> createState() =>
      _DamagedTreeCountFormScreenState();
}

class _DamagedTreeCountFormScreenState
    extends State<DamagedTreeCountFormScreen> {
  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController noOfTreesImpactedController = TextEditingController();
  TextEditingController damageController = TextEditingController();
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;
  final List<NaturalDamageCauseModelResultElement> _damages = [];
  NaturalDamageCauseModelResultElement? selectedDamages;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getDivision();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;
      });

      List<dynamic> resDamages = [];
      var res8 =
          await LocationController().getLocationsLocal(naturalDamageCauseTable);
      //print("Hello there"+ res[0].toString());
      if (res8 != null) {
        resDamages = res8;
        for (var i in resDamages) {
          NaturalDamageCauseModelResultElement model =
              NaturalDamageCauseModelResultElement(
                  resultId: i['Id'],
                  code: i['code'],
                  name: i['name'],
                  datecomparer: i['datecomparer']);

          _damages.add(model);
        }
      }
    } else {
      Get.back();
    }
  }

  submit() async{
    if (divisionController.text == '') {
      showCustomSnackBar("Please add A Division", context, ContentType.failure,
          title: "Failed");
    } else if (blockController.text.isEmpty) {
      showCustomSnackBar("Please add A Block", context, ContentType.failure,
          title: "Failed");
    } else {
      DamageTreeCountResultModel model = DamageTreeCountResultModel();
      model.blockId = selectedBlockId;
      model.typeOfDamageId = selectedDamages!.resultId.toString();
      model.notOfTreesAffected = noOfTreesImpactedController.text;

      List<dynamic> treeCount = [];
      treeCount.add(model);

      int i = await FarmersController().saveAll(treeCount, damageTreeCountTable);

      if (i >= 0) {
        print("Success here");
      }

      setState(() => isLoading = false);
      Get.to(() => const TreeSurveyDashboardScreen(),);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const TreeSurveyDashboardScreen(),
                          arguments: 1);
                    },
                  ),
                  BigText(
                    text: "DAMAGED TREE COUNT FORM",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: Dimensions.width15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: Dimensions.screenWidth / 2 - 20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BigText(
                          text: "Division",
                          color: Colors.black,
                          size: Dimensions.font16 - 2,
                        ),
                        FormFields(
                          textEditingController: divisionController,
                          inputType: TextInputType.text,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: Dimensions.screenWidth / 2 - 20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BigText(
                          text: "Block",
                          color: Colors.black,
                          size: Dimensions.font16 - 2,
                        ),
                        FormFields(
                          textEditingController: blockController,
                          inputType: TextInputType.text,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Select Type Of Damage",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              FormField<NaturalDamageCauseModelResultElement>(
                builder: (FormFieldState<NaturalDamageCauseModelResultElement>
                    state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      //hintStyle: TextStyle(color: AppColors.black),
                      filled: true,
                      fillColor: Colors.white70,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(Dimensions.radius15 - 5)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(Dimensions.radius15)),
                        borderSide: const BorderSide(
                            color: Colors.lightGreen, width: 2),
                      ),
                    ),
                    child: DropdownButtonHideUnderline(
                      child:
                          DropdownButton<NaturalDamageCauseModelResultElement>(
                        value: selectedDamages,
                        isExpanded: true,
                        onChanged: (NaturalDamageCauseModelResultElement?
                            newValue) async {
                          setState(() {
                            selectedDamages = newValue;
                            damageController.text =
                                selectedDamages!.resultId.toString();
                            print(
                                "Here is Nationality ==*** ${selectedDamages!.name.toString()} and id === ${selectedDamages!.resultId.toString()}");
                          });
                        },
                        items: _damages
                            .map((NaturalDamageCauseModelResultElement value) {
                          return DropdownMenuItem<
                              NaturalDamageCauseModelResultElement>(
                            value: value,
                            child: Text(value.name.toString(),
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  color: Colors.black,
                                )),
                          );
                        }).toList(),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "No. Of Trees Impacted",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              FormFields(
                textEditingController: noOfTreesImpactedController,
                inputType: TextInputType.number,
              ),
              SizedBox(
                height: Dimensions.height15,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    submit();
                  },
                  child: Container(
                    height: Dimensions.height30 * 2,
                    width: Dimensions.width30 * Dimensions.width10,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(Dimensions.radius30),
                    ),
                    child: Center(
                      child: BigText(
                        text: "Submit",
                        color: AppColors.textWhite,
                        size: Dimensions.font16,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
