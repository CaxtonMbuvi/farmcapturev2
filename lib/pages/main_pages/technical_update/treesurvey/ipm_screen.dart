import 'dart:convert';
import 'dart:io';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/technicalupdate_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as path;

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../controllers/location/location_controller.dart';
import '../../../../models/technical_update/farmers/ipm_management_model.dart';
import '../../../../models/technical_update/pesticide_disease_damage_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/base/snackbar.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';
import 'dashboard.dart';

class IPMManagementScreen extends StatefulWidget {
  const IPMManagementScreen({super.key});

  @override
  State<IPMManagementScreen> createState() => _IPMManagementScreenState();
}

class _IPMManagementScreenState extends State<IPMManagementScreen> {
  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController typeOfDiseaseController = TextEditingController();
  TextEditingController noOfTreesAffectedController = TextEditingController();
  TextEditingController damageCausedController = TextEditingController();
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;
  final List<PesticideDiseaseDamageModelResultElement> _diseaseType = [];
  PesticideDiseaseDamageModelResultElement? selectedDiseaseType;
  List<dynamic> imageList = [];

  File? image;
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getDivision();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;
      });

      List<dynamic> resDiseaseType = [];
      var res8 = await LocationController()
          .getLocationsLocal(pesticideAndDiseaseDamageTable);
      //print("Hello there"+ res[0].toString());
      if (res8 != null) {
        resDiseaseType = res8;
        for (var i in resDiseaseType) {
          PesticideDiseaseDamageModelResultElement model =
              PesticideDiseaseDamageModelResultElement(
                  resultId: i['Id'],
                  code: i['code'],
                  name: i['name'],
                  datecomparer: i['datecomparer']);

          _diseaseType.add(model);
        }
      }
    } else {
      Get.back();
    }
  }

  Future _getImage() async {
    final picker = ImagePicker();

    var pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (pickedFile != null) {
      final bytes = File(pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

       // Get the file from the picked file
          File imageFile = File(pickedFile.path);

          // Get the directory of the file
          String dir = path.dirname(imageFile.path);

          // Define a new file name
          String newFileName = "new_image_name${imageList.length}.jpg";

          // Create a new file instance with the new path
          File newImage = imageFile.renameSync('$dir/$newFileName');

          // Now you can use the new image
          image = File(newImage.path);
          print("image is" + image.toString());

      setState(() {
        //image = File(pickedFile.path);
        imageList.add(image);
        image.toString();
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    //return image;
  }

  _submit() async {
    if (divisionController.text == '') {
      showCustomSnackBar("Please add A Division", context, ContentType.failure,
          title: "Failed");
    } else if (blockController.text.isEmpty) {
      showCustomSnackBar("Please add A Block", context, ContentType.failure,
          title: "Failed");
    } else if (imageList.isEmpty) {
      showCustomSnackBar("Please add An Image", context, ContentType.failure,
          title: "Failed");
    } else {
      PestAndDiseaseResultModel model = PestAndDiseaseResultModel();
      model.blockId = selectedBlockId;
      model.divisionId = selectedDivisionId;
      model.typeOfDamageId = damageCausedController.text;
      model.noOfTreeAffected = noOfTreesAffectedController.text;
      model.damageCaused = damageCausedController.text;

      List<dynamic> ipms = [];
      ipms.add(model);

      int i = await FarmersController().saveAll(ipms, ipmManagementTable);

      if (i >= 0) {
        print("Success here");
        int j = 0;

        for (var element in imageList) {
         
          var transactionNo =
              'UNI_${DateTime.now().millisecondsSinceEpoch.toString()}';
          IntegratedPestManagementPhotoResultModel model2 =
              IntegratedPestManagementPhotoResultModel();
          model2.photoUrl = element.toString();
          model2.photoUniqNo = transactionNo.toString();
          model2.divisionId = selectedDivisionId;
          model2.blockId = selectedBlockId;
          model2.entryDate = DateTime.now().toString();

          List<dynamic> iPMphoto = [];
          iPMphoto.add(model2);

          j = await FarmersController()
              .saveAll(iPMphoto, ipmManagementImagesTable);
        }

        if (j >= 0) {
          setState(() => isLoading = false);
          Get.to(() => const TechnicalUpdateDashboardScreen(), arguments: 1);
        } else {
          setState(() => isLoading = false);
        }
      } else {
        setState(() => isLoading = false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const TechnicalUpdateDashboardScreen(),
                          arguments: 1);
                    },
                  ),
                  BigText(
                    text: "IPM DAMAGE FORM",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: isLoading
            ? const CustomLoader()
            : Container(
                padding: EdgeInsets.symmetric(horizontal: Dimensions.width15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: Dimensions.height20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: Dimensions.screenWidth / 2 - 20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Division",
                                color: Colors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              FormFields(
                                textEditingController: divisionController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: Dimensions.screenWidth / 2 - 20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Block",
                                color: Colors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              FormFields(
                                textEditingController: blockController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Type of Disease",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    SizedBox(
                      height: 60.0,
                      child:
                          FormField<PesticideDiseaseDamageModelResultElement>(
                        builder: (FormFieldState<
                                PesticideDiseaseDamageModelResultElement>
                            state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                  color: Colors.lightGreen,
                                  width: 2,
                                ),
                              ),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<
                                  PesticideDiseaseDamageModelResultElement>(
                                value: selectedDiseaseType,
                                isExpanded: true,
                                onChanged:
                                    (PesticideDiseaseDamageModelResultElement?
                                        newValue) async {
                                  setState(() {
                                    selectedDiseaseType = newValue;
                                    typeOfDiseaseController.text =
                                        selectedDiseaseType!.resultId
                                            .toString();
                                    print(
                                        "Here is Disease Type ==*** ${selectedDiseaseType!.name.toString()} and id === ${selectedDiseaseType!.resultId.toString()}");
                                  });
                                },
                                items: _diseaseType.map(
                                    (PesticideDiseaseDamageModelResultElement
                                        value) {
                                  return DropdownMenuItem<
                                      PesticideDiseaseDamageModelResultElement>(
                                    value: value,
                                    child: Text(
                                      value.name.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "No. Of Trees Affected",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    FormFields(
                      textEditingController: noOfTreesAffectedController,
                      inputType: TextInputType.number,
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Damage Caused(Short Comment)",
                      color: Colors.black,
                      size: Dimensions.font16 - 2,
                    ),
                    FormFields(
                      maxLines: 3,
                      textEditingController: damageCausedController,
                      inputType: TextInputType.text,
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ElevatedButton(
                        onPressed: _getImage,
                        child: Text('Add Image'),
                      ),
                    ),
                    imageList.isEmpty
                        ? Container()
                        : GridView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: imageList.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 4.0,
                              mainAxisSpacing: 4.0,
                            ),
                            itemBuilder: (BuildContext context, int index) {
                              File image1 = imageList[index];

                              return ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius15),
                                child: Image.file(image1, fit: BoxFit.cover),
                              );
                            },
                          ),
                    SizedBox(
                      height: Dimensions.height30,
                    ),
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          _submit();
                        },
                        child: Container(
                          height: Dimensions.height30 * 2,
                          width: Dimensions.width30 * Dimensions.width10,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius30),
                          ),
                          child: Center(
                            child: BigText(
                              text: "Submit",
                              color: AppColors.textWhite,
                              size: Dimensions.font16,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height30,
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
