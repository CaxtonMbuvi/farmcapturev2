import 'package:farmcapturev2/pages/main_pages/technical_update/technicalupdate_dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/blocktreecount_screen.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/damagedTreeCount_screen.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/gapfilling_screen.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/girth_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/small_text.dart';

class TreeSurveyDashboardScreen extends StatefulWidget {
  const TreeSurveyDashboardScreen({super.key});

  @override
  State<TreeSurveyDashboardScreen> createState() =>
      _TreeSurveyDashboardScreenState();
}

class _TreeSurveyDashboardScreenState extends State<TreeSurveyDashboardScreen> {
  var _selectedIndex = Get.arguments;
  //int _selectedIndex = -1;

  List<dynamic> items = [
    {
      "image": 'assets/images/ic_tree_count.png',
      "name": "Tree Count",
      "descr": "Trees Count Form"
    },
    {
      "image": 'assets/images/ic_tree_damage.png',
      "name": "Damage/Loss Count",
      "descr": "Tree Damage Count Form"
    },
    {
      "image": 'assets/images/ic_girth.png',
      "name": "Girth Module",
      "descr": "Tree Girth Measurements"
    },
    {
      "image": 'assets/images/ic_gap.png',
      "name": "Gap Filling",
      "descr": "New Planting(Gap Filling)"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30 * Dimensions.height10 / 1.5,
                child: Stack(
                  children: [
                    Container(
                      height: Dimensions.height15 * 10,
                      padding: EdgeInsets.only(
                        top: Dimensions.height30 * 2,
                        left: Dimensions.width20,
                        right: Dimensions.width20,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(Dimensions.radius30 * 4),
                          bottomRight: Radius.circular(Dimensions.radius30 * 4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: Dimensions.iconSize24,
                                  ),
                                  onPressed: () {
                                    Get.to(() => const TechnicalUpdateDashboardScreen(), arguments: 0);
                                  },
                                ),
                                BigText(
                                  text: "TREE SURVEY DASHBOARD",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font20,
                                ),
                                SizedBox(
                                  width: Dimensions.width30,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: Dimensions.width10),
                  child: Center(
                    child: GridView.builder(
                      itemCount: items.length,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 3 / 2,
                          crossAxisSpacing: 30,
                          mainAxisSpacing: 50),
                      itemBuilder: (context, index) {
                        var item = items[index];
                        return GestureDetector(
                          onTap: () async {
                            setState(() {
                              if (_selectedIndex == index) {
                                if (_selectedIndex == 0) {
                                  Get.to(() => const BlockTreeCountFormScreen(),);
                                } else if (_selectedIndex == 1) {
                                  Get.to(() => const DamagedTreeCountFormScreen(),);
                                } else if (_selectedIndex == 2) {
                                  Get.to(() => const GirthScreen(),);
                                } else if (_selectedIndex == 3) {
                                  Get.to(() => const GapFillingScreen(),);
                                }
                              } else {
                                _selectedIndex = index;
                              }
                              print(_selectedIndex.toString());
                            });
                            if (_selectedIndex == 0) {
                              Get.to(() => const BlockTreeCountFormScreen(),);
                            } else if (_selectedIndex == 1) {
                              Get.to(() => const DamagedTreeCountFormScreen(),);
                            } else if (_selectedIndex == 2) {
                              Get.to(() => const GirthScreen(),);
                            } else if (_selectedIndex == 3) {
                              Get.to(() => const GapFillingScreen(),);
                            }
                          },
                          child: Container(
                            height: Dimensions.height30 * 6,
                            width: Dimensions.width30 * 6,
                            decoration: _selectedIndex == index
                                ? BoxDecoration(
                                    color: AppColors.mainColor,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20 / 4),
                                  )
                                : BoxDecoration(
                                    color: AppColors.textWhite,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20 / 4),
                                    border: Border.all(
                                      width: 2,
                                      color: AppColors.mainColor,
                                    )),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  item["image"].toString(),
                                  width: 60,
                                  height: 60,
                                  color: _selectedIndex == index
                                      ? AppColors.textWhite
                                      : AppColors.mainColor,
                                ),
                                BigText(
                                  text: item["name"].toString(),
                                  color: AppColors.black,
                                  size: Dimensions.font16 - 4,
                                ),
                                const Divider(
                                  color: Colors.black,
                                  indent: 20,
                                  endIndent: 20,
                                ),
                                SmallText(
                                  text: item["descr"].toString(),
                                  color: AppColors.black,
                                  size: Dimensions.font12,
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
