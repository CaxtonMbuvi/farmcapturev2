import 'dart:convert';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/models/visit/visitinspection_model.dart';
import 'package:farmcapturev2/resources/widgets/small_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../models/technical_update/farmers/girth_measurement.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/base/snackbar.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';
import 'dashboard.dart';

class GirthScreen extends StatefulWidget {
  const GirthScreen({super.key});

  @override
  State<GirthScreen> createState() => _GirthScreenState();
}

class _GirthScreenState extends State<GirthScreen> {
  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController treeNoController = TextEditingController();
  TextEditingController girthController = TextEditingController();
  final TextEditingController _controller2 = TextEditingController();
  List<dynamic> filteredItems2 = [];
  //List<dynamic> filteredItems = [];
  String dateString = '';
  List<dynamic> _allRecords = [];
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;
  bool selectedTreeNo = false;

  String getDate(String a) {
    //var date = DateTime.parse(a.substring(0, 10));
    var date = DateTime.parse(a);
    print(date.toString());
    return DateFormat('EEEE, d MMM, yyyy').format(date).toString();
  }

  getAllTrees() async {
    try {
      List<dynamic> girths = [];
      var res = await FarmersController().getAllGirthMeasurements(
          'main_division_id', selectedDivisionId, 'block_id', selectedBlockId);

      if (res != null) {
        setState(() {
          filteredItems2 = res;
          //girths = res;
        });

        // for (var i in girths) {
        //   GirthMeasurementResultModel model = GirthMeasurementResultModel(
        //     id: i['_Id'],
        //     treeNo: i['tree_no'],
        //     girthLengthCm: i['girth_measure'],
        //     entryDate: i['date_today'],
        //   );
        //   print("Model is" + model.toString());

        //   _allRecords.add(model);
        // }

        setState(() => isLoading = false);
      } else {
        print("Did Not get girths");
        setState(() => isLoading = false);
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    _getDivision();

    _controller2.addListener(() {
      setState(() {
        filteredItems2 = _allRecords
            .where((item) =>
                item['tree_no']
                    .toString()
                    .toLowerCase()
                    .contains(_controller2.text.toLowerCase()) ||
                item['girth_measure']
                    .toString()
                    .toLowerCase()
                    .contains(_controller2.text.toLowerCase()) ||
                item['date_today']
                    .toString()
                    .toLowerCase()
                    .contains(_controller2.text.toLowerCase()))
            .toList();
      });
    });

    treeNoController.addListener(() {
      List<dynamic> filteredItems = _allRecords
          .where((item) => item['tree_no']
              .toString()
              .toLowerCase()
              .contains(_controller2.text.toLowerCase()))
          .toList();
      if (filteredItems.isNotEmpty) {
        setState(() {
          selectedTreeNo = true;
        });
      } else {
        setState(() {
          selectedTreeNo = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _controller2.dispose();
    super.dispose();
  }

  _getDivision() async {
    setState(() => isLoading = true);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;
      });

      await getAllTrees();
      setState(() => isLoading = false);
    } else {
      Get.back();
    }
  }

  _submit() async {
    if (divisionController.text == '') {
      showCustomSnackBar("Please add A Division", context, ContentType.failure,
          title: "Failed");
    } else if (blockController.text.isEmpty) {
      showCustomSnackBar("Please add A Block", context, ContentType.failure,
          title: "Failed");
    } else if (treeNoController.text.isEmpty) {
      showCustomSnackBar(
          "Please add A Tree Number", context, ContentType.failure,
          title: "Failed");
    } else if (girthController.text.isEmpty) {
      showCustomSnackBar(
          "Please add A Girth Measure", context, ContentType.failure,
          title: "Failed");
    } else {
      List<dynamic> girths = [];
      var res = await FarmersController().getAllGirthMeasurements(
          'tree_no', treeNoController.text, 'block_id', selectedBlockId);
          if(res != null){
            setState(() {
              girths = res;
            });
          }
      
      GirthMeasurementResultModel model = GirthMeasurementResultModel();
      model.blockId = selectedBlockId;
      model.divisionId = selectedDivisionId;
      //model.entryDate = DateFormat.yMMMd().format(DateTime.now());
      model.entryDate = DateTime.now().toString();
      model.treeNo = treeNoController.text;
      model.girthLengthCm = girthController.text;
      if (girths.isNotEmpty) {
        String id = girths[0]["_Id"].toString();
        print("Found");
        int i = await FarmersController()
            .updateLocal(girthMeasurementTable, model, "_Id", id);
        if (i > 0) {
          setState(() => isLoading = false);
          Get.back();
          Get.to(
            () => const GirthScreen(),
          );
        } else {
          setState(() => isLoading = false);
          showCustomSnackBar(
              "Something Went Wrong", context, ContentType.failure,
              title: "Failed");
        }
      } else {
        List<dynamic> TreeCount = [];
        TreeCount.add(model);

        int i =
            await FarmersController().saveAll(TreeCount, girthMeasurementTable);

        if (i >= 0) {
          print("Success here");
        }

        setState(() => isLoading = false);
        Get.back();
        Get.to(
          () => const GirthScreen(),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const TreeSurveyDashboardScreen(),
                          arguments: 2);
                    },
                  ),
                  BigText(
                    text: "GIRTH FORM",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: isLoading
          ? const CustomLoader()
          : Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: Dimensions.width10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: Dimensions.height20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: Dimensions.screenWidth / 2 - 20,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BigText(
                                  text: "Division",
                                  color: Colors.black,
                                  size: Dimensions.font16 - 2,
                                ),
                                FormFields(
                                  textEditingController: divisionController,
                                  inputType: TextInputType.text,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: Dimensions.screenWidth / 2 - 20,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BigText(
                                  text: "Block",
                                  color: Colors.black,
                                  size: Dimensions.font16 - 2,
                                ),
                                FormFields(
                                  textEditingController: blockController,
                                  inputType: TextInputType.text,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: Dimensions.screenWidth / 2 - 20,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BigText(
                                  text: "Tree No.",
                                  color: Colors.black,
                                  size: Dimensions.font16 - 2,
                                ),
                                FormFields(
                                  textEditingController: treeNoController,
                                  inputType: TextInputType.number,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: Dimensions.screenWidth / 2 - 20,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BigText(
                                  text: "Girth(cm)",
                                  color: Colors.black,
                                  size: Dimensions.font16 - 2,
                                ),
                                FormFields(
                                  textEditingController: girthController,
                                  inputType: TextInputType.text,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Dimensions.height15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {},
                            child: Container(
                              height: Dimensions.height30 * 2,
                              width: Dimensions.screenWidth / 2 -
                                  Dimensions.width20,
                              decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius20),
                              ),
                              child: Center(
                                child: BigText(
                                  text: "Next",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font16,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _submit();
                            },
                            child: Container(
                              height: Dimensions.height30 * 2,
                              width: Dimensions.screenWidth / 2 -
                                  Dimensions.width20,
                              decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius20),
                              ),
                              child: Center(
                                child: BigText(
                                  text: "Submit",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font16,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      const Divider(
                        color: Colors.black,
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      Center(
                        child: BigText(
                          text: "GIRTH RECORDS SUMMARY",
                          color: Colors.black,
                          size: Dimensions.font16 - 2,
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      SizedBox(
                        height: Dimensions.height30 * 3,
                        child: Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: Dimensions.screenWidth / 1.3,
                                padding: EdgeInsets.only(
                                  top: Dimensions.height10,
                                  bottom: Dimensions.height10,
                                ),
                                child: Center(
                                  child: TextField(
                                    controller: _controller2,
                                    autocorrect: true,
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      hintText: "Search",
                                      hintStyle:
                                          const TextStyle(color: Colors.grey),
                                      filled: true,
                                      fillColor: Colors.white,
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(
                                                Dimensions.radius30)),
                                        borderSide: BorderSide(
                                            color: AppColors.textGrey,
                                            width: 2),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(
                                                Dimensions.radius30)),
                                        borderSide: BorderSide(
                                            color: AppColors.mainColor,
                                            width: 2),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Container(
                                  width: Dimensions.width30 * 2,
                                  height: Dimensions.height30 * 2,
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width15,
                                    right: Dimensions.width15,
                                  ),
                                  child: Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.magnifyingGlass,
                                      size: Dimensions.height20,
                                      color: AppColors.black,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Card(
                        // child: ListView.builder(
                        //   itemCount: 100,
                        //   shrinkWrap: true,
                        //   physics: NeverScrollableScrollPhysics(),
                        //   itemBuilder: (context, index) {
                        //     return Padding(
                        //       padding: EdgeInsets.symmetric(horizontal: Dimensions.width15,),
                        //       child: Column(
                        //         mainAxisAlignment: MainAxisAlignment.center,
                        //         children: [
                        //           SizedBox(height: Dimensions.height30,),
                        //           Row(
                        //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //             children: [
                        //               BigText(
                        //                 text: "Tree No: 1",
                        //                 color: Colors.grey,
                        //                 size: Dimensions.font16,
                        //               ),
                        //               BigText(
                        //                 text: "1.7 cm",
                        //                 color: Colors.blue,
                        //                 size: Dimensions.font16,
                        //               ),
                        //               BigText(
                        //                 text: "2023-05-12",
                        //                 color: Colors.black,
                        //                 size: Dimensions.font16,
                        //               ),
                        //             ],
                        //           ),
                        //           Divider(color: Colors.grey, thickness: 1.5,)
                        //         ],
                        //       ),
                        //     );
                        //   }
                        // ),
                        child: CustomScrollView(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          slivers: [
                            SliverList(
                              delegate: SliverChildBuilderDelegate(
                                childCount: filteredItems2.length,
                                addAutomaticKeepAlives: false,
                                addRepaintBoundaries: false,
                                (context, index) {
                                  return Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: Dimensions.width15,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          height: Dimensions.height30,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            BigText(
                                              text:
                                                  "Tree No: ${filteredItems2[index]['tree_no']}",
                                              color: Colors.grey,
                                              size: Dimensions.font16,
                                            ),
                                            BigText(
                                              text:
                                                  "${filteredItems2[index]['girth_measure']} cm",
                                              color: Colors.blue,
                                              size: Dimensions.font16,
                                            ),
                                            BigText(
                                              text:
                                                  "${getDate(filteredItems2[index]['date_today'])}",
                                              color: Colors.black,
                                              size: Dimensions.font16,
                                            ),
                                          ],
                                        ),
                                        const Divider(
                                          color: Colors.grey,
                                          thickness: 1.5,
                                        )
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}

// child: Center(
//           child: Padding(
//             padding: EdgeInsets.symmetric(horizontal: Dimensions.width10),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 SizedBox(
//                   height: Dimensions.height20,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     SizedBox(
//                       width: Dimensions.screenWidth / 2 - 20,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           BigText(
//                             text: "Division",
//                             color: Colors.black,
//                             size: Dimensions.font16 - 2,
//                           ),
//                           FormFields(
//                             textEditingController: divisionController,
//                             inputType: TextInputType.text,
//                           ),
//                         ],
//                       ),
//                     ),
//                     SizedBox(
//                       width: Dimensions.screenWidth / 2 - 20,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           BigText(
//                             text: "Block",
//                             color: Colors.black,
//                             size: Dimensions.font16 - 2,
//                           ),
//                           FormFields(
//                             textEditingController: blockController,
//                             inputType: TextInputType.text,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
                
                
//                 SizedBox(
//                   height: Dimensions.height10,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     SizedBox(
//                       width: Dimensions.screenWidth / 2 - 20,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           BigText(
//                             text: "Tree No.",
//                             color: Colors.black,
//                             size: Dimensions.font16 - 2,
//                           ),
//                           FormFields(
//                             textEditingController: treeNoController,
//                             inputType: TextInputType.text,
//                           ),
//                         ],
//                       ),
//                     ),
//                     SizedBox(
//                       width: Dimensions.screenWidth / 2 - 20,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           BigText(
//                             text: "Girth(cm)",
//                             color: Colors.black,
//                             size: Dimensions.font16 - 2,
//                           ),
//                           FormFields(
//                             textEditingController: girthController,
//                             inputType: TextInputType.text,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
                
                
//                 SizedBox(
//                   height: Dimensions.height15,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     GestureDetector(
//                       onTap: () {},
//                       child: Container(
//                         height: Dimensions.height30 * 2,
//                         width: Dimensions.screenWidth / 2 - Dimensions.width20,
//                         decoration: BoxDecoration(
//                           color: Colors.green,
//                           borderRadius:
//                               BorderRadius.circular(Dimensions.radius20),
//                         ),
//                         child: Center(
//                           child: BigText(
//                             text: "Next",
//                             color: AppColors.textWhite,
//                             size: Dimensions.font16,
//                           ),
//                         ),
//                       ),
//                     ),
//                     GestureDetector(
//                       onTap: () {},
//                       child: Container(
//                         height: Dimensions.height30 * 2,
//                         width: Dimensions.screenWidth / 2 - Dimensions.width20,
//                         decoration: BoxDecoration(
//                           color: Colors.green,
//                           borderRadius:
//                               BorderRadius.circular(Dimensions.radius20),
//                         ),
//                         child: Center(
//                           child: BigText(
//                             text: "Submit",
//                             color: AppColors.textWhite,
//                             size: Dimensions.font16,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
                
//                 SizedBox(
//                   height: Dimensions.height10,
//                 ),
//                 Divider(
//                   color: Colors.black,
//                 ),
//                 SizedBox(
//                   height: Dimensions.height10,
//                 ),
                
//                 Center(
//                   child: BigText(
//                     text: "GIRTH RECORDS SUMMARY",
//                     color: Colors.black,
//                     size: Dimensions.font16 - 2,
//                   ),
//                 ),
//                 SizedBox(
//                   height: Dimensions.height10,
//                 ),
                
//                 Expanded(
//                   child: Card(
//                     child: ListView.builder(
//                       itemCount: 100,
//                       itemBuilder: (context, index) {
//                         return Padding(
//                           padding: EdgeInsets.symmetric(horizontal: Dimensions.width15,),
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               SizedBox(height: Dimensions.height30,),
//                               Row(
//                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                 children: [
//                                   BigText(
//                                     text: "Tree No: 1",
//                                     color: Colors.grey,
//                                     size: Dimensions.font16,
//                                   ),
//                                   BigText(
//                                     text: "1.7 cm",
//                                     color: Colors.blue,
//                                     size: Dimensions.font16,
//                                   ),
//                                   BigText(
//                                     text: "2023-05-12",
//                                     color: Colors.black,
//                                     size: Dimensions.font16,
//                                   ),
//                                 ],
//                               ),
//                               Divider(color: Colors.grey, thickness: 1.5,)
//                             ],
//                           ),
//                         );
//                       }
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),