import 'dart:convert';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/treesurvey/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../models/technical_update/farmers/block_tree_count_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/base/snackbar.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';

class BlockTreeCountFormScreen extends StatefulWidget {
  const BlockTreeCountFormScreen({super.key});

  @override
  State<BlockTreeCountFormScreen> createState() => _BlockTreeCountFormScreenState();
}

class _BlockTreeCountFormScreenState extends State<BlockTreeCountFormScreen> {
  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController noOfPlantedTreesController = TextEditingController();
  TextEditingController noOfLinesController = TextEditingController();
  TextEditingController noOfLiveTreesController = TextEditingController();
  TextEditingController noOfTreesSuppliedPlantedController = TextEditingController();
  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getDivision();
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());
    print(data);
    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;
      });
    } else {
      Get.back();
    }
  }


  _addBlockTreeCount() async{
    if (divisionController.text == '') {
      showCustomSnackBar("Please add A Division", context, ContentType.failure,
          title: "Failed");
    } else if (blockController.text.isEmpty) {
      showCustomSnackBar("Please add A Block", context, ContentType.failure,
          title: "Failed");
    } else {
      BlockTreeCountResultModel blockTreeModel = BlockTreeCountResultModel();
      blockTreeModel.blockId = selectedBlockId;
      blockTreeModel.mainDivisionId = selectedDivisionId;
      blockTreeModel.noOfPlantedTrees = noOfPlantedTreesController.text;
      blockTreeModel.noOfLine = noOfLinesController.text;
      blockTreeModel.noOfLifeTree = noOfLiveTreesController.text;
      blockTreeModel.noOfTreeSuppliedPlanting = noOfTreesSuppliedPlantedController.text;

     
      List<dynamic> TreeCount = [];
      TreeCount.add(blockTreeModel);

      int i = await FarmersController().saveAll(TreeCount, farmerBlockTreeCountTable);

      if (i >= 0) {
        print("Success here");
      }

      setState(() => isLoading = false);
      Get.to(() => const TreeSurveyDashboardScreen(),);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const TreeSurveyDashboardScreen(),
                          arguments: 0);
                    },
                  ),
                  BigText(
                    text: "BLOCK TREE COUNT FORM",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: isLoading
        ? const CustomLoader()
        : Container(
            padding: EdgeInsets.symmetric(horizontal: Dimensions.width15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: Dimensions.height20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: Dimensions.screenWidth / 2 - 20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BigText(
                            text: "Division",
                            color: Colors.black,
                            size: Dimensions.font16 - 2,
                          ),
                          FormFields(
                            textEditingController: divisionController,
                            inputType: TextInputType.text,
                            readOnly: true,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: Dimensions.screenWidth / 2 - 20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BigText(
                            text: "Block",
                            color: Colors.black,
                            size: Dimensions.font16 - 2,
                          ),
                          FormFields(
                            textEditingController: blockController,
                            inputType: TextInputType.text,
                            readOnly: true,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: Dimensions.height10,
                ),
                BigText(
                  text: "No. Of Planted Trees",
                  color: Colors.black,
                  size: Dimensions.font16 - 2,
                ),
                FormFields(
                  textEditingController: noOfPlantedTreesController,
                  inputType: TextInputType.number,
                ),
                SizedBox(
                  height: Dimensions.height10,
                ),
                BigText(
                  text: "No. Of Lines",
                  color: Colors.black,
                  size: Dimensions.font16 - 2,
                ),
                FormFields(
                  textEditingController: noOfLinesController,
                  inputType: TextInputType.number,
                ),
                SizedBox(
                  height: Dimensions.height10,
                ),
                BigText(
                  text: "No. Of Live Trees",
                  color: Colors.black,
                  size: Dimensions.font16 - 2,
                ),
                FormFields(
                  textEditingController: noOfLiveTreesController,
                  inputType: TextInputType.number,
                ),
                SizedBox(
                  height: Dimensions.height10,
                ),
                BigText(
                  text: "No. Of Trees Supplied Planting",
                  color: Colors.black,
                  size: Dimensions.font16 - 2,
                ),
                FormFields(
                  textEditingController: noOfTreesSuppliedPlantedController,
                  inputType: TextInputType.number,
                ),
                SizedBox(
                  height: Dimensions.height15,
                ),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      _addBlockTreeCount();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(Dimensions.radius30),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Submit",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
      ),
    );
  }
}
