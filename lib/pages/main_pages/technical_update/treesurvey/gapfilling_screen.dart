import 'dart:convert';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/pages/main_pages/technical_update/technicalupdate_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../controllers/location/location_controller.dart';
import '../../../../models/estate/rubberclones_model.dart';
import '../../../../models/technical_update/farmers/gap_filling_model.dart';
import '../../../../resources/base/snackbar.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';
import 'dashboard.dart';

class GapFillingScreen extends StatefulWidget {
  const GapFillingScreen({super.key});

  @override
  State<GapFillingScreen> createState() => _GapFillingScreenState();
}

class _GapFillingScreenState extends State<GapFillingScreen> {
  TextEditingController divisionController = TextEditingController();
  TextEditingController blockController = TextEditingController();
  TextEditingController requisitionNoController = TextEditingController();
  TextEditingController dominantCloneController = TextEditingController();
  TextEditingController dominantCloneIdController = TextEditingController();
  TextEditingController cloneUsedInReplantingController =
      TextEditingController();
  TextEditingController cloneIdUsedInReplantingController =
      TextEditingController();
  TextEditingController lineNoController = TextEditingController();
  TextEditingController noOfResuppliedTreesController = TextEditingController();

  final List<RubberClonesResultElement> _rubberClones = [];
  RubberClonesResultElement? selectedrubberClones;

  String selectedDivisionName = '';
  String selectedDivisionId = '';
  String selectedBlockName = '';
  String selectedBlockId = '';
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    _getDivision();

    super.initState();
  }

  _getDivision() async {
    setState(() => isLoading = true);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var block = localStorage.getString('SelectedBlock');
    var data = jsonDecode(result.toString());
    var data2 = jsonDecode(block.toString());

    if (data != null && data2 != null) {
      setState(() {
        selectedDivisionId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();

        selectedBlockId = data2["Id"].toString();
        selectedBlockName = data2["name"].toString();

        dominantCloneController.text = data2["rubber_clones_Name"].toString();
        dominantCloneIdController.text = data2["rubber_clones_id"].toString();

        divisionController.text = selectedDivisionName;
        blockController.text = selectedBlockName;
      });

      print("Data is: " + selectedDivisionId.toString());

      print(data2["rubber_clones_Name"].toString() +
          data2["rubber_clones_id"].toString());

      List<dynamic> resClones = [];
      var res8 =
          await LocationController().getLocationsLocal(rubberClonesTable);
      //print("Hello there"+ res[0].toString());
      if (res8 != null) {
        resClones = res8;
        for (var i in resClones) {
          RubberClonesResultElement model = RubberClonesResultElement(
              resultId: i['Id'],
              code: i['code'],
              name: i['name'],
              datecomparer: i['datecomparer']);

          _rubberClones.add(model);
        }
      }

      setState(() => isLoading = false);
    } else {
      Get.back();
    }
  }

  _submit() async {
    //GapFillingResultModel
    if (divisionController.text == '') {
      showCustomSnackBar("Please add A Division", context, ContentType.failure,
          title: "Failed");
    } else if (blockController.text.isEmpty) {
      showCustomSnackBar("Please add A Block", context, ContentType.failure,
          title: "Failed");
    } else {
      GapFillingResultModel model = GapFillingResultModel();
      model.mainDivisionId = selectedDivisionId;
      model.blockId = selectedBlockId;
      model.cloneUsedId = cloneUsedInReplantingController.text;
      model.dominantCloneId = dominantCloneIdController.text;

      model.lineNo = lineNoController.text;
      model.entryDate = DateTime.now().toString();
      model.requisitionNo = requisitionNoController.text;
      model.noOfResuppliedTrees = noOfResuppliedTreesController.text;
      model.cloneUsedId = cloneIdUsedInReplantingController.text;

      List<dynamic> gapFilling = [];
      gapFilling.add(model);

      print(model.toString());

      int i = await FarmersController().saveAll(gapFilling, gapFillingTable);

      if (i >= 0) {
        print("Success here");
      }

      setState(() => isLoading = false);
      Get.to(
        () => const TechnicalUpdateDashboardScreen(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const TreeSurveyDashboardScreen(),
                          arguments: 3);
                    },
                  ),
                  BigText(
                    text: "GAP FILLING FORM",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: Dimensions.width15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: Dimensions.screenWidth / 2 - 20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BigText(
                          text: "Division",
                          color: Colors.black,
                          size: Dimensions.font16 - 2,
                        ),
                        FormFields(
                          textEditingController: divisionController,
                          inputType: TextInputType.text,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: Dimensions.screenWidth / 2 - 20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BigText(
                          text: "Block",
                          color: Colors.black,
                          size: Dimensions.font16 - 2,
                        ),
                        FormFields(
                          textEditingController: blockController,
                          inputType: TextInputType.text,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Requisition No.",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              FormFields(
                textEditingController: requisitionNoController,
                inputType: TextInputType.text,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Dominant Clone",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              FormFields(
                textEditingController: dominantCloneController,
                inputType: TextInputType.text,
                readOnly: true,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Clone Used In Re-Planting",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              SizedBox(
                height: 60.0,
                child: FormField<RubberClonesResultElement>(
                  builder: (FormFieldState<RubberClonesResultElement> state) {
                    return InputDecorator(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(Dimensions.radius15 - 5)),
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(Dimensions.radius15)),
                          borderSide: const BorderSide(
                            color: Colors.lightGreen,
                            width: 2,
                          ),
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<RubberClonesResultElement>(
                          value: selectedrubberClones,
                          isExpanded: true,
                          onChanged:
                              (RubberClonesResultElement? newValue) async {
                            setState(() {
                              selectedrubberClones = newValue;
                              cloneIdUsedInReplantingController.text =
                                  selectedrubberClones!.resultId.toString();
                              print(
                                  "Here is Rubber Clone ==*** ${selectedrubberClones!.name.toString()} and id === ${selectedrubberClones!.resultId.toString()}");
                            });
                          },
                          items: _rubberClones
                              .map((RubberClonesResultElement value) {
                            return DropdownMenuItem<RubberClonesResultElement>(
                              value: value,
                              child: Text(
                                value.name.toString(),
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Line No.",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              FormFields(
                textEditingController: lineNoController,
                inputType: TextInputType.number,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "No. Of Re-Supplies Trees",
                color: Colors.black,
                size: Dimensions.font16 - 2,
              ),
              FormFields(
                textEditingController: noOfResuppliedTreesController,
                inputType: TextInputType.number,
              ),
              SizedBox(
                height: Dimensions.height15,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    _submit();
                  },
                  child: Container(
                    height: Dimensions.height30 * 2,
                    width: Dimensions.width30 * Dimensions.width10,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(Dimensions.radius30),
                    ),
                    child: Center(
                      child: BigText(
                        text: "Submit",
                        color: AppColors.textWhite,
                        size: Dimensions.font16,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: Dimensions.height30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
