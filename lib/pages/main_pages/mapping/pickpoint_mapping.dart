import 'dart:async';
import 'dart:math';

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/farmers/farmmap_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/base/snackbar.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../farmers/single_farmer_dashboard.dart';

class PickPointMapping extends StatefulWidget {
  const PickPointMapping({super.key});

  @override
  State<PickPointMapping> createState() => _PickPointMappingState();
}

class _PickPointMappingState extends State<PickPointMapping> {
  var pageId = Get.arguments;
  List<dynamic> _singleFarmersLocal = [];
  bool isLoading = false;
  final Completer<GoogleMapController> _controllerGoogleMap = Completer();
  GoogleMapController? newGoogleMapController;
  Position? currentLocation;

  final Set<Marker> _markers = <Marker>{};
  final Set<Polygon> _polygons = <Polygon>{};
  List<LatLng> polygonLatLngs = <LatLng>[];
  final Set<Polyline> _polyline = <Polyline>{};

  final int _polygonIdCounter = 1;
  int _markerId = 1;
  int _polylineIdCounter = 1;
  bool onMap = false;
  String? map_category;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  Position? currentPosition;
  var geoLocator = Geolocator();

  locateCurrentPosition() async {
    /**this is getting user current position**/
    Position cPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentPosition = cPosition;
    LatLng latLngPosition =
        LatLng(currentPosition!.latitude, currentPosition!.longitude);

    /**this is animating camera to show user current position**/
    CameraPosition cameraPosition =
        CameraPosition(target: latLngPosition, zoom: 19);
    newGoogleMapController!
        .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  /// location permission*
  LocationPermission? _locationPermission;
  checkIfLocationPermissionAllowed() async {
    _locationPermission = await Geolocator.requestPermission();

    //if permission is denied
    if (_locationPermission == LocationPermission.denied) {
      _locationPermission =
          await Geolocator.requestPermission(); //ask user to allow permission
    }
  }

  _saveFarmMapLocal(var data) async {
    int i = await FarmersController().saveFarmMapLocal(data);
    if (i >= 0) {
      showCustomSnackBar("Mapping Successfull", context, ContentType.success,
          title: "Success");
      Get.back();
      Get.to(
        () => const SingleFarmerDashboard(),
        arguments: pageId,
      );
    } else {
      showCustomSnackBar("Mapping Failed", context, ContentType.failure,
          title: "Failed");
          Get.back();
      Get.to(
        () => const SingleFarmerDashboard(),
        arguments: pageId,
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);

    // _setMarker(LatLng(37.42796133580664, -122.085749655962));
    // polygonLatLngs.add(LatLng(37.42796133580664, -122.085749655962));
  }

  void _setMarker(LatLng point) {
    setState(() {
      _markers.add(
          Marker(markerId: MarkerId(_markerId.toString()), position: point));
      _markerId++;
    });
    print('Markers are: ${_markers.length}');
  }

  void _setPolyline() {
    final String polylineIdVal = 'polygon_$_polylineIdCounter';
    _polylineIdCounter++;

    _polyline.add(
      Polyline(
          polylineId: PolylineId(polylineIdVal),
          points: polygonLatLngs,
          width: 2),
    );
    print('PolyLines are ${_polyline.length.toString()}');
  }

  String generateRand() {
    var rnd = Random();
    var next = rnd.nextInt(10000);
    while (next < 1000) {
      next *= 10;
    }

    var date = DateTime.now().toString();
    var res = next.toString() + date;

    return res.toString();
  }

  Future _getSingleFarmerLocal(String memberNo) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal =
        await FarmersController().getSingleFarmerLocal(memberNo);

    if (_singleFarmersLocal.isNotEmpty) {
      print(_singleFarmersLocal[0]["full_name"]!.toString());

      await checkIfLocationPermissionAllowed();

      setState(() => isLoading = false);
    } else {
      print("Did not Get");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Map Farm",
                    color: Colors.black,
                    size: Dimensions.font16,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: isLoading
          ? const Center(child: CustomLoader())
          : Stack(
              children: [
                GoogleMap(
                  initialCameraPosition: _kGooglePlex,
                  mapType: MapType.satellite,
                  myLocationEnabled: true,
                  zoomGesturesEnabled: true,
                  zoomControlsEnabled: true,
                  markers: _markers,
                  polylines: _polyline,
                  onMapCreated: (GoogleMapController controller) {
                    _controllerGoogleMap.complete(controller);
                    newGoogleMapController = controller;

                    locateCurrentPosition();
                  },
                  onTap: (point) {
                    onMap
                        ? setState(() {
                            polygonLatLngs.add(point);
                            _setMarker(point);
                            _setPolyline();
                          })
                        : Fluttertoast.showToast(
                            msg: "You did not select START MAPPING");
                  },
                  // onTap: (point) {
                  //   setState(() {
                  //     polygonLatLngs.add(point);
                  //     _setMarker(point);
                  //     _setPolyline();
                  //   });
                  // },
                ),
                Container(
                  height: Dimensions.height10 * Dimensions.height15,
                  width: (Dimensions.screenWidth / 2) +
                      Dimensions.width10 * Dimensions.width15,
                  margin: EdgeInsets.only(
                      top: Dimensions.height10, left: Dimensions.width10),
                  decoration: BoxDecoration(
                    color: Colors.grey[700],
                    borderRadius: BorderRadius.circular(Dimensions.radius15),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                          child: Text(
                        "Tap Mark to get Location Co-ordinates",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: Dimensions.font16 - 2,
                            fontWeight: FontWeight.bold),
                      )),
                      SizedBox(
                        height: Dimensions.height10 / 2,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                _confirmLocationDialog();
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width45 * 4,
                                padding:
                                    EdgeInsets.all(Dimensions.height10 / 2),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "START FARM MAPPING NOW",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            GestureDetector(
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width45 * 4,
                                padding: const EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                  color: Colors.redAccent,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "END FARM MAPPING(COMPLETE)",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                if (polygonLatLngs.isEmpty) {
                                  Fluttertoast.showToast(
                                      msg: "You did not select any points");
                                } else {
                                  polygonLatLngs.clear();
                                  _markers.clear();
                                  onMap = false;
                                }
                              });
                            },
                            child: Container(
                              height: Dimensions.height20 + Dimensions.height30,
                              width: Dimensions.width10 * Dimensions.width10,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(0),
                              ),
                              child: Center(
                                  child: SmallText(
                                text: "CLEAR ALL",
                                color: Colors.black,
                                size: Dimensions.font12,
                              )),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    if (polygonLatLngs.isEmpty || polygonLatLngs.length < 2) {
                      onMap
                          ? Fluttertoast.showToast(msg: "INVALID SHAPE FILE")
                          : Fluttertoast.showToast(msg: "You did not select START MAPPING");
                    } else {
                      var rand = generateRand();

                      final data = FarmMapModel(
                        memberId: _singleFarmersLocal[0]["Id"]!.toString(),
                        polygon: polygonLatLngs.toString(),
                        mapCategory: map_category.toString(),
                        memberNo:
                            _singleFarmersLocal[0]["member_no"]!.toString(),
                        transactionNo: rand.toString(),
                        status: "none",
                      );

                      print("Polygon is : ${polygonLatLngs.toString()}");
                      print(data.toString());

                      await _saveFarmMapLocal(data);
                    }
                  },
                  child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        height: Dimensions.height20 + Dimensions.height30,
                        width: Dimensions.width45 * 4,
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(0),
                        ),
                        child: Center(
                            child: SmallText(
                          text: "SAVE FARM MAPPING",
                          color: Colors.black,
                          size: Dimensions.font12,
                        )),
                      )),
                )
              ],
            ),
    );
  }

  Future<void> _confirmLocationDialog() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SimpleDialog(
                title: const Text("Confirm Location"),
                children: [
                  SimpleDialogOption(
                      onPressed: () {},
                      child: Column(
                        children: [
                          SmallText(
                            text:
                                "Kindly Confirm your current location is mapped correctly with red marker. The land you map cannot be more than 50 meters away from your currect location. If the location is not accurate, Restart this page and wait for a moment to get GPS. Ideally You must stand at the center of the farm",
                            color: Colors.black,
                            size: Dimensions.font12,
                          ),
                          SizedBox(
                            height: Dimensions.height15,
                          ),
                          BigText(
                            text: "Please Select the Area to Map",
                            color: Colors.black,
                          )
                        ],
                      )),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  SimpleDialogOption(
                    onPressed: () {},
                    child: GestureDetector(
                      child: Container(
                        width: Dimensions.width30 * Dimensions.width10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height15,
                          bottom: Dimensions.height15,
                          left: Dimensions.width30,
                          right: Dimensions.width30,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.blueAccent,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 3)),
                        child: Center(
                            child: BigText(
                          text: "MAP WHOLE AREA",
                          color: Colors.white,
                          size: Dimensions.font12,
                        )),
                      ),
                      onTap: () {
                        setState(() {
                          onMap = true;
                          map_category = "Total";
                        });
                        Navigator.pop(context);
                        // setState(){
                        //
                        // };
                        //startMappingInsideMap();
                        // Navigator.push(context, MaterialPageRoute(builder: (c) => FarmMappingScreen(member_id: member_id)));
                      },
                    ),
                  ),
                  SimpleDialogOption(
                    onPressed: () {},
                    child: GestureDetector(
                      child: Container(
                        width: 300,
                        padding: const EdgeInsets.only(
                            top: 13, bottom: 13, left: 25, right: 25),
                        decoration: BoxDecoration(
                            color: AppColors.subColor,
                            borderRadius: BorderRadius.circular(5)),
                        child: Center(
                          child: BigText(
                            text: "MAP PRODUCTION AREA",
                            color: Colors.white,
                            size: Dimensions.font12,
                          ),
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          onMap = true;
                          map_category = "Production";
                        });
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              );
            },
          );
        });
  }
}
