import 'dart:async';
import 'dart:convert';
import 'dart:ui' as ui;

import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/estates/estates_controller.dart';
import '../../../models/infrastructure/infrastructurepolygons_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/base/snackbar.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/form_field.dart';
import '../../../resources/widgets/small_text.dart';
import '../infrastructure_mapping/infrastructure_dashboard.dart';

class InfrastructureMappingScreen2 extends StatefulWidget {
  const InfrastructureMappingScreen2({super.key});

  @override
  State<InfrastructureMappingScreen2> createState() =>
      _InfrastructureMappingScreen2State();
}

class _InfrastructureMappingScreen2State
    extends State<InfrastructureMappingScreen2> {
  final _selectedIndex = Get.arguments[0];
  final _selectedType = Get.arguments[1];
  final _selectedImage = Get.arguments[2];
  final _selectedTypeId = Get.arguments[3];

  bool isLoading = false;
  var mainDivId = "";
  var blockId = "";
  double area = 0.0;
  String selectedDivisionName = '';
  bool onMap = false;

  // FOR LOADING MAP

  void _onMapCreated(GoogleMapController controller) async {
    _controller = controller;
    _controllerCompleter.complete(controller);
  }

  TextEditingController infrastructureNameController = TextEditingController();
  late GoogleMapController _controller;
  final Completer<GoogleMapController> _controllerCompleter = Completer();
  LatLng _cameraPosition = const LatLng(6.34384805954185, -10.3558099864803);
  final Set<Marker> _markers = <Marker>{};
  final List<Set<Marker>> _markersList = [];
  final Set<Polyline> _polyline = <Polyline>{};
  final List<List<Polygon>> _listInfrastructurePolygons =
      []; //For Mapping Division And Blocks

  _setPolygon(List<LatLng> points) async {
    Polygon pol = Polygon(
      polygonId: PolygonId('$selectedDivisionName'),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.green,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listInfrastructurePolygons.add(lispol);
  }

  _setPolygon2(List<LatLng> points) async {
    Polygon pol = Polygon(
      polygonId: PolygonId(''),
      points: points,
      strokeWidth: 2,
      strokeColor: Colors.blue,
      fillColor: Colors.transparent,
    );

    List<Polygon> lispol = [];
    lispol.add(pol);
    _listInfrastructurePolygons.add(lispol);
  }

  LatLng calculatePolygonCenter(List<LatLng> polygonCoordinates) {
    double latitudeSum = 0.0;
    double longitudeSum = 0.0;

    for (LatLng coordinate in polygonCoordinates) {
      latitudeSum += coordinate.latitude;
      longitudeSum += coordinate.longitude;
    }

    double latitudeAverage = latitudeSum / polygonCoordinates.length;
    double longitudeAverage = longitudeSum / polygonCoordinates.length;

    return LatLng(latitudeAverage, longitudeAverage);
  }

  //END

  // FOR THE ITEMS BEING MAPPED
  BitmapDescriptor pinLocationIcon = BitmapDescriptor.defaultMarker;
  int _markerId = 1;
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  void setCustomMapPin() async {
    final Uint8List markerIcon = await getBytesFromAsset(_selectedImage, 70);

    pinLocationIcon = BitmapDescriptor.fromBytes(markerIcon);
  }

  bool _isPointInsidePolygon(LatLng point, List<LatLng> polygon) {
    print("Point is****" + point.toString());
    print("Polygon is****" + polygon.toString());
    bool isInside = false;
    for (int i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
      if ((polygon[i].longitude > point.longitude) !=
              (polygon[j].longitude > point.longitude) &&
          (point.latitude <
              (polygon[j].latitude - polygon[i].latitude) *
                      (point.longitude - polygon[i].longitude) /
                      (polygon[j].longitude - polygon[i].longitude) +
                  polygon[i].latitude)) {
        isInside = !isInside;
      }
    }
    return isInside;
  }

  final Set<Marker> pickingMarker = <Marker>{};
  void _setMarker(LatLng point) async {
    setState(() {
      pickingMarker.add(Marker(
          markerId: MarkerId("Marker+${infrastructureNameController.text}"),
          // icon: pinLocationIcon,
          position: point));

      _markersList.add(pickingMarker);
    });
  }

  // FOR POLYGON INFRASTRUCTURE
  final List<LatLng> _polygonType = [];
  final List<List<LatLng>> _listInfrastructurePolygonLatLng = [];
  List<dynamic> transactionNos = [];

  _getInfrastructureCategories() async {
    List<dynamic> infraList = [];
    var polyRes = await EstateController()
        .getInfrastructureCategoryById(mainDivId, _selectedTypeId);
    if (polyRes != null) {
      infraList = polyRes;

      for (int i = 0; i < infraList.length; i++) {
        final transaction = infraList[i]["transaction_no"];
        transactionNos.add(transaction);
      }

      await _getAllPolyGonInfrustructure();
    } else {
      setState(() => isLoading = false);
    }
  }

  Future _setPolyTypeMarker(List<LatLng> allInfrapoints) async {
    Fluttertoast.showToast(
        msg: "Found All Points ${allInfrapoints.toString()}");
    for (int i = 0; i < allInfrapoints.length; i++) {
      LatLng point = allInfrapoints[i];
      final Set<Marker> marker = <Marker>{};
      setState(() {
        marker.add(Marker(
            markerId: MarkerId("Marker+${infrastructureNameController.text}"),
            icon: pinLocationIcon,
            position: point));
        _markersList.add(marker);
      });

      //_markersList.add(_markers);

      Fluttertoast.showToast(msg: "Markers Are ${_markersList.toString()}");
    }
  }

  _getAllPolyGonInfrustructure() async {
    for (int i = 0; i < transactionNos.length; i++) {
      var res = await EstateController()
          .getInfrastructurelatlongById(transactionNos[i]);
      if (res != null) {
        List<dynamic> infrastructurePolygon = [];
        List<LatLng> allInfrapoints = [];
        infrastructurePolygon = res;

        for (int j = 0; j < infrastructurePolygon.length; j++) {
          final lat = double.parse(infrastructurePolygon[j]["latitude"]);
          final lng = double.parse(infrastructurePolygon[j]["longitude"]);
          final name = infrastructurePolygon[j]["infrastructure_name"];

          infrastructureNameController.text = name;

          allInfrapoints.add(LatLng(lat, lng));

          // final Set<Marker> _marker = <Marker>{};
          // _marker.add(Marker(
          // markerId: MarkerId("Marker+${infrastructureNameController.text}"),
          // icon: pinLocationIcon,
          // position: LatLng(lat, lng),
          // infoWindow: InfoWindow(
          //   title: infrastructureNameController.text.toString(),
          // )));

          // _markersList.add(_marker);

          // Fluttertoast.showToast(msg: "Markers Are ${_markersList.toString()}");
        }
        if (_selectedType == "polyGon") {
          print(allInfrapoints.toString());
          await _setPolygon2(allInfrapoints);
          LatLng center = calculatePolygonCenter(allInfrapoints);
          _cameraPosition = center;
          setState(() => isLoading = false);
        }
        // setState(() => isLoading = false);
        // if (_selectedType == "polyPoint") {
        //   // final Set<Marker> _marker = <Marker>{};
        //   // _marker.add(Marker(
        //   // markerId: MarkerId("Marker+${infrastructureNameController.text}"),
        //   // icon: pinLocationIcon,
        //   // position: allInfrapoints.first,
        //   // infoWindow: InfoWindow(
        //   //   title: infrastructureNameController.text.toString(),
        //   // )));

        //   // _markersList.add(_marker);
        //   // await _setPolyTypeMarker(allInfrapoints);
        //   setState(() => isLoading = false);
        // }
        // else{
        //   await _setPolygon(allInfrapoints);

        //   setState(() => isLoading = false);
        // }

        // _listInfrastructurePolygonLatLng.add(allInfrapoints);
        // print(_listInfrastructurePolygonLatLng.toString() + ": Is List");
      } else {
        Fluttertoast.showToast(msg: "Null at getting latlng");
        setState(() => isLoading = false);
      }
    }
    //await _buildAllPolygons();
  }

  // FOR POLYLINE INFRASTRUCTURE

  // FOR POLYPOINT INFRASTRUCTURE

  // FOR INITSTATE
  List<dynamic> _divisionsPolygon = [];
  List<LatLng> points = [];

  _getDivisionLatLngFromDatabase(var id) async {
    var res = await EstateController().getlatlongById(id.toString());
    if (res != null) {
      _divisionsPolygon = res;
      for (int i = 0; i < _divisionsPolygon.length; i++) {
        final lat = double.parse(_divisionsPolygon[i]["latitude"]);
        final lng = double.parse(_divisionsPolygon[i]["longi"]);

        points.add(LatLng(lat, lng));
      }

      await _setPolygon(points);
      LatLng center = calculatePolygonCenter(points);
      _cameraPosition = center;

      await _getInfrastructureCategories();
      // if (_selectedType == "polyGon") {
      //   //await _getAllPolyGonInfrustructure(mainDivId);
      //   await _getInfrastructureCategories();
      // }
      // if (_selectedType == "polyPoint") {

      // }
    } else {}
  }

  @override
  void initState() {
    super.initState();

    _getDivision();
  }

  _getDivision() async {
    setState(() => isLoading = true);
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var data = jsonDecode(result.toString());

    if (data != null) {
      setState(() {
        mainDivId = data["Id"].toString();
        selectedDivisionName = data["division_name"].toString();
      });
      setCustomMapPin();
      print(mainDivId);

      await _getDivisionLatLngFromDatabase(mainDivId);
    } else {
      Get.back();
    }
  }

  _getDate() {
    DateTime dateTime = DateTime.now();
    return DateFormat('yyyy-MM-dd').format(dateTime).toString();
  }

  _savePolyType() async {
    var transactionNo =
        'INFPOL_${DateTime.now().millisecondsSinceEpoch.toString()}';
    String dateTime = _getDate();

    setState(() => isLoading = true);
    List<dynamic> divPolyType = [];
    for (var element in _polygonType) {
      FarmersInfrastructurePolygonModel model =
          FarmersInfrastructurePolygonModel();
      model.infraTypeId = _selectedTypeId.toString();
      model.infrastructureName = infrastructureNameController.text;
      model.mainDivId = mainDivId.toString();
      model.longitude = element.latitude.toString();
      model.latitude = element.latitude.toString();
      model.polygonId = mainDivId.toString();
      model.landSize = area.toString();
      model.transactionNo = transactionNo;
      model.datetime = dateTime;

      divPolyType.add(model);
    }

    // showCustomSnackBar(
    //       "Block saved successfully at model + ${divPolyType.toString()}", context, ContentType.success,
    //       title: "Success");

    int? i = await EstateController()
        .saveAll(divPolyType, farmerInfrastructurePolygonTable);

    if (i != null) {
      List<dynamic> divPolyCategory = [];
      FarmersInfrastructureCatModel catModel = FarmersInfrastructureCatModel();
      catModel.infrastructureTypeId = _selectedTypeId.toString();
      catModel.transactionNo = transactionNo;
      catModel.mainDivId = mainDivId.toString();
      catModel.datetime = dateTime;

      divPolyCategory.add(catModel);

      int? j = await EstateController()
          .saveAll(divPolyCategory, farmerInfrastructureCategoryTable);

      if (j != null) {
        showCustomSnackBar(
            "Block saved successfully", context, ContentType.success,
            title: "Success");

        Get.to(() => const InfrastructureDashboardScreen(),
            arguments: _selectedIndex);
      } else {}
    } else {
      showCustomSnackBar(
          "Ooops Something Went Wrong", context, ContentType.failure,
          title: "Failure");

      Get.to(() => const InfrastructureDashboardScreen(),
          arguments: _selectedIndex);
    }
  }

  _saveInfrastructure() async {
    await _savePolyType();
    // if (_selectedType == "polyGon") {
    //   await _savePolyType();
    // }
    // if (_selectedType == "polyPoint") {
    //   await _savePolyType();
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            Get.to(() => const InfrastructureDashboardScreen(),
                arguments: _selectedIndex);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
            size: Dimensions.iconSize24,
          ),
        ),
      ),
      body: isLoading
          ? SizedBox(
              height: Dimensions.screenHeight,
              width: Dimensions.screenWidth,
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [CustomLoader()],
              ),
            )
          : Stack(
              children: [
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: _cameraPosition,
                    zoom: 13.0,
                  ),
                  mapType: MapType.satellite,
                  markers: _markersList.expand((element) => element).toSet(),
                  polylines: _polyline,
                  polygons:
                      _listInfrastructurePolygons.expand((i) => i).toSet(),
                  onTap: (point) {
                    onMap
                        ? _selectedType == "polyLine"
                            ? Fluttertoast.showToast(msg: "Plotting PolyLine")
                            : _selectedType == "polyPoint"
                                ? _isPointInsidePolygon(point, points)
                                    ? _polygonType.isNotEmpty
                                        ? setState(() {
                                            _polygonType.add(point);
                                            _setMarker(point);
                                          })
                                        : setState(() {
                                            _polygonType.clear();
                                            _polygonType.add(point);
                                            _setMarker(point);
                                          })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "The Point is Outside Mapping Area")
                                : _selectedType == "polyGon"
                                    ? _isPointInsidePolygon(point, points)
                                        ? setState(() {
                                            _polygonType.add(point);
                                            _setMarker(point);
                                            _setPolyline(point);
                                          })
                                        : Fluttertoast.showToast(
                                            msg:
                                                "The Point is Outside Mapping Area")
                                    : Fluttertoast.showToast(
                                        msg: "No Category Named")
                        : Fluttertoast.showToast(
                            msg: "You did not select START MAPPING");
                  },
                ),
                Positioned(
                  top: 50,
                  child: Container(
                    padding: EdgeInsets.all(Dimensions.height15),
                    height: Dimensions.height15 * Dimensions.height15,
                    width:
                        (Dimensions.screenWidth / 2) + Dimensions.width45 * 3,
                    margin: EdgeInsets.only(
                        top: Dimensions.height10, left: Dimensions.width10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                            child: Text(
                          "Tap START to get Co-ordinates",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: Dimensions.font16 - 2,
                              fontWeight: FontWeight.bold),
                        )),
                        SizedBox(
                          height: Dimensions.height10 / 2,
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Dialog(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          elevation: 0,
                                          backgroundColor: Colors.transparent,
                                          child: mapWith(context),
                                        );
                                      });
                                });
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "  START  ",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () async {
                                //dialog to choose mode of mapping
                                onMap
                                    ? showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            elevation: 0,
                                            backgroundColor: Colors.transparent,
                                            child: restartBox(context),
                                          );
                                        })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "RESTART",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width30,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  onMap
                                      ? _deletePoint()
                                      : Fluttertoast.showToast(
                                          msg:
                                              "You did not select START MAPPING");
                                });
                              },
                              child: Icon(
                                Icons.delete_outline_sharp,
                                color: AppColors.buttonRed,
                                size: Dimensions.iconSize24 * 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                onMap
                                    ? showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Dialog(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            elevation: 0,
                                            backgroundColor: Colors.transparent,
                                            child:
                                                saveInfrastructureItem(context),
                                          );
                                        })
                                    : Fluttertoast.showToast(
                                        msg:
                                            "You did not select START MAPPING");
                              },
                              child: Container(
                                height:
                                    Dimensions.height20 + Dimensions.height30,
                                width: Dimensions.width10 * Dimensions.width10,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                child: Center(
                                    child: SmallText(
                                  text: "COMPLETE",
                                  color: Colors.black,
                                  size: Dimensions.font12,
                                )),
                              ),
                            ),
                            const SizedBox(),
                            Column(
                              children: [
                                BigText(
                                  text: "Total Acreage (m2)",
                                  color: Colors.white,
                                  size: Dimensions.font12,
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                BigText(
                                  text: area.toString(),
                                  color: Colors.white,
                                  size: Dimensions.font12,
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  mapWith(context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(
            left: Dimensions.width10,
            top: Dimensions.height20,
            right: Dimensions.width10,
            bottom: Dimensions.height20,
          ),
          margin: EdgeInsets.only(top: Dimensions.height20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(Dimensions.radius30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Type Select",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              SmallText(
                text: "Would you like to ?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {
                        onMap = true;
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Pick Points",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: Dimensions.width30 * 2,
                  ),
                  GestureDetector(
                      onTap: () {
                        Get.back();
                        // Get.to(() => const LivePick());
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Walk",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: Dimensions.height20,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 3,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Cancel",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))),
            ],
          ),
        ),
        Positioned(
          left: Dimensions.width10 / 2,
          right: Dimensions.width10 / 2,
          child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: Dimensions.radius20,
              child: FaIcon(
                FontAwesomeIcons.circleQuestion,
                size: Dimensions.height45,
                color: Colors.green,
              )),
        ),
      ],
    );
  }

  _restart(String id) async {
    await EstateController().deleteReserveById(id);

    setState(() {});
    Get.back();
  }

  restartBox(context) {
    return Stack(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "Confirm Restart",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Would you like to restart mapping?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: BigText(
                          text: "Cancel",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _restart("1");
                        },
                        child: BigText(
                          text: "Ok",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  void _removePolygonMarker() {
    if (_markers.isNotEmpty && _polyline.isNotEmpty) {
      setState(() {
        _markers.remove(_markers.last);
        _polyline.remove(_polyline.last);
        _polygonType.remove(_polygonType.last);
      });
    }

    print('Markers are: ${_markers.length}');
    print('PolyLatLangs are: ${_polygonType.length}');
  }

  void _deletePoint() {
    if (_selectedType == "polyLine") {
      // if (_polyline.isEmpty || _markers.isEmpty) {
      // } else {
      //   _removeMarker();
      // }
    } else if (_selectedType == "polyGon") {
      if (_polygonType.isEmpty) {
      } else {
        _removePolygonMarker();
      }
    }
  }

  saveInfrastructureItem(context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
                color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
          ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 10,
          ),
          SmallText(
            text: "Infrastructure Name ",
            color: Colors.black,
            size: Dimensions.font16,
          ),
          FormFields(
            textEditingController: infrastructureNameController,
            inputType: TextInputType.text,
          ),
          const SizedBox(
            height: 10,
          ),
          Align(
              alignment: Alignment.bottomRight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.back();
                      _saveInfrastructure();
                    },
                    child: BigText(
                      text: "Continue",
                      color: Colors.black,
                      size: Dimensions.font16,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: BigText(
                      text: "Cancel",
                      color: Colors.black,
                      size: Dimensions.font16,
                    ),
                  )
                ],
              )),
        ],
      ),
    );
  }

  // _setPolygon2(List<LatLng> points) async {
  //   Polygon pol = Polygon(
  //     polygonId: PolygonId('$selectedDivisionName'),
  //     points: points,
  //     strokeWidth: 2,
  //     strokeColor: Colors.green,
  //     fillColor: Colors.transparent,
  //   );

  //   List<Polygon> lispol = [];
  //   lispol.add(pol);
  //   _listInfrastructurePolygons.add(lispol);
  // }

  void _setPolyline(LatLng location) {
    final Polyline polyline = Polyline(
      polylineId: PolylineId(_polyline.length.toString()),
      points: [if (_polyline.isNotEmpty) _polyline.last.points.last, location],
      color: Colors.blue,
      width: 2,
    );

    setState(() {
      _polyline.add(polyline);
    });
  }

  _getPolyline(List<LatLng> points) {
    for (var element in points) {
      _setPolyline(element);
    }
  }

  List<LatLng> tempList = [
    LatLng(6.3352219999795185, 6.3352219999795185),
    LatLng(6.326313387075682, 6.326313387075682),
    LatLng(6.334868777437318, 6.334868777437318),
    LatLng(6.337997454352039, 6.337997454352039),
    LatLng(6.336617559885325, 6.336617559885325)
  ];

  List<Polygon> mapPolygonList = [];

  _buildAllPolygons() {
    Polygon polygon = Polygon(
      polygonId: const PolygonId('block_Polygon${1}'),
      points: tempList,
      strokeWidth: 2,
      strokeColor: Colors.blue,
      fillColor: Colors.blue.withOpacity(0.3),
    );
    // int j = 0;
    // while (j < _listInfrastructurePolygonLatLng.length) {
    //   Polygon polygon = Polygon(
    //     polygonId: const PolygonId('block_Polygon${1}'),
    //     points: _listInfrastructurePolygonLatLng[j],
    //     strokeWidth: 2,
    //     strokeColor: Colors.blue,
    //     fillColor: Colors.blue.withOpacity(0.3),
    //   );

    setState(() {
      mapPolygonList.add(polygon);
      _listInfrastructurePolygons.add(mapPolygonList);
    });

    //   j++;
    // }
  }
}
