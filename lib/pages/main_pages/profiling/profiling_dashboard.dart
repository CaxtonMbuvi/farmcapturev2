import 'package:flutter/material.dart';

import '../../../resources/base/full_screen_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import 'add_new_farmer.dart';

class ProfilingDashboard extends StatefulWidget {
  const ProfilingDashboard({Key? key}) : super(key: key);

  @override
  State<ProfilingDashboard> createState() => _ProfilingDashboardState();
}

class _ProfilingDashboardState extends State<ProfilingDashboard> {

  void _showModal(BuildContext context) {
    Navigator.of(context).push(FullScreenModal());
  }

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _showModal(context);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Dimensions.height30 * 2),
          child: AppBar(
            backgroundColor: Colors.green,
            actions: [
              Container(
                width: Dimensions.screenWidth,
                height: Dimensions.height30 * 2,
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                  color: Colors.green,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(5, 5),
                        color: AppColors.gradientOne.withOpacity(0.1)),
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(-5, -5),
                        color: AppColors.gradientOne.withOpacity(0.1))
                  ],
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/images/fc_logo.png',
                          fit: BoxFit.contain, height: 32),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),

      body: SingleChildScrollView(
              child: Container(
                  height: Dimensions.screenHeight,
                  width: Dimensions.screenWidth,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/bg.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 50,),

                      Column(
                        children: [
                          GestureDetector(
                            onTap: (){
                              // Navigator.push(context, MaterialPageRoute(builder: (c) => AddNewFarmerScreen()));
                            },
                            child: Container(
                              height: Dimensions.height30 * 4,
                              width: Dimensions.width30 * 4,
                              decoration: BoxDecoration(
                                color: AppColors.textWhite,
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius30 * 5),
                              ),
                              child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_1.png',
                                        width: 60,
                                        height: 60,
                                      ),

                                    ],
                                  )
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),
                          BigText(
                            text: "All Farmers List",
                            color: AppColors.black,
                            size: Dimensions.font16 - 4,
                          ),
                        ],
                      ),

                      const SizedBox(height: 70,),

                      Column(
                        children: [
                          GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (c) => const AddNewFarmerScreen()));
                            },
                            child: Container(
                              height: Dimensions.height30 * 4,
                              width: Dimensions.width30 * 4,
                              decoration: BoxDecoration(
                                color: AppColors.textWhite,
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius30 * 5),
                              ),
                              child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_1.png',
                                        width: 60,
                                        height: 60,
                                      ),

                                    ],
                                  )
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),
                          BigText(
                            text: "Add New Farmer",
                            color: AppColors.black,
                            size: Dimensions.font16 - 4,
                          ),
                        ],
                      ),

                    ],
                  ),
              )
      ),
    );
  }
}
