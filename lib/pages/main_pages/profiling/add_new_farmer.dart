import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/custom_text_field.dart';

class AddNewFarmerScreen extends StatefulWidget {
  const AddNewFarmerScreen({Key? key}) : super(key: key);

  @override
  State<AddNewFarmerScreen> createState() => _AddNewFarmerScreenState();
}

class _AddNewFarmerScreenState extends State<AddNewFarmerScreen> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey=GlobalKey();

  TextEditingController fullNameController = TextEditingController();
  TextEditingController otherNamesController = TextEditingController();
  TextEditingController farmerNumberController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController natIdController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController noOfMaleController = TextEditingController();
  TextEditingController noOfWomanController = TextEditingController();
  TextEditingController peopleInHouseController = TextEditingController();
  TextEditingController dateJoinedCoopController = TextEditingController();

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2050)
    );

    if(picked != null && picked != date){
      setState(() {
        date = picked;
        String formattedDate = DateFormat('dd-MM-yyyy').format(picked);
        print(formattedDate);
        dobController.text = formattedDate;
        // dateJoinedCoopController.text = formattedDate;
        // familyMemDobController.text = formattedDate;
        print(date.toString());
      });
    }
  }

  /// lists*
  List<Map<String, dynamic>> landDetailsList = [];
  List<Map<String, dynamic>> familyMemberList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/images/fc_logo.png',
                        fit: BoxFit.contain, height: 32),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),

      body: Container(
          height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),

          padding: const EdgeInsets.all(3.0),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),

            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Column(
                    children: [
                      Form(
                        key: _formKey,
                          child: Column(
                            children: [

                              //full name
                              CustomTextField(
                                readOnly: false,
                                controller: fullNameController,
                                onSaved: (value) {},
                                hintText: "Full Name",
                                isObscure: false,
                                onTap: (value) {  },
                              ),
                              const SizedBox(
                                height: 5,
                              ),

                              //other names
                              CustomTextField(
                                readOnly: false,
                                controller: otherNamesController,
                                onSaved: (value) {},
                                labelText: "Other Names",
                                isObscure: false,
                                onTap: (value) {  },
                              ),
                              const SizedBox(
                                height: 5,
                              ),

                              //phone
                              CustomTextField(
                                readOnly: false,
                                controller: phoneController,
                                keyboardType: TextInputType.phone,
                                onSaved: (value) {},
                                labelText: "Phone Number",
                                isObscure: false,
                                onTap: (value) {  },
                              ),
                              const SizedBox(
                                height: 5,
                              ),

                              //nat ID
                              CustomTextField(
                                readOnly: false,
                                controller: natIdController,
                                keyboardType: TextInputType.number,
                                onSaved: (value) {},
                                labelText: "National Id No.",
                                isObscure: false,
                                onTap: (value) {  },
                              ),
                              const SizedBox(
                                height: 5,
                              ),

                              //email
                              CustomTextField(
                                readOnly: false,
                                controller: emailController,
                                onSaved: (value) {},
                                labelText: "Email Address",
                                isObscure: false,
                                onTap: (value) {  },
                              ),
                              const SizedBox(
                                height: 5,
                              ),


                              //nationality
                              // Container(
                              //   margin: const EdgeInsets.all(10.0),
                              //   child: DropdownButton<String>(
                              //     hint: Text("Nationality"),
                              //     dropdownColor: Colors.white,
                              //     icon: Icon(Icons.arrow_drop_down),
                              //     style: TextStyle(color: Colors.black, fontSize: 16),
                              //     // value: _nationality,
                              //     isExpanded: true,
                              //     isDense: true,
                              //     onChanged: (newValue) async {
                              //       setState(() {
                              //         _nationality = newValue;
                              //       });
                              //     },
                              //     items: _nationalityList.map((value) {
                              //       return DropdownMenuItem<String>(
                              //         value: value['name'].trim(),
                              //         child: Text(value['name'].trim(), style: TextStyle(color: Colors.black)),
                              //       );
                              //     }).toList(),
                              //   ),
                              // ),
                              // SizedBox(
                              //   height: 5,
                              // ),


                              //dob
                              CustomTextField(
                                readOnly: false,
                                controller: dobController,
                                onSaved: (value) { },
                                labelText: "Date Of Birth",
                                isObscure: false,
                                suffixIcon: IconButton(
                                  color: Colors.black,
                                  onPressed: (){
                                    selectedTimePicker(context);
                                  },
                                  icon: const Icon(Icons.calendar_month),
                                ),
                                onTap: (value) {},
                              ),

                              //gender
                              // Container(
                              //   margin: const EdgeInsets.all(10.0),
                              //   child: DropdownButton<String>(
                              //     hint: Text("Gender"),
                              //     dropdownColor: Colors.white,
                              //     icon: Icon(Icons.arrow_drop_down),
                              //     style: TextStyle(color: Colors.black, fontSize: 16),
                              //     value: _gender,
                              //     isExpanded: true,
                              //     isDense: true,
                              //     onChanged: (newValue) async {
                              //       setState(() {
                              //         _gender = newValue;
                              //       });
                              //     },
                              //     items: _genderList.map(( value) {
                              //       return DropdownMenuItem<String>(
                              //         value: value['gender'].trim(),
                              //         child: Text(value['gender'].trim(), style: TextStyle(color: Colors.black)),
                              //       );
                              //     }).toList(),
                              //   ),
                              // ),
                              const SizedBox(
                                height: 15,
                              ),

                              //region
                              const Center(child:Text("REGION DETAILS", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.green)), ),
                              const SizedBox(height: 15,),

                              // Container(
                              //   margin: const EdgeInsets.all(10.0),
                              //   child: DropdownButton<String>(
                              //     hint: Text("Region"),
                              //     value: _region,
                              //     isExpanded: true,
                              //     isDense: true,
                              //     dropdownColor: Colors.white,
                              //     icon: Icon(Icons.arrow_drop_down),
                              //     style: TextStyle(color: Colors.black, fontSize: 16),
                              //     onChanged: (newValue) async {
                              //       setState(() {
                              //         _region = newValue;
                              //       });
                              //     },
                              //     items: _regionsList.map(( value) {
                              //       return DropdownMenuItem<String>(
                              //         value: value['name'].trim(),
                              //         child: Text(value['name'].trim(), style: TextStyle(color: Colors.black)),
                              //       );
                              //     }).toList(),
                              //   ),
                              // ),

                              const SizedBox(
                                height: 5,
                              ),

                              // Container(
                              //   margin: const EdgeInsets.all(10.0),
                              //   child: DropdownButton<String>(
                              //     hint: Text("District"),
                              //     value: _district,
                              //     isExpanded: true,
                              //     isDense: true,
                              //     dropdownColor: Colors.white,
                              //     icon: Icon(Icons.arrow_drop_down),
                              //     style: TextStyle(color: Colors.black, fontSize: 16),
                              //     onChanged: (newValue) async {
                              //       setState(() {
                              //         _district = newValue;
                              //       });
                              //     },
                              //     items: _districtsList.map(( value) {
                              //       return DropdownMenuItem<String>(
                              //         value: value['name'].trim(),
                              //         child: Text(value['name'].trim(), style: TextStyle(color: Colors.black)),
                              //       );
                              //     }).toList(),
                              //   ),
                              // ),
                              const SizedBox(
                                height: 15,
                              ),

                              //land Details
                              const Center(child:Text("LAND DETAILS", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.green)), ),
                              const SizedBox(height: 15,),

                              Align(
                                alignment: Alignment.topRight,
                                child: GestureDetector(
                                  // onTap: () {
                                  //   _openAddLandDetailsDialog();
                                  //   // openLandDetailsDialog();
                                  // },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text("LAND DETAILS", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14,),),

                                      Container(
                                        width: 100,
                                        margin: const EdgeInsets.only(right: 10, bottom: 10),
                                        padding: const EdgeInsets.only(
                                            top: 10,
                                            bottom: 10,
                                            left: 20,
                                            right: 20
                                        ),
                                        decoration: BoxDecoration(
                                            color: AppColors.subColor,
                                            borderRadius: BorderRadius.circular(5)),
                                        child: Center(
                                          child: Text(
                                              "+ Add".toUpperCase(),
                                              style: const TextStyle(fontSize: 13, color: Colors.white)
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),

                                ),
                              ),

                              const SizedBox(
                                height: 15,
                              ),

                              //displaying land details entered
                              landDetailsList.isEmpty ?
                              Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width ,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: Colors.grey,
                                ),
                                child: const Center(
                                  child: Text("No land details Added", style: TextStyle(color: Colors.white),) ,
                                ) ,
                              ) :
                              Container(
                                  width: MediaQuery.of(context).size.width ,
                                  margin: const EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.grey,
                                  ),
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 5,),

                                      const Text("My Land Details", style: TextStyle(color: Colors.white, fontSize: 16),),
                                      const SizedBox(height: 15,),

                                      ListView.builder(
                                        physics: const NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: landDetailsList.length,
                                        itemBuilder: (context, index){
                                          return ListTile(
                                            title: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text("LR No: ${landDetailsList[index]['lr_no']}",
                                                  style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
                                                ),

                                                Text("Tenure: ${landDetailsList[index]['land_tenure']}",
                                                  style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.normal),
                                                ),

                                                Text("Farrow Area: ${landDetailsList[index]['farrowarea']}",
                                                  style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.normal),
                                                ),

                                                const Divider(height: 0.5, color: Colors.black,),

                                              ],
                                            ),
                                          )
                                          ;
                                        },
                                      ),
                                    ],
                                  )
                              ),

                              const SizedBox(
                                height: 15,
                              ),


                              const Center(child:Text("FAMILY DETAILS", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.green))),
                              const SizedBox(height: 15,),
                              const Text("Number of person living with farmer.",  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black)),
                              const SizedBox(height: 10,),

                              CustomTextField(
                                readOnly: false,
                                controller: noOfWomanController,
                                keyboardType: TextInputType.number,
                                onSaved: (value) {},
                                labelText: "Number of Woman",
                                isObscure: false,
                                onTap: (value) {  },
                              ),

                              const SizedBox(
                                height: 5,
                              ),

                              CustomTextField(
                                readOnly: false,
                                controller: noOfMaleController,
                                keyboardType: TextInputType.number,
                                onSaved: (value) {},
                                labelText: "Number of Male",
                                isObscure: false,
                                onTap: (value) {  },
                              ),



                              const SizedBox(height: 5,),
                              //marital status
                              // Container(
                              //   margin: const EdgeInsets.all(10.0),
                              //   child: DropdownButton<String>(
                              //     hint: Text("Marital Status"),
                              //     value: _maritalStatus,
                              //     isExpanded: true,
                              //     isDense: true,
                              //     dropdownColor: Colors.white,
                              //     icon: Icon(Icons.arrow_drop_down),
                              //     style: TextStyle(color: Colors.black, fontSize: 16),
                              //     onChanged: (newValue) async {
                              //       setState(() {
                              //         _maritalStatus = newValue;
                              //       });
                              //     },
                              //     items: _maritalTypesList.map(( value) {
                              //       return DropdownMenuItem<String>(
                              //         value: value['type'].trim(),
                              //         child: Text(value['type'].trim(), style: TextStyle(color: Colors.black)),
                              //       );
                              //     }).toList(),
                              //   ),
                              // ),

                              //people in house
                              CustomTextField(
                                readOnly: false,
                                controller: peopleInHouseController,
                                keyboardType: TextInputType.number,
                                onSaved: (value) {},
                                labelText: "People in House",
                                isObscure: false,
                                onTap: (value) {  },
                              ),

                              //open add family dialog
                              Align(
                                alignment: Alignment.topRight,
                                child: GestureDetector(
                                  // onTap: () {
                                  //   _openAddLandDetailsDialog();
                                  //   // openLandDetailsDialog();
                                  // },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text("ADD FAMILY DETAILS", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14,),),

                                      Container(
                                        width: 100,
                                        margin: const EdgeInsets.only(right: 10, bottom: 10),
                                        padding: const EdgeInsets.only(
                                            top: 10,
                                            bottom: 10,
                                            left: 20,
                                            right: 20
                                        ),
                                        decoration: BoxDecoration(
                                            color: AppColors.subColor,
                                            borderRadius: BorderRadius.circular(5)),
                                        child: Center(
                                          child: Text(
                                              "+ Add".toUpperCase(),
                                              style: const TextStyle(fontSize: 13, color: Colors.white)
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),

                                ),

                              ),

                              const SizedBox(
                                height: 15,
                              ),

                              //populating list for family members
                              familyMemberList.isEmpty ?
                              Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width ,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: Colors.grey,
                                ),
                                child: const Center(
                                  child: Text("No Family Members Added", style: TextStyle(color: Colors.white),) ,
                                ) ,
                              ) :
                              Container(
                                  width: MediaQuery.of(context).size.width ,
                                  margin: const EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.grey,
                                  ),
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 5,),

                                      const Text("My Family Members", style: TextStyle(color: Colors.white, fontSize: 16),),
                                      const SizedBox(height: 15,),

                                      ListView.builder(
                                        physics: const NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: familyMemberList.length,
                                        itemBuilder: (context, index){
                                          return ListTile(
                                            title: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text("Name: ${familyMemberList[index]['full_name']}",
                                                  style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
                                                ),

                                                Text("Relationship ${familyMemberList[index]['relationship']}",
                                                  style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.normal),
                                                ),

                                                const Divider(height: 0.5, color: Colors.black,),

                                              ],
                                            ),
                                          )
                                          ;
                                        },
                                      ),
                                    ],
                                  )
                              ),

                              const SizedBox(
                                height: 15,
                              ),
                              const Center(child:Text("AOB", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.green))),
                              const SizedBox(height: 10,),

                              //date join coop
                              CustomTextField(
                                readOnly: false,
                                controller: dateJoinedCoopController,
                                onSaved: (value) {},
                                labelText: "Date Join Coop",
                                isObscure: false,
                                suffixIcon: IconButton(
                                  color: Colors.black,
                                  onPressed: (){
                                    selectedTimePicker(context);
                                  },
                                  icon: const Icon(Icons.calendar_month),
                                ),
                                onTap: (value) {},
                              ),

                              const SizedBox(
                                height: 5,
                              ),

                              //participation in other program
                              // Container(
                              //   padding: EdgeInsets.all(16),
                              //   child: DropdownButton(
                              //       hint: Text("Other program",),
                              //       dropdownColor: Colors.white,
                              //       icon: Icon(Icons.arrow_drop_down),
                              //       iconSize: 30,
                              //       isExpanded: true,
                              //       style: TextStyle(color: Colors.black, fontSize: 16),
                              //       value: otherProgramChosen,
                              //       items: otherProgramItem.map((String otherProgramItem){
                              //         return DropdownMenuItem(
                              //           value: otherProgramItem,
                              //           child: Text(otherProgramItem),
                              //         );
                              //       }).toList(),
                              //       onChanged: (String? newValue){
                              //         setState(() {
                              //           otherProgramChosen = newValue!;
                              //         });
                              //       }
                              //   ),
                              // ),


                            ],
                          )
                      )
                    ],
                  ),
                )
              ],
            ),
          )

      ),
    );
  }
}
