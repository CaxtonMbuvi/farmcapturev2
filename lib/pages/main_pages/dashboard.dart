import 'dart:convert';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/models/estate/all_divisions_model.dart';
import 'package:farmcapturev2/models/estate/maindivisions_model.dart';
import 'package:farmcapturev2/models/farmers/additional_info/inputquestions_model.dart';
import 'package:farmcapturev2/models/farmers/additional_info/questionaireAnswers.dart';
import 'package:farmcapturev2/models/infrastructure/infrastructureApi_model.dart';
import 'package:farmcapturev2/models/technical_update/cpa_commercial_name_model.dart';
import 'package:farmcapturev2/models/technical_update/cpa_types_model.dart';
import 'package:farmcapturev2/models/technical_update/expected_answer_action_reason_model.dart';
import 'package:farmcapturev2/models/technical_update/intervention_apllication_method_model.dart';
import 'package:farmcapturev2/models/technical_update/intervention_type_model.dart';
import 'package:farmcapturev2/models/technical_update/natural_damage_cause_model.dart';
import 'package:farmcapturev2/models/technical_update/pesticide_disease_damage_model.dart';
import 'package:farmcapturev2/models/technical_update/weather_type_model.dart';
import 'package:farmcapturev2/models/visit/trainingsubtopic_model.dart';
import 'package:farmcapturev2/models/visit/trainingtopics_model.dart';
import 'package:farmcapturev2/models/visit/visitinspection_model.dart';
import 'package:farmcapturev2/models/visit/visitreasonlist_model.dart';
import 'package:farmcapturev2/pages/main_pages/estate/all_divisions.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/menu.dart';
//import 'package:farmcapturev2/pages/main_pages/visits/addgroup_trainingtopics.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../controllers/auth/auth_controller.dart';
import '../../controllers/farmers/farmers_controller.dart';
import '../../controllers/location/location_controller.dart';
import '../../db/db_helper.dart';
//import '../../models/additional_info/nextofkin_model.dart';
import '../../models/additional_info/nextofkin_model.dart';
import '../../models/additional_info/nextofkinupdateapi_model.dart';
import '../../models/additional_info/visitupdateapi_model.dart';
import '../../models/biodiversity/reservesTypes_model.dart';
import '../../models/estate/blocks_model.dart';
import '../../models/estate/divisionpolygon_model.dart';
import '../../models/estate/divisions_model.dart';
import '../../models/estate/new_blocks_model.dart';
import '../../models/estate/plantingtypes_model.dart';
import '../../models/estate/polygoncategory_model.dart';
import '../../models/estate/rubberclones_model.dart';
import '../../models/estate/tappingsystems_model.dart';
import '../../models/farmers/immediate_actions/actionsandreasons_model.dart';
import '../../models/farmers/allfarmers_model.dart';
import '../../models/farmers/bankbranches_model.dart';
import '../../models/farmers/banks_model.dart';
import '../../models/farmers/deduction_model.dart';
import '../../models/farmers/education_model.dart';
import '../../models/farmers/gender_type_model.dart';
import '../../models/farmers/location/county_model.dart';
import '../../models/farmers/location/district_model.dart';
import '../../models/farmers/location/village_model.dart';
import '../../models/farmers/location/ward_model.dart';
import '../../models/farmers/marital_status.dart';
import '../../models/farmers/nationality_model.dart';
import '../../models/farmers/relations_model.dart';
import '../../models/infrastructure/infrastructurepolygons_model.dart';
import '../../models/update/farmer/updatefarmerapi_model.dart';
import '../../resources/base/full_screen_loader.dart';
import '../../resources/base/snackbar.dart';
import '../../resources/utils/colors.dart';
import '../../resources/utils/dimensions.dart';
import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/small_text.dart';
import 'biodiversity/biodiversity_dashboard.dart';
import 'estate/estate_dashboard.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  var _selectedIndex = Get.arguments;
  //var _selectedIndex = Get.arguments[0];

  void _showModal(BuildContext context) {
    Navigator.of(context).push(FullScreenModal());
  }

  int take = 100;
  int? dateComparer;

  List<dynamic> _allFarmers = [];
  List<dynamic> _counties = [];
  List<dynamic> _districts = [];
  List<dynamic> _ward = [];
  List<dynamic> _village = [];
  List<dynamic> _genderTypes = [];
  List<dynamic> _nationalities = [];
  //List<dynamic> _crops = [];
  List<dynamic> _maritalStatus = [];
  List<dynamic> _levelOfEducation = [];
  //List<dynamic> _inpQuestions = [];
  List<dynamic> _relations = [];
  List<dynamic> _polyCategory = [];
  List<dynamic> _bankBranches = [];
  List<dynamic> _banks = [];
  List<dynamic> _divisionPolygon = [];
  List<dynamic> _deductions = [];
  List<dynamic> _divisions = [];
  List<dynamic> _blocks = [];
  List<dynamic> _rubberClones = [];
  List<dynamic> _plantingTypes = [];
  List<dynamic> _tappingSystems = [];
  List<dynamic> _mainDivisions = [];
  List<dynamic> _visitReasons = [];
  List<dynamic> _trainingTopics = [];
  List<dynamic> _trainingSubTopics = [];
  List<dynamic> _inputQuestions = [];
  List<dynamic> _inputQuestionaireAnswers = [];
  List<dynamic> _cpaTypes = [];
  List<dynamic> _cpaCommercialName = [];
  List<dynamic> _weatherType = [];
  List<dynamic> _interventionType = [];
  List<dynamic> _naturalDamageCause = [];
  List<dynamic> _pesticideAndDiseaseDamage = [];
  List<dynamic> _interventionApplicationMethod = [];
  List<dynamic> _expectedAnswerActionReason = [];
  List<dynamic> _infrastructureTypes = [];
  List<dynamic> _allDivisions = [];
  List<dynamic> _reservesTypes = [];
  List<dynamic> _actionReasons = [];
  List<dynamic> _childrenOnFarm = [];

  // UPDATE LISTS
  List<dynamic> _allFarmersToUpdate = [];
  List<dynamic> _allFarmersChildrenToUpdate = [];
  List<dynamic> _allFarmersVisitsToUpdate = [];

  bool isLoading = false;
  //List<dynamic> _allFarmersLocal = [];

  _syncData(bool isBlocks) async {
    setState(() {
      _showModal(context);
    });

    // if (i == 1) {
    //   _getDivisionPolygon();

    // }

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userName = localStorage.getString('UserName');
    var password = localStorage.getString('Password');

    var data = {
      "CurrentUser": {
        "PassWord": password,
        "UserName": userName,
        "Language": "English"
      },
      "Language": "English",
      "IsRenewalPasswordRequest": false
    };

    print(data.toString());
    // First Login to get the AUTHORIZATION KEY
    await _loginFarmerApi(data, isBlocks);
  }

  _loginFarmerApi(var data, bool isBlocks) async {
    FarmersController().isInteret().then((status) async {
      if (status.isSuccess) {
        AuthController().loginUserApi(data).then((status) async {
          if (status.isSuccess) {
            try {
              if (isBlocks) {
                await _getDivisionPolygon();
              } else {
                await _getAllFarmersApi();
              }
            } catch (e) {
              print('Error ${e.toString()}');
            }
          } else {
            showCustomSnackBar(status.message, context, ContentType.failure,
                title: "Failed");
            Navigator.pop(context);
          }
        });
      } else {
        showCustomSnackBar(status.message, context, ContentType.failure,
            title: "Failed");
        Navigator.pop(context);
      }
    });
  }

  Future<dynamic> checkSQLITEData(var tableName) async {
    var result =
        await DatabaseHelper.instance.readMaxDateComparerUsers(tableName);
    int datecomparer = 0;

    // var data = {
    //   "filter": {
    //     "filters": [
    //       {"field": "datecomparer", "operator": "gt", "value": datecomparer}
    //     ],
    //     "logic": "AND"
    //   },
    //   "take": 1000,
    //   "skip": 0
    // };

    print("DateComparer is: $result");
    if (result == "null") {
      //setState(() => dateComparer = 0);
      datecomparer = 0;
    } else {
      //setState(() => dateComparer = int.parse(result.toString()));
      datecomparer = int.parse(result.toString());
    }
    print("At Function$result");
    return {
      "filter": {
        "filters": [
          {"field": "datecomparer", "operator": "gt", "value": datecomparer}
        ],
        "logic": "AND"
      },
      "take": 1000,
      "skip": 0
    };
  }

  Future<dynamic> checkSQLITEDataPolygons(var tableName) async {
    String divId = seletedDivision["Id"].toString();
    var result = await DatabaseHelper.instance
        .readMaxDateComparerBlocks(tableName, divId);
    int datecomparer = 0;

    //   var data = {
    //   "filter": {
    //     "filters": [
    //       {"field": "datecomparer", "operator": "gt", "value": datecomparer},
    //       {"field": "main_division_id", "operator": "eq", "value": divId}
    //     ],
    //     "logic": "AND"
    //   },
    //   "take": 4,
    //   "skip": 0
    // };

    print("DateComparer is: $result");
    if (result == "null") {
      //setState(() => dateComparer = 0);
      datecomparer = 0;
    } else {
      //setState(() => dateComparer = int.parse(result.toString()));
      datecomparer = int.parse(result.toString());
    }
    print("At Function$result");
    return {
      "filter": {
        "filters": [
          {"field": "datecomparer", "operator": "gt", "value": datecomparer},
          {"field": "main_division_id", "operator": "eq", "value": divId}
        ],
        "logic": "AND"
      },
      "take": 10000,
      "skip": 0
    };
  }

  _getAllFarmersApi() {
    FarmersController().isInteret().then((status) async {
      if (status.isSuccess) {
        try {
          //print("Int DateComparer ${double.parse(dateComparer.toString())}");
          await _getFarmers();

          //await _getLocation();
        } catch (e) {
          print('Error ${e.toString()}');
        }
      } else {
        setState(() => isLoading = false);
      }
    });
  }

  _getFarmers() async {
    var data = await checkSQLITEData("farmers");

    var res = await FarmersController().getAllFarmersApi(data);
    print("Res is: $res");
    final jsonMap = json.decode(res.toString());

    if (jsonMap['Result']['Result'] != null) {
      try {
        var data2 = allFarmersModelFromJson(res.toString());
        _allFarmers = [];
        _allFarmers.addAll(data2.result!.result!.toList());

        print(jsonEncode(_allFarmers[0].fullName.toString()));

        int i = await FarmersController().saveAll(_allFarmers, farmersTable);
        if (i >= 0) {
          if (_allFarmers.length <= 1000) {
            //await _getAllPendingFarmersLocal();
            await _getLocation();
          } else {
            await _getAllFarmersApi();
          }
        } else {}
      } catch (e) {
        print("Error is: $e");
      }
    } else {
      await _getLocation();
      //await _getAllPendingFarmersLocal();
    }
  }

  _updateFarmer(var modelDB, String memberNo) async {
    int i = await FarmersController().updateFarmerLocal(modelDB, memberNo);

    if (i >= 0) {
      print("Success here");
    }
  }

  _updateLocal(
      String tableName, var modelDB, String column, String columnval) async {
    int i = await FarmersController()
        .updateLocal(tableName, modelDB, column, columnval);

    if (i >= 0) {
      print("Success here");
    }
  }

  List<dynamic> _allFarmersLocal = [];

  Future _getAllFarmersLocal() async {
    print("At Function");

    try {
      _allFarmersLocal = await FarmersController().getAllFarmersLocal();
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        print(_allFarmersLocal.length);

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    } catch (e) {}
  }

  _getLocation() async {
    // await _getAllDivisionsDataList();

    await _getReservesList();

    await _getActionsAndReasonsList();

    await _getChildrenOnFarm();

    // await _getInfrastructureItemsList();

    // await _getDeductions();

    // await _getCounty();

    // await _getDistrict();

    // await _getWard();

    // await _getVillage();

    // await _getGenderTypes();

    // await _getNationalities();

    // //await _getCrops();

    // await _getMaritalStatus();

    // //await _getInpQuestions();

    // await _getRelations();

    // await _getPolygonCategory();

    // await _getMainDivisions();

    // await _getLevelOfEducation();

    // await _getFarmerBanksBranches();

    // await _getFarmerBanks();

    // await _getDeductions();

    // await _getDivisions();

    // //await _getDivisionPolygon();

    // await _getBlocks();

    // await _getRubberClones();

    // await _getPlantingTypes();

    // await _getTapSystem();

    // await _getVisitReasons();

    // await _getTrainingTopics();

    // await _getTrainingSubTopics();

    // await _getInputQuestions();

    // await _getInputQuestionaireAnswers();

    // await _getCPATypesList();

    // await _getCPACommercialNameList();

    // await _getWeatherTypeList();

    // await _getInterventionTypesList();

    // await _getNaturalCauseOfDamageList();

    // await _getPesticidesAndDiseaseDamageList();

    // await _getInterventionsMethodOfApplicationList();

    // await _getExpectedAnswerActionsAndReasonsList();



    await _getAllPendingFarmersLocal();

    await _getUpdatedLocalChildren();

    
    // print(_counties.toString());
  }

  _getCounty() async {
    var data = await checkSQLITEData(countiesTable);
    print("$data Is Datecomparer");

    var resCounty =
        await LocationController().getLocationDetails(data, 'GetRegion');
    if (resCounty != null) {
      try {
        print(resCounty.toString());
        var data2 = countyModelFromJson(resCounty.toString());
        print(data2.result.toString());

        _counties = [];
        _counties.addAll(data2.result!.result!.toList());

        int i =
            await LocationController().saveLocations(countiesTable, _counties);

        if (i >= 0) {
          if (_counties.length < 1000) {
            return;
          } else {
            await _getCounty();
          }
        }
      } catch (e) {
        //Navigator.of(context).pop(FullScreenModal());
      }
    }
  }

  _getDistrict() async {
    var data = await checkSQLITEData(districtTable);
    print("$data Is Datecomparer");

    var resDistrict =
        await LocationController().getLocationDetails(data, 'GetDistrict');
    if (resDistrict != null) {
      try {
        print("District$resDistrict");
        var data2 = districtModelFromJson(resDistrict.toString());
        print(data2.result.toString());

        _districts = [];
        _districts.addAll(data2.result!.result!.toList());

        int i =
            await LocationController().saveLocations(districtTable, _districts);

        if (i >= 0) {
          if (_districts.length < 1000) {
            return;
          } else {
            await _getDistrict();
          }
        }
      } catch (e) {}
    }
  }

  _getWard() async {
    var data = await checkSQLITEData(wardTable);
    print("$data Is Datecomparer");

    var resWard =
        await LocationController().getLocationDetails(data, 'GetWard');
    if (resWard != null) {
      try {
        print("Ward$resWard");
        var data2 = wardModelFromJson(resWard.toString());
        print(data2.result.toString());

        _ward = [];
        _ward.addAll(data2.result!.result!.toList());

        int i = await LocationController().saveLocations(wardTable, _ward);

        if (i >= 0) {
          if (_ward.length < 1000) {
            return;
          } else {
            await _getWard();
          }
        }
      } catch (e) {}
    }
  }

  _getVillage() async {
    var data = await checkSQLITEData(villageTable);
    print("$data Is Datecomparer");

    var resVillage =
        await LocationController().getLocationDetails(data, 'GetVillage');
    if (resVillage != null) {
      try {
        print("Village$resVillage");
        var data2 = villageModelFromJson(resVillage.toString());
        print(data2.result.toString());

        _village = [];
        _village.addAll(data2.result!.result!.toList());

        int i =
            await LocationController().saveLocations(villageTable, _village);

        if (i >= 0) {
          if (_village.length < 1000) {
            return;
          } else {
            await _getVillage();
          }
        }
      } catch (e) {}
    }
  }

  _getGenderTypes() async {
    var data = await checkSQLITEData(genderTypeTable);
    print("$data Is Datecomparer");

    var resGender = await LocationController()
        .getLocationDetails(data, 'GetGenderTypesList');
    if (resGender != null) {
      try {
        print("Gender$resGender");
        var data2 = genderTypeModelFromJson(resGender.toString());
        print(data2.result.toString());

        _genderTypes = [];
        _genderTypes.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(genderTypeTable, _genderTypes);

        if (i >= 0) {
          if (_genderTypes.length < 1000) {
            return;
          } else {
            await _getGenderTypes();
          }
        }
      } catch (e) {}
    }
  }

  _getNationalities() async {
    var data = await checkSQLITEData(nationalityTable);
    print("$data Is Datecomparer");

    var resNationalities = await LocationController()
        .getLocationDetails(data, 'GetNationalityList');
    if (resNationalities != null) {
      try {
        print("Nationalities$resNationalities");
        var data2 = nationalitiesModelFromJson(resNationalities.toString());
        print(data2.result.toString());

        _nationalities = [];
        _nationalities.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(nationalityTable, _nationalities);

        if (i >= 0) {
          if (_nationalities.length < 1000) {
            return;
          } else {
            await _getNationalities();
          }
        }
      } catch (e) {}
    }
  }

  // _getCrops() async {
  //   var data = await checkSQLITEData(nationalityTable);
  //   print(data.toString() + " Is Datecomparer");

  //   var resCrops =
  //       await LocationController().getLocationDetails(data, 'GetCrops');
  //   print("Crops" + resCrops.toString());
  //   // var data2 = villageModelFromJson(resCrops.toString());
  //   // print(data2.result.toString());

  //   // _crops = [];
  //   // _crops.addAll(data2.result!.result!.toList());

  //   // int i = await LocationController().saveLocations(villageTable,_crops);

  //   // if (i >= 0) {
  //   //   print("Success here");
  //   // }
  // }

  _getMaritalStatus() async {
    var data = await checkSQLITEData(maritalStatusTable);
    print("$data Is Datecomparer");

    var resMaritalStatus = await LocationController()
        .getLocationDetails(data, 'GetMaritalStatusList');
    if (resMaritalStatus != null) {
      try {
        print("MaritalStatus$resMaritalStatus");
        var data2 = maritalStatusModelFromJson(resMaritalStatus.toString());
        print(data2.result.toString());

        _maritalStatus = [];
        _maritalStatus.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(maritalStatusTable, _maritalStatus);

        if (i >= 0) {
          if (_maritalStatus.length < 1000) {
            return;
          } else {
            await _getMaritalStatus();
          }
        }
      } catch (e) {}
    }
  }

  // _getInpQuestions() async {
  //   var data = await checkSQLITEData(maritalStatusTable);
  //   print(data.toString() + " Is Datecomparer");

  //   var resInpQuestions = await LocationController()
  //       .getLocationDetails(data, 'GetInpQuestionsList');
  //   print("InpQuestions" + resInpQuestions.toString());
  //   // var data2 = villageModelFromJson(resInpQuestions.toString());
  //   // print(data2.result.toString());

  //   // _inpQuestions = [];
  //   // _inpQuestions.addAll(data2.result!.result!.toList());

  //   // int i = await LocationController().saveLocations(villageTable,_inpQuestions);

  //   // if (i >= 0) {
  //   //   print("Success here");
  //   // }
  // }

  _getRelations() async {
    var data = await checkSQLITEData(relationsTable);
    print("$data Is Datecomparer");

    var resRelations =
        await LocationController().getLocationDetails(data, 'GetRelationsList');
    if (resRelations != null) {
      try {
        print("Relations$resRelations");
        var data2 = relationshipModelFromJson(resRelations.toString());
        print(data2.result.toString());

        _relations = [];
        _relations.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(relationsTable, _relations);

        if (i >= 0) {
          if (_relations.length < 1000) {
            return;
          } else {
            await _getRelations();
          }
        }
      } catch (e) {}
    }
  }

  _getPolygonCategory() async {
    var data = await checkSQLITEData(polygonCategoryTable);
    print("$data Is Datecomparer");

    var resPolygonCategory = await LocationController()
        .getLocationDetails(data, 'GetPolygonCategory');
    if (resPolygonCategory != null) {
      try {
        print("PolygonCategory$resPolygonCategory");
        var data2 = polygonCategoryModelFromJson(resPolygonCategory.toString());
        print(data2.result.toString());

        _polyCategory = [];
        _polyCategory.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(polygonCategoryTable, _polyCategory);

        if (i >= 0) {
          if (_polyCategory.length < 1000) {
            return;
          } else {
            await _getPolygonCategory();
          }
        }
      } catch (e) {}
    }
  }

  _getLevelOfEducation() async {
    var data = await checkSQLITEData(educationLevelTable);
    print("$data Is Datecomparer");

    var resEducation = await LocationController()
        .getLocationDetails(data, 'GetLeveOfEducation');
    if (resEducation != null) {
      try {
        print("LevelOfEducation$resEducation");
        var data2 = levelOfEducationModelFromJson(resEducation.toString());
        print(data2.result.toString());

        _levelOfEducation = [];
        _levelOfEducation.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(educationLevelTable, _levelOfEducation);

        if (i >= 0) {
          if (_levelOfEducation.length < 1000) {
            return;
          } else {
            await _getLevelOfEducation();
          }
        }
      } catch (e) {}
    }
  }

  _getFarmerBanksBranches() async {
    var data = await checkSQLITEData(bankBranchesTable);
    print("$data Is Datecomparer");

    var resBanksBranches = await LocationController()
        .getLocationDetails(data, 'GetFarmerBanksBranches');
    if (resBanksBranches != null) {
      try {
        print("BanksBranches$resBanksBranches");
        var data2 = bankBranchesModelFromJson(resBanksBranches.toString());
        print(data2.result.toString());

        _bankBranches = [];
        _bankBranches.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(bankBranchesTable, _bankBranches);

        if (i >= 0) {
          if (_bankBranches.length < 1000) {
            return;
          } else {
            await _getFarmerBanksBranches();
          }
        }
      } catch (e) {}
    }
  }

  _getFarmerBanks() async {
    var data = await checkSQLITEData(banksTable);
    print("$data Is Datecomparer");

    var resBanks =
        await LocationController().getLocationDetails(data, 'GetFarmerBanks');
    if (resBanks != null) {
      try {
        print("Banks$resBanks");
        var data2 = banksModelFromJson(resBanks.toString());
        print(data2.result.toString());

        _banks = [];
        _banks.addAll(data2.result!.result!.toList());

        int i = await LocationController().saveLocations(banksTable, _banks);

        if (i >= 0) {
          if (_banks.length < 1000) {
            return;
          } else {
            await _getFarmerBanks();
          }
        }
      } catch (e) {}
    }
  }

  _getDivisionPolygon() async {
    var data = await checkSQLITEDataPolygons(divisionPolygonTable);
    print(data.toString() + " Is Datecomparer");

    var resDivisionPolygon = await LocationController()
        .getLocationDetails(data, 'GetDivisionPolygon');

    Map<String, dynamic> resJson = jsonDecode(resDivisionPolygon);
    if (resJson.toString().isNotEmpty) {
      print(resJson['Result']['isSuccessull'].toString());
    }

    if (resJson['Result']['isSuccessull']) {
      try {
        print("DivisionPolygon" + resDivisionPolygon.toString());
        var data2 = blocksPolygonModelFromJson(resDivisionPolygon.toString());
        print(data2.result.toString());

        _divisionPolygon = [];
        //data2.result!.result![0].resultId
        print("polygon is: " + data2.result!.result![0].resultId.toString());
        _divisionPolygon.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(divisionPolygonTable, _divisionPolygon);

        if (i >= 0) {
          if (_divisionPolygon.length < 10000) {
            Navigator.of(context).pop(FullScreenModal());
          } else {
            await _getDivisionPolygon();
          }
        }
      } catch (e) {
        print("ERROR IS: " + e.toString());
        Navigator.of(context).pop(FullScreenModal());
      }
    } else {
      print("No Data");
      Navigator.of(context).pop(FullScreenModal());
    }
  }

  // _getDivisionPolygon() async {
  //   String divId = seletedDivision["Id"].toString();
  //   //var data = await checkSQLITEData(divisionPolygonTable);
  //   //print("$data Is Datecomparer");
  //   var result = await DatabaseHelper.instance
  //       .readMaxDateComparerBlocks(divisionPolygonTable, divId);
  //   int datecomparer = 0;

  //   print("DateComparer is: $result");
  //   if (result == "null") {
  //     setState(() => dateComparer = 0);
  //    // datecomparer = 0;
  //   } else {
  //     setState(() => dateComparer = int.parse(result.toString()));
  //     //datecomparer = int.parse(result.toString());
  //   }

  //   var data = {
  //     "filter": {
  //       "filters": [
  //         {"field": "datecomparer", "operator": "gt", "value": datecomparer},
  //         {"field": "main_division_id", "operator": "eq", "value": divId}
  //       ],
  //       "logic": "AND"
  //     },
  //     "take": 4,
  //     "skip": 0
  //   };

  //   var resDivisionPolygon = await LocationController()
  //       .getLocationDetails(data, 'GetDivisionPolygon');
  //   if (resDivisionPolygon != null) {
  //     print("Division is: $resDivisionPolygon");
  //     try {
  //       var data2 = blocksPolygonModelFromJson(resDivisionPolygon.toString());
  //       print(data2.result.toString());
  //       _divisionPolygon = [];
  //       _divisionPolygon.addAll(data2.result!.result!.toList());

  //       int i = await LocationController()
  //           .saveLocations(divisionPolygonTable, _divisionPolygon);

  //       if (i > 0) {
  //         if (_divisionPolygon.length < 1000) {
  //           Navigator.of(context).pop(FullScreenModal());
  //         } else {
  //           await _getDivisionPolygon();
  //         }
  //       } else {
  //         print("An Error at Local DB");
  //         Navigator.of(context).pop(FullScreenModal());
  //       }
  //     } catch (e) {
  //       print(e.toString());
  //     }
  //   }
  // }

  _getDeductions() async {
    var data = await checkSQLITEData(deductionsTable);
    print("$data Is Datecomparer");

    var resFarmerDeduction = await LocationController()
        .getLocationDetails(data, 'GetFarmersOptionalDeductions');
    if (resFarmerDeduction != null) {
      try {
        print("Deductions$resFarmerDeduction");
        var data2 = deductionsModelFromJson(resFarmerDeduction.toString());
        print(data2.result.toString());

        _deductions = [];
        _deductions.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(deductionsTable, _deductions);

        if (i >= 0) {
          if (_deductions.length < 1000) {
            return;
          } else {
            await _getDeductions();
          }
        }
      } catch (e) {}
    }
  }

  _getDivisions() async {
    var data = await checkSQLITEData(divisionsTable);
    print("$data Is Datecomparer");

    var resDivisions = await LocationController().getLocationDetailsDivision(
        data, 'Weightcapture/MobileService/GetDivisions');
    if (resDivisions != null) {
      try {
        print("Divisions$resDivisions");
        var data2 = divisionsModelFromJson(resDivisions.toString());
        print(data2.result.toString());

        _divisions = [];
        _divisions.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(divisionsTable, _divisions);

        if (i >= 0) {
          if (_divisions.length < 1000) {
            return;
          } else {
            await _getDivisions();
          }
        }
      } catch (e) {}
    }
  }

  _getBlocks() async {
    var data = await checkSQLITEData(blocksTable);
    print("$data Is Datecomparer");

    var resBlocks =
        await LocationController().getLocationDetails(data, 'GetBlocksProfile');
    if (resBlocks != null) {
      try {
        print("Blocks$resBlocks");
        var data2 = blocksModelFromJson(resBlocks.toString());
        print(data2.result.toString());

        _blocks = [];
        _blocks.addAll(data2.result.result.toList());

        int i = await LocationController().saveLocations(blocksTable, _blocks);

        if (i >= 0) {
          if (_blocks.length < 1000) {
            return;
          } else {
            await _getBlocks();
          }
        }
      } catch (e) {}
    }
  }

  _getRubberClones() async {
    var data = await checkSQLITEData(rubberClonesTable);
    print("$data Is Datecomparer");

    var resRubberClones =
        await LocationController().getLocationDetails(data, 'GetRubberClones');
    if (resRubberClones != null) {
      try {
        print("Rubber Clones$resRubberClones");
        var data2 = rubberClonesModelFromJson(resRubberClones.toString());
        print(data2.result.toString());

        _rubberClones = [];
        _rubberClones.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(rubberClonesTable, _rubberClones);

        if (i >= 0) {
          if (_rubberClones.length < 1000) {
            return;
          } else {
            await _getRubberClones();
          }
        }
      } catch (e) {}
    }
  }

  _getPlantingTypes() async {
    var data = await checkSQLITEData(plantingTypesTable);
    print("$data Is Datecomparer");

    var resPlantingTypes = await LocationController()
        .getLocationDetails(data, 'GetTypeOfPlanting');
    if (resPlantingTypes != null) {
      try {
        print("PlantingTypes$resPlantingTypes");
        var data2 = plantingTypesModelFromJson(resPlantingTypes.toString());
        print(data2.result.toString());

        _plantingTypes = [];
        _plantingTypes.addAll(data2.result!.result.toList());

        int i = await LocationController()
            .saveLocations(plantingTypesTable, _plantingTypes);

        if (i >= 0) {
          if (_plantingTypes.length < 1000) {
            return;
          } else {
            await _getPlantingTypes();
          }
        }
      } catch (e) {}
    }
  }

  _getTapSystem() async {
    var data = await checkSQLITEData(tapSystemsTable);
    print("$data Is Datecomparer");

    var resTapSystem = await LocationController()
        .getLocationDetails(data, 'GetTypeOfTappingSystem');
    if (resTapSystem != null) {
      try {
        print("Tap Systems$resTapSystem");
        var data2 = tappingSystemsModelFromJson(resTapSystem.toString());
        print(data2.result.toString());

        _tappingSystems = [];
        _tappingSystems.addAll(data2.result.result.toList());

        int i = await LocationController()
            .saveLocations(tapSystemsTable, _tappingSystems);

        if (i >= 0) {
          if (_tappingSystems.length < 1000) {
            return;
          } else {
            await _getTapSystem();
          }
        }
      } catch (e) {}
    }
  }

  _getMainDivisions() async {
    var data = await checkSQLITEData(mainDivisionsTable);
    print("$data Is Datecomparer");

    var resMainDivisions =
        await LocationController().getLocationDetails(data, 'GetMainDivision');
    if (resMainDivisions != null) {
      try {
        print("TMain Division$resMainDivisions");
        var data2 = mainDivisionsModelFromJson(resMainDivisions.toString());
        print(data2.result.toString());

        _mainDivisions = [];
        _mainDivisions.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(mainDivisionsTable, _mainDivisions);

        if (i >= 0) {
          if (_mainDivisions.length < 1000) {
            return;
          } else {
            await _getMainDivisions();
          }
        }
      } catch (e) {}
    }
  }

  _getVisitReasons() async {
    var data = await checkSQLITEData(visitReasonTable);
    print("$data Is Datecomparer");

    var resVisitReasons = await LocationController()
        .getLocationDetails(data, 'GetVisitReasonList');
    if (resVisitReasons != null) {
      try {
        print("Visit Reasons$resVisitReasons");
        var data2 = visitReasonModelFromJson(resVisitReasons.toString());
        print(data2.result.toString());

        _visitReasons = [];
        _visitReasons.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(visitReasonTable, _visitReasons);

        if (i >= 0) {
          if (_visitReasons.length < 1000) {
            return;
          } else {
            await _getVisitReasons();
          }
        }
      } catch (e) {}
    }
  }

  _getTrainingTopics() async {
    var data = await checkSQLITEData(trainingTopicsTable);
    print("$data Is Datecomparer");

    var resTrainingTopics = await LocationController()
        .getLocationDetails(data, 'GetTrainingTopicsList');
    if (resTrainingTopics != null) {
      try {
        print("Training Topics$resTrainingTopics");
        var data2 = trainingTopicsModelFromJson(resTrainingTopics.toString());
        print(data2.result.toString());

        _trainingTopics = [];
        _trainingTopics.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(trainingTopicsTable, _trainingTopics);

        if (i >= 0) {
          if (_trainingTopics.length < 1000) {
            return;
          } else {
            await _getTrainingTopics();
          }
        }
      } catch (e) {}
    }
  }

  _getTrainingSubTopics() async {
    var data = await checkSQLITEData(trainingSubTopicsTable);
    print("$data Is Datecomparer");

    var resTrainingSubTopics = await LocationController()
        .getLocationDetails(data, 'GetTrainingSubTopicsList');
    if (resTrainingSubTopics != null) {
      try {
        print("Training SubTopics$resTrainingSubTopics");
        var data2 =
            trainingSubTopicsModelFromJson(resTrainingSubTopics.toString());
        print(data2.result.toString());

        _trainingSubTopics = [];
        _trainingSubTopics.addAll(data2.result.result.toList());

        int i = await LocationController()
            .saveLocations(trainingSubTopicsTable, _trainingSubTopics);

        if (i >= 0) {
          if (_trainingSubTopics.length < 1000) {
            return;
          } else {
            await _getTrainingSubTopics();
          }
        }
      } catch (e) {}
    }
  }

  _getInputQuestions() async {
    var data = await checkSQLITEData(inputQuestionsTable);
    print("$data Is Datecomparer");

    var resInputQuestions = await LocationController()
        .getLocationDetails(data, 'GetInpQuestionsList');
    if (resInputQuestions != null) {
      try {
        print("Training SubTopics$resInputQuestions");
        var data2 = inpQuestionsModelFromJson(resInputQuestions.toString());
        print(data2.result.toString());

        _inputQuestions = [];
        _inputQuestions.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(inputQuestionsTable, _inputQuestions);

        if (i >= 0) {
          if (_inputQuestions.length < 1000) {
            return;
          } else {
            await _getInputQuestions();
          }
        }
      } catch (e) {}
    }
  }

  _getInputQuestionaireAnswers() async {
    var data = await checkSQLITEData(inputQuestionaireAnswersTable);
    print("$data Is Datecomparer");

    var resInputAnswers = await LocationController()
        .getLocationDetails(data, 'GetQuestionaireAnswers');
    if (resInputAnswers != null) {
      try {
        print("Input Answers$resInputAnswers");
        var data2 =
            questionaireAnswersModelFromJson(resInputAnswers.toString());
        print(data2.result.toString());

        _inputQuestionaireAnswers = [];
        _inputQuestionaireAnswers.addAll(data2.result!.result!.toList());

        int i = await LocationController().saveLocations(
            inputQuestionaireAnswersTable, _inputQuestionaireAnswers);

        if (i >= 0) {
          if (_inputQuestionaireAnswers.length < 1000) {
            return;
          } else {
            await _getInputQuestionaireAnswers();
          }
        }
      } catch (e) {}
    }
  }

  _getCPATypesList() async {
    var data = await checkSQLITEData(cpaTypeTable);
    print("$data Is Datecomparer");

    var rescpaTypes =
        await LocationController().getLocationDetails(data, 'GetCPATypesList');
    if (rescpaTypes != null) {
      try {
        print("Input Answers$rescpaTypes");
        var data2 = cpaTypesApiModelFromJson(rescpaTypes.toString());
        print(data2.result.toString());

        _cpaTypes = [];
        _cpaTypes.addAll(data2.result!.result!.toList());

        int i =
            await LocationController().saveLocations(cpaTypeTable, _cpaTypes);

        if (i >= 0) {
          if (_cpaTypes.length < 1000) {
            return;
          } else {
            await _getCPATypesList();
          }
        }
      } catch (e) {}
    }
  }

  _getCPACommercialNameList() async {
    var data = await checkSQLITEData(cpaCommercialNameTable);
    print("$data Is Datecomparer");

    var rescpaCommercialName = await LocationController()
        .getLocationDetails(data, 'GetCPACommercialNameList');
    if (rescpaCommercialName != null) {
      try {
        print("Input Answers$rescpaCommercialName");
        var data2 =
            cpaCommercialNameApiModelFromJson(rescpaCommercialName.toString());
        print(data2.result.toString());

        _cpaCommercialName = [];
        _cpaCommercialName.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(cpaCommercialNameTable, _cpaCommercialName);

        if (i >= 0) {
          if (_cpaCommercialName.length < 1000) {
            return;
          } else {
            await _getCPACommercialNameList();
          }
        }
      } catch (e) {}
    }
  }

  _getWeatherTypeList() async {
    var data = await checkSQLITEData(weatherTypeTable);
    print("$data Is Datecomparer");

    var resweatherType = await LocationController()
        .getLocationDetails(data, 'GetWeatherTypeList');
    if (resweatherType != null) {
      try {
        print("Input Answers$resweatherType");
        var data2 = weatherTypeApiModelFromJson(resweatherType.toString());
        print(data2.result.toString());

        _weatherType = [];
        _weatherType.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(weatherTypeTable, _weatherType);

        if (i >= 0) {
          if (_weatherType.length < 1000) {
            return;
          } else {
            await _getWeatherTypeList();
          }
        }
      } catch (e) {}
    }
  }

  _getInterventionTypesList() async {
    var data = await checkSQLITEData(interventionTypeTable);
    print("$data Is Datecomparer");

    var resInterventionType = await LocationController()
        .getLocationDetails(data, 'GetInterventionTypesList');
    if (resInterventionType != null) {
      try {
        print("Input Answers$resInterventionType");
        var data2 =
            interventionTypeApiModelFromJson(resInterventionType.toString());
        print(data2.result.toString());

        _interventionType = [];
        _interventionType.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(interventionTypeTable, _interventionType);

        if (i >= 0) {
          if (_interventionType.length < 1000) {
            return;
          } else {
            await _getInterventionTypesList();
          }
        }
      } catch (e) {}
    }
  }

  _getNaturalCauseOfDamageList() async {
    var data = await checkSQLITEData(naturalDamageCauseTable);
    print("$data Is Datecomparer");

    var resNaturalDamageCause = await LocationController()
        .getLocationDetails(data, 'GetNaturalCauseOfDamageList');
    if (resNaturalDamageCause != null) {
      try {
        print("Input Answers$resNaturalDamageCause");
        var data2 = naturalCauseOfDamageApiModelFromJson(
            resNaturalDamageCause.toString());
        print(data2.result.toString());

        _naturalDamageCause = [];
        _naturalDamageCause.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(naturalDamageCauseTable, _naturalDamageCause);

        if (i >= 0) {
          if (_naturalDamageCause.length < 1000) {
            return;
          } else {
            await _getNaturalCauseOfDamageList();
          }
        }
      } catch (e) {}
    }
  }

  _getPesticidesAndDiseaseDamageList() async {
    var data = await checkSQLITEData(pesticideAndDiseaseDamageTable);
    print("$data Is Datecomparer");

    var resPesticideAndDiseaseDamage = await LocationController()
        .getLocationDetails(data, 'GetPesticidesAndDiseaseDamageList');
    if (resPesticideAndDiseaseDamage != null) {
      try {
        print("Input Answers$resPesticideAndDiseaseDamage");
        var data2 = pesticidesAndDiseaseDamageApiModelFromJson(
            resPesticideAndDiseaseDamage.toString());
        print(data2.result.toString());

        _pesticideAndDiseaseDamage = [];
        _pesticideAndDiseaseDamage.addAll(data2.result!.result!.toList());

        int i = await LocationController().saveLocations(
            pesticideAndDiseaseDamageTable, _pesticideAndDiseaseDamage);

        if (i >= 0) {
          if (_pesticideAndDiseaseDamage.length < 1000) {
            return;
          } else {
            await _getPesticidesAndDiseaseDamageList();
          }
        }
      } catch (e) {}
    }
  }

  _getInterventionsMethodOfApplicationList() async {
    var data = await checkSQLITEData(interventionApplicationMethodTable);
    print("$data Is Datecomparer");

    var resInterventionApplicationMethod = await LocationController()
        .getLocationDetails(data, 'GetInterventionsMethodOfApplicationList');
    if (resInterventionApplicationMethod != null) {
      try {
        print("Input Answers$resInterventionApplicationMethod");
        var data2 = interventionsMethodOfApplicationApiModelFromJson(
            resInterventionApplicationMethod.toString());
        print(data2.result.toString());

        _interventionApplicationMethod = [];
        _interventionApplicationMethod.addAll(data2.result!.result!.toList());

        int i = await LocationController().saveLocations(
            interventionApplicationMethodTable, _interventionApplicationMethod);

        if (i >= 0) {
          if (_interventionApplicationMethod.length < 1000) {
            return;
          } else {
            await _getInterventionsMethodOfApplicationList();
          }
        }
      } catch (e) {}
    }
  }

  _getExpectedAnswerActionsAndReasonsList() async {
    var data = await checkSQLITEData(expectedAnswerActionReasonTable);
    print("$data Is Datecomparer");

    var resExpectedAnswerActionReason = await LocationController()
        .getLocationDetails(data, 'GetExpectedAnswerActionsAndReasonsList');
    if (resExpectedAnswerActionReason != null) {
      try {
        print("Input Answers$resExpectedAnswerActionReason");
        var data2 = expectedAnswerActionsAndReasonsApiModelFromJson(
            resExpectedAnswerActionReason.toString());
        print(data2.result.toString());

        _expectedAnswerActionReason = [];
        _expectedAnswerActionReason.addAll(data2.result!.result!.toList());

        int i = await LocationController().saveLocations(
            expectedAnswerActionReasonTable, _expectedAnswerActionReason);

        if (i >= 0) {
          if (_expectedAnswerActionReason.length < 1000) {
            return;
          } else {
            await _getExpectedAnswerActionsAndReasonsList();
          }
        }
      } catch (e) {}
    }
  }

  _getInfrastructureItemsList() async {
    var data = await checkSQLITEData(infrastructureTypesTable);
    print("$data Is Datecomparer");

    var resInfrastructureItems = await LocationController()
        .getLocationDetails(data, 'GetPolygonTypeInfrustureList');
    if (resInfrastructureItems != null) {
      try {
        print("Infra List$resInfrastructureItems");
        var data2 =
            infrastructureApiModelFromJson(resInfrastructureItems.toString());
        print(data2.result.toString());

        _infrastructureTypes = [];
        _infrastructureTypes.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(infrastructureTypesTable, _infrastructureTypes);

        if (i >= 0) {
          if (_infrastructureTypes.length < 1000) {
            return;
          } else {
            await _getInfrastructureItemsList();
          }
        }
      } catch (e) {}
    }
  }

  _getReservesList() async {
    var data = await checkSQLITEData(reservesTypesTable);

    var resReservesItems = await LocationController()
        .getLocationDetails(data, 'GetReservesAreaList');
    if (resReservesItems != null) {
      try {
        print("Reserves Types: $resReservesItems");

        var reservesData =
            reservesTypes_modelFromJson(resReservesItems.toString());
        print(reservesData.result.toString());

        _reservesTypes = [];
        _reservesTypes.addAll(reservesData.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(reservesTypesTable, _reservesTypes);

        if (i >= 0) {
          if (_reservesTypes.length < 1000) {
            return;
          } else {
            await _getReservesList();
          }
        }
      } catch (e) {}
    }
  }

  _getActionsAndReasonsList() async {
    var data = await checkSQLITEData(actionsReasonsTable);
    // var data = {
    //   "filter": {
    //     "filters": [
    //       {"field": "datecomparer", "operator": "gt", "value": 0}
    //     ],
    //     "logic": "AND"
    //   },
    //   "take": 1000,
    //   "skip": 0
    // };

    var resActionsAndReasons = await LocationController()
        .getLocationDetails(data, 'GetExpectedAnswerActionsAndReasonsList');
    if (resActionsAndReasons != null) {
      try {
        print("Reserves Types: $resActionsAndReasons");

        var actionReasonData =
            actionsAndReasonsModelFromJson(resActionsAndReasons.toString());
        print(actionReasonData.result.toString());

        _actionReasons = [];
        _actionReasons.addAll(actionReasonData.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(actionsReasonsTable, _actionReasons);

        if (i >= 0) {
          if (_actionReasons.length < 1000) {
            return;
          } else {
            await _getActionsAndReasonsList();
          }
        }
      } catch (e) {}
    }
  }

  _getAllDivisionsDataList() async {
    var data = await checkSQLITEData(allDivisionPolygonTable);
    print("$data Is Datecomparer at Divisions");

    var resallDivisions = await LocationController()
        .getLocationDetails(data, 'getdivisionpolygonall');
    if (resallDivisions != null) {
      print("Divisions $resallDivisions");

      try {
        var data2 = allDivisionsModelApiFromJson(resallDivisions.toString());
        print(data2.result.toString());

        _allDivisions = [];
        _allDivisions.addAll(data2.result!.result!.toList());

        int i = await LocationController()
            .saveLocations(allDivisionPolygonTable, _allDivisions);

        if (i >= 0) {
          if (_allDivisions.length < 1000) {
            return;
          } else {
            await _getAllDivisionsDataList();
          }
        }
      } catch (e) {}
    }
  }

  _getChildrenOnFarm() async {
    var data = await checkSQLITEData(nextKinTable);
  
    var childrenOnFarmItems = await LocationController()
        .getLocationDetails(data, 'GetChildrenOnFarmList');
    if (childrenOnFarmItems != null) {
      //try {
      print("Children On Farm: $childrenOnFarmItems");
  
      var cofData =
      nextOfKinModelFromJson(childrenOnFarmItems.toString());
      print(cofData.result.toString());
  
      _childrenOnFarm = [];
      _childrenOnFarm.addAll(cofData.result!.result!.toList());
  
      int i = await LocationController()
          .saveLocations(nextKinTable, _childrenOnFarm);
  
      if (i >= 0) {
        if (_childrenOnFarm.length < 1000) {
          return;
        } else {
          await _getChildrenOnFarm();
        }
      }
      //} catch (e) {}
    }
  }

  // UPDATE FUNCTIONS
  _getAllPendingFarmersLocal() async {
    print("At Pending Farmers Function");

    var res = await FarmersController().getAllUnUpdatedFarmersLocal();
    if (res != null) {
      _allFarmersToUpdate = res;
      print(_allFarmersToUpdate[0]["full_name"]!.toString());
      print(_allFarmersToUpdate.length);

      for (var element in _allFarmersToUpdate) {
        var farmer = {
          "member_no": element["member_no"].toString(),
          "middle_name": element["middle_name"].toString(),
          "last_name": element["last_name"].toString(),
          "other_names": "other names",
          "first_name": element["full_name"].toString(),
          "marital_status_id": element["marital_status"].toString(),
          "phone1": element["phone1"].toString(),
          "gender_type_id": element["gender_type_id"].toString(),
          "nat_id": element["nat_id"].toString(),
          "dob": element["dob"].toString(),
          "region_id": element["regionId"].toString(),
          "district_id": element["districtId"].toString(),
          "ward_id": element["wardId"].toString(),
          "village_id": element["villageId"].toString(),
          "collection_centre_Id": "",
          "nationality_id": element["nationalityId"].toString(),
          "bank_details_id": element["bankdetailsId"].toString(),
          "bankbranch_id": element["bankbranchId"].toString(),
          "acc_no": element["accNo"].toString()
        };

        var res = await FarmersController()
            .updateAllFarmersApi(farmer, 'ProfileFarmer');
        if (res != null) {
          //UpdateFarmerApiModel updateFarmer = UpdateFarmerApiModel();
          var data2 = updateFarmerApiModelFromJson(res.toString());
          FarmerModelDB modelDB = FarmerModelDB();
          modelDB.syncId = 's';
          modelDB.resultId = int.parse(data2.result!.id.toString());
          modelDB.fullName = data2.result!.firstName;
          modelDB.firstName = data2.result!.firstName;
          modelDB.lastName = data2.result!.lastName;
          modelDB.middleName = data2.result!.middleName;
          modelDB.memberNo = data2.result!.memberNo;
          modelDB.phone1 = data2.result!.phone1;
          modelDB.dob = data2.result!.dob;
          modelDB.natId = data2.result!.natId;
          modelDB.genderTypeId = data2.result!.genderTypeId.toString();
          modelDB.maritalStatus = data2.result!.maritalStatusId.toString();
          modelDB.isActive = true;
          modelDB.collectionCentreId =
              data2.result!.collectionCentreId.toString();
          modelDB.regionId = data2.result!.regionId.toString();
          modelDB.districtId = data2.result!.districtId.toString();
          modelDB.wardId = data2.result!.wardId.toString();
          modelDB.nationalityId = data2.result!.nationalityId.toString();
          modelDB.bankdetailsId = data2.result!.bankDetailsId.toString();
          modelDB.bankbranchId = data2.result!.bankbranchId.toString();
          modelDB.accNo = data2.result!.accNo;
          modelDB.mainCategoryCode = element["main_category_code"]!.toString();

          await _updateFarmer(modelDB, data2.result!.memberNo.toString());
        }

        print("Farmer is: $farmer");
        //Navigator.of(context).pop(FullScreenModal());
      }
    } else {
      print("Did not Get");
      //Navigator.of(context).pop(FullScreenModal());
      //await _getUpdatedLocalChildren();
    }
  }

  _getUpdatedLocalChildren() async {
    // await _getAllFarmersLocal();
    // setState(() {});
    // // for (var element in _allFarmersLocal) {
     // await _getAllPendingFarmersChildrenLocal(element["member_no"]);
    // // }
    // await _getAllPendingFarmersVisitsLocal();

    Navigator.of(context).pop(FullScreenModal());
  }

  // _getAllPendingFarmersChildrenLocal(String memberNo) async {
  //   //setState(() => isLoading = true);
  //   print("At FarmerChild Function");
  //
  //   try {
  //     var res = await FarmersController().getAllUnUpdatedLocal(
  //         nextKinTable, "member_no", memberNo, "sid", "p", "_Id");
  //
  //     print("${res}Is here");
  //
  //     if (res.toString().isNotEmpty) {
  //       _allFarmersChildrenToUpdate = res;
  //       print(_allFarmersChildrenToUpdate[0]["full_name"]!.toString());
  //       print(_allFarmersChildrenToUpdate.length);
  //
  //       for (var element in _allFarmersChildrenToUpdate) {
  //         var farmerChild = {
  //           "member_no": element["member_no"].toString(),
  //           "birth_certificate_no": element["birth_certificate_no"].toString(),
  //           "full_name": element["full_name"].toString(),
  //           "date_of_birth": element["date_of_birth"].toString(),
  //           "is_schooled": element["is_schooled"].toString(),
  //           "relationship_id": element["relationship_id"].toString(),
  //           "phonenumber": element["phonenumber"].toString(),
  //           "age": element["age"].toString(),
  //           "gender_type_id": element["gender_type_id"].toString(),
  //           "level_of_education_id":
  //               element["level_of_education_id"].toString(),
  //           "is_orphaned": element["is_orphaned"].toString()
  //         };
  //
  //         var res = await FarmersController()
  //             .updateApi(farmerChild, 'ProfileChildOnFarm');
  //         if (res != null) {
  //           var data2 = updateFarmerChildApiModelFromJson(res.toString());
  //           NextOfKinModel kinModel = NextOfKinModel();
  //
  //           kinModel.sid = 's';
  //           kinModel.memberNo = data2.result!.memberNo;
  //           kinModel.fullName = data2.result!.fullName;
  //           kinModel.birthCertificateNo = data2.result!.birthCertificateNo;
  //           kinModel.dateOfBirth = data2.result!.dateOfBirth.toString();
  //           kinModel.relationshipId = data2.result!.relationshipId.toString();
  //           kinModel.maritalStatusId = data2.result!.maritalStatusId;
  //           kinModel.phonenumber = data2.result!.phonenumber;
  //           kinModel.genderTypeId = data2.result!.genderTypeId.toString();
  //           kinModel.levelOfEducationId =
  //               data2.result!.levelOfEducationId.toString();
  //           kinModel.isSchooled = data2.result!.isSchooled;
  //           kinModel.isOrphaned = data2.result!.isOrphaned.toString();
  //
  //           await _updateLocal(nextKinTable, kinModel, "member_no",
  //               data2.result!.memberNo.toString());
  //         }
  //
  //         print("Farmer is: $farmerChild");
  //       }
  //     } else {
  //       //showCustomSnackBar("Did not Get Un-Updated Farmers", context, ContentType.failure, title: "FAILURE");
  //       print("Did not Get Un-Updated Farmers");
  //       return;
  //     }
  //   } catch (e) {
  //     print(e.toString());
  //     showCustomSnackBar("Did not Update NextKin + ${e.toString()}", context,
  //         ContentType.failure,
  //         title: "FAILURE");
  //   }
  // }

// UPDATE VISITS
  _getAllPendingFarmersVisitsLocal() async {
    //setState(() => isLoading = true);
    print("At FarmerVisit Function");

    try {
      var res = await FarmersController()
          .getSingleLocal(farmerVisitInspectionTable, "syncId", "p");

      print("${res}Is here");

      if (res.toString().isNotEmpty) {
        _allFarmersVisitsToUpdate = res;
        print(
            _allFarmersVisitsToUpdate[0]["visit_transaction_code"]!.toString());
        print(_allFarmersVisitsToUpdate.length);

        for (var element in _allFarmersVisitsToUpdate) {
          var farmerVisit = {
            "visit_transaction_code":
                element["visit_transaction_code"].toString(),
            "visit_reason_id": element["visit_reason_id"].toString(),
            "visit_date": element["visit_date"].toString(),
            "member_No": element["member_No"].toString(),
            "new_dev_blocks_id": element["new_dev_blocks_id"].toString(),
            "comments": element["comments"].toString(),
            "verification_date": element["verification_date"].toString(),
            "user_id": element["user_id"].toString(),
            "lat": element["lat"].toString(),
            "lon": element["lon"].toString()
          };

          var res = await FarmersController()
              .updateApi(farmerVisit, 'AddFarmInspectionVisit');
          if (res != null) {
            var data2 = updateFarmerVisitApiModelFromJson(res.toString());
            VisitInspectionModelResultElement visitModel =
                VisitInspectionModelResultElement();
            visitModel.visitTransactionCode =
                data2.result!.visitTransactionCode;
            visitModel.visitReason = data2.result!.visitReason;
            visitModel.visitDate = data2.result!.visitDate;
            visitModel.memberNo = data2.result!.memberNo;
            visitModel.newDevBlocksId = data2.result!.newDevBlocksId;
            visitModel.comments = data2.result!.comments;
            visitModel.verificationDate = data2.result!.verificationDate;
            visitModel.userId = data2.result!.userId;
            visitModel.lat = data2.result!.lat;
            visitModel.lon = data2.result!.lon;

            await _updateLocal(farmerVisitInspectionTable, visitModel,
                "member_no", data2.result!.memberNo.toString());
          }

          print("Visit is: $farmerVisit");
        }
      } else {
        //showCustomSnackBar("Did not Get Un-Updated Farmers", context, ContentType.failure, title: "FAILURE");
        print("Did not Get Un-Updated Visit");
        return;
      }
    } catch (e) {
      print(e.toString());
      showCustomSnackBar("Did not Update Visit + ${e.toString()}", context,
          ContentType.failure,
          title: "FAILURE");
    }
  }

  List<dynamic> items = [
    {
      "image": 'assets/images/outgrowers.png',
      "name": "OUTGROWERS",
      "descr": "External Outgrowers"
    },
    {
      "image": 'assets/images/ic_3_1.png',
      "name": "ESTATES",
      "descr": "Estates Profiling"
    },
    {
      "image": 'assets/images/ic_2_3.png',
      "name": "FETCH BLOCKS",
      "descr": "Sync Maps for Offline view"
    },
    {
      "image": 'assets/images/ic_2_3.png',
      "name": "BioDiversity",
      "descr": "Sync Maps for Offline view"
    },
  ];

  final String _selectedValue = 'None';
  Map<String, dynamic> seletedDivision = {};

  Future<void> _showFullPagePopup() async {
    final result = await showDialog<Map<String, dynamic>>(
      context: context,
      builder: (BuildContext context) {
        return const AllDivisionsScreen();
      },
    );
    SharedPreferences localStorage = await SharedPreferences.getInstance();

    if (result != null) {
      setState(() {
        seletedDivision = result;
        localStorage.setString('SelectedDivision', jsonEncode(result));
        // _selectedValue = result;
      });
    }
    // else{
    //   var result = localStorage.getString('SelectedDivision');
    //   setState(() {
    //     seletedIndex = jsonDecode(result.toString());
    //   });
    // }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (seletedDivision.isNotEmpty) {
      print("Division$seletedDivision");
    } else {
      _getDivision();
    }
  }

  _getDivision() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var result = localStorage.getString('SelectedDivision');
    var data = jsonDecode(result.toString());
    print(data);
    if (data != null) {
      setState(() {
        seletedDivision = data;
      });
    }
  }

  // var selectedDivision;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        width: Dimensions.screenWidth,
        //height: Dimensions.screenHeight,
        child: Column(
          children: [
            SizedBox(
              height: Dimensions.height30 * (Dimensions.height10),
              child: Stack(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        top: Dimensions.height30,
                        left: Dimensions.width20,
                        right: Dimensions.width20),
                    height: Dimensions.height15 * (Dimensions.height20),
                    decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(Dimensions.radius30 * 4),
                          bottomRight: Radius.circular(Dimensions.radius30 * 4),
                        )),
                    child: SizedBox(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SmallText(
                                text: "Hello👋,  Welcome Back",
                                size: Dimensions.font20,
                                color: AppColors.textWhite,
                              ),
                              IconButton(
                                icon: FaIcon(
                                  FontAwesomeIcons.ellipsisVertical,
                                  size: Dimensions.height30,
                                  color: AppColors.textWhite,
                                ),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return SimpleDialog(
                                        title: const Text('Select a Page'),
                                        children: <Widget>[
                                          ListTile(
                                            leading: const Icon(Icons.settings),
                                            title: const Text('Settings'),
                                            onTap: () {},
                                          ),
                                          ListTile(
                                            leading: const Icon(Icons.help),
                                            title: const Text('Help'),
                                            onTap: () {},
                                          ),
                                          ListTile(
                                            leading: const Icon(Icons.info),
                                            title: const Text('About'),
                                            onTap: () {},
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: Dimensions.height20,
                          ),
                          GestureDetector(
                            onTap: () async {
                              _syncData(false);
                            },
                            child: Container(
                              height: Dimensions.height20 * 3,
                              width: Dimensions.width20 * 7.5,
                              decoration: BoxDecoration(
                                  color: AppColors.subColor,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30)),
                              child: Center(
                                child: Container(
                                  padding: EdgeInsets.only(
                                    top: Dimensions.height10 / 3,
                                    bottom: Dimensions.height10 / 3,
                                    left: Dimensions.width10 / 2,
                                    right: Dimensions.width10 / 2,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      FaIcon(
                                        FontAwesomeIcons.rotate,
                                        size: Dimensions.height30,
                                        color: AppColors.textWhite,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width20,
                                      ),
                                      SmallText(
                                        text: "SYNC(0)",
                                        size: Dimensions.font20,
                                        color: AppColors.textWhite,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),
                          Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.circleInfo,
                                  size: Dimensions.height10,
                                  color: AppColors.buttonRed,
                                ),
                                SizedBox(
                                  width: Dimensions.width10,
                                ),
                                SmallText(
                                  text: "Kindly SYNC before start",
                                  size: Dimensions.font14,
                                  color: Colors.black,
                                ),
                                GestureDetector(
                                  onTap: () async {
                                    SharedPreferences localStorage =
                                        await SharedPreferences.getInstance();
                                    localStorage.clear();
                                  },
                                  child: SmallText(
                                    text: "Clear",
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // Positioned(
                  //   top: 220,
                  //   left: Dimensions.width20 * (Dimensions.width10 / 1.9),
                  //   child: Center(
                  //     child: Container(
                  //       height: Dimensions.height20 * 3,
                  //       width: Dimensions.width20 * Dimensions.width15,
                  //       decoration: BoxDecoration(
                  //           color: Colors.white,
                  //           borderRadius: BorderRadius.circular(Dimensions.radius20)),
                  //       child: Center(
                  //         child: BigText(
                  //           text: "Dashboard",
                  //           size: Dimensions.font20,
                  //           color: AppColors.black,
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
            SizedBox(
              height: Dimensions.height45 + 5,
            ),
            InkWell(
              onTap: () {},
              child: Text(
                  'Selected value: ${seletedDivision["division_name"].toString()}'),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: Dimensions.height20 * 3,
                  width: Dimensions.screenWidth / 2,
                  decoration: const BoxDecoration(
                    color: Colors.grey,
                  ),
                  child: Center(
                    child: BigText(
                      text: seletedDivision.isEmpty
                          ? "NONE"
                          : seletedDivision["division_name"].toString(),
                      size: Dimensions.font20,
                      color: AppColors.black,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _showFullPagePopup();
                  },
                  child: Container(
                    height: Dimensions.height20 * 3,
                    width: Dimensions.screenWidth / 2,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Center(
                      child: BigText(
                        text: "Change",
                        size: Dimensions.font20,
                        color: AppColors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),

            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: Dimensions.width10),
                //   child: GridView.count(
                //     shrinkWrap: true,
                // crossAxisCount: 2,
                // crossAxisSpacing: 2.0,
                // mainAxisSpacing: 0.0,
                // children: List.generate(items.length, (index) {
                //   var item = items[index];
                //   return GestureDetector(
                //         onTap: () {
                //           setState(() {
                //             if (_selectedIndex == index) {
                //               if (_selectedIndex == 0) {
                //                 Get.to(
                //                   () => const MenuScreen(),
                //                 );
                //               } else if (_selectedIndex == 1) {
                //                 Get.to(() => const EstateDashboardScreen());
                //               }
                //             } else {
                //               _selectedIndex = index;
                //             }
                //             print(_selectedIndex.toString());
                //           });
                //           if (_selectedIndex == 0) {
                //             Get.to(() => const MenuScreen(), arguments: -1);
                //           } else if (_selectedIndex == 1) {
                //             Get.to(() => const EstateDashboardScreen());
                //           }
                //         },
                //         child: Row(
                //           mainAxisAlignment: MainAxisAlignment.center,
                //           children: [
                //             Container(
                //               height: Dimensions.height30 * 4,
                //               width: Dimensions.width30 * 7,
                //               decoration: _selectedIndex == index
                //                   ? BoxDecoration(
                //                       color: AppColors.mainColor,
                //                       borderRadius: BorderRadius.circular(
                //                           Dimensions.radius20 / 4),
                //                     )
                //                   : BoxDecoration(
                //                       color: AppColors.textWhite,
                //                       borderRadius: BorderRadius.circular(
                //                           Dimensions.radius20 / 4),
                //                       border: Border.all(
                //                         width: 2,
                //                         color: AppColors.mainColor,
                //                       )),
                //               child: Column(
                //                 mainAxisAlignment: MainAxisAlignment.center,
                //                 children: [
                //                   Image.asset(
                //                     item["image"].toString(),
                //                     width: 60,
                //                     height: 60,
                //                     color: _selectedIndex == index
                //                         ? AppColors.textWhite
                //                         : AppColors.mainColor,
                //                   ),
                //                   BigText(
                //                     text: item["name"].toString(),
                //                     color: AppColors.black,
                //                     size: Dimensions.font16 - 4,
                //                   ),
                //                 ],
                //               ),
                //             ),
                //           ],
                //         ),
                //       );
                // }  ),
                // ),

                child: GridView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: items.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 1,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 19),
                  itemBuilder: (context, index) {
                    var item = items[index];
                    return GestureDetector(
                      onTap: () {
                        if (seletedDivision.isEmpty) {
                          showCustomSnackBar("Kindly Select A Division",
                              context, ContentType.failure,
                              title: "Failed");
                        } else {
                          setState(() {
                            if (_selectedIndex == index) {
                              if (_selectedIndex == 0) {
                                print("Division${seletedDivision["Id"]}");
                                Get.to(() => const MenuScreen(), arguments: -1);
                              } else if (_selectedIndex == 1) {
                                Get.to(() => const EstateDashboardScreen(),
                                    arguments:
                                        seletedDivision["Id"].toString());
                              } else if (_selectedIndex == 2) {
                                _syncData(true);
                              } else if (_selectedIndex == 3) {
                                Get.to(
                                  () => const BioDiversityDashBoardScreen(),
                                );
                              }
                            } else {
                              _selectedIndex = index;
                            }
                            print(_selectedIndex.toString());
                          });
                          if (_selectedIndex == 0) {
                            Get.to(() => const MenuScreen(), arguments: -1);
                          } else if (_selectedIndex == 1) {
                            Get.to(() => const EstateDashboardScreen(),
                                arguments: seletedDivision["Id"].toString());
                          } else if (_selectedIndex == 2) {
                            _syncData(true);
                          } else if (_selectedIndex == 3) {
                            Get.to(
                              () => const BioDiversityDashBoardScreen(),
                            );
                          }
                          // else if (_selectedIndex == 3) {
                          //   Get.to(() => const BioDiversityDashBoardScreen(),
                          //       arguments: seletedDivision["Id"].toString());
                          // }
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            //height: Dimensions.height30 * 6,
                            width: Dimensions.width30 * 6,
                            // margin: EdgeInsets.only(
                            //   bottom: 20,
                            // ),
                            decoration: _selectedIndex == index
                                ? BoxDecoration(
                                    color: AppColors.mainColor,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20 / 4),
                                  )
                                : BoxDecoration(
                                    //color: AppColors.textWhite,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20 / 4),
                                    border: Border.all(
                                      width: 2,
                                      color: AppColors.mainColor,
                                    )),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  item["image"].toString(),
                                  width: 60,
                                  height: 60,
                                  color: _selectedIndex == index
                                      ? AppColors.textWhite
                                      : AppColors.mainColor,
                                ),
                                BigText(
                                  text: item["name"].toString(),
                                  color: AppColors.black,
                                  size: Dimensions.font16 - 4,
                                ),
                                const Divider(
                                  color: Colors.black,
                                  indent: 20,
                                  endIndent: 20,
                                ),
                                SmallText(
                                  text: item["descr"].toString(),
                                  color: AppColors.black,
                                  size: Dimensions.font12,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),

            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Column(
            //       children: [
            //         GestureDetector(
            //           onTap: () {
            //             Get.to(() => const MenuScreen(), arguments: -1);
            //           },
            //           child: Container(
            //             height: Dimensions.height30 * 4,
            //             width: Dimensions.width30 * 4,
            //             decoration: BoxDecoration(
            //               color: AppColors.textWhite,
            //               borderRadius:
            //                   BorderRadius.circular(Dimensions.radius30 * 5),
            //             ),
            //             child: Center(
            //               child: Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,
            //                 children: [
            //                   Image.asset(
            //                     'assets/images/outgrowers.png',
            //                     width: 50,
            //                     height: 50,
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //         ),
            //         SizedBox(
            //           height: Dimensions.height10,
            //         ),
            //         BigText(
            //           text: "OUTGROWERS",
            //           color: AppColors.black,
            //           size: Dimensions.font16 - 4,
            //         ),
            //         SizedBox(
            //           height: Dimensions.height10 / 2,
            //         ),
            //         SizedBox(
            //             width: Dimensions.width45 + Dimensions.width45,
            //             child: const Divider()),
            //         SmallText(
            //           text: "External Farmers",
            //           color: Colors.blueGrey,
            //           size: Dimensions.font16 - 4,
            //         )
            //       ],
            //     ),
            //   ],
            // ),
            // SizedBox(
            //   height: Dimensions.height45 + 5,
            // ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Column(
            //       children: [
            //         GestureDetector(
            //           onTap: () {
            //             Get.to(() => const EstateDashboardScreen());
            //           },
            //           child: Container(
            //             height: Dimensions.height30 * 4,
            //             width: Dimensions.width30 * 4,
            //             decoration: BoxDecoration(
            //               color: AppColors.textWhite,
            //               borderRadius:
            //                   BorderRadius.circular(Dimensions.radius30 * 5),
            //             ),
            //             child: Center(
            //               child: Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,
            //                 children: [
            //                   Image.asset(
            //                     'assets/images/ic_3_1.png',
            //                     width: 60,
            //                     height: 60,
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //         ),
            //         SizedBox(
            //           height: Dimensions.height10,
            //         ),
            //         BigText(
            //           text: "ESTATES",
            //           color: AppColors.black,
            //           size: Dimensions.font16 - 4,
            //         ),
            //         SizedBox(
            //           height: Dimensions.height10 / 2,
            //         ),
            //         SizedBox(
            //             width: Dimensions.width45 + Dimensions.width45,
            //             child: const Divider()),
            //         SmallText(
            //           text: "Internal Estates",
            //           color: Colors.blueGrey,
            //           size: Dimensions.font16 - 4,
            //         )
            //       ],
            //     ),
            //     // Column(
            //     //   children: [
            //     //     GestureDetector(
            //     //       onTap: () {
            //     //         Get.to(() => AllVisitFarmersScreen());
            //     //       },
            //     //       child: Container(
            //     //         height: Dimensions.height30 * 4,
            //     //         width: Dimensions.width30 * 4,
            //     //         decoration: BoxDecoration(
            //     //           color: AppColors.textWhite,
            //     //           borderRadius: BorderRadius.circular(
            //     //               Dimensions.radius30 * 5),
            //     //         ),
            //     //         child: Center(
            //     //           child: Column(
            //     //             mainAxisAlignment: MainAxisAlignment.center,
            //     //             children: [
            //     //               Image.asset(
            //     //                 'assets/images/ic_1_4.png',
            //     //                 width: 50,
            //     //                 height: 50,
            //     //               ),
            //     //             ],
            //     //           ),
            //     //         ),
            //     //       ),
            //     //     ),
            //     //     SizedBox(
            //     //       height: Dimensions.height10,
            //     //     ),
            //     //     BigText(
            //     //       text: 'Visit',
            //     //       color: AppColors.black,
            //     //       size: Dimensions.font16 - 4,
            //     //     ),
            //     //   ],
            //     // ),
            //   ],
            // ),
            // SizedBox(
            //   height: Dimensions.height45 + 5,
            // ),

            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     GestureDetector(
            //       onTap: () async {
            //         _syncData();
            //       },
            //       child: Column(
            //         children: [
            //           Container(
            //             height: Dimensions.height30 * 4,
            //             width: Dimensions.width30 * 4,
            //             decoration: BoxDecoration(
            //               color: AppColors.textWhite,
            //               borderRadius: BorderRadius.circular(
            //                   Dimensions.radius30 * 5),
            //             ),
            //             child: Center(
            //               child: Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,
            //                 children: [
            //                   Image.asset(
            //                     'assets/images/ic_1_6.png',
            //                     width: 50,
            //                     height: 50,
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: Dimensions.height10,
            //           ),
            //           BigText(
            //             text: 'Synchronization(0)',
            //             color: AppColors.black,
            //             size: Dimensions.font16 - 4,
            //           ),
            //         ],
            //       ),
            //     ),
            //   ],
            // ),
            SizedBox(
              height: Dimensions.height10,
            ),
          ],
        ),
      ),
    );
  }
}
