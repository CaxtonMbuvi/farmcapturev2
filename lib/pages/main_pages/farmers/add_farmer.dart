import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../controllers/location/location_controller.dart';
import '../../../models/farmers/allfarmers_model.dart';
import '../../../models/farmers/bankbranches_model.dart';
import '../../../models/farmers/banks_model.dart';
import '../../../models/farmers/gender_type_model.dart';
import '../../../models/farmers/location/county_model.dart';
import '../../../models/farmers/location/district_model.dart';
import '../../../models/farmers/location/village_model.dart';
import '../../../models/farmers/location/ward_model.dart';
import '../../../models/farmers/marital_status.dart';
import '../../../models/farmers/nationality_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/custom_text_field.dart';

class AddFarmerScreen extends StatefulWidget {
  const AddFarmerScreen({super.key});

  @override
  State<AddFarmerScreen> createState() => _AddFarmerScreenState();
}

class _AddFarmerScreenState extends State<AddFarmerScreen> {
  var pageId = Get.arguments;
  bool isLoading = false;
  var farmerNameController = TextEditingController();
  var firstNameController = TextEditingController();
  var middleNameController = TextEditingController();
  var lastNameController = TextEditingController();
  var otherNameController = TextEditingController();
  var maritalStatusController = TextEditingController();
  var phoneNoController = TextEditingController();
  var genderController = TextEditingController();
  var nationalIdController = TextEditingController();
  var dobController = TextEditingController();
  var bankIdController = TextEditingController();
  var bankBranchIdController = TextEditingController();
  var accNoController = TextEditingController();
  var passportController = TextEditingController();
  var photoController = TextEditingController();

  var regionIdController = TextEditingController();
  var districtIdController = TextEditingController();
  var wardIdController = TextEditingController();
  var villageIdController = TextEditingController();
  var colCenterController = TextEditingController();
  var nationalityController = TextEditingController();
  FarmerModelDB modelDB = FarmerModelDB();

  var typeController = TextEditingController();

  String? _colCenterId;
  String genderNmae = 'NONE';
  String maritalStatus = 'NONE';
  String bankName = 'NONE';
  String bankBranchName = 'NONE';
  String accountNumber = 'NONE';
  String countyName = 'NONE';
  String districtName = 'NONE';
  String wardName = 'NONE';
  String villageName = 'NONE';
  String colCenterName = 'NONE';
  String nationalityName = 'NONE';
  File? image;
  //Uint8List? profileImageLocal;
  String passportImageBit64 = '';

  // Initial Selected Value
  String dropdownvalue = 'Item 1';

  final List<Map<String, dynamic>> _allItems = [
    {"id": 1, "name": "Andy", "age": 29},
    {"id": 2, "name": "Aragon", "age": 40},
    {"id": 3, "name": "Bob", "age": 5},
    {"id": 4, "name": "Barbara", "age": 35},
    {"id": 5, "name": "Candy", "age": 21},
    {"id": 6, "name": "Colin", "age": 55},
    {"id": 7, "name": "Audra", "age": 30},
    {"id": 8, "name": "Banana", "age": 14},
    {"id": 9, "name": "Caversky", "age": 100},
    {"id": 10, "name": "Becky", "age": 32},
  ];

//Drop Down Lists Here
  final List<BankResultElement> _banks = [];
  final List<BankBranchResultElement> _bankBranches = [];

  final List<GenderResultElement> _genderTypes = [];
  final List<MaritalStatusResultElement> _maritalStatus = [];
  final List<CountyResultElement> _counties = [];
  final List<DistrictResultElement> _districts = [];
  final List<WardResultElement> _ward = [];
  final List<VillageResultElement> _village = [];
  final List<CountyResultElement> _colCenter1 = [];
  final List<NationalityResultElement> _nationalities = [];
  

// Selected Dropdown elements
  BankResultElement? selectedBank;
  BankBranchResultElement? selectedBankBranch;

  GenderResultElement? selectedGender;
  MaritalStatusResultElement? selectedMaritalStatus;
  CountyResultElement? selectedCounty;
  DistrictResultElement? selectedDistrict;
  WardResultElement? selectedWard;
  VillageResultElement? selectedVillage;
  // ? selectedColCenter;
  NationalityResultElement? selectedNationality;

  // String For Names

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        dobController.text = formattedDate;

        print(date.toString());
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _getLocationDetails();
  }

  _getLocationDetails() async {
    setState(() => isLoading = true);
    List<dynamic> resGender =
        await LocationController().getLocationsLocal(genderTypeTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resGender) {
      GenderResultElement model = GenderResultElement(
          resultId: i['Id'],
          code: i['code'],
          name: i['name'],
          datecomparer: i['datecomparer']);

      _genderTypes.add(model);
    }

    //selectedGender = _genderTypes[0];

    List<dynamic> resMaritalStatus =
        await LocationController().getLocationsLocal(maritalStatusTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resMaritalStatus) {
      MaritalStatusResultElement model = MaritalStatusResultElement(
          resultId: i['Id'],
          code: i['code'],
          name: i['name'],
          datecomparer: i['datecomparer']);

      _maritalStatus.add(model);
    }

    //selectedMaritalStatus = _maritalStatus[0];

    List<dynamic> resCounty =
        await LocationController().getLocationsLocal(countiesTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resCounty) {
      CountyResultElement model = CountyResultElement(
          resultId: i['Id'],
          colorCode: i['color_code'],
          name: i['name'],
          datecomparer: i['datecomparer']);

      _counties.add(model);
    }

    //selectedCounty = _counties[0];

    List<dynamic> resDistrict =
        await LocationController().getLocationsLocal(districtTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resDistrict) {
      DistrictResultElement model = DistrictResultElement(
          resultId: i['Id'],
          colorCode: i['color_code'],
          name: i['name'],
          regionId: i['region_id'],
          datecomparer: i['datecomparer']);

      _districts.add(model);
    }

    //selectedDistrict = _districts[0];

    List<dynamic> resWard =
        await LocationController().getLocationsLocal(wardTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resWard) {
      WardResultElement model = WardResultElement(
          resultId: i['Id'],
          colorCode: i['color_code'],
          name: i['name'],
          districtId: i['districtId'],
          datecomparer: i['datecomparer']);

      _ward.add(model);
    }

    //selectedWard = _ward[0];

    List<dynamic> resVillage =
        await LocationController().getLocationsLocal(villageTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resVillage) {
      VillageResultElement model = VillageResultElement(
          resultId: i['Id'],
          colorCode: i['color_code'],
          name: i['name'],
          wardId: i['wardId'],
          datecomparer: i['datecomparer']);

      _village.add(model);
    }

    List<dynamic> resBanks =
        await LocationController().getLocationsLocal(banksTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resBanks) {
      BankResultElement model = BankResultElement(
          resultId: i['Id'],
          code: i['color_code'],
          name: i['name'],
          datecomparer: i['datecomparer']);

      _banks.add(model);
    }

    List<dynamic> resBankBranch =
        await LocationController().getLocationsLocal(bankBranchesTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resBankBranch) {
      BankBranchResultElement model = BankBranchResultElement(
          resultId: i['Id'],
          code: i['color_code'],
          bankDetailsId: i['bank_details_id'],
          name: i['name'],
          datecomparer: i['datecomparer']);

      _bankBranches.add(model);
    }

    //selectedVillage = _village[0];

    // List<dynamic> resColCenter =
    //     await LocationController().getLocationsLocal(villageTable);
    // //print("Hello there"+ res[0].toString());
    // for (var i in resColCenter) {
    //   Col model = VillageResultElement(
    //       resultId: i['Id'],
    //       colorCode: i['color_code'],
    //       name: i['name'],
    //       wardId: i['wardId'],
    //       datecomparer: i['datecomparer']);

    //   _village1.add(model);
    // }

    // selectedVillage = _village1[0];

    List<dynamic> resNationality =
        await LocationController().getLocationsLocal(nationalityTable);
    //print("Hello there"+ res[0].toString());
    for (var i in resNationality) {
      NationalityResultElement model = NationalityResultElement(
          resultId: i['Id'],
          code: i['color_code'],
          name: i['name'],
          datecomparer: i['datecomparer']);

      _nationalities.add(model);
    }

    //selectedNationality = _nationalities[0];
    setState(() => isLoading = false);
  }

  Future _getImage() async {
    final picker = ImagePicker();

    var pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (pickedFile != null) {
      final bytes = File(pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

      setState(() {
        photoController.text = img64;
        image = File(pickedFile.path);
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    print(photoController.text.toString());
    //return image;
  }

  _addFarmer() async {
    modelDB.syncId = 'p';
    modelDB.fullName = firstNameController.text;
    modelDB.phone1 = phoneNoController.text;
    modelDB.dob = dobController.text;
    modelDB.natId = nationalIdController.text;
    modelDB.genderTypeId = genderController.text;
    modelDB.maritalStatus = maritalStatusController.text;
    modelDB.collectionCentreId = colCenterController.text;
    modelDB.regionId = regionIdController.text;
    modelDB.districtId = districtIdController.text;
    modelDB.wardId = wardIdController.text;
    modelDB.villageId = villageIdController.text;
    modelDB.passportUrl = passportController.text;
    modelDB.photoString = photoController.text;
    modelDB.nationalityId = nationalityController.text;
    modelDB.bankdetailsId = bankIdController.text;
    modelDB.bankbranchId = bankBranchIdController.text;
    modelDB.accNo = accNoController.text;
    modelDB.mainCategoryCode = 'F001';

    List<dynamic> farmer = [];
    farmer.add(modelDB);

    int i = await FarmersController().saveAll(farmer, farmersTable);

    if (i >= 0) {
      print("Success here");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Add Farmer Profile",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: isLoading
              ? const CustomLoader()
              : Container(
                  color: AppColors.textWhite,
                  width: Dimensions.screenWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: Dimensions.height15,
                      ),
                      Container(
                        width: Dimensions.screenWidth,
                        padding: EdgeInsets.only(
                          top: Dimensions.height45,
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                          bottom: Dimensions.height15,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: BigText(
                                text: "Personal Details",
                                color: AppColors.mainColor,
                                size: Dimensions.font20,
                              ),
                            ),

                            CustomTextField(
                              readOnly: false,
                              controller: firstNameController,
                              onSaved: (value) {},
                              labelText: "Full Name",
                              isObscure: false,
                              onTap: (value) {},
                            ),

                            CustomTextField(
                              readOnly: false,
                              controller: nationalIdController,
                              onSaved: (value) {},
                              labelText: "National ID",
                              isObscure: false,
                              onTap: (value) {},
                            ),

                            CustomTextField(
                              readOnly: false,
                              controller: phoneNoController,
                              onSaved: (value) {},
                              labelText: "Phone No",
                              isObscure: false,
                              onTap: (value) {},
                            ),

                            SizedBox(
                              height: Dimensions.height15,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<MaritalStatusResultElement>(
                                hint: const Text("Marital Status"),
                                value: selectedMaritalStatus,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged: (MaritalStatusResultElement?
                                    newValue) async {
                                  setState(() {
                                    selectedMaritalStatus = newValue;
                                    maritalStatusController.text =
                                        selectedMaritalStatus!.resultId
                                            .toString();
                                    print(
                                        "Here is Marital status ==*** ${selectedMaritalStatus!.name.toString()} and id === ${selectedMaritalStatus!.resultId.toString()}");
                                  });
                                },
                                items: _maritalStatus
                                    .map((MaritalStatusResultElement value) {
                                  return DropdownMenuItem<
                                      MaritalStatusResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height15,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<GenderResultElement>(
                                hint: const Text("Gender"),
                                value: selectedGender,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged:
                                    (GenderResultElement? newValue) async {
                                  setState(() {
                                    selectedGender = newValue;
                                    genderController.text =
                                        selectedGender!.resultId.toString();
                                    print(
                                        "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
                                  });
                                },
                                items: _genderTypes
                                    .map((GenderResultElement gender) {
                                  return DropdownMenuItem<GenderResultElement>(
                                    value: gender,
                                    child: Text(gender.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            //dob
                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: TextFormField(
                                onTap: () {
                                  selectedTimePicker(context);
                                },
                                controller: dobController,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                readOnly: true,
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  border: const UnderlineInputBorder(),
                                  focusColor: AppColors.mainColor,
                                  //hintText: "Date Of Birth",
                                  contentPadding: const EdgeInsets.only(left: 5),
                                  suffixIcon: const Icon(Icons.calendar_month),
                                  labelText: "Date Of Birth",
                                ),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height15,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<BankResultElement>(
                                hint: const Text("Bank"),
                                value: selectedBank,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged: (BankResultElement? newValue) async {
                                  setState(() {
                                    selectedBank = newValue;
                                    bankIdController.text =
                                        selectedBank!.resultId.toString();
                                    print(
                                        "Here is Bank ==*** ${selectedBank!.name.toString()} and id === ${selectedBank!.resultId.toString()}");
                                  });
                                },
                                items: _banks.map((BankResultElement value) {
                                  return DropdownMenuItem<BankResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height15,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<BankBranchResultElement>(
                                hint: const Text("Bank Branch"),
                                value: selectedBankBranch,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged:
                                    (BankBranchResultElement? newValue) async {
                                  setState(() {
                                    selectedBankBranch = newValue;
                                    bankBranchIdController.text =
                                        selectedBankBranch!.resultId.toString();
                                    print(
                                        "Here is Bank Branch ==*** ${selectedBankBranch!.name.toString()} and id === ${selectedBankBranch!.resultId.toString()}");
                                  });
                                },
                                items: _bankBranches
                                    .map((BankBranchResultElement value) {
                                  return DropdownMenuItem<
                                      BankBranchResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height15,
                            ),

                            CustomTextField(
                              readOnly: false,
                              controller: accNoController,
                              onSaved: (value) {},
                              labelText: "Account Number",
                              isObscure: false,
                              onTap: (value) {},
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    height: Dimensions.height10 *
                                        Dimensions.height20,
                                    width: Dimensions.screenWidth / 2,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20 / 2),
                                        color: AppColors.textGrey),
                                    child: image != null
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius15),
                                            child: Image.file(image!,
                                                fit: BoxFit.cover),
                                          )
                                        : const Center(
                                            child: Text('Please add a photo'),
                                          ),
                                  ),
                                  SizedBox(
                                    width: Dimensions.width30,
                                  ),
                                  MaterialButton(
                                    color: Colors.blue,
                                    onPressed: _getImage,
                                    child: Text(
                                      "Take a Photo",
                                      style: TextStyle(
                                          color: AppColors.textWhite,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height45,
                            ),

                            // LOCATION DETAILS
                            SizedBox(
                              height: Dimensions.height30,
                            ),
                            Center(
                              child: BigText(
                                text: "Location Details",
                                color: AppColors.mainColor,
                                size: Dimensions.font20,
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height30,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<CountyResultElement>(
                                hint: const Text("County"),
                                value: selectedCounty,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged:
                                    (CountyResultElement? newValue) async {
                                  setState(() {
                                    selectedCounty = newValue;
                                    regionIdController.text =
                                        selectedCounty!.resultId.toString();
                                    print(
                                        "Here is County ==*** ${selectedCounty!.name.toString()} and id === ${selectedCounty!.resultId.toString()}");
                                  });
                                },
                                items:
                                    _counties.map((CountyResultElement value) {
                                  return DropdownMenuItem<CountyResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height20,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<DistrictResultElement>(
                                hint: const Text("District"),
                                value: selectedDistrict,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged:
                                    (DistrictResultElement? newValue) async {
                                  setState(() {
                                    selectedDistrict = newValue;
                                    districtIdController.text =
                                        selectedDistrict!.resultId.toString();
                                    print(
                                        "Here is District ==*** ${selectedDistrict!.name.toString()} and id === ${selectedDistrict!.resultId.toString()}");
                                  });
                                },
                                items: _districts
                                    .map((DistrictResultElement value) {
                                  return DropdownMenuItem<
                                      DistrictResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height20,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<WardResultElement>(
                                hint: const Text("Town"),
                                value: selectedWard,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged: (WardResultElement? newValue) async {
                                  setState(() {
                                    selectedWard = newValue;
                                    wardIdController.text =
                                        selectedWard!.resultId.toString();
                                    print(
                                        "Here is Ward ==*** ${selectedWard!.name.toString()} and id === ${selectedWard!.resultId.toString()}");
                                  });
                                },
                                items: _ward.map((WardResultElement value) {
                                  return DropdownMenuItem<WardResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height20,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<VillageResultElement>(
                                hint: const Text("Village"),
                                value: selectedVillage,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged:
                                    (VillageResultElement? newValue) async {
                                  setState(() {
                                    selectedVillage = newValue;
                                    villageIdController.text =
                                        selectedVillage!.resultId.toString();
                                    print(
                                        "Here is Village ==*** ${selectedVillage!.name.toString()} and id === ${selectedVillage!.resultId.toString()}");
                                  });
                                },
                                items:
                                    _village.map((VillageResultElement value) {
                                  return DropdownMenuItem<VillageResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height20,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<String>(
                                hint: const Text("Collection Center"),
                                value: _colCenterId,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged: (newValue) async {
                                  setState(() {
                                    _colCenterId = newValue;
                                    //colCenterController.text =
                                  });
                                },
                                items: _allItems.map((value) {
                                  return DropdownMenuItem<String>(
                                    value: value['name'].trim(),
                                    child: Text(value['name'].trim(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height20,
                            ),

                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: DropdownButton<NationalityResultElement>(
                                hint: const Text("Nationality"),
                                value: selectedNationality,
                                isExpanded: true,
                                isDense: true,
                                dropdownColor: Colors.white,
                                icon: const Icon(Icons.arrow_drop_down),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged:
                                    (NationalityResultElement? newValue) async {
                                  setState(() {
                                    selectedNationality = newValue;
                                    nationalityController.text =
                                        selectedNationality!.resultId
                                            .toString();
                                    print(
                                        "Here is Nationality ==*** ${selectedNationality!.name.toString()} and id === ${selectedNationality!.resultId.toString()}");
                                  });
                                },
                                items: _nationalities
                                    .map((NationalityResultElement value) {
                                  return DropdownMenuItem<
                                      NationalityResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            _addFarmer();
                          },
                          child: Container(
                            height: Dimensions.height30 * 2,
                            width: Dimensions.width30 * Dimensions.width10,
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius:
                                  BorderRadius.circular(Dimensions.radius30),
                            ),
                            child: Center(
                              child: BigText(
                                text: "Submit",
                                color: AppColors.textWhite,
                                size: Dimensions.font16,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
