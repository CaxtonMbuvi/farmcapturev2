// import 'dart:convert';
// import 'dart:io';

// import 'package:farmcapturev2/pages/main_pages/farmers/single_farmer_dashboard.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:intl/intl.dart';

// import '../../../controllers/farmers/farmers_controller.dart';
// import '../../../controllers/location/location_controller.dart';
// import '../../../models/farmers/allfarmers_model.dart';
// import '../../../models/farmers/bankbranches_model.dart';
// import '../../../models/farmers/banks_model.dart';
// import '../../../models/farmers/deduction_model.dart';
// import '../../../models/farmers/gender_type_model.dart';
// import '../../../models/farmers/location/county_model.dart';
// import '../../../models/farmers/location/district_model.dart';
// import '../../../models/farmers/location/village_model.dart';
// import '../../../models/farmers/location/ward_model.dart';
// import '../../../models/farmers/marital_status.dart';
// import '../../../models/farmers/nationality_model.dart';
// import '../../../resources/base/custom_loader.dart';
// import '../../../resources/utils/colors.dart';
// import '../../../resources/utils/dimensions.dart';
// import '../../../resources/widgets/big_text.dart';
// import '../../../resources/widgets/form_field.dart';
// import '../../../resources/widgets/small_text.dart';

// class FarmerDetailScreen extends StatefulWidget {
//   const FarmerDetailScreen({super.key});

//   @override
//   State<FarmerDetailScreen> createState() => _FarmerDetailScreenState();
// }

// class _FarmerDetailScreenState extends State<FarmerDetailScreen> {
//   var pageId = Get.arguments;
//   var fullNameController = TextEditingController();
//   //var firstNameController = TextEditingController();
//   var middleNameController = TextEditingController();
//   var lastNameController = TextEditingController();
//   var otherNameController = TextEditingController();
//   var maritalStatusController = TextEditingController();
//   var phoneNoController = TextEditingController();
//   var genderController = TextEditingController();
//   var nationalIdController = TextEditingController();
//   var dobController = TextEditingController();
//   var bankIdController = TextEditingController();
//   var bankBranchIdController = TextEditingController();
//   var accNoController = TextEditingController();
//   var passportController = TextEditingController();
//   var photoController = TextEditingController();

//   var regionIdController = TextEditingController();
//   var districtIdController = TextEditingController();
//   var wardIdController = TextEditingController();
//   var villageIdController = TextEditingController();
//   var colCenterController = TextEditingController();
//   var nationalityController = TextEditingController();

//   var typeController = TextEditingController();

//   var farmerDeductionController = TextEditingController();

//   List<Map<String, dynamic>> farmerDeductionsList = [];
//   String? _colCenterId;
//   String genderNmae = 'NONE';
//   String maritalStatus = 'NONE';
//   String bankName = 'NONE';
//   String bankBranchName = 'NONE';
//   String accountNumber = 'NONE';
//   String countyName = 'NONE';
//   String districtName = 'NONE';
//   String wardName = 'NONE';
//   String villageName = 'NONE';
//   String colCenterName = 'NONE';
//   String nationalityName = 'NONE';
//   File? image;
//   //Uint8List? profileImageLocal;
//   String passportImageBit64 = '';

//   // Initial Selected Value
//   String dropdownvalue = 'Item 1';

//   final List<Map<String, dynamic>> _allItems = [
//     {"id": 1, "name": "Andy", "age": 29},
//     {"id": 2, "name": "Aragon", "age": 40},
//     {"id": 3, "name": "Bob", "age": 5},
//     {"id": 4, "name": "Barbara", "age": 35},
//     {"id": 5, "name": "Candy", "age": 21},
//     {"id": 6, "name": "Colin", "age": 55},
//     {"id": 7, "name": "Audra", "age": 30},
//     {"id": 8, "name": "Banana", "age": 14},
//     {"id": 9, "name": "Caversky", "age": 100},
//     {"id": 10, "name": "Becky", "age": 32},
//   ];

// //Drop Down Lists Here
//   final List<BankResultElement> _banks = [];
//   final List<BankBranchResultElement> _bankBranches = [];

//   final List<GenderResultElement> _genderTypes = [];
//   final List<MaritalStatusResultElement> _maritalStatus = [];
//   final List<CountyResultElement> _counties = [];
//   final List<DistrictResultElement> _districts = [];
//   final List<WardResultElement> _ward = [];
//   final List<VillageResultElement> _village = [];
//   final List<CountyResultElement> _colCenter1 = [];
//   final List<NationalityResultElement> _nationalities = [];
//   final List<ResultElementDeduct> _deductions = [];
//   //List<dynamic> farmerDeductionsList = [];

// // Selected Dropdown elements
//   BankResultElement? selectedBank;
//   BankBranchResultElement? selectedBankBranch;

//   GenderResultElement? selectedGender;
//   MaritalStatusResultElement? selectedMaritalStatus;
//   CountyResultElement? selectedCounty;
//   DistrictResultElement? selectedDistrict;
//   WardResultElement? selectedWard;
//   VillageResultElement? selectedVillage;
//   // ? selectedColCenter;
//   NationalityResultElement? selectedNationality;
//   ResultElementDeduct? selectedDeductions;

//   // String For Names
  
//   /// for date picker*
//   DateTime date = DateTime.now();
//   Future<void> selectedTimePicker(BuildContext context) async {
//     final DateTime? picked = await showDatePicker(
//         context: context,
//         initialDate: date,
//         firstDate: DateTime(1960),
//         lastDate: DateTime(2024));

//     if (picked != null && picked != date) {
//       setState(() {
//         date = picked;
//         String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
//         dobController.text = formattedDate;

//         print(date.toString());
//       });
//     }
//   }

//   FarmerModelDB modelDB = FarmerModelDB();
//   DeductionsResultElement deduction = DeductionsResultElement();

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();

//     _getSingleFarmerLocal(pageId);
//   }

//   bool isLoading = false;
//   List<dynamic> _allFarmersLocal = [];

//   Future _getSingleFarmerLocal(String memberNO) async {
//     setState(() => isLoading = true);
//     print("At Function");
//     _allFarmersLocal = await FarmersController().getSingleFarmerLocal(memberNO);

//     if (_allFarmersLocal.isNotEmpty) {
//       print(_allFarmersLocal[0]["full_name"]!.toString());
//       fullNameController.text = _allFarmersLocal[0]["full_name"]!.toString();
//       middleNameController.text =
//           (_allFarmersLocal[0]["middle_name"] ?? '').toString();
//       lastNameController.text =
//           (_allFarmersLocal[0]["last_name"] ?? '').toString();
//       nationalIdController.text = _allFarmersLocal[0]["nat_id"]!.toString();
//       //nationalityController.text = _allFarmersLocal[0]["nationality"]!.toString();
//       phoneNoController.text = _allFarmersLocal[0]["phone1"]!.toString();
//       genderController.text =
//           (_allFarmersLocal[0]["gender_type_id"] ?? 0).toString();
//       regionIdController.text =
//           (_allFarmersLocal[0]["regionId"] ?? 0).toString();
//       districtIdController.text =
//           (_allFarmersLocal[0]["districtId"] ?? 0).toString();
//       wardIdController.text = (_allFarmersLocal[0]["wardId"] ?? 0).toString();
//       villageIdController.text =
//           (_allFarmersLocal[0]["villageId"] ?? 0).toString();
//       nationalityController.text =
//           (_allFarmersLocal[0]["nationalityId"] ?? 0).toString();
//       bankIdController.text =
//           (_allFarmersLocal[0]["bankdetailsId"] ?? 0).toString();
//       bankBranchIdController.text =
//           (_allFarmersLocal[0]["bankbranchId"] ?? 0).toString();
//       accNoController.text =
//           (_allFarmersLocal[0]["accNo"] ?? accountNumber).toString();

//       maritalStatusController.text =
//           (_allFarmersLocal[0]["marital_status"] ?? 0).toString();
//       // photoController.text =
//       //     (_allFarmersLocal[0]["photoString"] ?? "none").toString();
//       //farmerEmailController.text = _allFarmersLocal[0]["email"].toString() ?? "email";
//       //farmerGenderController.text = _allFarmersLocal[0]["gender_type_id"]!.toString();

//       // print("Info here is" +
//       //     (_allFarmersLocal[0]["accNo"] ?? accountNumber).toString());

//       await _getDetailNames();
//       await _getLocationDetails();

//       setState(() => isLoading = false);

//       //print("List of Users: " + userModelList.length.toString());
//     } else {
//       print("Did not Get");
//     }
//   }

//   _getDetailNames() async {
//     if (genderController.text.toString() != '0') {
//       List<dynamic> genderList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'gendertypes', 'Id', genderController.text.toString());

//       if (res != null) {
//         genderList = res;
//         genderNmae = genderList[0]['name'].toString();
//         print("GetDetails Gender: ${genderList[0]['name'].toString()}");
//       }
//     }
//     if (regionIdController.text.toString() != '0') {
//       List<dynamic> regionList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'counties', 'Id', regionIdController.text.toString());

//       if (res != null) {
//         regionList = res;
//         countyName = regionList[0]['name'].toString();
//         //selectedCounty = CountyResultElement.fromJson(regionList[0]);
//         print("GetDetails Region: ${regionList[0]['name'].toString()}");
//       }
//     }
//     if (districtIdController.text.toString() != '0') {
//       List<dynamic> districtList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'districts', 'Id', districtIdController.text.toString());

//       if (res != null) {
//         districtList = res;
//         districtName = districtList[0]['name'].toString();
//         print("GetDetails District: ${districtList[0]['name'].toString()}");
//       }
//     }
//     if (wardIdController.text.toString() != '0') {
//       List<dynamic> wardList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'wards', 'Id', wardIdController.text.toString());

//       if (res != null) {
//         wardList = res;
//         wardName = wardList[0]['name'].toString();
//         print("GetDetails Ward: ${wardList[0]['name'].toString()}");
//       }
//     }
//     if (villageIdController.text.toString() != '0') {
//       List<dynamic> villageList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'village', 'Id', villageIdController.text.toString());

//       if (res != null) {
//         villageList = res;
//         villageName = villageList[0]['name'].toString();
//         print("GetDetails Village: ${villageList[0]['name'].toString()}");
//       }
//     }
//     if (nationalityController.text.toString() != '0' ||
//         nationalityController.text.isEmpty) {
//       List<dynamic> nationalityList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'nationalities', 'Id', nationalityController.text.toString());

//       if (res != null) {
//         nationalityList = res;
//         nationalityName = nationalityList[0]['name'].toString();
//         print(
//             "GetDetails Nationality: ${nationalityController.text.toString()}");
//       }
//     }
//     if (bankIdController.text.toString() != '0') {
//       List<dynamic> bankList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'banks', 'Id', bankIdController.text.toString());

//       if (res != null) {
//         bankList = res;
//         bankName = bankList[0]['name'].toString();
//         print("GetDetails Bank: ${bankIdController.text.toString()}");
//       }
//     }
//     if (bankBranchIdController.text.toString() != '0') {
//       List<dynamic> bankBranchList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'bankbranches', 'Id', bankBranchIdController.text.toString());

//       if (res != null) {
//         bankBranchList = res;
//         bankBranchName = bankBranchList[0]['name'].toString();
//         print("GetDetails Bank: ${bankBranchIdController.text.toString()}");
//       }
//     }
//     if (maritalStatusController.text.toString() != '0') {
//       List<dynamic> maritalStatusList = [];
//       var res = await LocationController().getLocationsLocalById(
//           'maritalstatus', 'Id', maritalStatusController.text.toString());

//       if (res != null) {
//         maritalStatusList = res;
//         maritalStatus = maritalStatusList[0]['name'].toString();
//         print(
//             "GetDetails MaritalStatus: ${bankBranchIdController.text.toString()}");
//       }
//     }
//     // if (photoController.text.toString() != "none") {
//     //   var _decodedImage = base64Decode(photoController.text.toString());
//     //   profileImageLocal = _decodedImage;
//     // }

//     setState(() {});
//   }

//   _getLocationDetails() async {
//     List<dynamic> resGender = [];
//     var res = await LocationController().getLocationsLocal(genderTypeTable);
//     //print("Hello there"+ res[0].toString());
//     if (res != null) {
//       resGender = res;
//       for (var i in resGender) {
//         GenderResultElement model = GenderResultElement(
//             resultId: i['Id'],
//             code: i['code'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _genderTypes.add(model);
//       }
//     }

//     //selectedGender = _genderTypes[0];

//     List<dynamic> resMaritalStatus = [];
//     var res1 = await LocationController().getLocationsLocal(maritalStatusTable);
//     //print("Hello there"+ res[0].toString());
//     if (res1 != null) {
//       resMaritalStatus = res1;
//       for (var i in resMaritalStatus) {
//         MaritalStatusResultElement model = MaritalStatusResultElement(
//             resultId: i['Id'],
//             code: i['code'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _maritalStatus.add(model);
//       }
//     }

//     //selectedMaritalStatus = _maritalStatus[0];

//     List<dynamic> resCounty = [];
//     var res2 = await LocationController().getLocationsLocal(countiesTable);
//     //print("Hello there"+ res[0].toString());
//     if (res2 != null) {
//       resCounty = res2;
//       for (var i in resCounty) {
//         CountyResultElement model = CountyResultElement(
//             resultId: i['Id'],
//             colorCode: i['color_code'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _counties.add(model);
//       }
//     }

//     //selectedCounty = _counties[0];
//     List<dynamic> resDistrict = [];
//     var res3 = await LocationController().getLocationsLocal(districtTable);
//     //print("Hello there"+ res[0].toString());
//     if (res3 != null) {
//       resDistrict = res3;
//       for (var i in resDistrict) {
//         DistrictResultElement model = DistrictResultElement(
//             resultId: i['Id'],
//             colorCode: i['color_code'],
//             name: i['name'],
//             regionId: i['region_id'],
//             datecomparer: i['datecomparer']);

//         _districts.add(model);
//       }
//     }

//     //selectedDistrict = _districts[0];
//     List<dynamic> resWard = [];
//     var res4 = await LocationController().getLocationsLocal(wardTable);
//     //print("Hello there"+ res[0].toString());
//     if (res4 != null) {
//       resWard = res4;
//       for (var i in resWard) {
//         WardResultElement model = WardResultElement(
//             resultId: i['Id'],
//             colorCode: i['color_code'],
//             name: i['name'],
//             districtId: i['districtId'],
//             datecomparer: i['datecomparer']);

//         _ward.add(model);
//       }
//     }

//     //selectedWard = _ward[0];
//     List<dynamic> resVillage = [];
//     var res5 = await LocationController().getLocationsLocal(villageTable);
//     //print("Hello there"+ res[0].toString());
//     if (res5 != null) {
//       resVillage = res5;
//       for (var i in resVillage) {
//         VillageResultElement model = VillageResultElement(
//             resultId: i['Id'],
//             colorCode: i['color_code'],
//             name: i['name'],
//             wardId: i['wardId'],
//             datecomparer: i['datecomparer']);

//         _village.add(model);
//       }
//     }

//     List<dynamic> resBanks = [];
//     var res6 = await LocationController().getLocationsLocal(banksTable);
//     //print("Hello there"+ res[0].toString());
//     if (res6 != null) {
//       resBanks = res6;
//       for (var i in resBanks) {
//         BankResultElement model = BankResultElement(
//             resultId: i['Id'],
//             code: i['color_code'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _banks.add(model);
//       }
//     }

//     //selectedVillage = _village[0];

//     // List<dynamic> resColCenter =
//     //     await LocationController().getLocationsLocal(villageTable);
//     // //print("Hello there"+ res[0].toString());
//     // for (var i in resColCenter) {
//     //   Col model = VillageResultElement(
//     //       resultId: i['Id'],
//     //       colorCode: i['color_code'],
//     //       name: i['name'],
//     //       wardId: i['wardId'],
//     //       datecomparer: i['datecomparer']);

//     //   _village1.add(model);
//     // }

//     // selectedVillage = _village1[0];

//     List<dynamic> resNationality = [];
//     var res8 = await LocationController().getLocationsLocal(nationalityTable);
//     //print("Hello there"+ res[0].toString());
//     if (res8 != null) {
//       resNationality = res8;
//       for (var i in resNationality) {
//         NationalityResultElement model = NationalityResultElement(
//             resultId: i['Id'],
//             code: i['color_code'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _nationalities.add(model);
//       }
//     }

//     //selectedNationality = _nationalities[0];
//     List<dynamic> resDeductions = [];
//     var res9 = await LocationController().getLocationsLocal(deductionsTable);
//     //print("Hello there"+ res[0].toString());
//     if (res9 != null) {
//       resDeductions = res9;
//       for (var i in resDeductions) {
//         ResultElementDeduct model = ResultElementDeduct(
//             resultId: i['Id'],
//             code: i['code'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _deductions.add(model);
//       }
//     }

//     await _getDeductions();
//   }

//   Future _getImage() async {
//     final picker = ImagePicker();

//     var pickedFile =
//         await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

//     if (pickedFile != null) {
//       final bytes = File(pickedFile.path).readAsBytesSync();
//       String img64 = base64Encode(bytes);

//       setState(() {
//         image = File(pickedFile.path);
//         photoController.text = image.toString();
//       });
//     }
//     //String filename = (image.split("/").last).toString();
//     print(image);
//     print(photoController.text.toString());
//     //return image;
//   }

//   _getDeductions() async {
//     try {
//       farmerDeductionsList = await FarmersController()
//           .getFarmerDeductions(_allFarmersLocal[0]["member_no"]!.toString());
//     } catch (e) {
//       print(e.toString());
//     }
//   }

//   _getBankBranches(var bankBranchId) async {
//     List<dynamic> resBankBranch = [];
//     var res7 = await LocationController().getLocationsLocalById(
//         bankBranchesTable, 'bank_details_id', bankBranchId);
//     //print("Hello there"+ res[0].toString());
//     if (res7 != null) {
//       resBankBranch = res7;
//       for (var i in resBankBranch) {
//         BankBranchResultElement model = BankBranchResultElement(
//             resultId: i['Id'],
//             code: i['color_code'],
//             bankDetailsId: i['bank_details_id'],
//             name: i['name'],
//             datecomparer: i['datecomparer']);

//         _bankBranches.add(model);
//       }
//     }
//   }

//   _addDeduction() async {
//     Navigator.pop(context);
//     setState(() => isLoading = true);
//     deduction.code = selectedDeductions!.code;
//     deduction.memberNo = _allFarmersLocal[0]["member_no"]!.toString();
//     deduction.name = selectedDeductions!.name;
//     deduction.resultId = selectedDeductions!.resultId;
//     deduction.datecomparer = selectedDeductions!.datecomparer;
//     List<dynamic> deductions = [];
//     deductions.add(deduction);

//     var i = await FarmersController().checkFarmerDeductions(
//         deduction.memberNo.toString(), deduction.resultId.toString());
//     if (i != null) {
//       print('Already Added');
//       setState(() => isLoading = false);
//     } else {
//       int i =
//           await FarmersController().saveAll(deductions, farmersDeductionsTable);

//       if (i >= 0) {
//         print("Success here");

//         await _getDeductions();
//         setState(() => isLoading = false);
//       }
//     }
//   }

//   _deleteFarmerDeduction(var deductionId) async {
//     setState(() => isLoading = true);
//     var res = await FarmersController().deleteFarmerDeduction(deductionId);
//     await _getDeductions();
//     setState(() => isLoading = false);
//   }

//   _updateFarmer() async {
//     setState(() => isLoading = true);
//     modelDB.syncId = 'p';
//     modelDB.resultId = _allFarmersLocal[0]["Id"];
//     modelDB.fullName = fullNameController.text;
//     modelDB.firstName = fullNameController.text;
//     modelDB.lastName = lastNameController.text;
//     modelDB.middleName = middleNameController.text;
//     modelDB.memberNo = _allFarmersLocal[0]["member_no"]!.toString();
//     modelDB.phone1 = phoneNoController.text;
//     modelDB.dob = dobController.text;
//     modelDB.natId = nationalIdController.text;
//     modelDB.genderTypeId = genderController.text;
//     modelDB.maritalStatus = maritalStatusController.text;
//     modelDB.memberCategoryId =
//         _allFarmersLocal[0]["member_category_id"]!.toString();
//     modelDB.memberSubCategoryId =
//         _allFarmersLocal[0]["member_sub_category_id"]!.toString();
//     modelDB.isActive = _allFarmersLocal[0]["Is_active"] == 1 ? true : false;
//     modelDB.divisionId = _allFarmersLocal[0]["division_id"];
//     modelDB.routeId = _allFarmersLocal[0]["route_id"];
//     modelDB.collectionCentreId = colCenterController.text;
//     modelDB.gangId = _allFarmersLocal[0]["gang_id"];
//     modelDB.departmentId = _allFarmersLocal[0]["department_id"];
//     modelDB.datecomparer = _allFarmersLocal[0]["datecomparer"];
//     modelDB.regionId = regionIdController.text;
//     modelDB.districtId = districtIdController.text;
//     modelDB.wardId = wardIdController.text;
//     modelDB.passportUrl = passportController.text;
//     modelDB.photoString = photoController.text;
//     modelDB.nationalityId = nationalityController.text;
//     modelDB.bankdetailsId = bankIdController.text;
//     modelDB.bankbranchId = bankBranchIdController.text;
//     modelDB.accNo = accNoController.text;
//     modelDB.mainCategoryCode = _allFarmersLocal[0]["main_category_code"];

//     print(" Names: ${modelDB.middleName}${modelDB.lastName}");

//     int i = await FarmersController().updateFarmerLocal(
//         modelDB, _allFarmersLocal[0]["member_no"]!.toString());

//     if (i >= 0) {
//       print("Success here");
//       _getSingleFarmerLocal(pageId);
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () async {
//         try {
//           Get.to(() => const SingleFarmerDashboard(), arguments: pageId);
//           return true; // true allows navigating back
//         } catch (e) {
//           //print('Error when closing the database input window.');
//           return false; // false prevents navigating back
//         }
//       },
//       child: Scaffold(
//         appBar: PreferredSize(
//           preferredSize: Size.fromHeight(Dimensions.height30 * 2),
//           child: AppBar(
//             backgroundColor: Colors.green,
//             actions: [
//               Container(
//                 width: Dimensions.screenWidth,
//                 height: Dimensions.height30 * 2,
//                 padding: EdgeInsets.only(
//                   left: Dimensions.width15,
//                   right: Dimensions.width15,
//                 ),
//                 decoration: BoxDecoration(
//                   color: Colors.green,
//                   boxShadow: [
//                     BoxShadow(
//                         blurRadius: 3,
//                         offset: const Offset(5, 5),
//                         color: AppColors.gradientOne.withOpacity(0.1)),
//                     BoxShadow(
//                         blurRadius: 3,
//                         offset: const Offset(-5, -5),
//                         color: AppColors.gradientOne.withOpacity(0.1))
//                   ],
//                 ),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     IconButton(
//                       icon: Icon(
//                         Icons.arrow_back_ios,
//                         color: Colors.white,
//                         size: Dimensions.iconSize24,
//                       ),
//                       onPressed: () {
//                         Get.to(() => const SingleFarmerDashboard(),
//                             arguments: pageId);
//                       },
//                     ),
//                     BigText(
//                       text: "Edit Farmer Profile",
//                       color: AppColors.textWhite,
//                       size: Dimensions.font20,
//                     ),
//                     const SizedBox()
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//         body: SingleChildScrollView(
//           child: SafeArea(
//             child: isLoading
//                 ? const CustomLoader()
//                 : Container(
//                     color: AppColors.textWhite,
//                     width: Dimensions.screenWidth,
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         SizedBox(
//                           height: Dimensions.height15,
//                         ),
//                         Center(
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Container(
//                                 height: Dimensions.height30 * 2,
//                                 width: Dimensions.height30 * 2,
//                                 decoration: BoxDecoration(
//                                     borderRadius: BorderRadius.circular(
//                                         Dimensions.radius20 / 2),
//                                     color: AppColors.textGrey),
//                                 child: image != null
//                                     ? ClipRRect(
//                                         borderRadius: BorderRadius.circular(
//                                             Dimensions.radius30 * 2),
//                                         child: Image.file(
//                                           image!,
//                                           fit: BoxFit.cover,
//                                         ))
//                                     : const Center(
//                                         child: Text('No Photo'),
//                                       ),
//                               ),
//                               SizedBox(
//                                 height: Dimensions.height10,
//                               ),
//                               Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   BigText(
//                                     text: "Name: ",
//                                     color: AppColors.black,
//                                     size: Dimensions.font12,
//                                   ),
//                                   BigText(
//                                     text: fullNameController.text,
//                                     color: AppColors.mainColor,
//                                     size: Dimensions.font12,
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),
//                         ),
//                         SizedBox(
//                           height: Dimensions.height10,
//                         ),
//                         Container(
//                           child: Center(
//                             child: DataTable(
//                                 border:
//                                     TableBorder.all(color: AppColors.mainColor),
//                                 columns: const [
//                                   DataColumn(
//                                       label: Text('Personal Details',
//                                           style: TextStyle(
//                                               fontSize: 16,
//                                               fontWeight: FontWeight.bold))),
//                                   DataColumn(
//                                       label: Text('Location Details',
//                                           style: TextStyle(
//                                               fontSize: 16,
//                                               fontWeight: FontWeight.bold))),
//                                 ],
//                                 rows: [
//                                   DataRow(cells: [
//                                     DataCell(Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         SmallText(text: "Phone No : "),
//                                         SmallText(
//                                           text:
//                                               phoneNoController.text.toString(),
//                                           color: AppColors.mainColor,
//                                         ),
//                                       ],
//                                     )),
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "County : "),
//                                           SmallText(
//                                             text: countyName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ]),
//                                   DataRow(cells: [
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "Marital Status : "),
//                                           SmallText(
//                                             text: maritalStatus,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "District : "),
//                                           SmallText(
//                                             text: districtName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ]),
//                                   DataRow(cells: [
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "Gender : "),
//                                           SmallText(
//                                             text: genderNmae.toString(),
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "Town : "),
//                                           SmallText(
//                                             text: wardName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ]),
//                                   DataRow(cells: [
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "DOB : "),
//                                           SmallText(
//                                             text: wardName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "Village : "),
//                                           SmallText(
//                                             text: villageName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ]),
//                                   DataRow(cells: [
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(text: "Bank : "),
//                                           SmallText(
//                                             text: bankName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     DataCell(
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           SmallText(
//                                               text: "Collection Center : "),
//                                           SmallText(
//                                             text: colCenterName,
//                                             color: AppColors.mainColor,
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ]),
//                                   DataRow(
//                                     cells: [
//                                       DataCell(
//                                         Row(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.spaceBetween,
//                                           children: [
//                                             SmallText(
//                                               text: "Bank Branch : ",
//                                             ),
//                                             SmallText(
//                                               text: bankBranchName,
//                                               color: AppColors.mainColor,
//                                               size: Dimensions.font12 - 2,
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                       DataCell(
//                                         Row(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.spaceBetween,
//                                           children: [
//                                             SmallText(text: "Nationality : "),
//                                             SmallText(
//                                               text: nationalityName,
//                                               color: AppColors.mainColor,
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                     ],
//                                   ),
//                                   DataRow(
//                                     cells: [
//                                       DataCell(
//                                         Row(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.spaceBetween,
//                                           children: [
//                                             SmallText(text: "Acount No : "),
//                                             SmallText(
//                                               text: accNoController.text
//                                                   .toString(),
//                                               color: AppColors.mainColor,
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                       const DataCell(SizedBox()),
//                                     ],
//                                   ),
//                                 ]),
//                           ),
//                         ),
//                         Container(
//                           width: Dimensions.screenWidth,
//                           padding: EdgeInsets.only(
//                             top: Dimensions.height45,
//                             left: Dimensions.width15,
//                             right: Dimensions.width15,
//                             bottom: Dimensions.height15,
//                           ),
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Center(
//                                 child: BigText(
//                                   text: "Personal Details",
//                                   color: AppColors.mainColor,
//                                   size: Dimensions.font20,
//                                 ),
//                               ),

//                               SizedBox(
//                                 height: Dimensions.height10,
//                               ),

//                               SizedBox(
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     // CustomTextField(
//                                     //   readOnly: false,
//                                     //   controller: fullNameController,
//                                     //   onSaved: (value) {},
//                                     //   labelText: "First Name",
//                                     //   isObscure: false,
//                                     //   onTap: (value) {},
//                                     // ),
//                                     SmallText(
//                                       text: "First Name",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormFields(
//                                       textEditingController: fullNameController,
//                                       inputType: TextInputType.text,
//                                       readOnly: false,
//                                     ),
//                                     // CustomTextField(
//                                     //   readOnly: false,
//                                     //   controller: middleNameController,
//                                     //   onSaved: (value) {},
//                                     //   labelText: "Middle Name",
//                                     //   isObscure: false,
//                                     //   onTap: (value) {},
//                                     // ),
//                                     SmallText(
//                                       text: "Middle Name",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormFields(
//                                       textEditingController:
//                                           middleNameController,
//                                       inputType: TextInputType.text,
//                                     ),
//                                     // CustomTextField(
//                                     //   readOnly: false,
//                                     //   controller: lastNameController,
//                                     //   onSaved: (value) {},
//                                     //   labelText: "Last Name",
//                                     //   isObscure: false,
//                                     //   onTap: (value) {},
//                                     // ),

//                                     SmallText(
//                                       text: "Last Name",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormFields(
//                                       textEditingController: lastNameController,
//                                       inputType: TextInputType.text,
//                                     ),

//                                     // CustomTextField(
//                                     //   readOnly: false,
//                                     //   controller: nationalIdController,
//                                     //   onSaved: (value) {},
//                                     //   labelText: "ID Number",
//                                     //   isObscure: false,
//                                     //   onTap: (value) {},
//                                     // ),

//                                     SmallText(
//                                       text: "ID Number",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormFields(
//                                       textEditingController:
//                                           nationalIdController,
//                                       inputType: TextInputType.text,
//                                     ),

//                                     // CustomTextField(
//                                     //   readOnly: false,
//                                     //   controller: phoneNoController,
//                                     //   onSaved: (value) {},
//                                     //   labelText: "Phone No",
//                                     //   isObscure: false,
//                                     //   onTap: (value) {},
//                                     // ),

//                                     SmallText(
//                                       text: "Phone No",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormFields(
//                                       textEditingController: phoneNoController,
//                                       inputType: TextInputType.text,
//                                     ),

//                                     SizedBox(
//                                       height: Dimensions.height15,
//                                     ),

//                                     SmallText(
//                                       text: "Marital Status",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),

//                                     FormField<MaritalStatusResultElement>(
//                                       builder: (FormFieldState<
//                                               MaritalStatusResultElement>
//                                           state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintText: "Marital Status",
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 MaritalStatusResultElement>(
//                                               value: selectedMaritalStatus,
//                                               isDense: true,
//                                               onChanged:
//                                                   (MaritalStatusResultElement?
//                                                       newValue) async {
//                                                 setState(() {
//                                                   selectedMaritalStatus =
//                                                       newValue;
//                                                   maritalStatusController.text =
//                                                       selectedMaritalStatus!
//                                                           .resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is Marital status ==*** ${selectedMaritalStatus!.name.toString()} and id === ${selectedMaritalStatus!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _maritalStatus.map(
//                                                   (MaritalStatusResultElement
//                                                       value) {
//                                                 return DropdownMenuItem<
//                                                     MaritalStatusResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       style: const TextStyle(
//                                                           color: Colors.black)),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),

//                                     SizedBox(
//                                       height: Dimensions.height15,
//                                     ),

//                                     SmallText(
//                                       text: "Gender",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),

//                                     FormField<GenderResultElement>(
//                                       builder:
//                                           (FormFieldState<GenderResultElement>
//                                               state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintText: "Gender",
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 GenderResultElement>(
//                                               value: selectedGender,
//                                               isDense: true,
//                                               onChanged: (GenderResultElement?
//                                                   newValue) async {
//                                                 setState(() {
//                                                   selectedGender = newValue;
//                                                   genderController.text =
//                                                       selectedGender!.resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _genderTypes.map(
//                                                   (GenderResultElement value) {
//                                                 return DropdownMenuItem<
//                                                     GenderResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       style: const TextStyle(
//                                                           color: Colors.black)),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),

//                                     // //dob
//                                     SizedBox(
//                                       height: Dimensions.height15,
//                                     ),

//                                     SmallText(
//                                       text: "Date Of Birth",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),

//                                     Container(
//                                       padding: EdgeInsets.only(
//                                           top: Dimensions.height10,
//                                           bottom: Dimensions.height10),
//                                       child: TextField(
//                                         autocorrect: true,
//                                         controller: dobController,
//                                         keyboardType: TextInputType.text,
//                                         readOnly: false,
//                                         onTap: () {
//                                           selectedTimePicker(context);
//                                         },
//                                         decoration: InputDecoration(
//                                           suffixIcon:
//                                               const Icon(Icons.calendar_month),
//                                           hintStyle: const TextStyle(
//                                               color: Colors.grey),
//                                           filled: true,
//                                           fillColor: Colors.white70,
//                                           enabledBorder: OutlineInputBorder(
//                                             borderRadius: BorderRadius.all(
//                                                 Radius.circular(
//                                                     Dimensions.radius15 - 5)),
//                                             borderSide: const BorderSide(
//                                                 color: Colors.grey, width: 2),
//                                           ),
//                                           focusedBorder: OutlineInputBorder(
//                                             borderRadius: BorderRadius.all(
//                                                 Radius.circular(
//                                                     Dimensions.radius15)),
//                                             borderSide: const BorderSide(
//                                                 color: Colors.lightGreen,
//                                                 width: 2),
//                                           ),
//                                         ),
//                                       ),
//                                     ),

//                                     SizedBox(
//                                       height: Dimensions.height15,
//                                     ),

//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<BankResultElement>(
//                                     //     hint: Text("Bank"),
//                                     //     value: selectedBank,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged:
//                                     //         (BankResultElement? newValue) async {
//                                     //       setState(() {
//                                     //         selectedBank = newValue;
//                                     //         bankIdController.text =
//                                     //             selectedBank!.resultId.toString();
//                                     //         print(
//                                     //             "Here is Bank ==*** ${selectedBank!.name.toString()} and id === ${selectedBank!.resultId.toString()}");
//                                     //       });
//                                     //       await _getBankBranches(
//                                     //           selectedBank!.resultId.toString());
//                                     //     },
//                                     //     items:
//                                     //         _banks.map((BankResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           BankResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),

//                                     SmallText(
//                                       text: "Bank",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),

//                                     FormField<BankResultElement>(
//                                       builder:
//                                           (FormFieldState<BankResultElement>
//                                               state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 BankResultElement>(
//                                               value: selectedBank,
//                                               isDense: true,
//                                               onChanged: (BankResultElement?
//                                                   newValue) async {
//                                                 setState(() {
//                                                   selectedBank = newValue;
//                                                   bankIdController.text =
//                                                       selectedBank!.resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is Bank ==*** ${selectedBank!.name.toString()} and id === ${selectedBank!.resultId.toString()}");
//                                                 });
//                                                 await _getBankBranches(
//                                                     selectedBank!.resultId
//                                                         .toString());
//                                               },
//                                               items: _banks.map(
//                                                   (BankResultElement value) {
//                                                 return DropdownMenuItem<
//                                                     BankResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       style: const TextStyle(
//                                                           color: Colors.black)),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),

//                                     SizedBox(
//                                       height: Dimensions.height15,
//                                     ),

//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<BankBranchResultElement>(
//                                     //     hint: Text("Bank Branch"),
//                                     //     value: selectedBankBranch,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged: (BankBranchResultElement?
//                                     //         newValue) async {
//                                     //       setState(() {
//                                     //         selectedBankBranch = newValue;
//                                     //         bankBranchIdController.text =
//                                     //             selectedBankBranch!.resultId
//                                     //                 .toString();
//                                     //         print(
//                                     //             "Here is Bank Branch ==*** ${selectedBankBranch!.name.toString()} and id === ${selectedBankBranch!.resultId.toString()}");
//                                     //       });
//                                     //     },
//                                     //     items: _bankBranches
//                                     //         .map((BankBranchResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           BankBranchResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),

//                                     SmallText(
//                                       text: "Bank Branch",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),

//                                     FormField<BankBranchResultElement>(
//                                       builder: (FormFieldState<
//                                               BankBranchResultElement>
//                                           state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 BankBranchResultElement>(
//                                               value: selectedBankBranch,
//                                               isDense: true,
//                                               onChanged:
//                                                   (BankBranchResultElement?
//                                                       newValue) async {
//                                                 setState(() {
//                                                   selectedBankBranch = newValue;
//                                                   bankBranchIdController.text =
//                                                       selectedBankBranch!
//                                                           .resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is Bank Branch ==*** ${selectedBankBranch!.name.toString()} and id === ${selectedBankBranch!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _bankBranches.map(
//                                                   (BankBranchResultElement
//                                                       value) {
//                                                 return DropdownMenuItem<
//                                                     BankBranchResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       style: const TextStyle(
//                                                           color: Colors.black)),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),

//                                     SizedBox(
//                                       height: Dimensions.height15,
//                                     ),

//                                     SmallText(
//                                       text: "Account Number",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormFields(
//                                       textEditingController: accNoController,
//                                       inputType: TextInputType.text,
//                                     ),

//                                     SizedBox(
//                                       height: Dimensions.height10,
//                                     ),
//                                     Center(
//                                       child: Column(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.center,
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.center,
//                                         children: [
//                                           Container(
//                                             height: Dimensions.height10 *
//                                                 Dimensions.height20,
//                                             width: Dimensions.screenWidth / 2,
//                                             decoration: BoxDecoration(
//                                                 borderRadius:
//                                                     BorderRadius.circular(
//                                                         Dimensions.radius20 /
//                                                             2),
//                                                 color: AppColors.textGrey),
//                                             child: image != null
//                                                 ? ClipRRect(
//                                                     borderRadius:
//                                                         BorderRadius.circular(
//                                                             Dimensions
//                                                                 .radius15),
//                                                     child: Image.file(image!,
//                                                         fit: BoxFit.cover),
//                                                   )
//                                                 : const Center(
//                                                     child: Text(
//                                                         'Please add a photo'),
//                                                   ),
//                                           ),
//                                           SizedBox(
//                                             width: Dimensions.width30,
//                                           ),
//                                           MaterialButton(
//                                             color: Colors.blue,
//                                             onPressed: _getImage,
//                                             child: Text(
//                                               "Take a Photo",
//                                               style: TextStyle(
//                                                   color: AppColors.textWhite,
//                                                   fontWeight: FontWeight.bold),
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),

//                               SizedBox(
//                                 height: Dimensions.height45,
//                               ),

//                               // LOCATION DETAILS
//                               SizedBox(
//                                 height: Dimensions.height30,
//                               ),

//                               Center(
//                                 child: BigText(
//                                   text: "Location Details",
//                                   color: AppColors.mainColor,
//                                   size: Dimensions.font20,
//                                 ),
//                               ),

//                               SizedBox(
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     SizedBox(
//                                       height: Dimensions.height30,
//                                     ),
//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<CountyResultElement>(
//                                     //     hint: Text("County"),
//                                     //     value: selectedCounty,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged: (CountyResultElement?
//                                     //         newValue) async {
//                                     //       setState(() {
//                                     //         selectedCounty = newValue;
//                                     //         regionIdController.text =
//                                     //             selectedCounty!.resultId
//                                     //                 .toString();
//                                     //         print(
//                                     //             "Here is County ==*** ${selectedCounty!.name.toString()} and id === ${selectedCounty!.resultId.toString()}");
//                                     //       });
//                                     //     },
//                                     //     items: _counties
//                                     //         .map((CountyResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           CountyResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),

//                                     SmallText(
//                                       text: "Country",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormField<NationalityResultElement>(
//                                       builder: (FormFieldState<
//                                               NationalityResultElement>
//                                           state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             //hintStyle: TextStyle(color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 NationalityResultElement>(
//                                               value: selectedNationality,
//                                               isExpanded: true,
//                                               onChanged:
//                                                   (NationalityResultElement?
//                                                       newValue) async {
//                                                 setState(() {
//                                                   selectedNationality =
//                                                       newValue;
//                                                   nationalityController.text =
//                                                       selectedNationality!
//                                                           .resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is Nationality ==*** ${selectedNationality!.name.toString()} and id === ${selectedNationality!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _nationalities.map(
//                                                   (NationalityResultElement
//                                                       value) {
//                                                 return DropdownMenuItem<
//                                                     NationalityResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       overflow:
//                                                           TextOverflow.ellipsis,
//                                                       style: const TextStyle(
//                                                         color: Colors.black,
//                                                       )),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),
//                                     SizedBox(
//                                       height: Dimensions.height20,
//                                     ),

//                                     SmallText(
//                                       text: "County",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormField<CountyResultElement>(
//                                       builder:
//                                           (FormFieldState<CountyResultElement>
//                                               state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 CountyResultElement>(
//                                               value: selectedCounty,
//                                               isDense: true,
//                                               onChanged: (CountyResultElement?
//                                                   newValue) async {
//                                                 setState(() {
//                                                   selectedCounty = newValue;
//                                                   regionIdController.text =
//                                                       selectedCounty!.resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is County ==*** ${selectedCounty!.name.toString()} and id === ${selectedCounty!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _counties.map(
//                                                   (CountyResultElement value) {
//                                                 return DropdownMenuItem<
//                                                     CountyResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                     value.name.toString(),
//                                                     overflow:
//                                                         TextOverflow.ellipsis,
//                                                     style: const TextStyle(
//                                                         color: Colors.black),
//                                                   ),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),
//                                     SizedBox(
//                                       height: Dimensions.height20,
//                                     ),

//                                     SmallText(
//                                       text: "District",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormField<DistrictResultElement>(
//                                       builder:
//                                           (FormFieldState<DistrictResultElement>
//                                               state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 DistrictResultElement>(
//                                               value: selectedDistrict,
//                                               isDense: true,
//                                               onChanged: (DistrictResultElement?
//                                                   newValue) async {
//                                                 setState(() {
//                                                   selectedDistrict = newValue;
//                                                   districtIdController.text =
//                                                       selectedDistrict!.resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is District ==*** ${selectedDistrict!.name.toString()} and id === ${selectedDistrict!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _districts.map(
//                                                   (DistrictResultElement
//                                                       value) {
//                                                 return DropdownMenuItem<
//                                                     DistrictResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       overflow:
//                                                           TextOverflow.ellipsis,
//                                                       style: const TextStyle(
//                                                           color: Colors.black)),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),
//                                     SizedBox(
//                                       height: Dimensions.height20,
//                                     ),

//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child:
//                                     //       DropdownButton<DistrictResultElement>(
//                                     //     hint: Text("District"),
//                                     //     value: selectedDistrict,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged: (DistrictResultElement?
//                                     //         newValue) async {
//                                     //       setState(() {
//                                     //         selectedDistrict = newValue;
//                                     //         districtIdController.text =
//                                     //             selectedDistrict!.resultId
//                                     //                 .toString();
//                                     //         print(
//                                     //             "Here is District ==*** ${selectedDistrict!.name.toString()} and id === ${selectedDistrict!.resultId.toString()}");
//                                     //       });
//                                     //     },
//                                     //     items: _districts
//                                     //         .map((DistrictResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           DistrictResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),
//                                     // SizedBox(
//                                     //   height: Dimensions.height20,
//                                     // ),
//                                     SmallText(
//                                       text: "Town",
//                                       color: Colors.grey,
//                                       size: Dimensions.font16 - 2,
//                                     ),
//                                     FormField<WardResultElement>(
//                                       builder:
//                                           (FormFieldState<WardResultElement>
//                                               state) {
//                                         return InputDecorator(
//                                           decoration: InputDecoration(
//                                             hintStyle: TextStyle(
//                                                 color: AppColors.black),
//                                             filled: true,
//                                             fillColor: Colors.white70,
//                                             enabledBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15 - 5)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.grey, width: 2),
//                                             ),
//                                             focusedBorder: OutlineInputBorder(
//                                               borderRadius: BorderRadius.all(
//                                                   Radius.circular(
//                                                       Dimensions.radius15)),
//                                               borderSide: const BorderSide(
//                                                   color: Colors.lightGreen,
//                                                   width: 2),
//                                             ),
//                                           ),
//                                           //isEmpty: _currentSelectedValue == '',
//                                           child: DropdownButtonHideUnderline(
//                                             child: DropdownButton<
//                                                 WardResultElement>(
//                                               value: selectedWard,
//                                               isDense: true,
//                                               onChanged: (WardResultElement?
//                                                   newValue) async {
//                                                 setState(() {
//                                                   selectedWard = newValue;
//                                                   wardIdController.text =
//                                                       selectedWard!.resultId
//                                                           .toString();
//                                                   print(
//                                                       "Here is Ward ==*** ${selectedWard!.name.toString()} and id === ${selectedWard!.resultId.toString()}");
//                                                 });
//                                               },
//                                               items: _ward.map(
//                                                   (WardResultElement value) {
//                                                 return DropdownMenuItem<
//                                                     WardResultElement>(
//                                                   value: value,
//                                                   child: Text(
//                                                       value.name.toString(),
//                                                       overflow:
//                                                           TextOverflow.ellipsis,
//                                                       style: const TextStyle(
//                                                           color: Colors.black)),
//                                                 );
//                                               }).toList(),
//                                             ),
//                                           ),
//                                         );
//                                       },
//                                     ),
//                                     SizedBox(
//                                       height: Dimensions.height20,
//                                     ),

//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<WardResultElement>(
//                                     //     hint: Text("Town"),
//                                     //     value: selectedWard,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged:
//                                     //         (WardResultElement? newValue) async {
//                                     //       setState(() {
//                                     //         selectedWard = newValue;
//                                     //         wardIdController.text =
//                                     //             selectedWard!.resultId.toString();
//                                     //         print(
//                                     //             "Here is Ward ==*** ${selectedWard!.name.toString()} and id === ${selectedWard!.resultId.toString()}");
//                                     //       });
//                                     //     },
//                                     //     items:
//                                     //         _ward.map((WardResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           WardResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),
//                                     // SizedBox(
//                                     //   height: Dimensions.height20,
//                                     // ),
//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<VillageResultElement>(
//                                     //     hint: Text("Village"),
//                                     //     value: selectedVillage,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged: (VillageResultElement?
//                                     //         newValue) async {
//                                     //       setState(() {
//                                     //         selectedVillage = newValue;
//                                     //         print(
//                                     //             "Here is Village ==*** ${selectedVillage!.name.toString()} and id === ${selectedVillage!.resultId.toString()}");
//                                     //       });
//                                     //     },
//                                     //     items: _village
//                                     //         .map((VillageResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           VillageResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),
//                                     // SizedBox(
//                                     //   height: Dimensions.height20,
//                                     // ),
//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<String>(
//                                     //     hint: Text("Collection Center"),
//                                     //     value: _colCenterId,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged: (newValue) async {
//                                     //       setState(() {
//                                     //         _colCenterId = newValue;
//                                     //         //colCenterController.text =
//                                     //       });
//                                     //     },
//                                     //     items: _allItems.map((value) {
//                                     //       return DropdownMenuItem<String>(
//                                     //         value: value['name'].trim(),
//                                     //         child: Text(value['name'].trim(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),
//                                     // SizedBox(
//                                     //   height: Dimensions.height20,
//                                     // ),
//                                     // Container(
//                                     //   margin: const EdgeInsets.all(10.0),
//                                     //   child: DropdownButton<
//                                     //       NationalityResultElement>(
//                                     //     hint: Text("Nationality"),
//                                     //     value: selectedNationality,
//                                     //     isExpanded: true,
//                                     //     isDense: true,
//                                     //     dropdownColor: Colors.white,
//                                     //     icon: Icon(Icons.arrow_drop_down),
//                                     //     style: TextStyle(
//                                     //         color: Colors.black, fontSize: 16),
//                                     //     onChanged: (NationalityResultElement?
//                                     //         newValue) async {
//                                     //       setState(() {
//                                     //         selectedNationality = newValue;
//                                     //         nationalityController.text =
//                                     //             selectedNationality!.resultId
//                                     //                 .toString();
//                                     //         print(
//                                     //             "Here is Nationality ==*** ${selectedNationality!.name.toString()} and id === ${selectedNationality!.resultId.toString()}");
//                                     //       });
//                                     //     },
//                                     //     items: _nationalities.map(
//                                     //         (NationalityResultElement value) {
//                                     //       return DropdownMenuItem<
//                                     //           NationalityResultElement>(
//                                     //         value: value,
//                                     //         child: Text(value.name.toString(),
//                                     //             style: TextStyle(
//                                     //                 color: Colors.black)),
//                                     //       );
//                                     //     }).toList(),
//                                     //   ),
//                                     // ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                         SizedBox(
//                           height: Dimensions.height30,
//                         ),
//                         Center(
//                           child: BigText(
//                             text: "Farmer Deductions",
//                             color: AppColors.mainColor,
//                             size: Dimensions.font20,
//                           ),
//                         ),
//                         Container(
//                           child: Column(
//                             children: [
//                               //open add family dialog
//                               Align(
//                                 alignment: Alignment.topRight,
//                                 child: GestureDetector(
//                                   child: Container(
//                                     width: Dimensions.width20 *
//                                         (Dimensions.width10 / 1.5),
//                                     margin: EdgeInsets.only(
//                                       right: Dimensions.width10,
//                                     ),
//                                     padding: EdgeInsets.only(
//                                       top: Dimensions.height15,
//                                       bottom: Dimensions.height15,
//                                       left: Dimensions.width30,
//                                       right: Dimensions.width30,
//                                     ),
//                                     decoration: BoxDecoration(
//                                       color: AppColors.subColor,
//                                       borderRadius: BorderRadius.circular(
//                                           Dimensions.radius15 / 3),
//                                     ),
//                                     child: Center(
//                                       child: SmallText(
//                                         text: "+ ADD",
//                                         color: AppColors.textWhite,
//                                         size: Dimensions.font12,
//                                       ),
//                                     ),
//                                   ),
//                                   onTap: () {
//                                     openFarmerDeductionsDialog();
//                                   },
//                                 ),
//                               ),

//                               farmerDeductionsList.isEmpty
//                                   ? Container()
//                                   : Container(
//                                       width: MediaQuery.of(context).size.width,
//                                       margin: const EdgeInsets.all(10),
//                                       decoration: BoxDecoration(
//                                         borderRadius:
//                                             BorderRadius.circular(5.0),
//                                         color: Colors.grey,
//                                       ),
//                                       child: Column(
//                                         children: [
//                                           const SizedBox(
//                                             height: 5,
//                                           ),
//                                           const Text(
//                                             "Farmer Deductions",
//                                             style: TextStyle(
//                                                 color: Colors.white,
//                                                 fontSize: 16),
//                                           ),
//                                           const SizedBox(
//                                             height: 15,
//                                           ),
//                                           Column(
//                                             children: [
//                                               ListView.builder(
//                                                 physics:
//                                                     const NeverScrollableScrollPhysics(),
//                                                 shrinkWrap: true,
//                                                 itemCount:
//                                                     farmerDeductionsList.length,
//                                                 itemBuilder: (context, index) {
//                                                   return ListTile(
//                                                     title: Column(
//                                                       crossAxisAlignment:
//                                                           CrossAxisAlignment
//                                                               .start,
//                                                       children: [
//                                                         Row(
//                                                           mainAxisAlignment:
//                                                               MainAxisAlignment
//                                                                   .spaceEvenly,
//                                                           children: [
//                                                             Text(
//                                                               "NAME: ${farmerDeductionsList[index]['name']}",
//                                                               style: const TextStyle(
//                                                                   color: Colors
//                                                                       .black,
//                                                                   fontSize: 13,
//                                                                   fontWeight:
//                                                                       FontWeight
//                                                                           .normal),
//                                                             ),
//                                                             GestureDetector(
//                                                               onTap: () {
//                                                                 print(farmerDeductionsList[
//                                                                             index]
//                                                                         ['_Id']
//                                                                     .toString());
//                                                                 if (farmerDeductionsList[
//                                                                             index]
//                                                                         [
//                                                                         '_Id'] !=
//                                                                     null) {
//                                                                   _deleteFarmerDeduction(
//                                                                       farmerDeductionsList[
//                                                                               index]
//                                                                           [
//                                                                           '_Id']);
//                                                                 }
//                                                               },
//                                                               child: Icon(
//                                                                 Icons.delete,
//                                                                 color: Colors
//                                                                     .redAccent,
//                                                                 size: Dimensions
//                                                                     .iconSize24,
//                                                               ),
//                                                             )
//                                                           ],
//                                                         ),
//                                                         const Divider(
//                                                           height: 0.5,
//                                                           color: Colors.black,
//                                                         ),
//                                                       ],
//                                                     ),
//                                                   );
//                                                 },
//                                               ),
//                                             ],
//                                           ),
//                                         ],
//                                       )),
//                             ],
//                           ),
//                         ),
//                         SizedBox(
//                           height: Dimensions.height30,
//                         ),
//                         Center(
//                           child: GestureDetector(
//                             onTap: () {
//                               _updateFarmer();
//                             },
//                             child: Container(
//                               height: Dimensions.height30 * 2,
//                               width: Dimensions.width30 * Dimensions.width10,
//                               decoration: BoxDecoration(
//                                 color: Colors.green,
//                                 borderRadius:
//                                     BorderRadius.circular(Dimensions.radius30),
//                               ),
//                               child: Center(
//                                 child: BigText(
//                                   text: "Submit",
//                                   color: AppColors.textWhite,
//                                   size: Dimensions.font16,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         SizedBox(
//                           height: Dimensions.height30,
//                         ),
//                       ],
//                     ),
//                   ),
//           ),
//         ),
//       ),
//     );
//   }

//   /// crops cultivated in area dialog*
//   openFarmerDeductionsDialog() async {
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return StatefulBuilder(
//             builder: (BuildContext context, StateSetter setState) {
//               return SimpleDialog(
//                 title: BigText(
//                   text: "Add Farmer Deduction",
//                   color: AppColors.black,
//                   size: Dimensions.font16,
//                 ),
//                 children: [
//                   SimpleDialogOption(
//                     child: Container(
//                       margin: const EdgeInsets.all(10.0),
//                       child: SizedBox(
//                         width: Dimensions.screenWidth,
//                         child: DropdownButton<ResultElementDeduct>(
//                           hint: const Text("Deduction"),
//                           value: selectedDeductions,
//                           isExpanded: true,
//                           isDense: true,
//                           dropdownColor: Colors.white,
//                           icon: const Icon(Icons.arrow_drop_down),
//                           style: const TextStyle(
//                               color: Colors.black, fontSize: 16),
//                           onChanged: (ResultElementDeduct? newValue) async {
//                             setState(() {
//                               selectedDeductions = newValue;
//                               print(
//                                   "Here is Deduction ==*** ${selectedDeductions!.name.toString()} and id === ${selectedDeductions!.resultId.toString()}");
//                             });
//                           },
//                           items: _deductions.map((ResultElementDeduct value) {
//                             return DropdownMenuItem<ResultElementDeduct>(
//                               value: value,
//                               child: Text(value.name.toString(),
//                                   style: const TextStyle(color: Colors.black)),
//                             );
//                           }).toList(),
//                         ),
//                       ),
//                     ),
//                   ),
//                   SimpleDialogOption(
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: [
//                         GestureDetector(
//                           child: Container(
//                             width: 100,
//                             padding: const EdgeInsets.only(
//                                 top: 13, bottom: 13, left: 25, right: 25),
//                             decoration: BoxDecoration(
//                                 color: AppColors.subColor,
//                                 borderRadius: BorderRadius.circular(25)),
//                             child: Center(
//                               child: Text("save".toUpperCase(),
//                                   style: const TextStyle(
//                                       fontSize: 12, color: Colors.white)),
//                             ),
//                           ),
//                           onTap: () {
//                             _addDeduction();
//                           },
//                         ),
//                         GestureDetector(
//                           child: Container(
//                             width: 100,
//                             padding: const EdgeInsets.only(
//                                 top: 13, bottom: 13, left: 25, right: 25),
//                             decoration: BoxDecoration(
//                                 color: Colors.redAccent,
//                                 borderRadius: BorderRadius.circular(25)),
//                             child: Center(
//                               child: Text("cancel".toUpperCase(),
//                                   style: const TextStyle(
//                                       fontSize: 12, color: Colors.white)),
//                             ),
//                           ),
//                           onTap: () {
//                             Navigator.pop(context);
//                           },
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               );
//             },
//           );
//         });
//   }
// }
