import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../controllers/location/location_controller.dart';
import '../../../../models/additional_info/nextofkin_model.dart';
import '../../../../models/farmers/education_model.dart';
import '../../../../models/farmers/gender_type_model.dart';
import '../../../../models/farmers/location/county_model.dart';
import '../../../../models/farmers/marital_status.dart';
import '../../../../models/farmers/relations_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/form_field.dart';
import 'additionalinfo_dashboard.dart';

class NextOfKinScreen extends StatefulWidget {
  const NextOfKinScreen({super.key});

  @override
  State<NextOfKinScreen> createState() => _NextOfKinScreenState();
}

class _NextOfKinScreenState extends State<NextOfKinScreen> {
  var pageId = Get.arguments;
  var farmerNameController = TextEditingController();
  var farmerMemberNoController = TextEditingController();

  var kinbirthCertController = TextEditingController();
  var kinfullNameController = TextEditingController();
  var kindobController = TextEditingController();
  var kinisSchooledController = TextEditingController();
  var kinrelationshipIdController = TextEditingController();
  var kinMaritalStatusController = TextEditingController();
  var kinphoneNoController = TextEditingController();
  var kinageController = TextEditingController();
  var kinGenderTypeController = TextEditingController();
  var kinLevelOfEducationController = TextEditingController();
  var kinisOrphanedController = TextEditingController();
  String age = '';

  NextOfKinModelResultElement kinModel = NextOfKinModelResultElement();

  //Drop Down Lists Here
  final List<LevelOfEducationResultElement> _educationLevels = [];
  final List<RelationResultElement> _relationLevels = [];
  final List<GenderResultElement> _genderTypes = [];
  final List<MaritalStatusResultElement> _maritalStatus = [];
  final List<CountyResultElement> _colCenter1 = [];
  final List<String> _schooledTypes = ["Yes", "No"];
  final List<String> _orphanedTypes = ["Yes", "No"];

// Selected Dropdown elements

  GenderResultElement? selectedGender;
  MaritalStatusResultElement? selectedMaritalStatus;
  LevelOfEducationResultElement? selectedLevelOfEducation;
  RelationResultElement? selectedRelationship;
  String? selectedIsSchooled;
  String? selectedIsOrphaned;
  bool isSchooled = false;

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        kindobController.text = formattedDate;

        kinageController.text = (date.year - picked.year).toString();

        print(kinageController.text.toString());
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];

  Future _getSingleFarmerLocal(String memberNo) async {
    setState(() => isLoading = true);
    print("At Function");
    var res = await FarmersController().getSingleFarmerLocal(memberNo);
    if (res != null) {
      _allFarmersLocal = res;
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        farmerNameController.text =
            _allFarmersLocal[0]["full_name"]!.toString();
        farmerMemberNoController.text =
            _allFarmersLocal[0]["member_no"]!.toString();
        _getLocationDetails();

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    }
  }

  _getLevelOfEducation() async {
    List<dynamic> resEducation = [];
    var res2 =
        await LocationController().getLocationsLocal(educationLevelTable);
    //print("Hello there"+ res[0].toString());
    if (res2 != null) {
      resEducation = res2;
      for (var i in resEducation) {
        LevelOfEducationResultElement model = LevelOfEducationResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _educationLevels.add(model);
      }
    }
    setState(() {});
  }

  _getLocationDetails() async {
    List<dynamic> resGender = [];
    var res = await LocationController().getLocationsLocal(genderTypeTable);
    //print("Hello there"+ res[0].toString());
    if (res != null) {
      resGender = res;
      for (var i in resGender) {
        GenderResultElement model = GenderResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _genderTypes.add(model);
      }
    }

    //selectedGender = _genderTypes[0];

    List<dynamic> resMaritalStatus = [];
    var res1 = await LocationController().getLocationsLocal(maritalStatusTable);
    //print("Hello there"+ res[0].toString());
    if (res1 != null) {
      resMaritalStatus = res1;
      for (var i in resMaritalStatus) {
        MaritalStatusResultElement model = MaritalStatusResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _maritalStatus.add(model);
      }
    }

    //selectedMaritalStatus = _maritalStatus[0];

    List<dynamic> resRelation = [];
    var res3 = await LocationController().getLocationsLocal(relationsTable);
    //print("Hello there"+ res[0].toString());
    if (res3 != null) {
      resRelation = res3;
      for (var i in resRelation) {
        RelationResultElement model = RelationResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _relationLevels.add(model);
      }
    }

    //selectedNationality = _nationalities[0];
    setState(() => isLoading = false);
  }

  Future<String> _getAgeFromDOB(String a) async {
    var dt = DateTime.parse(date.toString());
    //var date =  DateFormat('d MMM, yyyy').format(dt);
    DateTime today = DateTime.now();

    return (today.year - dt.year).toString();
  }

  _getBirthCertNo() async {
    var res = await FarmersController()
        .getChildItemCount(farmerMemberNoController.text);

    return "${farmerMemberNoController.text}_${res+1}";
  }

  _addInfo() async {
    setState(() => isLoading = true);

    String age = await _getAgeFromDOB(kindobController.text);

    kinbirthCertController.text = await _getBirthCertNo();

    kinModel.sid = 'p';
    kinModel.memberNo = farmerMemberNoController.text;
    kinModel.fullName = kinfullNameController.text;
    kinModel.birthCertificateNo = kinbirthCertController.text;
    kinModel.dateOfBirth = kindobController.text;
    kinModel.relationshipId = kinrelationshipIdController.text;
    kinModel.maritalStatusId = kinMaritalStatusController.text;
    kinModel.phonenumber = kinphoneNoController.text;
    kinModel.genderTypeId = kinGenderTypeController.text;
    kinModel.levelOfEducationId = kinLevelOfEducationController.text;
    kinModel.isSchooled = isSchooled ? 1.toString() : 0.toString();
    kinModel.isOrphaned = selectedIsOrphaned.toString();
    kinModel.age = age;

    List<dynamic> kins = [];
    kins.add(kinModel);

    var res = await FarmersController().getAllUnUpdatedLocal(nextKinTable,
        "member_no", farmerMemberNoController.text, "sid", "p", "_Id");
    if (res == null) {
      int i = await FarmersController().saveAll(kins, nextKinTable);

      if (i >= 0) {
        print("Success here");
      }
    } else {
      int i = await FarmersController().updateLocal(
          nextKinTable, kinModel, "member_no", farmerMemberNoController.text);

      if (i >= 0) {
        print("Success here");
      }
    }

    setState(() => isLoading = false);
    Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const AdditionalInfoDashboardScreen(),
                          arguments: pageId);
                    },
                  ),
                  BigText(
                    text: "Children On The Farm",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        //physics: BouncingScrollPhysics(),
        child: isLoading
            ? const CustomLoader()
            : Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                //height: Dimensions.screenHeight,
                width: Dimensions.screenWidth,
                padding: EdgeInsets.only(
                  top: Dimensions.height10,
                  left: Dimensions.width10,
                  right: Dimensions.width10,
                  bottom: Dimensions.height15,
                ),
                child: SizedBox(
                  //height: Dimensions.screenHeight,
                  width: Dimensions.screenWidth,

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BigText(
                        text: "Name",
                        color: AppColors.black,
                        size: Dimensions.font16,
                      ),
                      FormFields(
                        textEditingController: kinfullNameController,
                        inputType: TextInputType.text,
                      ),

                      //dob
                      BigText(
                        text: "Date Of Birth",
                        color: Colors.black,
                        size: Dimensions.font16,
                      ),

                      Container(
                        padding: EdgeInsets.only(
                            top: Dimensions.height10 / 2,
                            bottom: Dimensions.height10),
                        child: TextField(
                          autocorrect: true,
                          controller: kindobController,
                          keyboardType: TextInputType.text,
                          readOnly: false,
                          onTap: () {
                            selectedTimePicker(context);
                          },
                          decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month),
                            hintStyle: const TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(Dimensions.radius15 - 5)),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 2),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(Dimensions.radius15)),
                              borderSide: const BorderSide(
                                  color: Colors.lightGreen, width: 2),
                            ),
                          ),
                        ),
                      ),

                      // BigText(
                      //   text: "Age",
                      //   color: AppColors.black,
                      //   size: Dimensions.font16,
                      // ),
                      // FormFields(
                      //   textEditingController: kinageController,
                      //   inputType: TextInputType.text,
                      //   readOnly: true,
                      // ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      // BigText(
                      //   text: "Birth Certificate Number",
                      //   color: AppColors.black,
                      //   size: Dimensions.font16,
                      // ),
                      // FormFields(
                      //   textEditingController: kinbirthCertController,
                      //   inputType: TextInputType.text,
                      // ),

                      // SizedBox(
                      //   height: Dimensions.height15,
                      // ),

                      BigText(
                        text: "Gender",
                        color: AppColors.black,
                        size: Dimensions.font16,
                      ),

                      FormField<GenderResultElement>(
                        builder: (FormFieldState<GenderResultElement> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              hintText: "Gender",
                              hintStyle: TextStyle(color: AppColors.black),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreen, width: 2),
                              ),
                            ),
                            //isEmpty: _currentSelectedValue == '',
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<GenderResultElement>(
                                value: selectedGender,
                                isDense: true,
                                onChanged:
                                    (GenderResultElement? newValue) async {
                                  setState(() {
                                    selectedGender = newValue;
                                    kinGenderTypeController.text =
                                        selectedGender!.resultId.toString();
                                    print(
                                        "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
                                  });
                                },
                                items: _genderTypes
                                    .map((GenderResultElement value) {
                                  return DropdownMenuItem<GenderResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(
                                            color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),

                      // Container(
                      //   margin: const EdgeInsets.all(10.0),
                      //   child: DropdownButton<GenderResultElement>(
                      //     hint: Text("Gender"),
                      //     value: selectedGender,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged: (GenderResultElement? newValue) async {
                      //       setState(() {
                      //         selectedGender = newValue;
                      //         kinGenderTypeController.text =
                      //             selectedGender!.resultId.toString();
                      //         print(
                      //             "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
                      //       });
                      //     },
                      //     items: _genderTypes.map((GenderResultElement gender) {
                      //       return DropdownMenuItem<GenderResultElement>(
                      //         value: gender,
                      //         child: Text(gender.name.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      BigText(
                        text: "Is Schooled",
                        color: AppColors.black,
                        size: Dimensions.font16,
                      ),

                      FormField<String>(
                        builder: (FormFieldState<String> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              hintText: "Is Schooled",
                              hintStyle: TextStyle(color: AppColors.black),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreen, width: 2),
                              ),
                            ),
                            //isEmpty: _currentSelectedValue == '',
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                value: selectedIsSchooled,
                                isDense: true,
                                onChanged: (String? newValue) async {
                                  setState(() {
                                    selectedIsSchooled = newValue;
                                  });
                                  if (selectedIsSchooled == "Yes") {
                                    isSchooled = true;
                                    await _getLevelOfEducation();
                                  } else {
                                    isSchooled = false;
                                    _educationLevels.clear();
                                  }
                                },
                                items: _schooledTypes.map((String schooled) {
                                  return DropdownMenuItem<String>(
                                    value: schooled,
                                    child: Text(schooled.toString(),
                                        style: const TextStyle(
                                            color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),

                      // Container(
                      //   margin: const EdgeInsets.all(10.0),
                      //   child: DropdownButton<String>(
                      //     hint: Text("Is Schooled"),
                      //     value: selectedIsSchooled,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged: (String? newValue) async {
                      //       setState(() {
                      //         selectedIsSchooled = newValue;
                      //       });
                      //       if (selectedIsSchooled == "Yes") {
                      //         isSchooled = true;
                      //         await _getLevelOfEducation();
                      //       } else {
                      //         isSchooled = false;
                      //         _educationLevels.clear();
                      //       }
                      //     },
                      //     items: _schooledTypes.map((String schooled) {
                      //       return DropdownMenuItem<String>(
                      //         value: schooled,
                      //         child: Text(schooled.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      BigText(
                        text: "Level Of Education",
                        color: AppColors.black,
                        size: Dimensions.font16,
                      ),

                      FormField<LevelOfEducationResultElement>(
                        builder: (FormFieldState<LevelOfEducationResultElement>
                            state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              hintText: "Level Of Education",
                              hintStyle: TextStyle(color: AppColors.black),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreen, width: 2),
                              ),
                            ),
                            //isEmpty: _currentSelectedValue == '',
                            child: DropdownButtonHideUnderline(
                              child:
                                  DropdownButton<LevelOfEducationResultElement>(
                                value: selectedLevelOfEducation,
                                isDense: true,
                                onChanged: (LevelOfEducationResultElement?
                                    newValue) async {
                                  setState(() {
                                    selectedLevelOfEducation = newValue;
                                    kinLevelOfEducationController.text =
                                        selectedLevelOfEducation!.resultId
                                            .toString();
                                    print(
                                        "Here is LevelOfEducation ==*** ${selectedLevelOfEducation!.name.toString()} and id === ${selectedLevelOfEducation!.resultId.toString()}");
                                  });
                                },
                                items: _educationLevels
                                    .map((LevelOfEducationResultElement value) {
                                  return DropdownMenuItem<
                                      LevelOfEducationResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(
                                            color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),

                      // Container(
                      //   margin: const EdgeInsets.all(10.0),
                      //   child: DropdownButton<LevelOfEducationResultElement>(
                      //     hint: Text("Level Of Education"),
                      //     value: selectedLevelOfEducation,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged:
                      //         (LevelOfEducationResultElement? newValue) async {
                      //       setState(() {
                      //         selectedLevelOfEducation = newValue;
                      //         kinLevelOfEducationController.text =
                      //             selectedLevelOfEducation!.resultId.toString();
                      //         print(
                      //             "Here is LevelOfEducation ==*** ${selectedLevelOfEducation!.name.toString()} and id === ${selectedLevelOfEducation!.resultId.toString()}");
                      //       });
                      //     },
                      //     items: _educationLevels
                      //         .map((LevelOfEducationResultElement value) {
                      //       return DropdownMenuItem<
                      //           LevelOfEducationResultElement>(
                      //         value: value,
                      //         child: Text(value.name.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      BigText(
                        text: "Relationship",
                        color: AppColors.black,
                        size: Dimensions.font16,
                      ),

                      FormField<RelationResultElement>(
                        builder: (FormFieldState<RelationResultElement> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              hintText: "Relationship",
                              hintStyle: TextStyle(color: AppColors.black),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreen, width: 2),
                              ),
                            ),
                            //isEmpty: _currentSelectedValue == '',
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<RelationResultElement>(
                                value: selectedRelationship,
                                isDense: true,
                                onChanged:
                                    (RelationResultElement? newValue) async {
                                  setState(() {
                                    selectedRelationship = newValue;
                                    kinrelationshipIdController.text =
                                        selectedRelationship!.resultId
                                            .toString();
                                    print(
                                        "Here is Relationship ==*** ${selectedRelationship!.name.toString()} and id === ${selectedRelationship!.resultId.toString()}");
                                  });
                                },
                                items: _relationLevels
                                    .map((RelationResultElement value) {
                                  return DropdownMenuItem<
                                      RelationResultElement>(
                                    value: value,
                                    child: Text(value.name.toString(),
                                        style: const TextStyle(
                                            color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),

                      // Container(
                      //   margin: const EdgeInsets.all(10.0),
                      //   child: DropdownButton<RelationResultElement>(
                      //     hint: Text("Relationship"),
                      //     value: selectedRelationship,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged: (RelationResultElement? newValue) async {
                      //       setState(() {
                      //         selectedRelationship = newValue;
                      //         kinrelationshipIdController.text =
                      //             selectedRelationship!.resultId.toString();
                      //         print(
                      //             "Here is Relationship ==*** ${selectedRelationship!.name.toString()} and id === ${selectedRelationship!.resultId.toString()}");
                      //       });
                      //     },
                      //     items: _relationLevels
                      //         .map((RelationResultElement value) {
                      //       return DropdownMenuItem<RelationResultElement>(
                      //         value: value,
                      //         child: Text(value.name.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      BigText(
                        text: "Is Orphaned",
                        color: AppColors.black,
                        size: Dimensions.font16,
                      ),

                      FormField<String>(
                        builder: (FormFieldState<String> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              hintText: "Is Orphaned",
                              hintStyle: TextStyle(color: AppColors.black),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreen, width: 2),
                              ),
                            ),
                            //isEmpty: _currentSelectedValue == '',
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                value: selectedIsOrphaned,
                                isDense: true,
                                onChanged: (String? newValue) async {
                                  setState(() {
                                    selectedIsOrphaned = newValue;
                                  });
                                },
                                items: _orphanedTypes.map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value.toString(),
                                        style: const TextStyle(
                                            color: Colors.black)),
                                  );
                                }).toList(),
                              ),
                            ),
                          );
                        },
                      ),

                      // Container(
                      //   margin: const EdgeInsets.all(10.0),
                      //   child: DropdownButton<String>(
                      //     hint: Text("Is Orphaned"),
                      //     value: selectedIsOrphaned,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged: (String? newValue) async {
                      //       setState(() {
                      //         selectedIsOrphaned = newValue;
                      //       });
                      //     },
                      //     items: _orphanedTypes.map((String value) {
                      //       return DropdownMenuItem<String>(
                      //         value: value,
                      //         child: Text(value.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),

                      SizedBox(
                        height: Dimensions.height15 * 2,
                      ),

                      // Container(
                      //   margin: const EdgeInsets.all(10.0),
                      //   child: DropdownButton<MaritalStatusResultElement>(
                      //     hint: Text("Marital Status"),
                      //     value: selectedMaritalStatus,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged:
                      //         (MaritalStatusResultElement? newValue) async {
                      //       setState(() {
                      //         selectedMaritalStatus = newValue;
                      //         kinMaritalStatusController.text =
                      //             selectedMaritalStatus!.resultId.toString();
                      //         print(
                      //             "Here is Marital status ==*** ${selectedMaritalStatus!.name.toString()} and id === ${selectedMaritalStatus!.resultId.toString()}");
                      //       });
                      //     },
                      //     items: _maritalStatus
                      //         .map((MaritalStatusResultElement value) {
                      //       return DropdownMenuItem<MaritalStatusResultElement>(
                      //         value: value,
                      //         child: Text(value.name.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),

                      // SizedBox(
                      //   height: Dimensions.height30,
                      // ),

                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: EdgeInsets.only(
                            bottom: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _addInfo();
                              });
                            },
                            child: Container(
                              height: Dimensions.height30 * 2,
                              width: Dimensions.screenWidth,
                              decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius30),
                              ),
                              child: Center(
                                child: BigText(
                                  text: "Submit",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font16,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
      ),
    );
  }
}
