import 'package:farmcapturev2/models/farmers/additional_info/inputquestions_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../models/farmers/additional_info/farmeranswers_model.dart';
import '../../../../models/farmers/additional_info/questionChoice_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/small_text.dart';
import 'additionalinfo_dashboard.dart';

class LivingConditionsScreen extends StatefulWidget {
  const LivingConditionsScreen({super.key});

  @override
  State<LivingConditionsScreen> createState() => _LivingConditionsScreenState();
}

class _LivingConditionsScreenState extends State<LivingConditionsScreen> {
  var pageId = Get.arguments;
  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];
  String memberName = "";
  // String? selectedValue;

  final List<QuestionChoicesModel> _livingConditionsQuestionChoices = [];
  final List<InpQuestionsModelResultElement> _livingConditionsQuestion = [];

  List<ChoiceModel?> selectedAnswers = [];
  List<String> selectedQuestions = [];

  _getLivingConditionsFarmQuestions() async {
    setState(() => isLoading = true);
    List<dynamic> resLivingConditionsQuestion = [];
    //var res = await FarmersController().getFarmQuestions('LC');
    var res = await FarmersController().getFarmLivingConditionsQuestions('LC');
    if (res != null) {
      resLivingConditionsQuestion = res;
      for (Map<String, dynamic> row in resLivingConditionsQuestion) {
        List<dynamic> choices = row['choices_text'].split(':');

        QuestionChoicesModel questionChoices = QuestionChoicesModel();
        questionChoices.questionId = row['Id'].toString();
        questionChoices.questionName = row['name'];

        // print("Hello there" + questionChoices.questionId.toString());
        // var data = {
        //   "questionName": row['name'],
        //   "questionId": row['id'],
        //   "choices": [],
        // };
        // do something with the question and choices
        List<ChoiceModel> choiseList = [];
        for (var element in choices) {
          ChoiceModel choices = ChoiceModel();
          List<dynamic> choice = element.split('|');

          var choicesdata = {
            "choiceName": choice[0].toString(),
            "choiceId": choice[1].toString()
          };
          choices.choiceId = choice[1].toString();
          choices.choiceName = choice[0].toString();
          choices.questionName = row['name'];
          choices.questionId = row['Id'].toString();

          choiseList.add(choices);
        }
        questionChoices.choices = choiseList;

        //print(questionChoices.toJson().toString());

        _livingConditionsQuestionChoices.add(questionChoices);

        var result = {};
      }

      //_livingConditionsQuestionChoices = resLivingConditionsQuestion;
    }
    //print("Hello there"+ res[0].toString());
    // if (res != null) {
    //   resLivingConditionsQuestion = res;
    //   for (var i in resLivingConditionsQuestion) {
    //     InpQuestionsModelResultElement model = InpQuestionsModelResultElement(
    //         resultId: i['id'],
    //         name: i['name'],
    //         identifierCode: i['identifier_code']);

    //     _livingConditionsQuestion.add(model);
    //   }
    // }

    setState(() => isLoading = false);
  }

  Future _getSingleFarmerLocal(String memberNo) async {
    setState(() => isLoading = true);
    print("At Function");
    var res = await FarmersController().getSingleFarmerLocal(memberNo);
    if (res != null) {
      _allFarmersLocal = res;
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        memberName = _allFarmersLocal[0]["full_name"]!.toString();

        await _getLivingConditionsFarmQuestions();

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  _addLivingConditions() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var UserId = localStorage.getString('UserId');
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    for (var element in selectedAnswers) {
      //print(element!.choiceName.toString());

      String transactionNumber =
          'TRN_${DateTime.now().millisecondsSinceEpoch.toString()}';

      FarmerAnswersModel farmeranswersmodel = FarmerAnswersModel();
      farmeranswersmodel.answerId = element!.choiceId;
      farmeranswersmodel.transactionNo = transactionNumber;
      farmeranswersmodel.dateTime = dateStr;
      farmeranswersmodel.status = 'p';
      farmeranswersmodel.farmInspectorQuizId = element.questionId;

      List<dynamic> item = [];
      item.add(farmeranswersmodel);

      var i = await FarmersController()
          .saveAllAdditional(item, farmerInputQuestionaireAnswersTable);
      if (i > 0){
        Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Dimensions.height30 * 2),
          child: AppBar(
            backgroundColor: Colors.green,
            actions: [
              Container(
                width: Dimensions.screenWidth,
                height: Dimensions.height30 * 2,
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                  color: Colors.green,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(5, 5),
                        color: AppColors.gradientOne.withOpacity(0.1)),
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(-5, -5),
                        color: AppColors.gradientOne.withOpacity(0.1))
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const AdditionalInfoDashboardScreen(),
                            arguments: pageId);
                      },
                    ),
                    BigText(
                      text: "Living Conditions",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    const SizedBox()
                  ],
                ),
              ),
            ],
          ),
        ),
        body: SafeArea(
            child: isLoading
                ? const CustomLoader()
                : Container(
                    width: Dimensions.screenWidth,
                    height: Dimensions.screenHeight,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/bg.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Column(
                      children: [
                        Container(
                          height: Dimensions.height20 * 2,
                          padding: EdgeInsets.only(
                            top: Dimensions.height10,
                            bottom: Dimensions.height10,
                          ),
                          child: BigText(
                            text: memberName,
                            color: AppColors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            //physics: const NeverScrollableScrollPhysics(),
                            itemCount:
                                _livingConditionsQuestionChoices.length + 1,
                            itemBuilder: (BuildContext context, int index) {
                              // print('List Is: ' +
                              //     _livingConditionsQuestionChoices[index]!
                              //         .choices
                              //         .toString());
                              if (index ==
                                  _livingConditionsQuestionChoices.length) {
                                return Column(
                                  children: [
                                    SizedBox(
                                      height: Dimensions.height30,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          selectedAnswers =
                                              _livingConditionsQuestionChoices
                                                  .map((QuestionChoicesModel
                                                      question) {
                                            return question.selectedChoice;
                                          }).toList();
                                        });
                          
                                        _addLivingConditions();
                                      },
                                      child: Container(
                                        height: Dimensions.height30 * 2,
                                        width: Dimensions.width30 *
                                            Dimensions.width10,
                                        decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius30),
                                        ),
                                        child: Center(
                                          child: BigText(
                                            text: "Submit",
                                            color: AppColors.textWhite,
                                            size: Dimensions.font16,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: Dimensions.height30,
                                    ),
                                  ],
                                );
                              }
                              QuestionChoicesModel questionChoices =
                                  _livingConditionsQuestionChoices[index];
                          
                              return Container(
                                padding: EdgeInsets.only(
                                  left: Dimensions.width15,
                                  right: Dimensions.width15,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: Dimensions.height30,
                                    ),
                          
                                    SmallText(
                                      text: questionChoices.questionName.toString(),
                                      color: AppColors.black,
                                      size: Dimensions.font16 - 2,
                                    ),
                        
                                    FormField<ChoiceModel>(
                                      builder: (FormFieldState<ChoiceModel> state) {
                                        return InputDecorator(
                                          decoration: InputDecoration(
                                            hintText: "Marital Status",
                                            hintStyle: TextStyle(color: AppColors.black),
                                            filled: true,
                                            fillColor: Colors.white70,
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(Dimensions.radius15 - 5)),
                                              borderSide: const BorderSide(color: Colors.grey, width: 2),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(Dimensions.radius15)),
                                              borderSide: const BorderSide(color: Colors.lightGreen, width: 2),
                                            ),
                                          ),
                                          //isEmpty: _currentSelectedValue == '',
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton<ChoiceModel>(
                                              hint: Text(questionChoices.questionName.toString()),
                                              value: questionChoices.selectedChoice,
                                              isDense: true,
                                              onChanged: (ChoiceModel? newValue) async {
                                                setState(() {
                                                  questionChoices.selectedChoice =
                                                  newValue;
                                                });
                                                print("Answer: $selectedAnswers");
                                              },
                                              items: questionChoices.choices!.map(
                                            (ChoiceModel value) {
                                          return DropdownMenuItem<
                                              ChoiceModel>(
                                                  value: value,
                                                  child: Text(value.choiceName.toString(),
                                                      style: const TextStyle(
                                                          color: Colors.black)),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              );
                            }
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                      ],
                    ),
                  )),
      ),
    );
  }
}
