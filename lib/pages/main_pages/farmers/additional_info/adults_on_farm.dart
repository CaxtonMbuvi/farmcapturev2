import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/controllers/farmers/farmers_controller.dart';
import 'package:farmcapturev2/resources/base/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../models/farmers/additional_info/farmeranswers_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../../../resources/widgets/small_text.dart';
import 'additionalinfo_dashboard.dart';

class AdultsOnFarm extends StatefulWidget {
  const AdultsOnFarm({super.key});

  @override
  State<AdultsOnFarm> createState() => _AdultsOnFarmState();
}

class _AdultsOnFarmState extends State<AdultsOnFarm> {

  var pageId = Get.arguments;
  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];
  final List<dynamic> _answeredQuestions = [];
  final Map<int, String> _answers = {};
  String memberName = "";
  final Map<int, TextEditingController> _controllers = {};

  List<dynamic> _adultQuestion = [];
  var hiredTemporaryController = TextEditingController();
  var hiredSchooledController = TextEditingController();
  var hiredMarriedController = TextEditingController();

  _getAdultsOnFarmQuestions() async {
    setState(() => isLoading = true);
    List<dynamic> resAdultQuestion = [];
    var res = await FarmersController().getFarmQuestions('AOF');
    //print("Hello there"+ res[0].toString());
    if (res != null) {
      _adultQuestion = res;
      //resAdultQuestion = res;
      // for (var i in resAdultQuestion) {
      //   InpQuestionsModelResultElement model = InpQuestionsModelResultElement(
      //       resultId: i['id'],
      //       name: i['name'],
      //       identifierCode: i['identifier_code']);

      //   _adultQuestion.add(model);
      // }
    }

    setState(() => isLoading = false);
  }

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    var res = await FarmersController().getSingleFarmerLocal(farmerId);
    if (res != null) {
      _allFarmersLocal = res;
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        memberName = _allFarmersLocal[0]["full_name"]!.toString();

        await _getAdultsOnFarmQuestions();

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
  }

  // Future<void> _loadAnswers() async {
  //   final answers = await _getAnswersFromDatabase();
  //   setState(() {
  //     for (final answer in answers) {
  //       final controller = _controllers[answer.questionId];
  //       if (controller != null) {
  //         controller.text = answer.choiceName.toString();
  //       }
  //     }
  //   });
  // }

  // Future<List<ChoiceModel>> _getAnswersFromDatabase() async {
  //   var res = await FarmersController().getSingleLocal(farmerInputQuestionaireAnswersTable, );

  //   final db = await DBHelper.database;
  //   final results = await db.query('answers');
  //   return results
  //       .map((row) => Answer(
  //             questionId: row['question_id'] as int,
  //             answer: row['answer'] as String,
  //           ))
  //       .toList();
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  _addAdultsOnTheFarm() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var UserId = localStorage.getString('UserId');
    var VisitNo = localStorage.getString('VisitNo');
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    for (var element in _answeredQuestions) {
      //print(element!.choiceName.toString());

      String transactionNumber =
          'TRN_${DateTime.now().millisecondsSinceEpoch.toString()}';

      FarmerAnswersModel farmeranswersmodel = FarmerAnswersModel();
      farmeranswersmodel.farmVisitTransactionNo = VisitNo;
      farmeranswersmodel.answerOption = element!.choiceName;
      farmeranswersmodel.transactionNo = transactionNumber;
      farmeranswersmodel.dateTime = dateStr;
      farmeranswersmodel.status = 'p';
      farmeranswersmodel.farmInspectorQuizId = element.questionId;

      List<dynamic> item = [];
      item.add(farmeranswersmodel);

      var i = await FarmersController()
          .saveAllAdditional(item, farmerInputQuestionaireAnswersTable);
      if (i > 0){
        Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
      }else{
        showCustomSnackBar("Something went wrong", context, ContentType.failure, title: "Failed");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Dimensions.height30 * 2),
          child: AppBar(
            backgroundColor: Colors.green,
            actions: [
              Container(
                width: Dimensions.screenWidth,
                height: Dimensions.height30 * 2,
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                  color: Colors.green,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(5, 5),
                        color: AppColors.gradientOne.withOpacity(0.1)),
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(-5, -5),
                        color: AppColors.gradientOne.withOpacity(0.1))
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const AdditionalInfoDashboardScreen(),
                            arguments: pageId);
                      },
                    ),
                    BigText(
                      text: "Adults On The Farm",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    const SizedBox()
                  ],
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: SafeArea(
              child: isLoading
                  ? const CustomLoader()
                  : Container(
                      width: Dimensions.screenWidth,
                      height: Dimensions.screenHeight,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/images/bg.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            height: Dimensions.height20 * 2,
                            padding: EdgeInsets.only(
                              top: Dimensions.height10,
                              bottom: Dimensions.height10,
                            ),
                            child: BigText(
                              text: memberName,
                              color: AppColors.black,
                              size: Dimensions.font16,
                            ),
                          ),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: _adultQuestion.length + 1,
                            itemBuilder: (BuildContext context, int index) {
                              if (index == _adultQuestion.length) {
                                return Column(
                                  children: [
                                    SizedBox(
                                      height: Dimensions.height30,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          print(_answeredQuestions[0]!
                                              .questionName
                                              .toString());
                                        });
                                        _addAdultsOnTheFarm();
                                      },
                                      child: Container(
                                        height: Dimensions.height30 * 2,
                                        width: Dimensions.width30 *
                                            Dimensions.width10,
                                        decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius30),
                                        ),
                                        child: Center(
                                          child: BigText(
                                            text: "Submit",
                                            color: AppColors.textWhite,
                                            size: Dimensions.font16,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: Dimensions.height30,
                                    ),
                                  ],
                                );
                              }
  
                              var item = _adultQuestion[index];
                              var AdultsController = TextEditingController();
                              ChoiceModel1 choiceModel = ChoiceModel1();
                              return Container(
                                padding: EdgeInsets.only(
                                  left: Dimensions.width15,
                                  right: Dimensions.width15,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: Dimensions.height30,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: Dimensions.width10,
                                      ),
                                      child: SmallText(
                                        text: item['name'].toString(),
                                        color: AppColors.black,
                                        size: Dimensions.font16 - 2,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          top: Dimensions.height10, bottom: Dimensions.height10),
                                      child: TextField(
                                        autocorrect: true,
                                        keyboardType: TextInputType.number,
                                        onChanged: (String answer) {
                                          String question =
                                              item['name'].toString();
                                          setState(() {
                                            _answers[index] =
                                                '$question:$answer';
                                            // _answeredQuestions.add(
                                            //     _answers[index] =
                                            //         question + ':' + answer);
                                            choiceModel.choiceName = answer;
                                            choiceModel.questionId =
                                                item['Id'].toString();
                                            choiceModel.questionName = question;
                                            _answeredQuestions.add(choiceModel);
                                            //_answers[index] = answer;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white70,
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(Dimensions.radius15 - 5)),
                                            borderSide: const BorderSide(color: Colors.grey, width: 2),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(Dimensions.radius15)),
                                            borderSide: const BorderSide(color: Colors.lightGreen, width: 2),
                                          ),
                                        ),
                                      )
                                    ),
                                  ],
                                ),
                              );
                            }
                          ),
                        ],
                      ),
                    )),
        ),
      ),
    );
  }
}

class ChoiceModel1 {
  ChoiceModel1({
    this.choiceName,
    this.questionId,
    this.questionName,
  });

  String? choiceName;
  String? questionId;
  String? questionName;

  factory ChoiceModel1.fromJson(Map<String, dynamic> json) => ChoiceModel1(
        choiceName: json["choiceName"],
        questionId: json["question_Id"],
        questionName: json["question_Name"],
      );

  Map<String, dynamic> toJson() => {
        "choiceName": choiceName,
        "questionId": questionId,
        "questionName": questionName,
      };
}
