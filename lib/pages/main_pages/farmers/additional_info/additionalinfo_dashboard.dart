import 'package:farmcapturev2/pages/main_pages/farmers/additional_info/adults_on_farm.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/additional_info/living_conditions.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/additional_info/nextofkin.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/additional_info/personal_assets.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/single_farmer_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import '../../visits/add_trainingtopics.dart';

class AdditionalInfoDashboardScreen extends StatefulWidget {
  const AdditionalInfoDashboardScreen({super.key});

  @override
  State<AdditionalInfoDashboardScreen> createState() =>
      _AdditionalInfoDashboardScreenState();
}

class _AdditionalInfoDashboardScreenState extends State<AdditionalInfoDashboardScreen> {
  var pageId = Get.arguments;
  var memberNoController = TextEditingController();

  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];
  String memberName = "";

  Future _getSingleFarmerLocal(String memberNo) async {
    setState(() => isLoading = true);
    print("At Function");
    var res = await FarmersController().getSingleFarmerLocal(memberNo);
    if (res != null) {
      _allFarmersLocal = res;
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        memberName = _allFarmersLocal[0]["full_name"]!.toString();
        memberNoController.text = _allFarmersLocal[0]["member_no"]!.toString();

        await _checkVisit();

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
  }

  _checkVisit() async {
    var res;
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    try {
      res = await FarmersController()
          .checkFarmerVisits(memberNoController.text, dateStr);
      if (res != null) {
        SharedPreferences localStorage =
              await SharedPreferences.getInstance();
          localStorage.setString('VisitNo', res[0]['visit_transaction_code'].toString());
        print("Value is :${res[0]['visit_transaction_code']}");
      }
      //return res;
    } catch (e) {}
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const SingleFarmerDashboard(),
                            arguments: pageId);
                      },
                    ),
                    BigText(
                      text: "Additional Info Dashboard",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: Container(
        height: Dimensions.screenHeight,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Container(
              height: Dimensions.height20 * 2,
              padding: EdgeInsets.only(
                top: Dimensions.height10,
                bottom: Dimensions.height10,
              ),
              child: BigText(
                text: memberName,
                color: AppColors.black,
                size: Dimensions.font16,
              ),
            ),
            Expanded(
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverToBoxAdapter(
                    child: Column(
                      children: [
                        // Training Topics
                        Padding(
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                            top: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () async {
                              // var res = await _checkVisit();
                              // if (res == null) {
                              //   print("Please add visit");
                              //   showCustomSnackBar("Please Add a visit",
                              //       context, ContentType.failure,
                              //       title: "Failed");
                              // } else 
                                Get.to(
                                  () => const TrainingTopicsScreen(),
                                  arguments: pageId.toString(),
                                );
                              
                            },
                            child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(
                                      Dimensions.radius20,
                                    )),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width20,
                                    right: Dimensions.width15,
                                  ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_5.png',
                                        width: Dimensions.width30 * 3,
                                        height: Dimensions.height30 * 3,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 / 2,
                                      ),
                                      BigText(
                                        text: "Individual Training",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ),

                        // NEXT OF KIN
                        Padding(
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                            top: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () async {
                              await _chooseWhatToDoNoK();
                              // Get.to(() => const NextOfKinScreen(),
                              //     arguments: pageId);
                            },
                            child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(
                                      Dimensions.radius20,
                                    )),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width20,
                                    right: Dimensions.width15,
                                  ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_nextofkin.png',
                                        width: Dimensions.width30 * 3,
                                        height: Dimensions.height30 * 3,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 / 2,
                                      ),
                                      BigText(
                                        text: "Children On The Farm",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ),

                        // ADULTS ON THE FARM
                        Padding(
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                            top: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Get.to(() => const AdultsOnFarm(),
                                  arguments: pageId);
                            },
                            child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(
                                      Dimensions.radius20,
                                    )),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width20,
                                    right: Dimensions.width15,
                                  ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_adults.png',
                                        width: Dimensions.width30 * 3,
                                        height: Dimensions.height30 * 3,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 / 2,
                                      ),
                                      BigText(
                                        text: "Adults On The Farm",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ),

                        // WORKING CONDITIONS
                        Padding(
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                            top: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(
                                      Dimensions.radius20,
                                    )),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width20,
                                    right: Dimensions.width15,
                                  ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_workingconditions.png',
                                        width: Dimensions.width30 * 3,
                                        height: Dimensions.height30 * 3,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 / 2,
                                      ),
                                      BigText(
                                        text: "Working Conditions",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ),

                        // LIVING CONDITIONS
                        Padding(
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                            top: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Get.to(() => const LivingConditionsScreen(),
                                  arguments: pageId);
                            },
                            child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(
                                      Dimensions.radius20,
                                    )),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width20,
                                    right: Dimensions.width15,
                                  ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_livingconditions.png',
                                        width: Dimensions.width30 * 3,
                                        height: Dimensions.height30 * 3,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 / 2,
                                      ),
                                      BigText(
                                        text: "Living Conditions",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ),

                        // PERSONAL ASSETS
                        Padding(
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                            top: Dimensions.height10,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Get.to(() => const PersonalAssetsScreen(),
                                  arguments: pageId);
                            },
                            child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(
                                      Dimensions.radius20,
                                    )),
                                child: Container(
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width20,
                                    right: Dimensions.width15,
                                  ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_personalassets.png',
                                        width: Dimensions.width30 * 3,
                                        height: Dimensions.height30 * 3,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 / 2,
                                      ),
                                      BigText(
                                        text: "Personal Assets",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                        ),

                        SizedBox(
                          height: Dimensions.height20,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _chooseWhatToDoNoK() async{
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState){
              return SimpleDialog(
                title: Center(
                  child: BigText(
                    text: 'SELECT ACTION',
                    color: Colors.black,
                    size: Dimensions.font16,
                  ),
                ),
                children: [

                  SimpleDialogOption(
                    onPressed: () {},
                    child: GestureDetector(
                      child: Container(
                        width: Dimensions.width30 * Dimensions.width10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height15,
                          bottom: Dimensions.height15,
                          left: Dimensions.width30,
                          right: Dimensions.width30,
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.mainColor,
                                width: Dimensions.width10 / 2),
                            color: Colors.grey[400],
                            borderRadius: BorderRadius.circular(5)),
                        child: Center(
                          child: BigText(
                            text: "ADD CHILDREN ON FARM",
                            color: Colors.black,
                            size: Dimensions.font12,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        Get.to(() => const NextOfKinScreen(),
                            arguments: pageId);

                      },
                    ),
                  ),


                  SimpleDialogOption(
                    onPressed: () {},
                    child: GestureDetector(
                      child: Container(
                        width: Dimensions.width30 * Dimensions.width10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height15,
                          bottom: Dimensions.height15,
                          left: Dimensions.width30,
                          right: Dimensions.width30,
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.mainColor,
                                width: Dimensions.width10 / 2),
                            color: Colors.grey[400],
                            borderRadius:
                            BorderRadius.circular(Dimensions.radius15 / 3)),
                        child: Center(
                          child: BigText(
                            text: "VIEW CHILDREN ON FARM",
                            color: Colors.black,
                            size: Dimensions.font12,
                          ),
                        ),
                      ),
                      onTap: () {
                        // Navigator.of(context).pop();
                        // Get.to(() => const ViewNextOfKinScreen(),
                        //     arguments: pageId);

                      },
                    ),
                  ),

                ],
              );
            }
          );
        }
    );

  }
}
