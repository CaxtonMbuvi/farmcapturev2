import 'package:farmcapturev2/pages/main_pages/farmers/additional_info/additionalinfo_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';

class ViewNextOfKinScreen extends StatefulWidget {
  const ViewNextOfKinScreen({Key? key}) : super(key: key);

  @override
  State<ViewNextOfKinScreen> createState() => _ViewNextOfKinScreenState();
}

class _ViewNextOfKinScreenState extends State<ViewNextOfKinScreen> {

  var pageId = Get.arguments;

  String kin_name = "";

  bool isLoading = false;
  List<dynamic> _allChildrenLocal = [];
  List<dynamic> filteredItems = [];


  @override
  void initState() {
    super.initState();

    _getAllChildrenLocal();

  }

  Future<void> _getAllChildrenLocal() async {

    setState(() => isLoading = true);

    print("Here NOK data ::: ");

    try{

      _allChildrenLocal = await FarmersController().getAllChildrenLocal(pageId);

      if (_allChildrenLocal.isNotEmpty) {

        print(_allChildrenLocal[0]["full_name"]!.toString());
        print(_allChildrenLocal.length);

        filteredItems = _allChildrenLocal;

      } else {
        print("Did not Get");
      }

    }catch (e) {
      setState(() => isLoading = false);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                    blurRadius: 3,
                    offset: const Offset(5, 5),
                    color: AppColors.gradientOne.withOpacity(0.1),
                  ),
                  BoxShadow(
                    blurRadius: 3,
                    offset: const Offset(-5, -5),
                    color: AppColors.gradientOne.withOpacity(0.1),
                  )
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const AdditionalInfoDashboardScreen(), arguments: 0);
                      },
                    ),
                    BigText(
                      text: "Children On Farm List",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [

            SizedBox(
              height: Dimensions.height30,
            ),

            Expanded(
                child: CustomScrollView(
                    slivers: <Widget>[
                      SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (context, index){
                                return Column(
                                  children: [
                                    Padding(
                                        padding:  EdgeInsets.only(
                                            left: Dimensions.width10,
                                            right: Dimensions.width10,
                                            top: Dimensions.height10
                                        ),
                                      child: GestureDetector(
                                        onTap: () async {
                                          setState(() {
                                            kin_name = _allChildrenLocal[index]
                                            ["full_name"]!
                                                .toString();
                                          });
                                        },
                                        child: Container(

                                          padding: EdgeInsets.only(
                                            left: Dimensions.width20,
                                          ),

                                          child: Row(
                                            children: [
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.start,

                                                children: [
                                                  BigText(
                                                    width: Dimensions.width45 * 7,
                                                    text:
                                                    "Name: ${filteredItems[index]['full_name']}",
                                                    color: AppColors.mainColor,
                                                    size: Dimensions.font14 + 1,
                                                  ),
                                                  SizedBox(
                                                    height: Dimensions.height10 / 2,
                                                  ),

                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              }
                          )
                      )
                    ]
                )
            ),
          ],
        ),
      ),
    );
  }
}
