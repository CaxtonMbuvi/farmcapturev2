import 'package:farmcapturev2/pages/main_pages/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import 'all_farmers.dart';

class FarmersScreen extends StatefulWidget {
  const FarmersScreen({super.key});

  @override
  State<FarmersScreen> createState() => _FarmersScreenState();
}

class _FarmersScreenState extends State<FarmersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const DashboardScreen());
                      },
                    ),
                    BigText(
                      text: "Manage Farmers",
                      color: AppColors.textWhite,
                      size: Dimensions.font28,
                    ),
                    const SizedBox()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            height: Dimensions.screenHeight - (Dimensions.height30 * 3),
            width: Dimensions.screenWidth,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg.png"),
                fit: BoxFit.cover,
              ),
            ),
            padding: EdgeInsets.only(
              top: Dimensions.height45,
              left: Dimensions.width30 * 2.0,
              right: Dimensions.width30 * 2.0,
            ),
            child: Column(
              children: [
                SizedBox(
                  height: Dimensions.height30 * 2,
                ),
                GestureDetector(
                  onTap: () {
                    Get.to(() => const AllFarmersScreen());
                  },
                  child: Column(
                    children: [
                      Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius30 * 5),
                        ),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/ic_1_1.png',
                                width: 60,
                                height: 60,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      BigText(
                        text: "Farmers List",
                        color: AppColors.black,
                        size: Dimensions.font16 - 4,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: Dimensions.height30 * 3,
                ),
                Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        //Get.to(() => AddFarmerScreen());
                      },
                      child: Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius30 * 5),
                        ),
                        child: const Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.edit_note,
                                size: 60,
                                color: Colors.green,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Add Farmer",
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
