import 'dart:io';

import 'package:farmcapturev2/pages/main_pages/farmers/additional_info/additionalinfo_dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/all_farmers.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/immediate_actions/immediate_actions.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../mapping/pickpoint_mapping.dart';
import '../mapping/walk_mapping.dart';
import 'edit_farmer.dart';

class SingleFarmerDashboard extends StatefulWidget {
  const SingleFarmerDashboard({Key? key}) : super(key: key);

  @override
  State<SingleFarmerDashboard> createState() => _SingleFarmerDashboardState();
}

class _SingleFarmerDashboardState extends State<SingleFarmerDashboard> {
  //var pageId = Get.arguments;
  var memberNo = "0";
  var _selectedIndex = -1;
  var farmerNameController = TextEditingController();
  List<dynamic> items = [
    {
      "image": 'assets/images/icupdatefarmer_profile.png',
      "name": "Farmer Details"
    },
    {"image": 'assets/images/ic_farmer_agreement.png', "name": "Farm Profile"},
    {"image": 'assets/images/ic_2_3.png', "name": "Farm Mapping"},
    {
      "image": 'assets/images/ic_tree_inspection.png',
      "name": "Technical Update"
    },
    {"image": 'assets/images/ic_alp.png', "name": "Immediate Actions"},
    {"image": 'assets/images/ic_survey.png', "name": "Surveys"},
  ];

  List<dynamic> _singleFarmersLocal = [];
  bool isLoading = false;
  String farmer_name = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal();
  }

  Future _getSingleFarmerLocal() async {
    setState(() => isLoading = true);
    print("At Function");

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var res = localStorage.getString('SelectedFarmer');

    setState(() {
      memberNo = res.toString();
    });

    _singleFarmersLocal =
        await FarmersController().getSingleFarmerLocal(memberNo.toString());

    if (_singleFarmersLocal.isNotEmpty) {
      print(_singleFarmersLocal[0]["full_name"]!.toString());
      farmerNameController.text =
          _singleFarmersLocal[0]["full_name"]!.toString();
      farmer_name = farmerNameController.text.toString();
      print(farmer_name);

      setState(() => isLoading = false);
    } else {
      print("Did not Get");
    }
  }

  /// location permission*
  LocationPermission? _locationPermission;
  checkIfLocationPermissionAllowed() async {
    _locationPermission = await Geolocator.requestPermission();

    //if permission is denied
    if (_locationPermission == LocationPermission.denied) {
      _locationPermission =
          await Geolocator.requestPermission(); //ask user to allow permission
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          Get.to(() => const AllFarmersScreen());
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        body:
            // SingleChildScrollView(
            //     child: Container(
            //         height: Dimensions.screenHeight - (Dimensions.height30 * 3),
            //         decoration: const BoxDecoration(
            //           image: DecorationImage(
            //             image: AssetImage("assets/images/bg.png"),
            //             fit: BoxFit.cover,
            //           ),
            //         ),
            //         padding: EdgeInsets.only(
            //           left: Dimensions.width30 * 2.0,
            //           right: Dimensions.width30 * 2.0,
            //         ),
            //         child: Center(
            //           child: Column(
            //               mainAxisAlignment: MainAxisAlignment.center,

            //               children: [

            //               BigText(
            //                 text: farmer_name,
            //                 color: Colors.black,
            //                 size: Dimensions.font12,
            //               ),

            //               SizedBox(height: Dimensions.height20,),

            //               //individual profiling
            //               Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,

            //                 children: [
            //                   GestureDetector(
            //                     onTap: () {
            //                       Get.to(
            //                             () => const FarmerDetailScreen(),
            //                         arguments: pageId,
            //                       );
            //                       //Get.to(() => TestPage());
            //                     },
            //                     child: Container(
            //                       height: Dimensions.height30 * 4,
            //                       width: Dimensions.width30 * 4,
            //                       decoration: BoxDecoration(
            //                         color: AppColors.textWhite,
            //                         borderRadius: BorderRadius.circular(
            //                             Dimensions.radius30 * 5),
            //                       ),
            //                       child: Center(
            //                         child: Column(
            //                           mainAxisAlignment:
            //                           MainAxisAlignment.center,
            //                           children: [
            //                             Image.asset(
            //                               'assets/images/icupdatefarmer_profile.png',
            //                               width: 60,
            //                               height: 60,
            //                             ),
            //                           ],
            //                         ),
            //                       ),
            //                     ),
            //                   ),
            //                   SizedBox(
            //                     height: Dimensions.height10,
            //                   ),
            //                   BigText(
            //                     text: "Individual Profiling",
            //                     color: AppColors.black,
            //                     size: Dimensions.font16 - 4,
            //                   ),
            //                   SizedBox(
            //                     height: Dimensions.height10 / 2,
            //                   ),
            //                   Container(
            //                       width: Dimensions.width45 + 40,
            //                       child: Divider()),
            //                   SmallText(
            //                     text: "Farmer's Profiling",
            //                     color: Colors.blueGrey,
            //                     size: Dimensions.font16 - 4,
            //                   )
            //                 ],
            //               ),

            //                 SizedBox(height: Dimensions.height20,),

            //                 //additional information
            //               Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,

            //                 children: [
            //                   GestureDetector(
            //                     onTap: () {
            //                       Get.to(() => AdditionalInfoDashboardScreen(), arguments: pageId);
            //                     },
            //                     child: Container(
            //                       height: Dimensions.height30 * 4,
            //                       width: Dimensions.width30 * 4,
            //                       decoration: BoxDecoration(
            //                         color: AppColors.textWhite,
            //                         borderRadius: BorderRadius.circular(
            //                             Dimensions.radius30 * 5),
            //                       ),
            //                       child: Center(
            //                         child: Column(
            //                           mainAxisAlignment:
            //                           MainAxisAlignment.center,
            //                           children: [
            //                             Image.asset(
            //                               'assets/images/ic_farmer_agreement.png',
            //                               width: 60,
            //                               height: 60,
            //                             ),
            //                           ],
            //                         ),
            //                       ),
            //                     ),
            //                   ),
            //                   SizedBox(
            //                     height: Dimensions.height10,
            //                   ),
            //                   BigText(
            //                     text: "Additional Information",
            //                     color: AppColors.black,
            //                     size: Dimensions.font16 - 4,
            //                   ),
            //                   SizedBox(
            //                     height: Dimensions.height10 / 2,
            //                   ),
            //                   Container(
            //                       width: Dimensions.width45 + 40,
            //                       child: Divider()),
            //                   SmallText(
            //                     text: "Additional Information",
            //                     color: Colors.blueGrey,
            //                     size: Dimensions.font16 - 4,
            //                   )
            //                 ],
            //               ),

            //                 SizedBox(height: Dimensions.height20,),

            //                 //farm mapping
            //               Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,

            //                 children: [
            //                   GestureDetector(
            //                     onTap: () async {
            //                       await checkIfLocationPermissionAllowed();
            //                       await openModeOfMappingDialog();
            //                     },
            //                     child: Container(
            //                       height: Dimensions.height30 * 4,
            //                       width: Dimensions.width30 * 4,
            //                       decoration: BoxDecoration(
            //                         color: AppColors.textWhite,
            //                         borderRadius: BorderRadius.circular(
            //                             Dimensions.radius30 * 5),
            //                       ),
            //                       child: Center(
            //                         child: Column(
            //                           mainAxisAlignment:
            //                           MainAxisAlignment.center,
            //                           children: [
            //                             Image.asset(
            //                               'assets/images/ic_2_3.png',
            //                               width: 60,
            //                               height: 60,
            //                             ),
            //                           ],
            //                         ),
            //                       ),
            //                     ),
            //                   ),
            //                   SizedBox(
            //                     height: Dimensions.height10,
            //                   ),
            //                   BigText(
            //                     text: "Farm Mapping",
            //                     color: AppColors.black,
            //                     size: Dimensions.font16 - 4,
            //                   ),
            //                   SizedBox(
            //                     height: Dimensions.height10 / 2,
            //                   ),
            //                   Container(
            //                       width: Dimensions.width45 + 40,
            //                       child: Divider()),
            //                   SmallText(
            //                     text: "Mapping Farms",
            //                     color: Colors.blueGrey,
            //                     size: Dimensions.font16 - 4,
            //                   )
            //                 ],
            //               ),

            //               // Row(
            //               //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               //   children: [
            //               //     //individual profiling
            //               //     Column(
            //               //       children: [
            //               //         GestureDetector(
            //               //           onTap: () {
            //               //             Get.to(
            //               //               () => const FarmerDetailScreen(),
            //               //               arguments: pageId,
            //               //             );
            //               //             //Get.to(() => TestPage());
            //               //           },
            //               //           child: Container(
            //               //             height: Dimensions.height30 * 4,
            //               //             width: Dimensions.width30 * 4,
            //               //             decoration: BoxDecoration(
            //               //               color: AppColors.textWhite,
            //               //               borderRadius: BorderRadius.circular(
            //               //                   Dimensions.radius30 * 5),
            //               //             ),
            //               //             child: Center(
            //               //               child: Column(
            //               //                 mainAxisAlignment:
            //               //                     MainAxisAlignment.center,
            //               //                 children: [
            //               //                   Image.asset(
            //               //                     'assets/images/ic_1_1.png',
            //               //                     width: 60,
            //               //                     height: 60,
            //               //                   ),
            //               //                 ],
            //               //               ),
            //               //             ),
            //               //           ),
            //               //         ),
            //               //         SizedBox(
            //               //           height: Dimensions.height10,
            //               //         ),
            //               //         BigText(
            //               //           text: "Individual Profiling",
            //               //           color: AppColors.black,
            //               //           size: Dimensions.font16 - 4,
            //               //         ),
            //               //         SizedBox(
            //               //           height: 5,
            //               //         ),
            //               //         Container(
            //               //             width: Dimensions.width45 + 40,
            //               //             child: Divider()),
            //               //         SmallText(
            //               //           text: "Farmer's Profiling",
            //               //           color: Colors.blueGrey,
            //               //           size: Dimensions.font16 - 4,
            //               //         )
            //               //       ],
            //               //     ),
            //               //
            //               //     //farmer agreement
            //               //     Column(
            //               //       children: [
            //               //         GestureDetector(
            //               //           onTap: () {
            //               //             Get.to(
            //               //               () => const FarmerAgreementScreen(),
            //               //               arguments: pageId,
            //               //             );
            //               //           },
            //               //           child: Container(
            //               //             height: Dimensions.height30 * 4,
            //               //             width: Dimensions.width30 * 4,
            //               //             decoration: BoxDecoration(
            //               //               color: AppColors.textWhite,
            //               //               borderRadius: BorderRadius.circular(
            //               //                   Dimensions.radius30 * 5),
            //               //             ),
            //               //             child: Center(
            //               //               child: Column(
            //               //                 mainAxisAlignment:
            //               //                     MainAxisAlignment.center,
            //               //                 children: [
            //               //                   Image.asset(
            //               //                     'assets/images/ic_1_2.png',
            //               //                     width: 60,
            //               //                     height: 60,
            //               //                   ),
            //               //                 ],
            //               //               ),
            //               //             ),
            //               //           ),
            //               //         ),
            //               //         SizedBox(
            //               //           height: Dimensions.height10,
            //               //         ),
            //               //         BigText(
            //               //           text: "Farmer Agreement",
            //               //           color: AppColors.black,
            //               //           size: Dimensions.font16 - 4,
            //               //         ),
            //               //         SizedBox(
            //               //           height: 5,
            //               //         ),
            //               //         Container(
            //               //             width: Dimensions.width45 + 40,
            //               //             child: Divider()),
            //               //         SmallText(
            //               //           text: "Organization Agreement",
            //               //           color: Colors.blueGrey,
            //               //           size: Dimensions.font16 - 4,
            //               //         )
            //               //       ],
            //               //     ),
            //               //   ],
            //               // ),
            //               // SizedBox(
            //               //   height: Dimensions.height45 + 45,
            //               // ),
            //               // Row(
            //               //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               //   children: [
            //               //     //additional information
            //               //     Column(
            //               //       children: [
            //               //         GestureDetector(
            //               //           onTap: () {
            //               //             Get.to(() => AdditionalInfoDashboardScreen(), arguments: pageId);
            //               //           },
            //               //           child: Container(
            //               //             height: Dimensions.height30 * 4,
            //               //             width: Dimensions.width30 * 4,
            //               //             decoration: BoxDecoration(
            //               //               color: AppColors.textWhite,
            //               //               borderRadius: BorderRadius.circular(
            //               //                   Dimensions.radius30 * 5),
            //               //             ),
            //               //             child: Center(
            //               //               child: Column(
            //               //                 mainAxisAlignment:
            //               //                     MainAxisAlignment.center,
            //               //                 children: [
            //               //                   Image.asset(
            //               //                     'assets/images/ic_1_1.png',
            //               //                     width: 60,
            //               //                     height: 60,
            //               //                   ),
            //               //                 ],
            //               //               ),
            //               //             ),
            //               //           ),
            //               //         ),
            //               //         SizedBox(
            //               //           height: Dimensions.height10,
            //               //         ),
            //               //         BigText(
            //               //           text: "Additional Information",
            //               //           color: AppColors.black,
            //               //           size: Dimensions.font16 - 4,
            //               //         ),
            //               //         SizedBox(
            //               //           height: 5,
            //               //         ),
            //               //         Container(
            //               //             width: Dimensions.width45 + 40,
            //               //             child: Divider()),
            //               //         SmallText(
            //               //           text: "Additional Information",
            //               //           color: Colors.blueGrey,
            //               //           size: Dimensions.font16 - 4,
            //               //         )
            //               //       ],
            //               //     ),
            //               //
            //               //     //farm mapping
            //               //     Column(
            //               //       children: [
            //               //         GestureDetector(
            //               //           onTap: () async {
            //               //             await checkIfLocationPermissionAllowed();
            //               //             await openModeOfMappingDialog();
            //               //           },
            //               //           child: Container(
            //               //             height: Dimensions.height30 * 4,
            //               //             width: Dimensions.width30 * 4,
            //               //             decoration: BoxDecoration(
            //               //               color: AppColors.textWhite,
            //               //               borderRadius: BorderRadius.circular(
            //               //                   Dimensions.radius30 * 5),
            //               //             ),
            //               //             child: Center(
            //               //               child: Column(
            //               //                 mainAxisAlignment:
            //               //                     MainAxisAlignment.center,
            //               //                 children: [
            //               //                   Image.asset(
            //               //                     'assets/images/ic_2_3.png',
            //               //                     width: 60,
            //               //                     height: 60,
            //               //                   ),
            //               //                 ],
            //               //               ),
            //               //             ),
            //               //           ),
            //               //         ),
            //               //         SizedBox(
            //               //           height: Dimensions.height10,
            //               //         ),
            //               //         BigText(
            //               //           text: "Farm Mapping",
            //               //           color: AppColors.black,
            //               //           size: Dimensions.font16 - 4,
            //               //         ),
            //               //         SizedBox(
            //               //           height: 5,
            //               //         ),
            //               //         Container(
            //               //             width: Dimensions.width45 + 40,
            //               //             child: Divider()),
            //               //         SmallText(
            //               //           text: "Mapping Farms",
            //               //           color: Colors.blueGrey,
            //               //           size: Dimensions.font16 - 4,
            //               //         )
            //               //       ],
            //               //     ),
            //               //   ],
            //               // ),
            //             ]
            //           ),
            //         )
            //       )
            // )
            SingleChildScrollView(
          child: Container(
            //height: Dimensions.screenHeight,
            width: Dimensions.screenWidth,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: Dimensions.height30 * Dimensions.height10 / 1.5,
                  child: Stack(
                    children: [
                      Container(
                        height: Dimensions.height15 * 10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height30 * 2,
                          left: Dimensions.width20,
                          right: Dimensions.width20,
                        ),
                        decoration: BoxDecoration(
                          color: AppColors.mainColor,
                          borderRadius: BorderRadius.only(
                            bottomLeft:
                                Radius.circular(Dimensions.radius30 * 4),
                            bottomRight:
                                Radius.circular(Dimensions.radius30 * 4),
                          ),
                        ),
                        child: SizedBox(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  IconButton(
                                    icon: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.white,
                                      size: Dimensions.iconSize24,
                                    ),
                                    onPressed: () {
                                      Get.to(() => const AllFarmersScreen());
                                    },
                                  ),
                                  BigText(
                                    text: "Single Farmer Dashboard",
                                    color: AppColors.textWhite,
                                    size: Dimensions.font20,
                                  ),
                                  SizedBox(
                                    width: Dimensions.width30,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: Dimensions.screenHeight,
                  padding: EdgeInsets.only(
                    left: Dimensions.height30,
                    right: Dimensions.height30,
                    top: Dimensions.height20,
                  ),
                  child: GridView.builder(
                    itemCount: items.length,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 3 / 2,
                            crossAxisSpacing: 30,
                            mainAxisSpacing: 30),
                    itemBuilder: (context, index) {
                      var item = items[index];
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            if (_selectedIndex == index) {
                              if (_selectedIndex == 0) {
                                // Get.to(
                                //   () => const FarmerDetailScreen(),
                                //   arguments: memberNo,
                                // );
                              } else if (_selectedIndex == 1) {
                                Get.to(
                                    () => const AdditionalInfoDashboardScreen(),
                                    arguments: memberNo);
                              } else if (_selectedIndex == 2) {
                                checkIfLocationPermissionAllowed();
                                openModeOfMappingDialog();
                              } else if (_selectedIndex == 3) {
                                /**go to technical update screen**/
                                // Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
                              } else if (_selectedIndex == 4) {
                                Get.to(() => const ImmediateActionsScreen(),
                                    arguments: memberNo);
                              } else if (_selectedIndex == 5) {
                                /**go to surveys screen**/
                              }
                            } else {
                              _selectedIndex = index;
                            }
                            print(_selectedIndex.toString());
                          });
                          if (_selectedIndex == 0) {
                            // Get.to(
                            //   () => const FarmerDetailScreen(),
                            //   arguments: memberNo,
                            // );
                          } else if (_selectedIndex == 1) {
                            Get.to(() => const AdditionalInfoDashboardScreen(),
                                arguments: memberNo);
                          } else if (_selectedIndex == 2) {
                            checkIfLocationPermissionAllowed();
                            openModeOfMappingDialog();
                          } else if (_selectedIndex == 3) {
                            /**go to technical update screen**/
                            // Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
                          } else if (_selectedIndex == 4) {
                            Get.to(() => const ImmediateActionsScreen(),
                                arguments: memberNo);
                          } else if (_selectedIndex == 5) {
                            /**go to surveys screen**/
                          }
                        },
                        child: Container(
                          height: Dimensions.height30 * 4,
                          width: Dimensions.width30 * 4,
                          decoration: _selectedIndex == index
                              ? BoxDecoration(
                                  color: AppColors.mainColor,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius20 / 4),
                                )
                              : BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius20 / 4),
                                  border: Border.all(
                                    width: 2,
                                    color: AppColors.mainColor,
                                  )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                item["image"].toString(),
                                width: 60,
                                height: 60,
                                color: _selectedIndex == index
                                    ? AppColors.textWhite
                                    : AppColors.mainColor,
                              ),
                              BigText(
                                text: item["name"].toString(),
                                color: AppColors.black,
                                size: Dimensions.font16 - 4,
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //dialog to choose mode of mapping
  Future<void> openModeOfMappingDialog() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SimpleDialog(
                title: Center(
                  child: BigText(
                    text: 'SELECT MODE OF MAPPING',
                    color: Colors.black,
                    size: Dimensions.font16,
                  ),
                ),
                children: [
                  SimpleDialogOption(
                    onPressed: () {},
                    child: GestureDetector(
                      child: Container(
                        width: Dimensions.width30 * Dimensions.width10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height15,
                          bottom: Dimensions.height15,
                          left: Dimensions.width30,
                          right: Dimensions.width30,
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.mainColor,
                                width: Dimensions.width10 / 2),
                            color: Colors.grey[400],
                            borderRadius: BorderRadius.circular(5)),
                        child: Center(
                          child: BigText(
                            text: "WALKING AROUND THE FARM",
                            color: Colors.black,
                            size: Dimensions.font12,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Get.to(() => const LiveLocationPage(),
                            arguments: memberNo.toString());
                      },
                    ),
                  ),
                  SimpleDialogOption(
                    onPressed: () {},
                    child: GestureDetector(
                      child: Container(
                        width: Dimensions.width30 * Dimensions.width10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height15,
                          bottom: Dimensions.height15,
                          left: Dimensions.width30,
                          right: Dimensions.width30,
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.mainColor,
                                width: Dimensions.width10 / 2),
                            color: Colors.grey[400],
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 3)),
                        child: Center(
                          child: BigText(
                            text: "PICKING POINTS",
                            color: Colors.black,
                            size: Dimensions.font12,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        Get.to(() => const PickPointMapping(),
                            arguments: memberNo.toString());
                      },
                    ),
                  ),
                ],
              );
            },
          );
        });
  }
}
