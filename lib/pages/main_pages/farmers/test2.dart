import 'package:farmcapturev2/pages/main_pages/visits/addgroup_trainingtopics.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import 'all_farmers.dart';

class MyHomePage2 extends StatefulWidget {
  const MyHomePage2({super.key});

  @override
  _MyHomePage2State createState() => _MyHomePage2State();
}

class _MyHomePage2State extends State<MyHomePage2> {
  int _selectedIndex = -1;

  final Color _defaultColor = Colors.grey[300]!;

  List<dynamic> items = [
    {"image": 'assets/images/ic_farmvisits.png', "name": "VISITS"},
    {"image": 'assets/images/ic_grouptraining.png', "name": "GROUP TRAINING"},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Colorful Containers'),
      ),
      body: Container(
        padding: EdgeInsets.only(
          left: Dimensions.height30,
          right: Dimensions.height30,
          top: Dimensions.height20,
        ),
        child: GridView.builder(
          itemCount: items.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 3 / 2,
              crossAxisSpacing: 30,
              mainAxisSpacing: 20),
          itemBuilder: (context, index) {
            var item = items[index];
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (_selectedIndex == index) {
                    _selectedIndex = -1;
                  } else {
                    _selectedIndex = index;
                  }
                  print(_selectedIndex.toString());
                });
                if (_selectedIndex == 0) {
                  Get.to(() => const AllFarmersScreen());
                } else if (_selectedIndex == 1) {
                  Get.to(() => const GroupTrainingTopicsScreen());
                }
              },
              child: Container(
                height: Dimensions.height30 * 4,
                width: Dimensions.width30 * 4,
                decoration: _selectedIndex == index ?
                BoxDecoration(
                  color: AppColors.mainColor,
                  borderRadius:
                      BorderRadius.circular(Dimensions.radius20 / 4),
                ):
                BoxDecoration(
                    color: AppColors.textWhite,
                    borderRadius: BorderRadius.circular(
                        Dimensions.radius20 / 4),
                    border: Border.all(
                      width: 2,
                      color: AppColors.mainColor,
                    )
                  ),
                // decoration: BoxDecoration(
                //   color: _selectedIndex == index
                //       ? AppColors.mainColor
                //       : _defaultColor,
                //   borderRadius: BorderRadius.circular(Dimensions.radius20 / 4),
                // ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      item["image"].toString(),
                      width: 60,
                      height: 60,
                      color: _selectedIndex == index ? AppColors.textWhite : AppColors.mainColor,
                    ),
                    BigText(
                      text: item["name"].toString(),
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
