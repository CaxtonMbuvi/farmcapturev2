
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:image_gallery_saver/image_gallery_saver.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/form_field.dart';
import '../../../resources/widgets/small_text.dart';

class FarmerAgreementScreen extends StatefulWidget {
  const FarmerAgreementScreen({Key? key}) : super(key: key);

  @override
  State<FarmerAgreementScreen> createState() => _FarmerAgreementScreenState();
}

class _FarmerAgreementScreenState extends State<FarmerAgreementScreen> {

  var pageId = Get.arguments;
  var dateLeafController = TextEditingController();
  var farmerNameController = TextEditingController();
  var farmerPhoneController = TextEditingController();
  var farmerIDController = TextEditingController();
  var farmerLocationController = TextEditingController();
  var farmerEmailController = TextEditingController();
  var farmerLRNumberController = TextEditingController();
  var farmerAcerageController = TextEditingController();
  var farmerbankNameController = TextEditingController();
  var farmeraccountNoController = TextEditingController();
  bool isLoading = false;
  List<dynamic> _singleFarmersLocal = [];
  String signaturePath = "";

  final GlobalKey<SfSignaturePadState> signatureGlobalKey = GlobalKey();

  _handleClearButtonPressed() async {
    signatureGlobalKey.currentState!.clear();
  }

  _handleSaveButtonPressed() async {
    RenderSignaturePad boundary = signatureGlobalKey.currentContext!.findRenderObject() as RenderSignaturePad;
    ui.Image image = await boundary.toImage();
    ByteData? byteData = await (image.toByteData(format: ui.ImageByteFormat.png));

    // ByteData byteData = await (image.toByteData(format: ui.ImageByteFormat.png) as FutureOr<ByteData>);
    if(byteData != null){
      final time = DateTime.now().millisecond;
      final name = "signature_$time.png";
      final result = await ImageGallerySaver.saveImage(byteData.buffer.asUint8List(), quality: 100, name: name);
      print("here saki ${result['filePath'].toString()}");

      setState(() {
        signaturePath = result['filePath'].toString();
        print("Here I am: $signaturePath");
      });
      print(result);

      final isSuccess = result['isSuccess'];
      // signatureGlobalKey.currentState!.clear();
      // if(isSuccess){
      //   await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context){
      //     return Scaffold(
      //       appBar: AppBar(),
      //       body: Center(
      //         child: Container(
      //           color: Colors.grey,
      //           child: Image.memory(byteData.buffer.asUint8List()),
      //         ),
      //       ),
      //     );
      //   }
      //   )
      //   );
      // }
    }
  }

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2050)
    );

    if(picked != null && picked != date){
      setState(() {
        date = picked;
        String formattedDate = DateFormat('dd-MM-yyyy').format(picked);
        print(formattedDate);
        dateLeafController.text = formattedDate;
        print(date.toString());
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }


  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal = await FarmersController().getSingleFarmerLocal(farmerId);

    if (_singleFarmersLocal.isNotEmpty) {

      print(_singleFarmersLocal[0]["full_name"]!.toString());
      farmerNameController.text = _singleFarmersLocal[0]["full_name"]!.toString();
      farmerPhoneController.text = _singleFarmersLocal[0]["phone1"]!.toString();
      farmerIDController.text = _singleFarmersLocal[0]["nat_id"]!.toString();
      // farmerEmailController.text = _singleFarmersLocal[0]["email"]!.toString();
      // farmerLocationController.text = _singleFarmersLocal[0]["gender_type_id"]!.toString();


      setState(() => isLoading = false);

    } else {
      print("Did not Get");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Farmer's Agreement",
                    color: AppColors.textWhite,
                    size: Dimensions.font28,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),

      body: SingleChildScrollView(
        child: SafeArea(
          child: isLoading
              ? SizedBox(
                height: Dimensions.screenHeight,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomLoader()
                  ],
                ),
              )
              :
              SizedBox(
                width: Dimensions.screenWidth,
                child: Column(
                  children: [
                    SizedBox(
                      height: Dimensions.height30 - 20,
                    ),

                    Container(
                        width: Dimensions.screenWidth,
                        padding: EdgeInsets.only(
                          top: Dimensions.height45,
                          left: Dimensions.width15 - 5,
                          right: Dimensions.width15 - 5,
                          bottom: Dimensions.height15,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(
                                Dimensions.radius20 * 2,
                              ),
                              topLeft: Radius.circular(
                                Dimensions.radius20 * 2,
                              ),
                            )
                        ),
                        margin: const EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: [
                            BigText(
                              text: "OUTGROWERS AGREEMENT FOR THE SUPPLY OF GREENLEAF PREAMBLE",
                              color: Colors.black,
                              size: Dimensions.font18 - 4,
                            ),

                            const SizedBox(height: 10,),

                            SmallText(
                              text: "We refer to your expression to deliver your green leaf to our factory dated",
                              color: Colors.black,
                              size: Dimensions.font16 - 2,
                            ),

                      Container(
                          padding: EdgeInsets.only(
                              top: Dimensions.height10, bottom: Dimensions.height10),
                          child: TextField(
                            autocorrect: true,
                            controller: dateLeafController,
                            keyboardType: TextInputType.datetime,
                            decoration: InputDecoration(
                              hintText: "Date Signed",
                              hintStyle: const TextStyle(color: Colors.white),
                              suffixIcon: IconButton(
                                color: Colors.black,
                                onPressed: (){
                                  selectedTimePicker(context);
                                },
                                icon: const Icon(Icons.calendar_month),
                              ),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: BorderSide(color: AppColors.textGrey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(Dimensions.radius15)),
                                borderSide: const BorderSide(color: Colors.green, width: 2),
                              ),
                            ),
                          )),

                            FormFields(
                              textEditingController: dateLeafController,
                              hintText: "Date Signed",
                              icon: Icons.calendar_month,
                            ),

                            const SizedBox(height: 10,),

                            SmallText(
                              text: "CAPTURE wishes to inform you that you have been listed as our out grower in the supply of green leaf subject to the terms and conditions below.",
                              color: Colors.black,
                              size: Dimensions.font18 - 4,
                            ),

                            const SizedBox(height: 10,),

                            SmallText(
                              text: "This agreement entered between  FACTORY  and ",
                              color: Colors.black,
                              size: Dimensions.font18 - 4,
                            ),

                            const SizedBox(height: 10,),

                            BigText(
                              text: "Farmer Name",
                              color: Colors.black,
                              size: Dimensions.font18 - 4,
                            ),
                            FormFields(
                              textEditingController: farmerNameController,
                              hintText: "Farmer Name",
                            ),

                            const SizedBox(height: 10,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [

                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        BigText(
                                          text: "Farmer Phone",
                                          color: Colors.black,
                                          size: Dimensions.font18 - 4,
                                        ),
                                        FormFields(
                                          textEditingController: farmerPhoneController,
                                          hintText: "Farmer Phone",
                                        ),
                                      ],
                                    )
                                ),

                                const SizedBox(width: 2,),

                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        BigText(
                                          text: "Farmer ID No:",
                                          color: Colors.black,
                                          size: Dimensions.font18 - 4,
                                        ),
                                        FormFields(
                                          textEditingController: farmerIDController,
                                          hintText: "Farmer ID No",
                                        ),
                                      ],
                                    )
                                ),

                              ],
                            ),

                            const SizedBox(height: 10,),

                            BigText(
                              text: "Farmer Location",
                              color: Colors.black,
                              size: Dimensions.font18 - 4,
                            ),
                            FormFields(
                              textEditingController: farmerLocationController,
                              hintText: "Farmer Location",
                            ),

                            const SizedBox(height: 10,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [

                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        BigText(
                                          text: "Farm LR Number",
                                          color: Colors.black,
                                          size: Dimensions.font18 - 4,
                                        ),
                                        FormFields(
                                          textEditingController: farmerLRNumberController,
                                          hintText: "Farm LR Number",
                                        ),
                                      ],
                                    )
                                ),

                                const SizedBox(width: 2,),

                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        BigText(
                                          text: "Farm Acerage",
                                          color: Colors.black,
                                          size: Dimensions.font18 - 4,
                                        ),
                                        FormFields(
                                          textEditingController: farmerAcerageController,
                                          hintText: "Farm Acerage",
                                        ),
                                      ],
                                    )
                                ),

                              ],
                            ),

                          ],
                        )
                    ),

                    SizedBox(height: Dimensions.height30,),

                    BigText(
                      text: "AGREEMENT DETAILS",
                      color: AppColors.black,
                      size: Dimensions.font20,
                    ),

                    Container(
                      width: Dimensions.screenWidth,
                      padding: EdgeInsets.only(
                        top: Dimensions.height45,
                        left: Dimensions.width15 - 5,
                        right: Dimensions.width15 - 5,
                        bottom: Dimensions.height15,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(
                              Dimensions.radius20 * 2,
                            ),
                            topLeft: Radius.circular(
                              Dimensions.radius20 * 2,
                            ),
                          )
                      ),
                      margin: const EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: [

                            BigText(
                              text: "Definitions",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            const SizedBox(height: 10,),

                            SmallText(
                              text: "1. The out grower: For the purpose of this agreement the Out grower will be understood to mean the supplier of green leaf from his/her own farm to the processor.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            const SizedBox(height: 8,),

                            SmallText(
                              text: "2. Processor: the processor will be understood to mean the Factory",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),


                            SizedBox(height: Dimensions.height10 + 10,),

                            BigText(
                              text: "CONDITIONS",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "The outgrowers",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            SmallText(
                              text: "1. Shall supply green leaf that measures to the processors quality requirement of minimum of 75% good leaf.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "2. Shall restrict their plucked green leaf to the acceptable standards of 2 leaves and a bud",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "3. Shall NOT use any prohibited chemicals in their farms.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "4. Commit to keep a comprehensive record of farm operations such as; plucking, pruning, fertilizer application, weeds control, management of pests and disease control.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "5. Commit to promote best agricultural practices and to enhance efficient agriculture, biodiversity and conservation.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "6. Out growers must be willing to abide by international best practices and standardization such as ISO 22000-2005, KEBS, NEMA, Rainforest alliance, Fair trade and any other standards that may be introduced from time to time.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "7. Commit to be part of the program in accordance with the processors outgrowership strategy.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "8. Be willing to participate in the processors fuel wood outgrowership strategy.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "9. Be willing to participate as an out grower in tea development initiatives.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "10. The out grower shall disclose acreage of his/her tea farm, clones planted, year of planting and proof of ownership.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "11. By signing this agreement, the out grower, demonstrates a commitment to exit from any green leaf supply agreements other than this one, in accordance with Tea Board Regulations.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "12. The out grower will meet the cost of green leaf transport from the farm to the factory.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "13. This contract will expire after three years upon which will be reviewed.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),


                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "Processor",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            SmallText(
                              text: "1. The processor shall circulate policy procedures and provide on farm technical advice from time to time.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "2. The Processor shall pay the out grower the initial payments (monthly payments) within ten days AFTER the end of each month for the green leaf delivered to the Factory. The buying price will be subject to review from time to time as dictated by market forces during the intervening period.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "3. The processor as part of its overall outgrowership strategy will register out grower’s tea and provide services related to tea.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "4. The processor shall further pay the out growers a bonus (yearly payment) within three months after the end of the financial year. This payment will be determined by the market forces during the intervening period.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "5. The processor commits to provide technical services in appropriate cultural agronomical practices in tea husbandry and Environmental conservation.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "6. The processor will be responsible for the management of green leaf transport logistics.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SmallText(
                              text: "7. All farm inputs supplied will be sourced competitively on terms to be agreed at the time of confirmed order.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "GENERAL CONDITIONS",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10 - 2,),

                            SmallText(
                              text: "The out grower and the processor pursuant to this agreement shall give unlimited access for: ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SizedBox(height: Dimensions.height10 - 2,),

                            SmallText(
                              text: "1. Inspection ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "2. Audits and ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "3. Visits by various certification bodies ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SizedBox(height: Dimensions.height10 - 5,),

                            SmallText(
                              text: "For the conditions above the out grower and the processor will enter into a relationship with in accordance with the processors operation strategy.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "SANCTIONING PROCEDURE",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            SmallText(
                              text: "a) Observation: identification of potential non-Conformities. Warning letter/ verbal warning is given at this stage",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "b)	In case of ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "i.	Minor Non-conformities, i.e., if a farmer does not comply with SAS 2020 requirements, the GA requests for corrective action ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "ii.	Major Non-conformities, i.e., when a farmer does not address minor non conformities or does not comply with core requirements, then the farmer is suspended.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "c)	Termination: When a farmer does not address the above non conformities after the Suspension period, then termination letter is issued.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "RIGHT TO APPEAL",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            SmallText(
                              text: "•	The farmer within one month of receiving warnings or suspension letter or termination letter have the right to appeal to the RA committee, in writing for the issues raised in regards to SEMS compliance.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "•	The Committee shall deliberate on the case on an agreed date based on the facts submitted by the aggrieved parties and guided by the Group Internal Management Systems and SAN Standards requirement make a ruling and inform the aggrieved parties. The ruling of the committee shall be communicated in writing to the appealing farm and it shall be final.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "RIGHT TO RESIGN",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            SmallText(
                              text: "• The farmer shall have the right to resign.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "•	The farmer shall give in writing 3 months’ notice to resign to the group administrator. The group administrator shall on receiving the letter reply back accepting resignation of the farmer upon the expiry of the notice. ",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "•	The farm products shall be excluded from the systems of the certified teas upon expiry of the notice.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "•	Farmers who have been inactive for 6 months shall be deemed to have resigned and shall be excluded from the SEMS",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),


                            SizedBox(height: Dimensions.height10,),

                            BigText(
                              text: "TERMINATION",
                              color: AppColors.black,
                              size: Dimensions.font20 - 2,
                            ),

                            SizedBox(height: Dimensions.height10,),

                            SmallText(
                              text: "• Either party is at liberty to terminate this agreement by giving a written notice of three months. This contract will be in force as and when both the processor and the out grower have an understanding.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                            SmallText(
                              text: "• Agree with the conditions set out in this agreement and confirm that all the information given is true and that I agree to take full responsibility for the errors and omissions contained in the information I have agreed to.",
                              color: Colors.black,
                              size: Dimensions.font16 - 4,
                            ),

                          ],
                        )
                    ),

                    SizedBox(height: Dimensions.height30,),

                    BigText(
                      text: "BANK DETAILS",
                      color: AppColors.black,
                      size: Dimensions.font20,
                    ),

                    Container(
                      width: Dimensions.screenWidth,
                      padding: EdgeInsets.only(
                        top: Dimensions.height45,
                        left: Dimensions.width15 - 5,
                        right: Dimensions.width15 - 5,
                        bottom: Dimensions.height15,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(
                              Dimensions.radius20 * 2,
                            ),
                            topLeft: Radius.circular(
                              Dimensions.radius20 * 2,
                            ),
                          )
                      ),
                      margin: const EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          BigText(
                            text: "Bank Name",
                            color: Colors.black,
                            size: Dimensions.font18 - 4,
                          ),
                          FormFields(
                            textEditingController: farmerbankNameController,
                            hintText: "Bank Name",
                          ),

                          BigText(
                            text: "Account Number",
                            color: Colors.black,
                            size: Dimensions.font18 - 4,
                          ),
                          FormFields(
                            textEditingController: farmeraccountNoController,
                            hintText: "Account Number",
                          ),

                        ],
                      ),
                    ),

                    SizedBox(height: Dimensions.height30,),

                    BigText(
                      text: "FARMER SIGNATURE",
                      color: AppColors.black,
                      size: Dimensions.font20,
                    ),

                    Container(
                      width: Dimensions.screenWidth,
                      padding: EdgeInsets.only(
                        top: Dimensions.height45,
                        left: Dimensions.width15 - 5,
                        right: Dimensions.width15 - 5,
                        bottom: Dimensions.height15,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(
                              Dimensions.radius20 * 2,
                            ),
                            topLeft: Radius.circular(
                              Dimensions.radius20 * 2,
                            ),
                          )
                      ),
                      margin: const EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey)
                              ),
                              child: SfSignaturePad(
                                key: signatureGlobalKey,
                                backgroundColor: Colors.white,
                                strokeColor: Colors.black,
                                minimumStrokeWidth: 3.0,
                                maximumStrokeWidth: 6.0,
                              ),
                            ),
                          ),

                          const SizedBox(height: 10,),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              TextButton(
                                  onPressed: _handleSaveButtonPressed,
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.black)),
                                  child: const Text("Save", style: TextStyle(fontSize: 15, color: Colors.white),)
                              ),

                              TextButton(
                                  onPressed: _handleClearButtonPressed,
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.black)),
                                  child: const Text("Clear", style: TextStyle(fontSize: 15, color: Colors.white),)
                              ),

                            ],
                          ),

                        ],
                      ),
                    ),

                    SizedBox(height: Dimensions.height20,),
                    
                    Container(
                      margin: EdgeInsets.only(bottom: Dimensions.height10),
                      child: Align(
                        alignment: Alignment.center,
                        child: GestureDetector(
                          child: Container(
                            width: Dimensions.height30 * Dimensions.height10,
                            padding: const EdgeInsets.only(
                                top: 13, bottom: 13, left: 25, right: 25),
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                BorderRadius.circular(Dimensions.radius20)
                              ),
                            child: Center(
                              child: BigText(
                                text: "save".toUpperCase(),
                                size: Dimensions.font18,
                                color: AppColors.textWhite,
                              )
                            ),
                          ),
                          onTap: () {
                            print("Next Agreement");
                            // _login();
                          },
                        ),
                      ),
                    )
                  ],
                ),

          ),

        )
      )

    );
  }
}
