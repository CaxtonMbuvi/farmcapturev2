import 'package:farmcapturev2/pages/main_pages/dashboard.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/all_farmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../visits/addgroup_trainingtopics.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({super.key});

  @override
  State<MenuScreen> createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  var _selectedIndex = Get.arguments;
  //var selectedDivision = Get.arguments[1];
  //int _selectedIndex = -1;

  List<dynamic> items = [
    {"image": 'assets/images/ic_farmvisits.png', "name": "VISITS"},
    {"image": 'assets/images/ic_grouptraining.png', "name": "GROUP TRAINING"},
    {"image": 'assets/images/ic_grouptraining.png', "name": "ADD FARMER"},
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

   //if(selectedDivision != null ) print("Division$selectedDivision");
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          //height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Container(
              //   width: Dimensions.screenWidth,
              //   height: Dimensions.height30 * 2,
              //   color: Colors.white,
              //
              //   child:  Center(
              //     child: BigText(
              //       text: "OUTGROWERS DASHBOARD",
              //       color: AppColors.black,
              //       size: Dimensions.font16,
              //     ),
              //   ),
              // ),
              //

              SizedBox(
                height: Dimensions.height30 * Dimensions.height10 / 1.5,
                child: Stack(
                  children: [
                    Container(
                      height: Dimensions.height15 * 10,
                      padding: EdgeInsets.only(
                        top: Dimensions.height30 * 2,
                        left: Dimensions.width20,
                        right: Dimensions.width20,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.mainColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(Dimensions.radius30 * 4),
                          bottomRight: Radius.circular(Dimensions.radius30 * 4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: Dimensions.iconSize24,
                                  ),
                                  onPressed: () {
                                    Get.to(() => const DashboardScreen(), arguments: 0);
                                  },
                                ),
                                BigText(
                                  text: "Outgrowers Dashboard",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font20,
                                ),
                                SizedBox(
                                  width: Dimensions.width30,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                height: Dimensions.screenHeight,
                padding: EdgeInsets.only(
                  left: Dimensions.height30,
                  right: Dimensions.height30,
                  top: Dimensions.height20,
                ),
                child: GridView.builder(
                  itemCount: items.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 30,
                      mainAxisSpacing: 30),
                  itemBuilder: (context, index) {
                    var item = items[index];
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          if (_selectedIndex == index) {
                            if (_selectedIndex == 0) {
                              Get.to(() => const AllFarmersScreen(),);
                            } else if (_selectedIndex == 1) {
                              Get.to(() => const GroupTrainingTopicsScreen(),);
                            }
                          } else {
                            _selectedIndex = index;
                          }
                        });
                        if (_selectedIndex == 0) {
                          Get.to(() => const AllFarmersScreen());
                        } else if (_selectedIndex == 1) {
                          Get.to(() => const GroupTrainingTopicsScreen(),);
                        }
                      },
                      child: Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 4,
                        decoration: _selectedIndex == index ?
                        BoxDecoration(
                          color: AppColors.mainColor,
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius20 / 4),
                        ):
                        BoxDecoration(
                            color: AppColors.textWhite,
                            borderRadius: BorderRadius.circular(
                                Dimensions.radius20 / 4),
                            border: Border.all(
                              width: 2,
                              color: AppColors.mainColor,
                            )
                          ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              item["image"].toString(),
                              width: 60,
                              height: 60,
                              color: _selectedIndex == index ? AppColors.textWhite : AppColors.mainColor,
                            ),
                            BigText(
                              text: item["name"].toString(),
                              color: AppColors.black,
                              size: Dimensions.font16 - 4,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
