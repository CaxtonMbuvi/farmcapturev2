import 'package:farmcapturev2/pages/main_pages/farmers/single_farmer_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../controllers/location/location_controller.dart';
import '../../../models/additional_info/nextofkin_model.dart';
import '../../../models/farmers/education_model.dart';
import '../../../models/farmers/gender_type_model.dart';
import '../../../models/farmers/location/county_model.dart';
import '../../../models/farmers/marital_status.dart';
import '../../../models/farmers/relations_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/custom_text_field.dart';

class AdditionalInfoScreen extends StatefulWidget {
  const AdditionalInfoScreen({super.key});

  @override
  State<AdditionalInfoScreen> createState() => _AdditionalInfoScreenState();
}

class _AdditionalInfoScreenState extends State<AdditionalInfoScreen> {
  var pageId = Get.arguments;
  var farmerNameController = TextEditingController();
  var farmerMemberNoController = TextEditingController();

  var kinbirthCertController = TextEditingController();
  var kinfullNameController = TextEditingController();
  var kindobController = TextEditingController();
  var kinisSchooledController = TextEditingController();
  var kinrelationshipIdController = TextEditingController();
  var kinMaritalStatusController = TextEditingController();
  var kinphoneNoController = TextEditingController();
  var kinageController = TextEditingController();
  var kinGenderTypeController = TextEditingController();
  var kinLevelOfEducationController = TextEditingController();
  var kinisOrphanedController = TextEditingController();

  NextOfKinModelResultElement kinModel = NextOfKinModelResultElement();

  //Drop Down Lists Here
  final List<LevelOfEducationResultElement> _educationLevels = [];
  final List<RelationResultElement> _relationLevels = [];
  final List<GenderResultElement> _genderTypes = [];
  final List<MaritalStatusResultElement> _maritalStatus = [];
  final List<CountyResultElement> _colCenter1 = [];

// Selected Dropdown elements

  GenderResultElement? selectedGender;
  MaritalStatusResultElement? selectedMaritalStatus;
  LevelOfEducationResultElement? selectedLevelOfEducation;
  RelationResultElement? selectedRelationship;

  /// for date picker*
  DateTime date = DateTime.now();
  Future<void> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        kindobController.text = formattedDate;

        print(date.toString());
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    var res = await FarmersController().getSingleFarmerLocal(farmerId);
    if (res != null) {
      _allFarmersLocal = res;
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        farmerNameController.text =
            _allFarmersLocal[0]["full_name"]!.toString();
        farmerMemberNoController.text =
            _allFarmersLocal[0]["member_no"]!.toString();
        _getLocationDetails();

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    }
  }

  _getLocationDetails() async {
    List<dynamic> resGender = [];
    var res = await LocationController().getLocationsLocal(genderTypeTable);
    //print("Hello there"+ res[0].toString());
    if (res != null) {
      resGender = res;
      for (var i in resGender) {
        GenderResultElement model = GenderResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _genderTypes.add(model);
      }
    }

    //selectedGender = _genderTypes[0];

    List<dynamic> resMaritalStatus = [];
    var res1 = await LocationController().getLocationsLocal(maritalStatusTable);
    //print("Hello there"+ res[0].toString());
    if (res1 != null) {
      resMaritalStatus = res1;
      for (var i in resMaritalStatus) {
        MaritalStatusResultElement model = MaritalStatusResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _maritalStatus.add(model);
      }
    }

    //selectedMaritalStatus = _maritalStatus[0];

    List<dynamic> resEducation = [];
    var res2 =
        await LocationController().getLocationsLocal(educationLevelTable);
    //print("Hello there"+ res[0].toString());
    if (res2 != null) {
      resEducation = res2;
      for (var i in resEducation) {
        LevelOfEducationResultElement model = LevelOfEducationResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _educationLevels.add(model);
      }
    }

    List<dynamic> resRelation = [];
    var res3 = await LocationController().getLocationsLocal(relationsTable);
    //print("Hello there"+ res[0].toString());
    if (res3 != null) {
      resRelation = res3;
      for (var i in resRelation) {
        RelationResultElement model = RelationResultElement(
            resultId: i['Id'],
            code: i['code'],
            name: i['name'],
            datecomparer: i['datecomparer']);

        _relationLevels.add(model);
      }
    }

    //selectedNationality = _nationalities[0];
    setState(() => isLoading = false);
  }

  _addInfo() async {
    setState(() => isLoading = true);

    kinModel.sid = 'p';
    kinModel.memberNo = farmerMemberNoController.text;
    kinModel.fullName = kinfullNameController.text;
    kinModel.birthCertificateNo = kinbirthCertController.text;
    kinModel.dateOfBirth = kindobController.text;
    kinModel.relationshipId = kinrelationshipIdController.text;
    kinModel.maritalStatusId = kinMaritalStatusController.text;
    kinModel.phonenumber = kinphoneNoController.text;
    kinModel.genderTypeId = kinGenderTypeController.text;
    kinModel.levelOfEducationId = kinLevelOfEducationController.text;

    List<dynamic> kins = [];
    kins.add(kinModel);

    int i = await FarmersController().saveAll(kins, nextKinTable);

    if (i >= 0) {
      print("Success here");
    }

    setState(() => isLoading = false);
    Get.to(() => const SingleFarmerDashboard(),arguments: pageId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Farmer Profile",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: isLoading
              ? const CustomLoader()
              : Container(
                  color: AppColors.textWhite,
                  width: Dimensions.screenWidth,
                  child: Column(
                    children: [
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                      BigText(
                        text: "Next Of Kin",
                        color: AppColors.mainColor,
                        size: Dimensions.font20,
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),

                      CustomTextField(
                        readOnly: false,
                        controller: kinfullNameController,
                        onSaved: (value) {},
                        labelText: "Name",
                        isObscure: false,
                        onTap: (value) {},
                      ),

                      CustomTextField(
                        readOnly: false,
                        controller: kinphoneNoController,
                        onSaved: (value) {},
                        labelText: "Phone Number",
                        isObscure: false,
                        onTap: (value) {},
                      ),

                      //dob
                      Container(
                        margin: const EdgeInsets.all(10.0),
                        child: TextFormField(
                          onTap: () {
                            selectedTimePicker(context);
                          },
                          controller: kindobController,
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.text,
                          readOnly: true,
                          style: const TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            border: const UnderlineInputBorder(),
                            focusColor: AppColors.mainColor,
                            //hintText: "Date Of Birth",
                            contentPadding: const EdgeInsets.only(left: 5),
                            suffixIcon: const Icon(Icons.calendar_month),
                            labelText: "Date Of Birth",
                          ),
                        ),
                      ),

                      CustomTextField(
                        readOnly: false,
                        controller: kinbirthCertController,
                        onSaved: (value) {},
                        labelText: "Birth Certificate Number",
                        isObscure: false,
                        onTap: (value) {},
                      ),
                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      Container(
                        margin: const EdgeInsets.all(10.0),
                        child: DropdownButton<GenderResultElement>(
                          hint: const Text("Gender"),
                          value: selectedGender,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: Colors.white,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: const TextStyle(color: Colors.black, fontSize: 16),
                          onChanged: (GenderResultElement? newValue) async {
                            setState(() {
                              selectedGender = newValue;
                              kinGenderTypeController.text =
                                  selectedGender!.resultId.toString();
                              print(
                                  "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
                            });
                          },
                          items: _genderTypes.map((GenderResultElement gender) {
                            return DropdownMenuItem<GenderResultElement>(
                              value: gender,
                              child: Text(gender.name.toString(),
                                  style: const TextStyle(color: Colors.black)),
                            );
                          }).toList(),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      Container(
                        margin: const EdgeInsets.all(10.0),
                        child: DropdownButton<LevelOfEducationResultElement>(
                          hint: const Text("Level Of Education"),
                          value: selectedLevelOfEducation,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: Colors.white,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: const TextStyle(color: Colors.black, fontSize: 16),
                          onChanged:
                              (LevelOfEducationResultElement? newValue) async {
                            setState(() {
                              selectedLevelOfEducation = newValue;
                              kinLevelOfEducationController.text =
                                  selectedLevelOfEducation!.resultId.toString();
                              print(
                                  "Here is LevelOfEducation ==*** ${selectedLevelOfEducation!.name.toString()} and id === ${selectedLevelOfEducation!.resultId.toString()}");
                            });
                          },
                          items: _educationLevels
                              .map((LevelOfEducationResultElement value) {
                            return DropdownMenuItem<
                                LevelOfEducationResultElement>(
                              value: value,
                              child: Text(value.name.toString(),
                                  style: const TextStyle(color: Colors.black)),
                            );
                          }).toList(),
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      Container(
                        margin: const EdgeInsets.all(10.0),
                        child: DropdownButton<RelationResultElement>(
                          hint: const Text("Relationship"),
                          value: selectedRelationship,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: Colors.white,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: const TextStyle(color: Colors.black, fontSize: 16),
                          onChanged: (RelationResultElement? newValue) async {
                            setState(() {
                              selectedRelationship = newValue;
                              kinrelationshipIdController.text =
                                  selectedRelationship!.resultId.toString();
                              print(
                                  "Here is Relationship ==*** ${selectedRelationship!.name.toString()} and id === ${selectedRelationship!.resultId.toString()}");
                            });
                          },
                          items: _relationLevels
                              .map((RelationResultElement value) {
                            return DropdownMenuItem<RelationResultElement>(
                              value: value,
                              child: Text(value.name.toString(),
                                  style: const TextStyle(color: Colors.black)),
                            );
                          }).toList(),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      Container(
                        margin: const EdgeInsets.all(10.0),
                        child: DropdownButton<MaritalStatusResultElement>(
                          hint: const Text("Marital Status"),
                          value: selectedMaritalStatus,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: Colors.white,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: const TextStyle(color: Colors.black, fontSize: 16),
                          onChanged:
                              (MaritalStatusResultElement? newValue) async {
                            setState(() {
                              selectedMaritalStatus = newValue;
                              kinMaritalStatusController.text =
                                  selectedMaritalStatus!.resultId.toString();
                              print(
                                  "Here is Marital status ==*** ${selectedMaritalStatus!.name.toString()} and id === ${selectedMaritalStatus!.resultId.toString()}");
                            });
                          },
                          items: _maritalStatus
                              .map((MaritalStatusResultElement value) {
                            return DropdownMenuItem<MaritalStatusResultElement>(
                              value: value,
                              child: Text(value.name.toString(),
                                  style: const TextStyle(color: Colors.black)),
                            );
                          }).toList(),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height30,
                      ),

                      GestureDetector(
                        onTap: () {
                          _addInfo();
                        },
                        child: Container(
                          height: Dimensions.height30 * 2,
                          width: Dimensions.width30 * Dimensions.width10,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius30),
                          ),
                          child: Center(
                            child: BigText(
                              text: "Submit",
                              color: AppColors.textWhite,
                              size: Dimensions.font16,
                            ),
                          ),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height30,
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
