import 'package:farmcapturev2/pages/main_pages/farmers/menu.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/single_farmer_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import '../../../controllers/farmers/farmers_controller.dart';
import '../../../controllers/location/location_controller.dart';
import 'package:intl/intl.dart';
import '../../../models/visit/visitinspection_model.dart';
import '../../../models/visit/visitreasonlist_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../dashboard.dart';

class AllVisitFarmersTestScreen extends StatefulWidget {
  const AllVisitFarmersTestScreen({super.key});

  @override
  State<AllVisitFarmersTestScreen> createState() =>
      _AllVisitFarmersTestScreenState();
}

class _AllVisitFarmersTestScreenState extends State<AllVisitFarmersTestScreen> {
  String farmer_name = "";
  String date = "";
  String farmerId = '';
  final List<VisitReasonModelResultElement> _visitReasons = [];
  List<dynamic> _singleFarmersLocal = [];
  List<dynamic> _allPreviousVisits = [];
  VisitReasonModelResultElement? _selectedVisitReason;
  Position? _currentPosition;

  var dateController = TextEditingController();
  var stringDateController = TextEditingController();
  var memberNoController = TextEditingController();
  var transactionCodeController = TextEditingController();
  var visitReasonController = TextEditingController();
  //var placeController = TextEditingController();
  var notesController = TextEditingController();
  var latController = TextEditingController();
  var longController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAllFarmersLocal();
  }

  String searchString = "";

  //List<dynamic> _allFarmers = [];
  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];

  Future _getAllFarmersLocal() async {
    setState(() => isLoading = true);
    print("At Function");

    try {
      _allFarmersLocal = await FarmersController().getAllFarmersLocal();
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        print(_allFarmersLocal.length);

        DateTime today = DateTime.now();
        String dateStr = "${today.day}-${today.month}-${today.year}";
        dateController.text = dateStr;

        date = getDate(DateTime.now().toString());
        stringDateController.text = date;
        transactionCodeController.text =
            'VST_${DateTime.now().millisecondsSinceEpoch.toString()}';

        await _getVisitReasons();

        await _getCurrentPosition();

        setState(() => isLoading = false);

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    } catch (e) {
      setState(() => isLoading = false);
      await openModeOfMappingDialog();
    }
  }

  String getDate(String a) {
    var date = DateTime.parse(a.substring(0, 10));
    return DateFormat('EEEE, d MMM, yyyy').format(date).toString();
  }

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal = [];
    var res = await FarmersController().getSingleFarmerLocal(farmerId);

    if (res != null) {
      setState(() {
        _singleFarmersLocal = res;
      });
      if (_singleFarmersLocal.isNotEmpty) {
        print(_singleFarmersLocal[0]["full_name"]!.toString());
        // farmer_name = _singleFarmersLocal[0]["full_name"]!.toString();
        memberNoController.text =
            _singleFarmersLocal[0]["member_no"]!.toString();
        //dateController.text = DateTime.now().toString();
        DateTime today = DateTime.now();
        String dateStr = "${today.day}-${today.month}-${today.year}";
        dateController.text = dateStr;

        date = getDate(DateTime.now().toString());
        stringDateController.text = date;
        transactionCodeController.text =
            'VST_${DateTime.now().millisecondsSinceEpoch.toString()}';

        print(farmer_name);

        await _getAllVisits(memberNoController.text);

        await _getVisitReasons();

        await _getCurrentPosition();

        //await _addVisitPopUp();

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
  }

  _getVisitReasons() async {
    List<dynamic> resVisitReasons = [];
    var res = await LocationController().getLocationsLocal(visitReasonTable);
    //print("Hello there"+ res[0].toString());
    if (res != null) {
      setState(() {
        resVisitReasons = res;
      });
      for (var i in resVisitReasons) {
        VisitReasonModelResultElement model = VisitReasonModelResultElement(
            resultId: i['Id'],
            name: i['name'],
            code: i['code'],
            datecomparer: i['datecomparer']);

        _visitReasons.add(model);
      }
    }
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() {
        latController.text = position.latitude.toString();
        longController.text = position.longitude.toString();
      });
    }).catchError((e) {
      debugPrint(e.toString());
    });
  }

  _getAllVisits(var memberNo) async {
    _allPreviousVisits = [];
    try {
      var res = await FarmersController().getAllFarmerVisits(memberNo);
      if (res != null) {
        setState(() {
          _allPreviousVisits = res;
        });
        print("Found the visits");
      } else {
        print("Found No visits");
      }
    } catch (e) {
      print("Error on Visits");
    }
  }

  _checkVisit() async {
    var res;
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    try {
      res = await FarmersController()
          .checkFarmerVisits(memberNoController.text, dateStr);
      return res;
    } catch (e) {}
  }

  _addVisit() async {
    if (visitReasonController.text.isNotEmpty) {
      //setState(() => isLoading = true);

      VisitInspectionModelResultElement visitModel =
          VisitInspectionModelResultElement();
      visitModel.visitTransactionCode = transactionCodeController.text;
      visitModel.memberNo = memberNoController.text;
      visitModel.lat = latController.text;
      visitModel.lon = longController.text;
      visitModel.visitDate = dateController.text;
      visitModel.visitReason = visitReasonController.text;
      visitModel.syncId = 'p';
      visitModel.comments = notesController.text;

      print("Visit Model${visitModel.toJson()}");

      List<dynamic> assVisits = [];
      assVisits.add(visitModel);

      try {
        var res = await _checkVisit();
        if (res != null) {
          print(res.toString());
          print("Visit Already Exists");
          Navigator.of(context).pop();
        } else {
          int i = await FarmersController()
              .saveAll(assVisits, farmerVisitInspectionTable);
          await _getAllVisits(memberNoController.text);
          setState(() {});
          Navigator.of(context).pop();
          Get.to(() => const SingleFarmerDashboard(), arguments: farmerId);
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          Get.to(() => const MenuScreen());
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Dimensions.height30 * 2),
          child: AppBar(
            backgroundColor: Colors.green,
            actions: [
              Container(
                width: Dimensions.screenWidth,
                height: Dimensions.height30 * 2,
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                  color: Colors.green,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(5, 5),
                        color: AppColors.gradientOne.withOpacity(0.1)),
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(-5, -5),
                        color: AppColors.gradientOne.withOpacity(0.1))
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const MenuScreen());
                      },
                    ),
                    BigText(
                      text: "Visits test",
                      color: AppColors.textWhite,
                      size: Dimensions.font28,
                    ),
                    const SizedBox()
                  ],
                ),
              ),
            ],
          ),
        ),
        body: isLoading
            ? SizedBox(
                height: Dimensions.screenHeight,
                child: const Center(
                  child: CustomLoader(),
                ))
            : SingleChildScrollView(
                child: Container(
                  width: Dimensions.screenWidth,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/bg.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                      SizedBox(
                        height: Dimensions.height30 * 3,
                        child: Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  width: Dimensions.screenWidth / 1.3,
                                  padding: EdgeInsets.only(
                                    top: Dimensions.height10,
                                    bottom: Dimensions.height10,
                                  ),
                                  child: Center(
                                    child: TextField(
                                      onChanged: (value) {
                                        setState(() {
                                          searchString =
                                              value.toString().toLowerCase();
                                        });
                                      },
                                      autocorrect: true,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                        hintText: "Search Visit",
                                        hintStyle:
                                            const TextStyle(color: Colors.grey),
                                        filled: true,
                                        fillColor: Colors.white,
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                  Dimensions.radius30)),
                                          borderSide: BorderSide(
                                              color: AppColors.textGrey,
                                              width: 2),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                  Dimensions.radius30)),
                                          borderSide: BorderSide(
                                              color: AppColors.mainColor,
                                              width: 2),
                                        ),
                                      ),
                                    ),
                                  )),
                              Flexible(
                                child: Container(
                                  width: Dimensions.width30 * 2,
                                  height: Dimensions.height30 * 2,
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width15,
                                    right: Dimensions.width15,
                                  ),
                                  child: Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.magnifyingGlass,
                                      size: Dimensions.height20,
                                      color: AppColors.black,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      // SizedBox(
                      //   height: Dimensions.height30 * 6,
                      //   child: Center(
                      //     child: Row(
                      //       crossAxisAlignment: CrossAxisAlignment.center,
                      //       mainAxisAlignment: MainAxisAlignment.center,
                      //       children: [
                      //         Container(
                      //             width: Dimensions.screenWidth / 1.3,
                      //             padding: EdgeInsets.only(
                      //               top: Dimensions.height10,
                      //               bottom: Dimensions.height10,
                      //             ),
                      //             child: Center(
                      //               child: TextField(
                      //                 onChanged: (value) {
                      //                   setState(() {
                      //                     searchString = value.toString().toLowerCase();
                      //                   });
                      //                 },
                      //                 autocorrect: true,
                      //                 keyboardType: TextInputType.text,
                      //                 decoration: InputDecoration(
                      //                   hintText: "Search Farmer",
                      //                   hintStyle:
                      //                       TextStyle(color: Colors.grey),
                      //                   filled: true,
                      //                   fillColor: Colors.white,
                      //                   enabledBorder: OutlineInputBorder(
                      //                     borderRadius: BorderRadius.all(
                      //                         Radius.circular(
                      //                             Dimensions.radius30)),
                      //                     borderSide: BorderSide(
                      //                         color: AppColors.textGrey,
                      //                         width: 2),
                      //                   ),
                      //                   focusedBorder: OutlineInputBorder(
                      //                     borderRadius: BorderRadius.all(
                      //                         Radius.circular(
                      //                             Dimensions.radius30)),
                      //                     borderSide: BorderSide(
                      //                         color: Colors.lightGreenAccent,
                      //                         width: 2),
                      //                   ),
                      //                 ),
                      //               ),
                      //             )),
                      //         Flexible(
                      //           child: Container(
                      //             width: Dimensions.width30 * 2,
                      //             height: Dimensions.height30 * 2,
                      //             padding: EdgeInsets.only(
                      //               left: Dimensions.width15,
                      //               right: Dimensions.width15,
                      //             ),
                      //             child: Center(
                      //               child: FaIcon(
                      //                 FontAwesomeIcons.magnifyingGlass,
                      //                 size: Dimensions.height20,
                      //                 color: Colors.white,
                      //               ),
                      //             ),
                      //           ),
                      //         )
                      //       ],
                      //     ),
                      //   ),
                      // ),
                      Container(
                        width: Dimensions.screenWidth,
                        padding: EdgeInsets.only(
                          top: Dimensions.height45,
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                        ),
                        decoration: BoxDecoration(
                            color: AppColors.textWhite,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(
                                Dimensions.radius20 * 2,
                              ),
                              topLeft: Radius.circular(
                                Dimensions.radius20 * 2,
                              ),
                            )),
                        child: Column(
                          children: [
                            SizedBox(
                              child: Column(
                                children: [
                                  ListView.separated(
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: _allFarmersLocal.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return _allFarmersLocal[index]
                                                  ["full_name"]!
                                              .toString()
                                              .toLowerCase()
                                              .contains(searchString)
                                          ? Padding(
                                              padding: EdgeInsets.only(
                                                  left: Dimensions.width10,
                                                  right: Dimensions.width10,
                                                  top: Dimensions.height10),
                                              child: GestureDetector(
                                                  onTap: () async {
                                                    setState(() {
                                                      farmer_name =
                                                          _allFarmersLocal[
                                                                      index]
                                                                  ["full_name"]!
                                                              .toString();
                                                      memberNoController.text =
                                                          _allFarmersLocal[
                                                                      index]
                                                                  ["member_no"]!
                                                              .toString();
                                                      farmerId =
                                                          _allFarmersLocal[
                                                                  index]["Id"]!
                                                              .toString();
                                                    });
                                                    var res =
                                                        await _checkVisit();
                                                    if (res == null) {
                                                      await _addVisitPopUp();
                                                    }

                                                    // if (res == null)
                                                    //   _getSingleFarmerLocal(
                                                    //       _allFarmersLocal[
                                                    //               index]["Id"]!
                                                    //           .toString());
                                                    else {
                                                      Get.to(
                                                          () =>
                                                              const SingleFarmerDashboard(),
                                                          arguments:
                                                              _allFarmersLocal[
                                                                          index]
                                                                      ["Id"]!
                                                                  .toString());
                                                    }
                                                  },
                                                  child: Container(
                                                    height:
                                                        Dimensions.height30 * 4,
                                                    width:
                                                        Dimensions.screenWidth,
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey[300],
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                Dimensions
                                                                    .radius20)),
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                        left:
                                                            Dimensions.width20,
                                                        right:
                                                            Dimensions.width15,
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          BigText(
                                                            text:
                                                                "Name: ${_allFarmersLocal[index]["full_name"].toString()}",
                                                            color: AppColors
                                                                .mainColor,
                                                            size: Dimensions
                                                                .font16,
                                                          ),
                                                          BigText(
                                                            text:
                                                                "Nat ID: ${_allFarmersLocal[index]["nat_id"].toString()}",
                                                            color:
                                                                AppColors.black,
                                                            size: Dimensions
                                                                .font16,
                                                          ),
                                                          BigText(
                                                            text:
                                                                "Phone: ${_allFarmersLocal[index]["phone1"].toString()}",
                                                            color:
                                                                AppColors.black,
                                                            size: Dimensions
                                                                .font16,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )),
                                            )
                                          : Container();
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return _allFarmersLocal[index]
                                                  ["full_name"]!
                                              .toString()
                                              .toLowerCase()
                                              .contains(searchString)
                                          ? Container()
                                          : Container();
                                    },
                                  ),
                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),
                                  Container(
                                    height: Dimensions.height30 * 2,
                                    width: Dimensions.screenWidth,
                                    padding: EdgeInsets.only(
                                        left: Dimensions.width20,
                                        right: Dimensions.width20,
                                        top: Dimensions.height10),
                                    decoration: BoxDecoration(
                                        color: Colors.grey[300],
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20)),
                                    child: Center(
                                      child: BigText(
                                        text: "No Item",
                                        color: AppColors.black,
                                        size: Dimensions.font18,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  //dialog to choose mode of mapping
  Future<void> openModeOfMappingDialog() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: contentBox(context),
          );
        });
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "No Farmers",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Kindly SYNC to continue",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: GestureDetector(
                    onTap: () {
                      Get.back();
                      Get.to(() => const DashboardScreen());
                    },
                    child: BigText(
                      text: "Ok",
                      color: Colors.black,
                      size: Dimensions.font16,
                    ),
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }

  _addVisitPopUp() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(
            margin: EdgeInsets.all(Dimensions.height10),
            child: AlertDialog(
              insetPadding: EdgeInsets.zero,
              title: Center(
                child: Column(
                  children: [
                    BigText(
                      text: "NEW VISIT FORM",
                      color: AppColors.black,
                      size: Dimensions.font16,
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    SizedBox(
                        width: Dimensions.screenWidth, child: const Divider()),
                  ],
                ),
              ),
              content: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: SizedBox(
                    child: Column(
                      children: [
                        Container(
                          width: Dimensions.screenWidth,
                          padding: EdgeInsets.only(
                            left: Dimensions.width10 / 2,
                            right: Dimensions.width10 / 2,
                          ),
                          decoration: BoxDecoration(
                              //color: Colors.green,
                              borderRadius: BorderRadius.only(
                            topRight: Radius.circular(
                              Dimensions.radius20,
                            ),
                            topLeft: Radius.circular(
                              Dimensions.radius20,
                            ),
                          )),

                          //farmer name
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BigText(
                                text: "Farmer Name",
                                color: AppColors.black,
                                size: Dimensions.font16,
                              ),
                              Container(
                                width: Dimensions.screenWidth,
                                decoration: const ShapeDecoration(
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                      width: 1.0,
                                      style: BorderStyle.solid,
                                      color: Colors.lightGreen,
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                  ),
                                ),
                                padding: EdgeInsets.only(
                                  top: Dimensions.height20,
                                  bottom: Dimensions.height20,
                                  left: Dimensions.height10,
                                  right: Dimensions.height10,
                                ),
                                child: BigText(
                                  text: farmer_name,
                                  size: Dimensions.font16,
                                  color: Colors.black38,
                                ),
                              ),

                              SizedBox(
                                height: Dimensions.height15,
                              ),

                              //visit date
                              BigText(
                                text: "Visit Date",
                                color: AppColors.black,
                                size: Dimensions.font16,
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                  bottom: Dimensions.height10,
                                ),
                                child: TextField(
                                  autocorrect: true,
                                  readOnly: true,
                                  controller: stringDateController,
                                  keyboardType: TextInputType.datetime,
                                  decoration: InputDecoration(
                                    hintText: "Date",
                                    hintStyle: const TextStyle(color: Colors.white),
                                    filled: true,
                                    fillColor: Colors.white70,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              Dimensions.radius15 - 5)),
                                      borderSide: const BorderSide(
                                          color: Colors.lightGreen, width: 1),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(Dimensions.radius15)),
                                      borderSide: const BorderSide(
                                          color: Colors.green, width: 1),
                                    ),
                                  ),
                                ),
                              ),

                              SizedBox(
                                height: Dimensions.height10 / 2,
                              ),

                              //lat long
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  //latitude
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        BigText(
                                          text: "GPS Latitude ",
                                          color: Colors.black,
                                          size: Dimensions.font16,
                                        ),
                                        Container(
                                          width: Dimensions.screenWidth / 3,
                                          padding: EdgeInsets.only(
                                            top: Dimensions.height20,
                                            bottom: Dimensions.height20,
                                            left: Dimensions.height10,
                                            right: Dimensions.height10,
                                          ),
                                          decoration: const ShapeDecoration(
                                            shape: RoundedRectangleBorder(
                                              side: BorderSide(
                                                width: 1.0,
                                                style: BorderStyle.solid,
                                                color: Colors.lightGreen,
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5.0)),
                                            ),
                                          ),
                                          child: BigText(
                                            text: latController.text.toString(),
                                            color: Colors.black38,
                                            size: Dimensions.font16,
                                          ),
                                        ),
                                      ]),

                                  //longitude
                                  Column(
                                    children: [
                                      BigText(
                                        text: "GPS Longitude ",
                                        color: Colors.black,
                                        size: Dimensions.font16,
                                      ),
                                      Container(
                                        width: Dimensions.screenWidth / 3,
                                        padding: EdgeInsets.only(
                                          top: Dimensions.height20,
                                          bottom: Dimensions.height20,
                                          left: Dimensions.height10,
                                          right: Dimensions.height10,
                                        ),
                                        decoration: const ShapeDecoration(
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                              width: 1.0,
                                              style: BorderStyle.solid,
                                              color: Colors.lightGreen,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5.0)),
                                          ),
                                        ),
                                        child: BigText(
                                          text: longController.text.toString(),
                                          // color: Color.fromARGB(255, 77, 76, 76),
                                          color: Colors.black38,
                                          size: Dimensions.font16,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: Dimensions.height10,
                              ),

                              BigText(
                                text: "Visit Reason",
                                color: Colors.black,
                                size: Dimensions.font16,
                              ),

                              Container(
                                width: Dimensions.screenWidth,
                                decoration: const ShapeDecoration(
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                      width: 1.0,
                                      style: BorderStyle.solid,
                                      color: Colors.lightGreen,
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                  ),
                                ),
                                padding: EdgeInsets.only(
                                  top: Dimensions.height20,
                                  bottom: Dimensions.height20,
                                  left: Dimensions.height10,
                                  right: Dimensions.height10,
                                ),
                                // margin: const EdgeInsets.all(10.0),
                                child: DropdownButton<
                                    VisitReasonModelResultElement>(
                                  hint: const Text("Visit Reason"),
                                  value: _selectedVisitReason,
                                  isExpanded: true,
                                  isDense: true,
                                  dropdownColor: Colors.white,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 16),
                                  onChanged: (VisitReasonModelResultElement?
                                      newValue) async {
                                    setState(() {
                                      _selectedVisitReason = newValue;
                                      visitReasonController.text =
                                          _selectedVisitReason!.name.toString();
                                      print(
                                          "Here is reason ==*** ${_selectedVisitReason!.name.toString()} and id === ${_selectedVisitReason!.resultId.toString()}");
                                    });
                                  },
                                  items: _visitReasons.map(
                                      (VisitReasonModelResultElement item) {
                                    return DropdownMenuItem<
                                        VisitReasonModelResultElement>(
                                      value: item,
                                      child: Text(item.name.toString(),
                                          style:
                                              const TextStyle(color: Colors.black)),
                                    );
                                  }).toList(),
                                ),
                              ),

                              SizedBox(
                                height: Dimensions.height15,
                              ),

                              //visit comments
                              BigText(
                                text: "Visit Comments(Short Description)",
                                color: Colors.black,
                                size: Dimensions.font16,
                              ),
                              Container(
                                //height: Dimensions.height30* Dimensions.height45,
                                padding: EdgeInsets.only(
                                    top: Dimensions.height10,
                                    bottom: Dimensions.height10),
                                child: TextField(
                                  maxLines: 2,
                                  autocorrect: true,
                                  controller: notesController,
                                  keyboardType: TextInputType.multiline,
                                  decoration: InputDecoration(
                                    hintText: "Notes",
                                    hintStyle: const TextStyle(color: Colors.white),
                                    filled: true,
                                    fillColor: Colors.white70,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              Dimensions.radius15 - 5)),
                                      borderSide: const BorderSide(
                                          color: Colors.lightGreen, width: 1),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(Dimensions.radius15)),
                                      borderSide: const BorderSide(
                                          color: Colors.green, width: 2),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: Dimensions.height15,
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                height: Dimensions.height30 * 2,
                                width: Dimensions.screenWidth / 3,

                                // width: Dimensions.width30 * Dimensions.width10,
                                decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                  child: BigText(
                                    text: "Cancel",
                                    color: AppColors.textWhite,
                                    size: Dimensions.font16,
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                _addVisit();
                              },
                              child: Container(
                                height: Dimensions.height30 * 2,
                                width: Dimensions.screenWidth / 3,
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Center(
                                  child: BigText(
                                    text: "Submit",
                                    color: AppColors.textWhite,
                                    size: Dimensions.font16,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                      ],
                    ),
                  )),
              // actions: <Widget>[
              //   TextButton(
              //     child: Text('Cancel'),
              //     onPressed: () {
              //       Navigator.of(context).pop();
              //     },
              //   ),
              //   TextButton(
              //     child: Text('OK'),
              //     onPressed: () {
              //       setState(() {
              //         Navigator.of(context).pop();
              //       });
              //     },
              //   ),
              // ],
            ),
          );
        });
  }
}
