import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../models/additional_info/nextofkin_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import 'alp_signature.dart';

class AlpChildren extends StatefulWidget {
  const AlpChildren({super.key});

  @override
  State<AlpChildren> createState() => _AlpChildrenState();
}

class _AlpChildrenState extends State<AlpChildren> {
  bool isLoading = false;
  List<dynamic> allActions = [];
  List<bool> checkedItems = [];
  List<String> selectedItems = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getChildren();
  }

  _getChildren() async {
    setState(() => isLoading = true);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    String memberNo = localStorage.getString("SelectedFarmerId").toString();

    var res = await FarmersController().getAllChildrenLocal(memberNo);
    if (res != null) {
      for (var i in res) {
        var item = {
          "Id": i['Id'],
          "full_name": i['full_name'],
        };

        allActions.add(item);
      }
    }
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(20),
      child: Container(
        constraints: const BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            isLoading
                ? const Center(
                    child: CustomLoader(),
                  )
                : Padding(
                    padding: EdgeInsets.all(Dimensions.height10),
                    child: BigText(
                      text: "Children",
                      color: AppColors.black,
                    ),
                  ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  left: Dimensions.height15,
                  right: Dimensions.height15,
                  top: Dimensions.height20,
                ),
                child: ListView.builder(
                  itemCount: allActions.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    var item = allActions[index];
                    return Column(
                      children: [
                        SizedBox(
                          child: CheckboxListTile(
                            title: Text('${item["full_name"].toString()}'),
                            value: selectedItems.contains(item["full_name"]),
                            onChanged: (bool? value) {
                              setState(() {
                                selectedItems.contains(item["full_name"])
                                    ? selectedItems.remove(item["full_name"])
                                    : selectedItems.add(item["full_name"]);
                              });
                            },
                          ),
                        ),
                        const Divider()
                      ],
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 20),
            // TODO: Display data from the database

            Padding(
              padding: EdgeInsets.all(Dimensions.height10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Back",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      if (selectedItems.isEmpty) {
                        Fluttertoast.showToast(
                            msg: "You did not select an A child");
                      } else {
                        SharedPreferences localStorage =
                            await SharedPreferences.getInstance();
                        localStorage.setString('SelectedAlpActionPlans',
                            jsonEncode({"AlpActionPlans": selectedItems}));

                        //print(localStorage.getString("SelectedAlpReasons").toString());
                        Fluttertoast.showToast(
                            msg:
                                "You did not select a Reason ${localStorage.getString("SelectedAlpReasons").toString()}");
                        _toSignaturePad();
                      }
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Submit",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _toSignaturePad() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return const AlpSignature();
      },
    );
  }
}
