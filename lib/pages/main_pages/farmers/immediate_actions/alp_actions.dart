import 'dart:convert';

import 'package:farmcapturev2/controllers/farmers/farmers_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import 'alp_child_select.dart';

class AlpActionPlan extends StatefulWidget {
  const AlpActionPlan({super.key, required this.selectedItem});
  final String selectedItem;

  @override
  State<AlpActionPlan> createState() => _AlpActionPlanState();
}

class _AlpActionPlanState extends State<AlpActionPlan> {
  bool isLoading = false;
  List<dynamic> allActions = [];
  List<bool> checkedItems = [];
  List<int> selectedItems = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getActions();
  }

  _getActions() async {
    setState(() => isLoading = true);
    var res2 = await FarmersController()
        .getReasonsActions(widget.selectedItem, "ACTION PLAN");
    List<dynamic> resReasons = [];

    if (res2 != null) {
      resReasons = res2;
      print(res2.toString());
      for (var i in resReasons) {
        var item = {
          "Id": i['Id'],
          "answer": i['answer'],
        };

        allActions.add(item);
      }

      checkedItems = List<bool>.generate(resReasons.length, (index) => false);
    } else {
      Get.back();
    }
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.all(20),
      child: Container(
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            isLoading
                ? const Center(
                    child: CustomLoader(),
                  )
                : Padding(
                    padding: EdgeInsets.all(Dimensions.height10),
                    child: BigText(
                      text: "Action Plan",
                      color: AppColors.black,
                    ),
                  ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  left: Dimensions.height15,
                  right: Dimensions.height15,
                  top: Dimensions.height20,
                ),
                child: ListView.builder(
                  itemCount: allActions.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    var item = allActions[index];
                    return Column(
                      children: [
                        SizedBox(
                          child: CheckboxListTile(
                            title: Text('${item["answer"].toString()}'),
                            value: selectedItems.contains(item["Id"]),
                            onChanged: (bool? value) {
                              setState(() {
                                selectedItems.contains(item["Id"])
                                    ? selectedItems.remove(item["Id"])
                                    : selectedItems.add(item["Id"]);
                              });
                            },
                          ),
                        ),
                        const Divider()
                      ],
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 20),
            // TODO: Display data from the database

            Padding(
              padding: EdgeInsets.all(Dimensions.height10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Back",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      if (selectedItems.isEmpty) {
                        Fluttertoast.showToast(
                            msg: "You did not select an Action Plan");
                      } else {
                        SharedPreferences localStorage =
                            await SharedPreferences.getInstance();
                        localStorage.setString('SelectedAlpActionPlans',
                            jsonEncode({"AlpActionPlans": selectedItems}));

                        //print(localStorage.getString("SelectedAlpReasons").toString());
                        // Fluttertoast.showToast(
                        //     msg: "You did not select a Reason ${localStorage.getString("SelectedAlpReasons").toString()}");
                        _toAlpChildren();
                      }
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Submit",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _toAlpChildren() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return const AlpChildren();
      },
    );
  }
}
