import 'package:farmcapturev2/models/farmers/additional_info/inputquestions_model.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/immediate_actions/immediate_actions.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../controllers/farmers/farmers_controller.dart';
import '../../../../controllers/location/location_controller.dart';
import '../../../../models/farmers/additional_info/questionChoice_model.dart';
import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import 'alp_questions.dart';

class ALPTypesScreen extends StatefulWidget {
  const ALPTypesScreen({super.key});

  @override
  State<ALPTypesScreen> createState() => _ALPTypesScreenState();
}

class _ALPTypesScreenState extends State<ALPTypesScreen> {
  var pageId = Get.arguments;
  var _selectedIndex = Get.arguments;
  List<dynamic> alpTypesItems = [];
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAlpTypes();
  }

  _getAlpTypes() async {
    setState(() => isLoading = true);
    List<dynamic> resAlps = [];

    var response = await FarmersController().getAlpQuestionsCategories('ALP');

    // print("Reserves"+ response[0].toString());

    if (response != null) {
      resAlps = response;

      for (var i in resAlps) {
        var item = {
          "Id": i['Id'],
          "mainCategory": i['mainCategory'],
          "main_category_id": i['main_category_id'],
        };

        alpTypesItems.add(item);
      }
    }

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    
    //Fluttertoast.showToast(msg: localStorage.getString("SelectedFarmerId").toString());

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Dimensions.screenWidth,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? CustomLoader()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: Dimensions.height30 * Dimensions.height10 / 1.5,
                    child: Stack(
                      children: [
                        Container(
                          height: Dimensions.height15 * 10,
                          padding: EdgeInsets.only(
                            top: Dimensions.height30 * 2,
                            left: Dimensions.width20,
                            right: Dimensions.width20,
                          ),
                          decoration: BoxDecoration(
                            color: AppColors.mainColor,
                            borderRadius: BorderRadius.only(
                              bottomLeft:
                                  Radius.circular(Dimensions.radius30 * 4),
                              bottomRight:
                                  Radius.circular(Dimensions.radius30 * 4),
                            ),
                          ),
                          child: SizedBox(
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.white,
                                        size: Dimensions.iconSize24,
                                      ),
                                      onPressed: () {
                                        Get.to(
                                            () =>
                                                const ImmediateActionsScreen(),
                                            arguments: pageId);
                                      },
                                    ),
                                    BigText(
                                      text: "ALP TYPES",
                                      color: AppColors.textWhite,
                                      size: Dimensions.font20,
                                    ),
                                    SizedBox(
                                      width: Dimensions.width30,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(
                        left: Dimensions.height30,
                        right: Dimensions.height30,
                        top: Dimensions.height20,
                      ),
                      child: ListView.builder(
                        itemCount: alpTypesItems.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          var item = alpTypesItems[index];
                          return GestureDetector(
                            onTap: () async {
                              setState(() {
                                _selectedIndex = index;
                              });

                              Get.to(() => const AlpMainQuestionsScreen(), arguments: item["main_category_id"].toString());
                            },
                            child: Center(
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.screenWidth,
                                margin: const EdgeInsets.only(bottom: 20),
                                padding: EdgeInsets.only(
                                  left: Dimensions.width30,
                                ),
                                decoration: _selectedIndex == index
                                    ? BoxDecoration(
                                        color: AppColors.mainColor,
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20 / 4),
                                      )
                                    : BoxDecoration(
                                        color: AppColors.textWhite,
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20),
                                        border: Border.all(
                                          width: 2,
                                          color: AppColors.mainColor,
                                        )),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    BigText(
                                      text: item["mainCategory"].toString(),
                                      color: AppColors.black,
                                      size: Dimensions.font16 - 4,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
