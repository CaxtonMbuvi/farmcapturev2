import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'dart:ui' as ui;

import '../../../../resources/base/custom_loader.dart';
import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';

class AlpSignature extends StatefulWidget {
  const AlpSignature({super.key});

  @override
  State<AlpSignature> createState() => _AlpSignatureState();
}

class _AlpSignatureState extends State<AlpSignature> {
  bool isLoading = false;
  final GlobalKey<SfSignaturePadState> signatureGlobalKey = GlobalKey();
  String memberNo = "";

  _getFarmerNo() async {
    setState(() => isLoading = true);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    String _memberNo = localStorage.getString("SelectedFarmerId").toString();
    memberNo = _memberNo;

    setState(() => isLoading = false);
  }

  void _handleClearButtonPressed() {
    signatureGlobalKey.currentState!.clear();
  }

  Future<void> saveSignature() async {
    ui.Image image = await signatureGlobalKey.currentState!.toImage(pixelRatio: 3.0);
    ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData!.buffer.asUint8List();

    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    String imageFolderPath = '$appDocPath/imageSignatures';
    String imagePath = '$imageFolderPath/signature$memberNo.png';

    Directory(imageFolderPath).createSync(recursive: true);

    File imageFile = File(imagePath);
    await imageFile.writeAsBytes(pngBytes);

    print('Signature saved at: $imagePath');
  }

  // void _handleSaveButtonPressed() async {
  //   final data =
  //       await signatureGlobalKey.currentState!.toImage(pixelRatio: 3.0);
  //   final bytes = await data.toByteData(format: ui.ImageByteFormat.png);
  //   print(Image.memory(bytes!.buffer.asUint8List()).toString());
  //   // await Navigator.of(context).push(
  //   //   MaterialPageRoute(
  //   //     builder: (BuildContext context) {
  //   //       return Scaffold(
  //   //         appBar: AppBar(),
  //   //         body: Center(
  //   //           child: Container(
  //   //             color: Colors.grey[300],
  //   //             child: Image.memory(bytes!.buffer.asUint8List()),
  //   //           ),
  //   //         ),
  //   //       );
  //   //     },
  //   //   ),
  //   // );
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getFarmerNo();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.symmetric(horizontal: 30, vertical: 40),
      child: Container(
        constraints: const BoxConstraints.expand(),
        child: Column(
          children: [
            isLoading
                ? const Center(
                    child: CustomLoader(),
                  )
                : Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: BigText(
                      text: "Farmer's Signature",
                      color: AppColors.black,
                    ),
                  ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.grey)),
                  child: SfSignaturePad(
                    key: signatureGlobalKey,
                    backgroundColor: Colors.white,
                    strokeColor: Colors.black,
                    minimumStrokeWidth: 3.0,
                    maximumStrokeWidth: 5.0,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.all(Dimensions.height10),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      _handleClearButtonPressed();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        //borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Clear",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.all(Dimensions.height10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Back",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      saveSignature();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10 / 2,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(Dimensions.width10),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Submit",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
