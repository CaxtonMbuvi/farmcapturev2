import 'dart:convert';
import 'dart:io';
import 'package:farmcapturev2/pages/main_pages/farmers/immediate_actions/alp_types.dart';
import 'package:farmcapturev2/pages/main_pages/farmers/single_farmer_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';

class ImmediateActionsScreen extends StatefulWidget {
  const ImmediateActionsScreen({super.key});

  @override
  State<ImmediateActionsScreen> createState() => _ImmediateActionsScreenState();
}

class _ImmediateActionsScreenState extends State<ImmediateActionsScreen> {
  var pageId = Get.arguments;
  var _selectedIndex = -1;
  List<dynamic> items = [
    {
      "image": 'assets/images/icupdatefarmer_profile.png',
      "name": "Raise Prompt Action"
    },
    {
      "image": 'assets/images/ic_farmer_agreement.png',
      "name": "Pending Prompt Action"
    },
    {"image": 'assets/images/dot.png', "name": "Closed Prompt Action"},
  ];

  bool isLoading = false;

  _setFarmerId() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('SelectedFarmerId', pageId);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          Get.to(() => const SingleFarmerDashboard(), arguments: pageId);
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
            child: Container(
                width: Dimensions.screenWidth,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: Dimensions.height30 * Dimensions.height10 / 1.5,
                        child: Stack(
                          children: [
                            Container(
                              height: Dimensions.height15 * 10,
                              padding: EdgeInsets.only(
                                top: Dimensions.height30 * 2,
                                left: Dimensions.width20,
                                right: Dimensions.width20,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.mainColor,
                                borderRadius: BorderRadius.only(
                                  bottomLeft:
                                      Radius.circular(Dimensions.radius30 * 4),
                                  bottomRight:
                                      Radius.circular(Dimensions.radius30 * 4),
                                ),
                              ),
                              child: SizedBox(
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        IconButton(
                                          icon: Icon(
                                            Icons.arrow_back_ios,
                                            color: Colors.white,
                                            size: Dimensions.iconSize24,
                                          ),
                                          onPressed: () {
                                            Get.to(
                                                () =>
                                                    const SingleFarmerDashboard(),
                                                arguments: pageId);
                                          },
                                        ),
                                        BigText(
                                          text: "ALP DASHBOARD",
                                          color: AppColors.textWhite,
                                          size: Dimensions.font20,
                                        ),
                                        SizedBox(
                                          width: Dimensions.width30,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          height: Dimensions.screenHeight,
                          padding: EdgeInsets.only(
                            left: Dimensions.height30,
                            right: Dimensions.height30,
                            top: Dimensions.height20,
                          ),
                          child: GridView.builder(
                            itemCount: items.length,
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 3 / 2,
                                    crossAxisSpacing: 30,
                                    mainAxisSpacing: 30),
                            itemBuilder: (context, index) {
                              var item = items[index];
                              return GestureDetector(
                                onTap: () async{
                                  setState(() {
                                    _setFarmerId();
                                    
                                    if (_selectedIndex == index) {
                                      if (_selectedIndex == 0) {
                                        
                                        Get.to(
                                          () => const ALPTypesScreen(),
                                          arguments: pageId,
                                        );
                                      } else if (_selectedIndex == 1) {
                                        // Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
                                      } else if (_selectedIndex == 2) {
                                        // Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
                                      }
                                    } else {
                                      _selectedIndex = index;
                                    }
                                    print(_selectedIndex.toString());
                                  });
                                  if (_selectedIndex == 0) {
                                    Get.to(
                                      () => const ALPTypesScreen(),
                                      arguments: pageId,
                                    );
                                  } else if (_selectedIndex == 1) {
                                    // Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
                                  } else if (_selectedIndex == 2) {
                                    // Get.to(() => const AdditionalInfoDashboardScreen(), arguments: pageId);
                                  }
                                },
                                child: Container(
                                  height: Dimensions.height30 * 4,
                                  width: Dimensions.width30 * 4,
                                  decoration: _selectedIndex == index
                                      ? BoxDecoration(
                                          color: AppColors.mainColor,
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius20 / 4),
                                        )
                                      : BoxDecoration(
                                          color: AppColors.textWhite,
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius20 / 4),
                                          border: Border.all(
                                            width: 2,
                                            color: AppColors.mainColor,
                                          )),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        item["image"].toString(),
                                        width: 60,
                                        height: 60,
                                        color: _selectedIndex == index
                                            ? AppColors.textWhite
                                            : AppColors.mainColor,
                                      ),
                                      BigText(
                                        text: item["name"].toString(),
                                        color: AppColors.black,
                                        size: Dimensions.font16 - 4,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ))
                    ]))),
      ),
    );
  }
}
