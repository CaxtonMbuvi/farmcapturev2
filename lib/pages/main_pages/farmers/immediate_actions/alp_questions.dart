import 'package:farmcapturev2/controllers/farmers/farmers_controller.dart';
import 'package:farmcapturev2/resources/base/custom_loader.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../resources/utils/colors.dart';
import '../../../../resources/utils/dimensions.dart';
import '../../../../resources/widgets/big_text.dart';
import 'alp_reasons.dart';
import 'alp_types.dart';

class AlpMainQuestionsScreen extends StatefulWidget {
  const AlpMainQuestionsScreen({super.key});

  @override
  State<AlpMainQuestionsScreen> createState() => _AlpMainQuestionsScreenState();
}

class _AlpMainQuestionsScreenState extends State<AlpMainQuestionsScreen> {
  var pageId = Get.arguments;
  bool isLoading = false;
  var _selectedIndex = Get.arguments;
  List<dynamic> questionReasons = [];
  List<bool> checkedItems = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getQuestions();
  }

  _getQuestions() async {
    setState(() => isLoading = true);
    var res = await FarmersController().getAlpQuestions(pageId);
    List<dynamic> resQuestions = [];

    if (res != null) {
      resQuestions = res;
      print(res.toString());

      for (var i in resQuestions) {
        var item = {
          "Id": i['Id'],
          "name": i['name'],
          "selected": false,
        };

        questionReasons.add(item);
      }

      checkedItems = List<bool>.generate(resQuestions.length, (index) => false);
    }

    setState(() => isLoading = false);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: Dimensions.screenWidth,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: isLoading
          ? const Center(
              child: CustomLoader(),
            )
          : Column(
              children: [
                SizedBox(
                  height: Dimensions.height30 * Dimensions.height10 / 1.5,
                  child: Stack(
                    children: [
                      Container(
                        height: Dimensions.height15 * 10,
                        padding: EdgeInsets.only(
                          top: Dimensions.height30 * 2,
                          left: Dimensions.width20,
                          right: Dimensions.width20,
                        ),
                        decoration: BoxDecoration(
                          color: AppColors.mainColor,
                          borderRadius: BorderRadius.only(
                            bottomLeft:
                                Radius.circular(Dimensions.radius30 * 4),
                            bottomRight:
                                Radius.circular(Dimensions.radius30 * 4),
                          ),
                        ),
                        child: SizedBox(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  IconButton(
                                    icon: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.white,
                                      size: Dimensions.iconSize24,
                                    ),
                                    onPressed: () {
                                      Get.to(() => const ALPTypesScreen(),
                                          arguments: pageId);
                                    },
                                  ),
                                  BigText(
                                    text: "ALP Questions",
                                    color: AppColors.textWhite,
                                    size: Dimensions.font20,
                                  ),
                                  SizedBox(
                                    width: Dimensions.width30,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: Dimensions.height15,
                      right: Dimensions.height15,
                      top: Dimensions.height20,
                    ),
                    child: ListView.builder(
                      itemCount: questionReasons.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) {
                        var item = questionReasons[index];
                        //bool valuefirst = false;
                        return Column(
                          children: [
                            InkWell(
                              child: SizedBox(
                                child: CheckboxListTile(
                                  title: Text('${item["name"].toString()}'),
                                  value: checkedItems[index],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      checkedItems[index] = value!;
                                    });
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlpReasons(selectedItem: item["Id"].toString());
                                      },
                                    );
                                  },
                                ),
                              ),
                            ),
                            const Divider()
                          ],
                        );
                        // GestureDetector(
                        //   onTap: () async {
                        //     setState(() {
                        //       _selectedIndex = index;
                        //     });
                        //   },
                        //   child: Center(
                        //     child: Container(
                        //       height: Dimensions.height30 * 4,
                        //       width: Dimensions.screenWidth,
                        //       margin: const EdgeInsets.only(bottom: 20),
                        //       padding: EdgeInsets.only(
                        //         left: Dimensions.width30,
                        //       ),
                        //       decoration: _selectedIndex == index
                        //           ? BoxDecoration(
                        //               color: AppColors.mainColor,
                        //               borderRadius: BorderRadius.circular(
                        //                   Dimensions.radius20 / 4),
                        //             )
                        //           : BoxDecoration(
                        //               color: AppColors.textWhite,
                        //               borderRadius: BorderRadius.circular(
                        //                   Dimensions.radius20),
                        //               border: Border.all(
                        //                 width: 2,
                        //                 color: AppColors.mainColor,
                        //               )),
                        //       child: Column(
                        //         mainAxisAlignment: MainAxisAlignment.start,
                        //         children: [
                        //           BigText(
                        //             text: item["name"].toString(),
                        //             color: AppColors.black,
                        //             size: Dimensions.font16 - 4,
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ),
                        // );
                      },
                    ),
                  ),
                ),
              ],
            ),
    ));
  }

  //dialog to choose mode of mapping
  Future<void> openModeOfMappingDialog() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SimpleDialog(
                title: Center(
                  child: BigText(
                    text: 'ALP Reasons',
                    color: Colors.black,
                    size: Dimensions.font16,
                  ),
                ),
                children: [],
              );
            },
          );
        });
  }
}
