import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/models/visit/visitreasonlist_model.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../controllers/location/location_controller.dart';
import '../../../models/visit/visitinspection_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/base/snackbar.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class AddVisitScreen extends StatefulWidget {
  const AddVisitScreen({super.key});

  @override
  State<AddVisitScreen> createState() => _AddVisitScreenState();
}

class _AddVisitScreenState extends State<AddVisitScreen> {
  var pageId = Get.arguments;

  bool isLoading = false;
  List<dynamic> _singleFarmersLocal = [];
  String farmer_name = "";
  String date = "";
  final List<VisitReasonModelResultElement> _visitReasons = [];

  List<dynamic> _allPreviousVisits = [];
  VisitReasonModelResultElement? _selectedVisitReason;
  Position? _currentPosition;

  var dateController = TextEditingController();
  var stringDateController = TextEditingController();
  var memberNoController = TextEditingController();
  var transactionCodeController = TextEditingController();
  var visitReasonController = TextEditingController();
  var visitReasonIdController = TextEditingController();
  var notesController = TextEditingController();
  var latController = TextEditingController();
  var longController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getSingleFarmerLocal(pageId);
  }

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal = [];
    var res = await FarmersController().getSingleFarmerLocal(farmerId);

    if (res != null) {
      setState(() {
        _singleFarmersLocal = res;
      });
      if (_singleFarmersLocal.isNotEmpty) {
        print(_singleFarmersLocal[0]["full_name"]!.toString());
        farmer_name = _singleFarmersLocal[0]["full_name"]!.toString();
        memberNoController.text =
            _singleFarmersLocal[0]["member_no"]!.toString();
        //dateController.text = DateTime.now().toString();
        DateTime today = DateTime.now();
        String dateStr = "${today.day}-${today.month}-${today.year}";
        dateController.text = dateStr;

        date = getDate(DateTime.now().toString());
        stringDateController.text = date;
        transactionCodeController.text =
            'VST_${DateTime.now().millisecondsSinceEpoch.toString()}';

        print(farmer_name);

        await _getAllVisits(memberNoController.text);

        await _getVisitReasons();

        await _getCurrentPosition();

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
  }

  _getVisitReasons() async {
    List<dynamic> resVisitReasons = [];
    var res = await LocationController().getLocationsLocal(visitReasonTable);
    //print("Hello there"+ res[0].toString());
    if (res != null) {
      setState(() {
        resVisitReasons = res;
      });
      for (var i in resVisitReasons) {
        VisitReasonModelResultElement model = VisitReasonModelResultElement(
            resultId: i['Id'],
            name: i['name'],
            code: i['code'],
            datecomparer: i['datecomparer']);

        _visitReasons.add(model);
      }
    }
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() {
        latController.text = position.latitude.toString();
        longController.text = position.longitude.toString();
      });
    }).catchError((e) {
      debugPrint(e.toString());
    });
  }

  String getDate(String a) {
    var date = DateTime.parse(a.substring(0, 10));
    return DateFormat('EEE, MMM d , yyyy').format(date).toString();
  }

  String getDateb(String dateString) {
    //String dateString = '6-3-2023';
    List<String> dateParts = dateString.split('-');
    String reformattedDateString =
        '${dateParts[2]}-${dateParts[1]}-${dateParts[0]}';
    DateTime dateTime = DateTime.parse(reformattedDateString);

    //var date = DateTime.parse(dateTime.to.substring(0, 10));
    return DateFormat('EEE, MMM d , yyyy').format(dateTime).toString();
  }

  _checkVisit() async {
    var res;
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    try {
      res = await FarmersController()
          .checkFarmerVisits(memberNoController.text, dateStr);
      return res;
    } catch (e) {}
  }

  _getAllVisits(var memberNo) async {
    _allPreviousVisits = [];
    try {
      var res = await FarmersController().getAllFarmerVisits(memberNo);
      if (res != null) {
        setState(() {
          _allPreviousVisits = res;
        });
        print("Found the visits");
      } else {
        print("Found No visits");
      }
    } catch (e) {
      print("Error on Visits");
    }
  }

  _addVisit() async {
    if (visitReasonController.text.isNotEmpty) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var UserId = localStorage.getString('UserId');
      //setState(() => isLoading = true);

      VisitInspectionModelResultElement visitModel =
          VisitInspectionModelResultElement();
      visitModel.visitTransactionCode = transactionCodeController.text;
      visitModel.memberNo = memberNoController.text;
      visitModel.lat = latController.text;
      visitModel.lon = longController.text;
      visitModel.visitDate = dateController.text;
      visitModel.visitReason = visitReasonController.text;
      visitModel.syncId = 'p';
      visitModel.comments = notesController.text;
      visitModel.userId = UserId.toString();
      visitModel.visitReasonId = visitReasonIdController.text;

      print("Visit Model${visitModel.toJson()}");

      List<dynamic> assVisits = [];
      assVisits.add(visitModel);

      try {
        var res = await _checkVisit();
        if (res != null) {
          print(res.toString());
          print("Visit Already Exists");
          Navigator.of(context).pop();
        } else {
          int i = await FarmersController()
              .saveAll(assVisits, farmerVisitInspectionTable);
          await _getAllVisits(memberNoController.text);
          setState(() {});
          Navigator.of(context).pop();
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Visits",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            color: Colors.white,
            width: Dimensions.screenWidth,
            child: isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
                : SizedBox(
                    height: Dimensions.screenHeight - Dimensions.height30 * 3,
                    child: Stack(
                      children: [
                        SizedBox(
                          height: Dimensions.height20 *
                              (Dimensions.height45 - Dimensions.height30 / 4),
                          child: ListView.builder(
                            shrinkWrap: true,
                            //physics: const NeverScrollableScrollPhysics(),
                            itemCount: _allPreviousVisits.length,
                            itemBuilder: (context, index) {
                              //final item = visits[index];
                              return Container(
                                margin:
                                    EdgeInsets.only(left: Dimensions.width20),
                                decoration: const BoxDecoration(),
                                //width: Dimensions.screenWidth / 1.5,
                                child: Row(
                                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Image.asset(
                                      'assets/images/ic_user.png',
                                      width: Dimensions.width30 * 2,
                                      height: Dimensions.width30 * 2,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SmallText(
                                          text:
                                              "Visit Reason: ${_allPreviousVisits[index]['visit_reason']}",
                                          color: Colors.black,
                                          size: Dimensions.font12 + 1,
                                        ),
                                        SmallText(
                                          text:
                                              "Date Of Visit: ${_allPreviousVisits[index]['visit_date'].toString()}",
                                          color: Colors.black,
                                          size: Dimensions.font12 + 1,
                                        ),
                                        SmallText(
                                          text: "Start Time: ",
                                          color: Colors.black,
                                          size: Dimensions.font12 + 1,
                                        ),
                                        SmallText(
                                          text: "End Time: ",
                                          color: Colors.black,
                                          size: Dimensions.font12 + 1,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            await FarmersController()
                                .deleteFarmerVisits(farmerVisitInspectionTable);
                          },
                          child: BigText(
                            text: "Clear",
                            color: AppColors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: EdgeInsets.only(
                              bottom: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              onTap: () async {
                                var res = await _checkVisit();
                                if (res == null) _addVisitPopUp();
                                showCustomSnackBar("Visit Already Added",
                                    context, ContentType.failure,
                                    title: "Failed");
                              },
                              child: Container(
                                height: Dimensions.height30 * 2,
                                width:
                                    Dimensions.screenWidth - Dimensions.width45,
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30),
                                ),
                                child: Center(
                                  child: BigText(
                                    text: "Add Visit",
                                    color: AppColors.textWhite,
                                    size: Dimensions.font16,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  _addVisitPopUp() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            insetPadding: EdgeInsets.zero,
            title: Center(
              child: BigText(
                text: "Add Visit",
                color: AppColors.black,
                size: Dimensions.font16,
              ),
            ),
            content: SingleChildScrollView(
                child: SizedBox(
              child: Column(
                children: [
                  Container(
                    width: Dimensions.screenWidth,
                    margin: EdgeInsets.only(top: Dimensions.height20),
                    padding: EdgeInsets.only(
                      top: Dimensions.height10,
                      left: Dimensions.width10,
                      right: Dimensions.width10,
                    ),
                    decoration: BoxDecoration(
                        //color: Colors.green,
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(
                        Dimensions.radius20 * 2,
                      ),
                      topLeft: Radius.circular(
                        Dimensions.radius20 * 2,
                      ),
                    )),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: BigText(
                            text: farmer_name,
                            size: Dimensions.font16,
                            color: AppColors.black,
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        BigText(
                          text: "Visit Date",
                          color: AppColors.mainColor,
                          size: Dimensions.font16,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            top: Dimensions.height10,
                            bottom: Dimensions.height10,
                          ),
                          child: TextField(
                            autocorrect: true,
                            readOnly: true,
                            controller: stringDateController,
                            keyboardType: TextInputType.datetime,
                            decoration: InputDecoration(
                              hintText: "Date",
                              hintStyle: const TextStyle(color: Colors.white),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: BorderSide(
                                    color: AppColors.textGrey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide:
                                    const BorderSide(color: Colors.green, width: 2),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),
                        BigText(
                          text: "Visit Reason",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: DropdownButton<VisitReasonModelResultElement>(
                            hint: const Text("Visit Reason"),
                            value: _selectedVisitReason,
                            isExpanded: true,
                            isDense: true,
                            dropdownColor: Colors.white,
                            icon: const Icon(Icons.arrow_drop_down),
                            style: const TextStyle(color: Colors.black, fontSize: 16),
                            onChanged: (VisitReasonModelResultElement?
                                newValue) async {
                              setState(() {
                                _selectedVisitReason = newValue;
                                visitReasonController.text =
                                    _selectedVisitReason!.name.toString();
                                visitReasonIdController.text =
                                    _selectedVisitReason!.resultId.toString();
                                print(
                                    "Here is reason ==*** ${_selectedVisitReason!.name.toString()} and id === ${_selectedVisitReason!.resultId.toString()}");
                              });
                            },
                            items: _visitReasons
                                .map((VisitReasonModelResultElement item) {
                              return DropdownMenuItem<
                                  VisitReasonModelResultElement>(
                                value: item,
                                child: Text(item.name.toString(),
                                    style: const TextStyle(color: Colors.black)),
                              );
                            }).toList(),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),
                        Row(
                          children: [
                            BigText(
                              text: "GPS Latitude: ",
                              color: Colors.black,
                              size: Dimensions.font16,
                            ),
                            BigText(
                              text: latController.text.toString(),
                              color: const Color.fromARGB(255, 77, 76, 76),
                              size: Dimensions.font16,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            BigText(
                              text: "GPS Longitude: ",
                              color: Colors.black,
                              size: Dimensions.font16,
                            ),
                            BigText(
                              text: longController.text.toString(),
                              color: const Color.fromARGB(255, 77, 76, 76),
                              size: Dimensions.font16,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),
                        BigText(
                          text: "Visit Comments(Short Description)",
                          color: Colors.black,
                          size: Dimensions.font16,
                        ),
                        Container(
                          //height: Dimensions.height30* Dimensions.height45,
                          padding: EdgeInsets.only(
                              top: Dimensions.height10,
                              bottom: Dimensions.height10),
                          child: TextField(
                            maxLines: 4,
                            autocorrect: true,
                            controller: notesController,
                            keyboardType: TextInputType.multiline,
                            decoration: InputDecoration(
                              hintText: "Notes",
                              hintStyle: const TextStyle(color: Colors.white),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15 - 5)),
                                borderSide: BorderSide(
                                    color: AppColors.textGrey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius15)),
                                borderSide:
                                    const BorderSide(color: Colors.green, width: 2),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _addVisit();
                    },
                    child: Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.width30 * Dimensions.width10,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius:
                            BorderRadius.circular(Dimensions.radius30),
                      ),
                      child: Center(
                        child: BigText(
                          text: "Submit",
                          color: AppColors.textWhite,
                          size: Dimensions.font16,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: Dimensions.height30,
                  ),
                ],
              ),
            )),
            actions: <Widget>[
              TextButton(
                child: const Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  setState(() {
                    Navigator.of(context).pop();
                  });
                },
              ),
            ],
          );
        });
  }
}
