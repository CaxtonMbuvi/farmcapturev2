
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import '../../../controllers/farmers/farmers_controller.dart';
import 'package:intl/intl.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import '../dashboard.dart';
import 'visit_dashboard.dart';

class AllVisitFarmersScreen extends StatefulWidget {
  const AllVisitFarmersScreen({super.key});

  @override
  State<AllVisitFarmersScreen> createState() => _AllVisitFarmersScreenState();
}

class _AllVisitFarmersScreenState extends State<AllVisitFarmersScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAllFarmersLocal();
  }

  String searchString = "";

  //List<dynamic> _allFarmers = [];
  bool isLoading = false;
  List<dynamic> _allFarmersLocal = [];

  Future _getAllFarmersLocal() async {
    setState(() => isLoading = true);
    print("At Function");

    try {
      _allFarmersLocal = await FarmersController().getAllFarmersLocal();
      if (_allFarmersLocal.isNotEmpty) {
        print(_allFarmersLocal[0]["full_name"]!.toString());
        print(_allFarmersLocal.length);

        setState(() => isLoading = false);

        //print("List of Users: " + userModelList.length.toString());
      } else {
        print("Did not Get");
      }
    } catch (e) {
      setState(() => isLoading = false);
      await openModeOfMappingDialog();
    }
  }

  String getDate(String a) {
    var date = DateTime.parse(a.substring(0, 10));
    return DateFormat('EEEE, d MMM, yyyy').format(date).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const DashboardScreen(),);
                    },
                  ),
                  BigText(
                    text: "Visits",
                    color: AppColors.textWhite,
                    size: Dimensions.font28,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            color: Colors.green,
            width: Dimensions.screenWidth,
            child: isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
                : Column(
                    children: [
                      SizedBox(
                        height: Dimensions.height30 * 6,
                        child: Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  width: Dimensions.screenWidth / 1.3,
                                  padding: EdgeInsets.only(
                                    top: Dimensions.height10,
                                    bottom: Dimensions.height10,
                                  ),
                                  child: Center(
                                    child: TextField(
                                      onChanged: (value) {
                                        setState(() {
                                          searchString =
                                              value.toString().toLowerCase();
                                        });
                                      },
                                      autocorrect: true,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                        hintText: "Search Farmer",
                                        hintStyle:
                                            const TextStyle(color: Colors.grey),
                                        filled: true,
                                        fillColor: Colors.white,
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                  Dimensions.radius30)),
                                          borderSide: BorderSide(
                                              color: AppColors.textGrey,
                                              width: 2),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                  Dimensions.radius30)),
                                          borderSide: const BorderSide(
                                              color: Colors.lightGreenAccent,
                                              width: 2),
                                        ),
                                      ),
                                    ),
                                  )),
                              Flexible(
                                child: Container(
                                  width: Dimensions.width30 * 2,
                                  height: Dimensions.height30 * 2,
                                  padding: EdgeInsets.only(
                                    left: Dimensions.width15,
                                    right: Dimensions.width15,
                                  ),
                                  child: Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.magnifyingGlass,
                                      size: Dimensions.height20,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: Dimensions.screenWidth,
                        padding: EdgeInsets.only(
                          top: Dimensions.height45,
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                        ),
                        decoration: BoxDecoration(
                            color: AppColors.textWhite,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(
                                Dimensions.radius20 * 2,
                              ),
                              topLeft: Radius.circular(
                                Dimensions.radius20 * 2,
                              ),
                            )),
                        child: Column(
                          children: [
                            Container(
                              child: Column(
                                children: [
                                  ListView.separated(
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: _allFarmersLocal.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return _allFarmersLocal[index]
                                                  ["full_name"]!
                                              .toString()
                                              .toLowerCase()
                                              .contains(searchString)
                                          ? Padding(
                                              padding: EdgeInsets.only(
                                                  left: Dimensions.width10,
                                                  right: Dimensions.width10,
                                                  top: Dimensions.height10),
                                              child: GestureDetector(
                                                  onTap: () {
                                                    Get.to(
                                                        () => const VisitDashboardScreen(),
                                                      arguments: _allFarmersLocal[index]["Id"]!.toString(),
                                                    );
                                                  },
                                                  child: Container(
                                                    height:
                                                        Dimensions.height30 * 4,
                                                    width:
                                                        Dimensions.screenWidth,
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey[300],
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                Dimensions
                                                                    .radius20)),
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                        left:
                                                            Dimensions.width20,
                                                        right:
                                                            Dimensions.width15,
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          BigText(
                                                            text:
                                                                "Name: ${_allFarmersLocal[index]["full_name"].toString()}",
                                                            color: AppColors
                                                                .mainColor,
                                                            size: Dimensions
                                                                .font16,
                                                          ),
                                                          BigText(
                                                            text:
                                                                "Nat ID: ${_allFarmersLocal[index]["nat_id"].toString()}",
                                                            color:
                                                                AppColors.black,
                                                            size: Dimensions
                                                                .font16,
                                                          ),
                                                          BigText(
                                                            text:
                                                                "Phone: ${_allFarmersLocal[index]["phone1"].toString()}",
                                                            color:
                                                                AppColors.black,
                                                            size: Dimensions
                                                                .font16,
                                                          ),
                                                          const Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              SizedBox(),
                                                              // SmallText(
                                                              //   text: getDate(_allFarmersLocal[
                                                              //               index]
                                                              //           [
                                                              //           "reg_date"]
                                                              //       .toString()),
                                                              //   size: Dimensions
                                                              //       .font16,
                                                              //   color: Colors
                                                              //       .black,
                                                              // ),
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  )),
                                            )
                                          : Container();
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return _allFarmersLocal[index]
                                                  ["full_name"]!
                                              .toString()
                                              .toLowerCase()
                                              .contains(searchString)
                                          ? Container()
                                          : Container();
                                    },
                                  ),
                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),
                                  Container(
                                    height: Dimensions.height30 * 2,
                                    width: Dimensions.screenWidth,
                                    padding: EdgeInsets.only(
                                        left: Dimensions.width20,
                                        right: Dimensions.width20,
                                        top: Dimensions.height10),
                                    decoration: BoxDecoration(
                                        color: Colors.grey[300],
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20)),
                                    child: Center(
                                      child: BigText(
                                        text: "No Item",
                                        color: AppColors.black,
                                        size: Dimensions.font18,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  //dialog to choose mode of mapping
  Future<void> openModeOfMappingDialog() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: contentBox(context),
          );
        });
  }

  contentBox(context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              BigText(
                text: "No Farmers",
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              SmallText(
                text: "Kindly SYNC to continue",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              const SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: GestureDetector(
                    onTap: () {
                      Get.back();
                      Get.to(() => const DashboardScreen());
                    },
                    child: BigText(
                      text: "Ok",
                      color: Colors.black,
                      size: Dimensions.font16,
                    ),
                  )),
            ],
          ),
        ),
        const Positioned(
          left: 5,
          right: 5,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 20,
            child: Icon(
              Icons.warning,
              color: Colors.redAccent,
              size: 45,
            ),
            // child: ClipRRect(
            //   borderRadius: BorderRadius.all(Radius.circular(10)),
            //     child: Image.asset('assets/images/ic_1_6.png')
            // ),
          ),
        ),
      ],
    );
  }
}
