
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import 'visits.dart';
import 'previous_visits.dart';
import 'visit_dashboard.dart';

class VisitSubDashboard extends StatefulWidget {
  const VisitSubDashboard({super.key});

  @override
  State<VisitSubDashboard> createState() => _VisitSubDashboardState();
}

class _VisitSubDashboardState extends State<VisitSubDashboard> {
  var pageId = Get.arguments;
  var farmerNameController = TextEditingController();
  List<dynamic> _singleFarmersLocal = [];
  bool isLoading = false;
  String farmer_name = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal =
        await FarmersController().getSingleFarmerLocal(farmerId);

    if (_singleFarmersLocal.isNotEmpty) {
      print(_singleFarmersLocal[0]["full_name"]!.toString());
      farmerNameController.text =
          _singleFarmersLocal[0]["full_name"]!.toString();
      farmer_name = farmerNameController.text.toString();
      print(farmer_name);

      setState(() => isLoading = false);
    } else {
      print("Did not Get");
    }
  }

  /// location permission*
  LocationPermission? _locationPermission;
  checkIfLocationPermissionAllowed() async {
    _locationPermission = await Geolocator.requestPermission();

    //if permission is denied
    if (_locationPermission == LocationPermission.denied) {
      _locationPermission =
          await Geolocator.requestPermission(); //ask user to allow permission
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        try {
          Get.to(() => const VisitDashboardScreen(),
           arguments: pageId.toString(),
          );
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(Dimensions.height30 * 2),
            child: AppBar(
              backgroundColor: Colors.green,
              actions: [
                Container(
                  width: Dimensions.screenWidth,
                  height: Dimensions.height30 * 2,
                  padding: EdgeInsets.only(
                    left: Dimensions.width15,
                    right: Dimensions.width15,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 3,
                          offset: const Offset(5, 5),
                          color: AppColors.gradientOne.withOpacity(0.1)),
                      BoxShadow(
                          blurRadius: 3,
                          offset: const Offset(-5, -5),
                          color: AppColors.gradientOne.withOpacity(0.1))
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                          size: Dimensions.iconSize24,
                        ),
                        onPressed: () {
                          Get.to(() => const VisitDashboardScreen(),
                           arguments: pageId.toString(),
                          );
                        },
                      ),
                      BigText(
                        text: "Manage Visit",
                        color: AppColors.textWhite,
                        size: Dimensions.font28,
                      ),
                      const SizedBox()
                    ],
                  ),
                ),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: SafeArea(
              child: Container(
                height: Dimensions.screenHeight - (Dimensions.height30 * 3),
                width: Dimensions.screenWidth,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                padding: EdgeInsets.only(
                  top: Dimensions.height45,
                  left: Dimensions.width30 * 2.0,
                  right: Dimensions.width30 * 2.0,
                ),
                child: Column(
                  children: [
                    BigText(
                      text: farmer_name,
                      color: AppColors.black,
                      size: Dimensions.font16,
                    ),
                    SizedBox(height: Dimensions.height30,),
                    //Add Visit
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Get.to(
                              () => const AddVisitScreen(),
                              arguments: pageId.toString(),
                            );
                          },
                          child: Container(
                            height: Dimensions.height30 * 4,
                            width: Dimensions.width30 * 4,
                            decoration: BoxDecoration(
                              color: AppColors.textWhite,
                              borderRadius: BorderRadius.circular(
                                  Dimensions.radius30 * 5),
                            ),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/images/ic_1_1.png',
                                    width: 60,
                                    height: 60,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        BigText(
                          text: "Add visit",
                          color: AppColors.black,
                          size: Dimensions.font16 - 4,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                            width: Dimensions.width45 + 40, child: const Divider()),
                        SmallText(
                          text: "visits",
                          color: Colors.blueGrey,
                          size: Dimensions.font16 - 4,
                        )
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height30 * 3,
                    ),
                    // Previous Visits
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Get.to(() => const PreviousVisitsScreen(),
                            arguments: pageId.toString()
                            );
                          },
                          child: Container(
                            height: Dimensions.height30 * 4,
                            width: Dimensions.width30 * 4,
                            decoration: BoxDecoration(
                              color: AppColors.textWhite,
                              borderRadius: BorderRadius.circular(
                                  Dimensions.radius30 * 5),
                            ),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/images/ic_1_1.png',
                                    width: 60,
                                    height: 60,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        BigText(
                          text: "Previous visits",
                          color: AppColors.black,
                          size: Dimensions.font16 - 4,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                            width: Dimensions.width45 + 40, child: const Divider()),
                        SmallText(
                          text: "visits",
                          color: Colors.blueGrey,
                          size: Dimensions.font16 - 4,
                        )
                      ],
                    ),
                  ]
                )
              )
            )
          )
        ),
    );
  }
}
