import 'package:farmcapturev2/models/farmers/allfarmers_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';

class SelectSessionMemberScreen extends StatefulWidget {
  const SelectSessionMemberScreen({super.key});

  @override
  State<SelectSessionMemberScreen> createState() =>
      _SelectSessionMemberScreenState();
}

class _SelectSessionMemberScreenState extends State<SelectSessionMemberScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAllFarmersLocal();
  }

  String searchString = "";

  //List<dynamic> _allFarmers = [];
  bool isLoading = false;
  final List<Map<String, dynamic>> _allFarmersLocal = [];
  final List<Map<String, dynamic>> selectedFarmersData = [];


  _getAllFarmersLocal() async {
    setState(() => isLoading = true);
    List<dynamic> resFarmers = [];
    var res = await FarmersController().getAllFarmersLocal();
    if (res != null) {
      resFarmers = res;
      for (var i in resFarmers) {
        FarmerModelDB model = FarmerModelDB(
            resultId: i['id'],
            fullName: i['full_name'],
            memberNo: i['member_no']);

        var newdata = {
          "id": model.resultId,
          "full_name": model.fullName,
          "member_no": model.memberNo,
          "selected": false
        };

        _allFarmersLocal.add(newdata);
      }
    }
    setState(() => isLoading = false);
  }

  String getDate(String a) {
    var date = DateTime.parse(a.substring(0, 10));
    return DateFormat('EEEE, d MMM, yyyy').format(date).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: const Text('List of Products'),
          ),
          body: Column(
            children: [
              SizedBox(
                height: Dimensions.height30 * 4,
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: Dimensions.screenWidth / 1.3,
                          padding: EdgeInsets.only(
                            top: Dimensions.height10,
                            bottom: Dimensions.height10,
                          ),
                          child: Center(
                            child: TextField(
                              onChanged: (value) {
                                setState(() {
                                  searchString = value.toString().toLowerCase();
                                });
                              },
                              autocorrect: true,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                hintText: "Search Farmer",
                                hintStyle: TextStyle(color: Colors.grey[700]),
                                filled: true,
                                fillColor: Colors.grey[300],
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(Dimensions.radius30)),
                                  borderSide: BorderSide(
                                      color: AppColors.textGrey, width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(Dimensions.radius30)),
                                  borderSide: const BorderSide(
                                      color: Colors.lightGreenAccent,
                                      width: 2),
                                ),
                              ),
                            ),
                          )),
                      Flexible(
                        child: Container(
                          width: Dimensions.width30 * 2,
                          height: Dimensions.height30 * 2,
                          padding: EdgeInsets.only(
                            left: Dimensions.width15,
                            right: Dimensions.width15,
                          ),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.magnifyingGlass,
                              size: Dimensions.height20,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: Dimensions.height10,),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  //physics: const NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: _allFarmersLocal.length,
                  itemBuilder: (context, index) {
                    final item = _allFarmersLocal[index];
                    return _allFarmersLocal[index]["full_name"]!
                            .toString()
                            .toLowerCase()
                            .contains(searchString)
                        ? GestureDetector(
                          onTap: () {
                            setState(() {
                              
                            });
                          },
                          child: Container(
                              margin: EdgeInsets.only(
                                top: Dimensions.height10 / 2,
                                bottom: Dimensions.height10 / 2,
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.green),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: CheckboxListTile(
                                title: Text(item['full_name']),
                                value: item['selected'],
                                activeColor: Colors.green,
                                checkColor: Colors.white,
                                onChanged: (value) {
                                  if (selectedFarmersData
                                        .contains(item)) {
                                      selectedFarmersData.remove(item);
                                      item['selected'] = value;
                                    } else {
                                      selectedFarmersData.add(item);
                                      item['selected'] = value;
                                    }
                                  setState(() {
                                    
                                  });
                                      
                                  print(selectedFarmersData.toString());
                                },
                              ),
                            ),
                        )
                        : Container();
                  },
                ),
              ),
            ],
          ),
        );
  }
}
