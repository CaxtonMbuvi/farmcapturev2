import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:farmcapturev2/pages/main_pages/visits/add_trainingtopics.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../resources/base/snackbar.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import 'visits.dart';
import 'farmers_list.dart';

class VisitDashboardScreen extends StatefulWidget {
  const VisitDashboardScreen({super.key});

  @override
  State<VisitDashboardScreen> createState() => _VisitDashboardScreenState();
}

class _VisitDashboardScreenState extends State<VisitDashboardScreen> {
  var pageId = Get.arguments;
  List<dynamic> _singleFarmersLocal = [];
  bool isLoading = false;
  String farmer_name = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getSingleFarmerLocal(pageId);
  }

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal =
        await FarmersController().getSingleFarmerLocal(farmerId);

    if (_singleFarmersLocal.isNotEmpty) {
      print(_singleFarmersLocal[0]["full_name"]!.toString());

      farmer_name = _singleFarmersLocal[0]["full_name"]!.toString();
      print(farmer_name);

      setState(() => isLoading = false);
    } else {
      print("Did not Get");
    }
  }

  _checkVisit() async {
    var res;
    DateTime today = DateTime.now();
    String dateStr = "${today.day}-${today.month}-${today.year}";
    try {
      res = await FarmersController().checkFarmerVisits(_singleFarmersLocal[0]["member_no"]!.toString(),dateStr);
      return res;
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          Get.to(
            () => const AllVisitFarmersScreen(),
          );
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Dimensions.height30 * 2),
          child: AppBar(
            backgroundColor: Colors.green,
            actions: [
              Container(
                width: Dimensions.screenWidth,
                height: Dimensions.height30 * 2,
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                  color: Colors.green,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(5, 5),
                        color: AppColors.gradientOne.withOpacity(0.1)),
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(-5, -5),
                        color: AppColors.gradientOne.withOpacity(0.1))
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(
                          () => const AllVisitFarmersScreen(),
                        );
                      },
                    ),
                    BigText(
                      text: "Visit Dashboard",
                      color: AppColors.textWhite,
                      size: Dimensions.font18,
                    ),
                    const SizedBox()
                  ],
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              height: Dimensions.screenHeight + Dimensions.height20,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg.png"),
                  fit: BoxFit.cover,
                ),
              ),
              padding: EdgeInsets.only(
                top: Dimensions.height20,
                left: Dimensions.width30 * 2,
                right: Dimensions.width30 * 2,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  BigText(
                    text: farmer_name,
                    color: AppColors.black,
                    size: Dimensions.font16,
                  ),
                  SizedBox(
                    height: Dimensions.height10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.to(
                                () => const AddVisitScreen(),
                                arguments: pageId.toString(),
                              );
                            },
                            child: Container(
                              height: Dimensions.height30 * 4,
                              width: Dimensions.width30 * 4,
                              decoration: BoxDecoration(
                                color: AppColors.textWhite,
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius30 * 5),
                              ),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/ic_1_1.png',
                                      width: 60,
                                      height: 60,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),
                          BigText(
                            text: "Visit",
                            color: AppColors.black,
                            size: Dimensions.font16 - 4,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () async{
                              var res = await _checkVisit();
                              if (res == null) {
                                print("Please add visit");
                                showCustomSnackBar("Please Add a visit", context, ContentType.failure, title: "Failed");
                              } else {
                                Get.to(
                                  () => const TrainingTopicsScreen(),
                                  arguments: pageId.toString(),
                                );
                              }
                            },
                            child: Container(
                              height: Dimensions.height30 * 4,
                              width: Dimensions.width30 * 4,
                              decoration: BoxDecoration(
                                color: AppColors.textWhite,
                                borderRadius: BorderRadius.circular(
                                    Dimensions.radius30 * 5),
                              ),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/ic_1_2.png',
                                      width: 50,
                                      height: 50,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),
                          BigText(
                            text: "Training Topics",
                            color: AppColors.black,
                            size: Dimensions.font16 - 4,
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: Dimensions.height45 + 5,
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Column(
                  //       children: [
                  //         GestureDetector(
                  //           child: Container(
                  //             height: Dimensions.height30 * 4,
                  //             width: Dimensions.width30 * 4,
                  //             decoration: BoxDecoration(
                  //               color: AppColors.textWhite,
                  //               borderRadius: BorderRadius.circular(
                  //                   Dimensions.radius30 * 5),
                  //             ),
                  //             child: Center(
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.center,
                  //                 children: [
                  //                   Image.asset(
                  //                     'assets/images/ic_1_3.png',
                  //                     width: 60,
                  //                     height: 60,
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           height: Dimensions.height10,
                  //         ),
                  //         BigText(
                  //           text: "Map Topics",
                  //           color: AppColors.black,
                  //           size: Dimensions.font16 - 4,
                  //         ),
                  //       ],
                  //     ),
                  //     Column(
                  //       children: [
                  //         GestureDetector(
                  //           onTap: () {

                  //           },
                  //           child: Container(
                  //             height: Dimensions.height30 * 4,
                  //             width: Dimensions.width30 * 4,
                  //             decoration: BoxDecoration(
                  //               color: AppColors.textWhite,
                  //               borderRadius: BorderRadius.circular(
                  //                   Dimensions.radius30 * 5),
                  //             ),
                  //             child: Center(
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.center,
                  //                 children: [
                  //                   Image.asset(
                  //                     'assets/images/ic_1_4.png',
                  //                     width: 50,
                  //                     height: 50,
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           height: Dimensions.height10,
                  //         ),
                  //         BigText(
                  //           text: 'Agri Season Register',
                  //           color: AppColors.black,
                  //           size: Dimensions.font16 - 4,
                  //         ),
                  //       ],
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(
                  //   height: Dimensions.height45 + 5,
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Column(
                  //       children: [
                  //         GestureDetector(
                  //           child: Container(
                  //             height: Dimensions.height30 * 4,
                  //             width: Dimensions.width30 * 4,
                  //             decoration: BoxDecoration(
                  //               color: AppColors.textWhite,
                  //               borderRadius: BorderRadius.circular(
                  //                   Dimensions.radius30 * 5),
                  //             ),
                  //             child: Center(
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.center,
                  //                 children: [
                  //                   Image.asset(
                  //                     'assets/images/ic_1_5.png',
                  //                     width: 60,
                  //                     height: 60,
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           height: Dimensions.height10,
                  //         ),
                  //         BigText(
                  //           text: 'Corrective Actions',
                  //           color: AppColors.black,
                  //           size: Dimensions.font16 - 4,
                  //         ),
                  //       ],
                  //     ),
                  //     GestureDetector(
                  //       onTap: () async {

                  //       },
                  //       child: Column(
                  //         children: [
                  //           Container(
                  //             height: Dimensions.height30 * 4,
                  //             width: Dimensions.width30 * 4,
                  //             decoration: BoxDecoration(
                  //               color: AppColors.textWhite,
                  //               borderRadius: BorderRadius.circular(
                  //                   Dimensions.radius30 * 5),
                  //             ),
                  //             child: Center(
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.center,
                  //                 children: [
                  //                   Image.asset(
                  //                     'assets/images/ic_1_4.png',
                  //                     width: 50,
                  //                     height: 50,
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //           SizedBox(
                  //             height: Dimensions.height10,
                  //           ),
                  //           BigText(
                  //             text: 'Documents Delivered',
                  //             color: AppColors.black,
                  //             size: Dimensions.font16 - 4,
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(
                  //   height: Dimensions.height45 + 5,
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Column(
                  //       children: [
                  //         GestureDetector(
                  //           child: Container(
                  //             height: Dimensions.height30 * 4,
                  //             width: Dimensions.width30 * 4,
                  //             decoration: BoxDecoration(
                  //               color: AppColors.textWhite,
                  //               borderRadius: BorderRadius.circular(
                  //                   Dimensions.radius30 * 5),
                  //             ),
                  //             child: Center(
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.center,
                  //                 children: [
                  //                   Image.asset(
                  //                     'assets/images/ic_1_5.png',
                  //                     width: 60,
                  //                     height: 60,
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           height: Dimensions.height10,
                  //         ),
                  //         BigText(
                  //           text: 'Occurrence Menu',
                  //           color: AppColors.black,
                  //           size: Dimensions.font16 - 4,
                  //         ),
                  //       ],
                  //     ),
                  //     GestureDetector(
                  //       onTap: () async {

                  //       },
                  //       child: Column(
                  //         children: [
                  //           Container(
                  //             height: Dimensions.height30 * 4,
                  //             width: Dimensions.width30 * 4,
                  //             decoration: BoxDecoration(
                  //               color: AppColors.textWhite,
                  //               borderRadius: BorderRadius.circular(
                  //                   Dimensions.radius30 * 5),
                  //             ),
                  //             child: Center(
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.center,
                  //                 children: [
                  //                   Image.asset(
                  //                     'assets/images/ic_1_4.png',
                  //                     width: 50,
                  //                     height: 50,
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //           SizedBox(
                  //             height: Dimensions.height10,
                  //           ),
                  //           BigText(
                  //             text: 'Update Register',
                  //             color: AppColors.black,
                  //             size: Dimensions.font16 - 4,
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(
                  //   height: Dimensions.height30,
                  // ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
