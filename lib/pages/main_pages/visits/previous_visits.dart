
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';
import 'visits.dart';
import 'visit_sub_dashboard.dart';

class PreviousVisitsScreen extends StatefulWidget {
  const PreviousVisitsScreen({super.key});

  @override
  State<PreviousVisitsScreen> createState() => _PreviousVisitsScreenState();
}

class _PreviousVisitsScreenState extends State<PreviousVisitsScreen> {
  var pageId = Get.arguments;
  List<dynamic> _singleFarmersLocal = [];
  bool isLoading = false;
  String searchString = "";
  String farmer_name = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getSingleFarmerLocal(pageId);
  }

  Future _getSingleFarmerLocal(String farmerId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal =
        await FarmersController().getSingleFarmerLocal(farmerId);

    if (_singleFarmersLocal.isNotEmpty) {
      print(_singleFarmersLocal[0]["full_name"]!.toString());
      farmer_name = _singleFarmersLocal[0]["full_name"]!.toString();
      print(farmer_name);

      setState(() => isLoading = false);
    } else {
      print("Did not Get");
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> allUsers = [
      {"id": 1, "name": "Andy", "age": 29},
      {"id": 2, "name": "Aragon", "age": 40},
      {"id": 3, "name": "Bob", "age": 5},
      {"id": 4, "name": "Barbara", "age": 35},
      {"id": 5, "name": "Candy", "age": 21},
      {"id": 6, "name": "Colin", "age": 55},
      {"id": 7, "name": "Audra", "age": 30},
      {"id": 8, "name": "Banana", "age": 14},
      {"id": 9, "name": "Caversky", "age": 100},
      {"id": 10, "name": "Becky", "age": 32},
    ];
    return WillPopScope(
      onWillPop: () async{
        try {
          Get.to(() => const VisitSubDashboard(),
           arguments: pageId.toString(),
          );
          return true; // true allows navigating back
        } catch (e) {
          //print('Error when closing the database input window.');
          return false; // false prevents navigating back
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Dimensions.height30 * 2),
          child: AppBar(
            backgroundColor: Colors.green,
            actions: [
              Container(
                width: Dimensions.screenWidth,
                height: Dimensions.height30 * 2,
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                decoration: BoxDecoration(
                  color: Colors.green,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(5, 5),
                        color: AppColors.gradientOne.withOpacity(0.1)),
                    BoxShadow(
                        blurRadius: 3,
                        offset: const Offset(-5, -5),
                        color: AppColors.gradientOne.withOpacity(0.1))
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const VisitSubDashboard(),
                         arguments: pageId.toString(),
                        );
                      },
                    ),
                    BigText(
                      text: "Previous Visits",
                      color: AppColors.textWhite,
                      size: Dimensions.font28,
                    ),
                    const SizedBox()
                  ],
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              color: Colors.green,
              width: Dimensions.screenWidth,
              child: isLoading
                  ? SizedBox(
                      height: Dimensions.screenHeight,
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [CustomLoader()],
                      ),
                    )
                  : allUsers.isEmpty
                      ? Container(
                          height: Dimensions.screenHeight,
                          color: Colors.green,
                          child: Center(
                            child: BigText(
                              text: "No Item",
                              color: Colors.black,
                              size: Dimensions.font20,
                            ),
                          ),
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: Dimensions.height15,
                            ),
                            Row(
                              children: [
                                BigText(
                                  text: "Farmer Name: ",
                                  color: AppColors.black,
                                  size: Dimensions.font16,
                                ),
                                SizedBox(
                                  width: Dimensions.width10,
                                ),
                                BigText(
                                  text: farmer_name,
                                  color: AppColors.textWhite,
                                  size: Dimensions.font18,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: Dimensions.height30 * 6,
                              child: Center(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                        width: Dimensions.screenWidth / 1.3,
                                        padding: EdgeInsets.only(
                                          top: Dimensions.height10,
                                          bottom: Dimensions.height10,
                                        ),
                                        child: Center(
                                          child: TextField(
                                            onChanged: (value) {
                                              setState(() {
                                                searchString = value
                                                    .toString()
                                                    .toLowerCase();
                                              });
                                            },
                                            autocorrect: true,
                                            keyboardType: TextInputType.text,
                                            decoration: InputDecoration(
                                              hintText: "Search Visit",
                                              hintStyle:
                                                  const TextStyle(color: Colors.grey),
                                              filled: true,
                                              fillColor: Colors.white,
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(
                                                        Dimensions.radius30)),
                                                borderSide: BorderSide(
                                                    color: AppColors.textGrey,
                                                    width: 2),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(
                                                        Dimensions.radius30)),
                                                borderSide: const BorderSide(
                                                    color:
                                                        Colors.lightGreenAccent,
                                                    width: 2),
                                              ),
                                            ),
                                          ),
                                        )),
                                    Flexible(
                                      child: Container(
                                        width: Dimensions.width30 * 2,
                                        height: Dimensions.height30 * 2,
                                        padding: EdgeInsets.only(
                                          left: Dimensions.width15,
                                          right: Dimensions.width15,
                                        ),
                                        child: Center(
                                          child: FaIcon(
                                            FontAwesomeIcons.magnifyingGlass,
                                            size: Dimensions.height20,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: Dimensions.screenWidth,
                              padding: EdgeInsets.only(
                                top: Dimensions.height45,
                                left: Dimensions.width15,
                                right: Dimensions.width15,
                              ),
                              decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      Dimensions.radius20 * 2,
                                    ),
                                    topLeft: Radius.circular(
                                      Dimensions.radius20 * 2,
                                    ),
                                  )),
                              child: Column(
                                children: [
                                  Container(
                                    child: Column(
                                      children: [
                                        ListView.separated(
                                          shrinkWrap: true,
                                          physics: const NeverScrollableScrollPhysics(),
                                          itemCount: allUsers.length,
                                          itemBuilder:
                                              (BuildContext context, int index) {
                                            return allUsers[index]["name"]!
                                                    .toString()
                                                    .toLowerCase()
                                                    .contains(searchString)
                                                ? Padding(
                                                    padding: EdgeInsets.only(
                                                        left: Dimensions.width10,
                                                        right: Dimensions.width10,
                                                        top: Dimensions.height10),
                                                    child: GestureDetector(
                                                        onTap: () {},
                                                        child: Container(
                                                          height: Dimensions
                                                                  .height30 *
                                                              4,
                                                          width: Dimensions
                                                              .screenWidth,
                                                          decoration: BoxDecoration(
                                                              color: Colors
                                                                  .grey[300],
                                                              borderRadius:
                                                                  BorderRadius.circular(
                                                                      Dimensions
                                                                          .radius20)),
                                                          child: Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                              left: Dimensions
                                                                  .width20,
                                                              right: Dimensions
                                                                  .width15,
                                                            ),
                                                            child: Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                BigText(
                                                                  text:
                                                                      "Name: ${allUsers[index]["name"]!.toString()}",
                                                                  color: AppColors
                                                                      .mainColor,
                                                                  size: Dimensions
                                                                      .font18,
                                                                ),
                                                                BigText(
                                                                  text:
                                                                      "Nat ID: ",
                                                                  color: AppColors
                                                                      .black,
                                                                  size: Dimensions
                                                                      .font16,
                                                                ),
                                                                BigText(
                                                                  text: "Phone: ",
                                                                  color: AppColors
                                                                      .black,
                                                                  size: Dimensions
                                                                      .font16,
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  children: [
                                                                    const SizedBox(),
                                                                    SmallText(
                                                                      text:
                                                                          'Date',
                                                                      size: Dimensions
                                                                          .font16,
                                                                      color: Colors
                                                                          .black,
                                                                    ),
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        )),
                                                  )
                                                : Container();
                                          },
                                          separatorBuilder:
                                              (BuildContext context, int index) {
                                            return allUsers[index]["name"]!
                                                    .toString()
                                                    .toLowerCase()
                                                    .contains(searchString)
                                                ? Container()
                                                : Container();
                                          },
                                        ),
                                        SizedBox(
                                          height: Dimensions.height15,
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            Get.to(() => const AddVisitScreen(),
                                            arguments: pageId.toString(),
                                            );
                                          },
                                          child: Container(
                                            height: Dimensions.height30 * 2,
                                            width: Dimensions.screenWidth,
                                            padding: EdgeInsets.only(
                                                left: Dimensions.width20,
                                                right: Dimensions.width20,
                                                top: Dimensions.height10),
                                            decoration: BoxDecoration(
                                                color: Colors.grey[300],
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Dimensions.radius20)),
                                            child: Center(
                                              child: BigText(
                                                text: "No Item(Add Visit)",
                                                color: AppColors.black,
                                                size: Dimensions.font18,
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: Dimensions.height15,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
            ),
          ),
        ),
      ),
    );
  }
}
