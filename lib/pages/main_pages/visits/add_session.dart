import 'dart:convert';
import 'dart:io';
import 'package:farmcapturev2/pages/main_pages/visits/add_trainingtopics.dart';
import 'package:farmcapturev2/resources/widgets/form_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/visit/farmerTrainingMain_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';

class AddSessionScreen extends StatefulWidget {
  const AddSessionScreen({super.key});

  @override
  State<AddSessionScreen> createState() => _AddSessionScreenState();
}

class _AddSessionScreenState extends State<AddSessionScreen> {
  var pageId = Get.arguments[0];
  var sessionNo = Get.arguments[1];
  var isView = Get.arguments[2];

  bool isLoading = false;
  var farmerNameController = TextEditingController();
  var noOfWorkersController = TextEditingController();
  var nofamilyMembersController = TextEditingController();
  var fslbController = TextEditingController();
  var nonFSLBController = TextEditingController();
  var photoController = TextEditingController();
  File? image;

  List<dynamic> _singleFarmersLocal = [];
  List<dynamic> _singleSessionLocal = [];
  String farmer_name = "";
  List<Map<String, dynamic>> addedtopics = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getSingleFarmerLocal(pageId);
  }

  Future _getSingleFarmerLocal(String memberNo) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleFarmersLocal = [];
    var res = await FarmersController().getSingleFarmerLocal(memberNo);

    if (res != null) {
      setState(() {
        _singleFarmersLocal = res;
      });
      if (_singleFarmersLocal.isNotEmpty) {
        print(_singleFarmersLocal[0]["full_name"]!.toString());
        farmer_name = _singleFarmersLocal[0]["full_name"]!.toString();
        farmerNameController.text = farmer_name;

        print(farmer_name);

        isView ? await _getSingleSessionLocal(sessionNo) : null;

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
  }

  Future _getSingleSessionLocal(String sessionId) async {
    print("At Function");
    _singleSessionLocal = [];
    var res = await FarmersController().getSingleSessionLocal(sessionId);

    if (res != null) {
      setState(() {
        _singleSessionLocal = res;
      });
      if (_singleSessionLocal.isNotEmpty) {
        // String? nofslb = _singleSessionLocal[0]["no_fslb_farmers"].toString();
        // String? nonfslb;
        // String? nopeople;
        // String? nofamilyMembers;
        // String? photo;
        print(_singleSessionLocal[0]["title"]!.toString());
        fslbController.text =
            _singleSessionLocal[0]["no_fslb_farmers"]!.toString();
        nonFSLBController.text =
            _singleSessionLocal[0]["no_non_fslb_farmers"]!.toString();
        noOfWorkersController.text =
            _singleSessionLocal[0]["numofpeople"]!.toString();
        nofamilyMembersController.text =
            _singleSessionLocal[0]["no_family_members"]!.toString();
        photoController.text = _singleSessionLocal[0]["photo_url"]!.toString();

        //String pathOnly = Uri.file().path;
        image = File(_singleSessionLocal[0]["photo_url"]!.toString().substring(
            7, _singleSessionLocal[0]["photo_url"]!.toString().length - 1));
      } else {
        print("Did not Get");
      }
    }
  }

  Future _getImage() async {
    final picker = ImagePicker();

    var pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (pickedFile != null) {
      final bytes = File(pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

      setState(() {
        image = File(pickedFile.path);
        photoController.text = image.toString();
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    print(photoController.text.toString());
    //return image;
  }

  _startSession() async {
    DateTime today = DateTime.now();
    // FarmersTrainingMainModel farmerTrainingMainModel =
    //     FarmersTrainingMainModel();
    List<dynamic> farmerTrainingMainList = [];
    // farmerTrainingMainModel.noFamilyMembers =
    //     nofamilyMembersController.text.toString();
    // farmerTrainingMainModel.noOfPeople = noOfWorkersController.text.toString();
    // farmerTrainingMainModel.nofslbFarmers = fslbController.text.toString();
    // farmerTrainingMainModel.no_nonfslbFarmers =
    //     nonFSLBController.text.toString();
    // farmerTrainingMainModel.startTime = today.toString();

    var object = {
      "start_time": today.toString(),
      "numofpeople": noOfWorkersController.text.toString(),
      "no_fslb_farmers": fslbController.text.toString(),
      "no_non_fslb_farmers": nonFSLBController.text.toString(),
      "no_family_members": nofamilyMembersController.text.toString(),
      "photo_url": photoController.text.toString(),
    };

    farmerTrainingMainList.add(object);
    int j = await FarmersController()
        .updateFarmerMainCoaching(farmerTrainingMainList, farmerTrainingMainTable, sessionNo);
    if (j > 0) {
      print("Print Success");
      Get.to(() => const TrainingTopicsScreen(), arguments: pageId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Add Session",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Container(
          decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
            // color: Colors.white,
            width: Dimensions.screenWidth,
            child: isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
                : SizedBox(
                    child: Column(
                      children: [
                        Container(
                          height: Dimensions.screenHeight,

                          // height: Dimensions.screenHeight + (Dimensions.height30 * 4),
                          width: Dimensions.screenWidth,
                          // margin: EdgeInsets.only(top: Dimensions.height30),
                          padding: EdgeInsets.only(
                            top: Dimensions.height10,
                            left: Dimensions.width10,
                            right: Dimensions.width10,
                            bottom: Dimensions.height15,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                              BigText(
                                text: "Farmer Name",
                                color: AppColors.black,
                                size: Dimensions.font16,
                              ),
                              FormFields(
                                textEditingController:farmerNameController,
                                inputType: TextInputType.text,
                                readOnly: true,
                              ),
                              SizedBox(
                                height: Dimensions.height15,
                              ),
                              BigText(
                                text: "Workers Present",
                                color: AppColors.black,
                                size: Dimensions.font16,
                              ),
                              isView?
                              FormFields(
                                textEditingController: noOfWorkersController,
                                inputType: TextInputType.number,
                                readOnly: true,
                              )
                              :
                              FormFields(
                                textEditingController: noOfWorkersController,
                                inputType: TextInputType.number,
                              ),
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                              BigText(
                                text: "Family Members Present",
                                color: AppColors.black,
                                size: Dimensions.font16,
                              ),
                              isView?
                              FormFields(
                                textEditingController: nofamilyMembersController,
                                inputType: TextInputType.number,
                                readOnly: true,
                              )
                              :
                              FormFields(
                                textEditingController: nofamilyMembersController,
                                inputType: TextInputType.number,
                              ),
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,

                                    children: [
                                      BigText(
                                        text: "FSLB Present",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 *
                                            Dimensions.width10 /
                                            2,
                                        child:
                                        isView ?
                                          FormFields(
                                            textEditingController:fslbController,
                                            inputType: TextInputType.number,
                                            readOnly: true,
                                          )
                                          :FormFields(
                                            textEditingController:fslbController,
                                            inputType: TextInputType.number,
                                          ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      BigText(
                                        text: "NON-FSLB Present",
                                        color: AppColors.black,
                                        size: Dimensions.font16,
                                      ),
                                      SizedBox(
                                        width: Dimensions.width45 *
                                            Dimensions.width10 /
                                            2,
                                        child: isView
                                        ?
                                        FormFields(
                                          textEditingController: nonFSLBController,
                                          inputType: TextInputType.number,
                                          readOnly: true,
                                        )
                                        :
                                        FormFields(
                                          textEditingController: nonFSLBController,
                                          inputType: TextInputType.number,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                              Center(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.center,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                  children: [
                                    BigText(
                                      text: "Session Photo",
                                      color: AppColors.black,
                                      size: Dimensions.font16 - 2,
                                    ),
                                    SizedBox(
                                      height: Dimensions.height10,
                                    ),
                                    Container(
                                      height: Dimensions.height10 *
                                          Dimensions.height20,
                                      width: Dimensions.screenWidth / 2,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(
                                                  Dimensions.radius20 / 2),
                                          color: AppColors.textGrey),
                                      child: image != null
                                          ? ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      Dimensions.radius15),
                                              child: Image.file(image!,
                                                  fit: BoxFit.cover),
                                            )
                                          : const Center(
                                              child: Text(
                                                  'Please add a photo'),
                                            ),
                                    ),
                                    SizedBox(
                                      width: Dimensions.width30,
                                    ),
                                    isView
                                    ?
                                    Container()
                                    :
                                    MaterialButton(
                                      color: Colors.blue,
                                      onPressed: _getImage,
                                      child: Text(
                                        "Take a Photo",
                                        style: TextStyle(
                                            color: AppColors.textWhite,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                        isView
                        ?
                        Container( )
                        :
                        GestureDetector(
                          onTap: () {
                            _startSession();
                          },
                          child: Container(
                            height: Dimensions.height30 * 2,
                            width: Dimensions.screenWidth,
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(
                                  Dimensions.radius30 / 2),
                            ),
                            child: Center(
                              child: BigText(
                                text: "Submit",
                                color: AppColors.textWhite,
                                size: Dimensions.font16,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30 / 2,
                        ),
                      ],
                    ),
                  ),
        ),
      ),
    );
  }
}
