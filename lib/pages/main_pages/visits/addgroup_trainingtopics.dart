import 'dart:math';

import 'package:farmcapturev2/pages/main_pages/farmers/menu.dart';
import 'package:farmcapturev2/pages/main_pages/visits/addgroup_session.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../controllers/location/location_controller.dart';
import '../../../models/visit/farmerTrainingMain_model.dart';
import '../../../models/visit/farmersTrainingTopics_model.dart';
import '../../../models/visit/trainingtopics_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class GroupTrainingTopicsScreen extends StatefulWidget {
  const GroupTrainingTopicsScreen({super.key});

  @override
  State<GroupTrainingTopicsScreen> createState() =>
      _GroupTrainingTopicsScreenState();
}

class _GroupTrainingTopicsScreenState extends State<GroupTrainingTopicsScreen> {
  bool isLoading = false;

  final List<dynamic> _singleFarmersLocal = [];
  String farmer_name = "";
  List<Map<String, dynamic>> addedtopics = [];
  List<dynamic> _allPreviousTrainingSessions = [];
  bool isSessionOpen = false;
  String userId = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAllGroupTrainingSessionsByDay(
        "farm_inspector_visitNo", "group", "category");
  }

  _getAllGroupTrainingSessionsByDay(
      var column, var category, var column1) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userId = localStorage.getString('UserId');
    setState(() {
      userId = userId.toString();
    });
    _allPreviousTrainingSessions = [];
    try {
      var res = await FarmersController()
          .getAllGroupFarmerTrainingSessions(userId, column, category, column1);
      if (res != null) {
        setState(() {
          _allPreviousTrainingSessions = res;
        });
        print("Found the sessions");
      } else {
        print("Found No sessions");
      }
    } catch (e) {
      print("Error on sessions$e");
    }
  }

  // _checkIfSessionOpen(String sessionId) async {
  //   var result = await DatabaseHelper.instance.checkActiveSession(sessionId);

  //   result.isEmpty ? isSessionOpen = false : isSessionOpen = true;
  //   //return result.isEmpty ? false : true;
  // }

  _closeOpenGroupSession(String sessionNo) async {
    DateTime today = DateTime.now();
    // FarmersTrainingMainModel farmerTrainingMainModel =
    //     FarmersTrainingMainModel();
    List<dynamic> farmerTrainingMainList = [];
    // farmerTrainingMainModel.noFamilyMembers =
    //     nofamilyMembersController.text.toString();
    // farmerTrainingMainModel.noOfPeople = noOfWorkersController.text.toString();
    // farmerTrainingMainModel.nofslbFarmers = fslbController.text.toString();
    // farmerTrainingMainModel.no_nonfslbFarmers =
    //     nonFSLBController.text.toString();
    // farmerTrainingMainModel.startTime = today.toString();

    var object = {
      "end_time": today.toString(),
    };

    farmerTrainingMainList.add(object);
    int j = await FarmersController().updateEndSession(
        farmerTrainingMainList, farmerTrainingMainTable, sessionNo);
    if (j > 0) {
      print("Print Success");
      //Get.to(() => TrainingTopicsScreen(), arguments: pageId);
      await _getAllGroupTrainingSessionsByDay(
          "farm_inspector_visitNo", "group", "category");
      setState(() {});
      Navigator.of(context).pop();
    }
  }

  String getDate(String a) {
    var date = DateTime.parse(a);
    return DateFormat('EEE, MMM d , yyyy, hh:mm aaa').format(date).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const MenuScreen(), arguments: 1);
                    },
                  ),
                  BigText(
                    text: "Training topics",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            color: Colors.white,
            width: Dimensions.screenWidth,
            child: isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
                : SizedBox(
                    height: Dimensions.screenHeight - Dimensions.height30 * 3,
                    child: Stack(
                      children: [
                        // BigText(
                        //   text: notesController.text.toString(),
                        //   color: Colors.black,
                        // ),
                        _allPreviousTrainingSessions.isEmpty
                            ? Container()
                            : SizedBox(
                                height: Dimensions.height20 *
                                    (Dimensions.height45 -
                                        Dimensions.height30 / 4),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  //physics: const NeverScrollableScrollPhysics(),
                                  itemCount:
                                      _allPreviousTrainingSessions.length,
                                  itemBuilder: (context, index) {
                                    //final item = visits[index];

                                    String? startTime =
                                        _allPreviousTrainingSessions[index]
                                            ['start_time'];
                                    String? endTime =
                                        _allPreviousTrainingSessions[index]
                                            ['end_time'];

                                    return GestureDetector(
                                      onTap: () async {
                                        endTime == null && startTime == null
                                            ? Get.to(
                                                () =>
                                                    const AddGroupSessionScreen(),
                                                arguments: [
                                                    _allPreviousTrainingSessions[
                                                        index]['sessionNo'],
                                                    false
                                                  ])
                                            : endTime != null &&
                                                    startTime != null
                                                ? Get.to(
                                                    () =>
                                                        const AddGroupSessionScreen(),
                                                    arguments: [
                                                        _allPreviousTrainingSessions[
                                                            index]['sessionNo'],
                                                        true
                                                      ])
                                                : _endGroupTrainingSessionPopUp(
                                                    _allPreviousTrainingSessions[
                                                            index]['sessionNo']
                                                        .toString(),
                                                  );
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: Dimensions.width20,
                                          top: Dimensions.height15,
                                        ),
                                        child: Row(
                                          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Image.asset(
                                              'assets/images/ic_farm_activities.png',
                                              width: Dimensions.width30 * 2,
                                              height: Dimensions.width30 * 2,
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    SmallText(
                                                      text: "Title: ",
                                                      color: Colors.black,
                                                      size:
                                                          Dimensions.font12 + 1,
                                                    ),
                                                    SmallText(
                                                      text:
                                                          _allPreviousTrainingSessions[
                                                                      index]
                                                                  ['title']
                                                              .toString(),
                                                      color: Colors.black,
                                                      size:
                                                          Dimensions.font12 + 1,
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    SmallText(
                                                      text: "Start Time: ",
                                                      color: Colors.black,
                                                      size:
                                                          Dimensions.font12 + 1,
                                                    ),
                                                    startTime != null
                                                        ? SmallText(
                                                            text: getDate(
                                                                startTime
                                                                    .toString()),
                                                            color: Colors.black,
                                                            size: Dimensions
                                                                    .font12 +
                                                                1,
                                                          )
                                                        : Container()
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    SmallText(
                                                      text: "End Time: ",
                                                      color: Colors.black,
                                                      size:
                                                          Dimensions.font12 + 1,
                                                    ),
                                                    endTime != null
                                                        ? SmallText(
                                                            text: getDate(endTime
                                                                .toString()),
                                                            color: Colors.black,
                                                            size: Dimensions
                                                                    .font12 +
                                                                1,
                                                          )
                                                        : Container()
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    SmallText(
                                                      text: "Status: ",
                                                      color: Colors.black,
                                                      size:
                                                          Dimensions.font12 + 1,
                                                    ),
                                                    startTime == null
                                                        ? Container(
                                                            width: Dimensions
                                                                    .height30 *
                                                                3,
                                                            height: Dimensions
                                                                .height20,
                                                            decoration: BoxDecoration(
                                                                color: Colors
                                                                    .yellow,
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        Dimensions.radius15 /
                                                                            3)),
                                                            child: Center(
                                                                child: BigText(
                                                              text: "Pending",
                                                              color: AppColors
                                                                  .black,
                                                              size: Dimensions
                                                                  .font12,
                                                            )),
                                                          )
                                                        : endTime == null
                                                            ? Container(
                                                                width: Dimensions
                                                                        .height30 *
                                                                    3,
                                                                height: Dimensions
                                                                    .height20,
                                                                decoration: BoxDecoration(
                                                                    color: Colors
                                                                        .green,
                                                                    borderRadius:
                                                                        BorderRadius.circular(Dimensions.radius15 /
                                                                            3)),
                                                                child: Center(
                                                                    child:
                                                                        BigText(
                                                                  text: "Open",
                                                                  color:
                                                                      AppColors
                                                                          .black,
                                                                  size: Dimensions
                                                                      .font12,
                                                                )),
                                                              )
                                                            : Container(
                                                                width: Dimensions
                                                                        .height30 *
                                                                    3,
                                                                height: Dimensions
                                                                    .height20,
                                                                decoration: BoxDecoration(
                                                                    color: Colors
                                                                        .grey,
                                                                    borderRadius:
                                                                        BorderRadius.circular(Dimensions.radius15 /
                                                                            3)),
                                                                child: Center(
                                                                    child:
                                                                        BigText(
                                                                  text:
                                                                      "Closed",
                                                                  color:
                                                                      AppColors
                                                                          .black,
                                                                  size: Dimensions
                                                                      .font12,
                                                                )),
                                                              ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: Dimensions.height20,
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: EdgeInsets.only(
                              bottom: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return _MyDialog(
                                        category: "group",
                                        userId: userId,
                                        onPreviousTrainingTopicsListChanged:
                                            (topics) {
                                          setState(() {
                                            _allPreviousTrainingSessions =
                                                topics;
                                          });
                                          print(_allPreviousTrainingSessions);
                                        },
                                      );
                                    });
                              },
                              child: Container(
                                height: Dimensions.height30 * 2,
                                width:
                                    Dimensions.screenWidth - Dimensions.width45,
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30),
                                ),
                                child: Center(
                                  child: BigText(
                                    text: "Add Topic",
                                    color: AppColors.textWhite,
                                    size: Dimensions.font16,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  _endGroupTrainingSessionPopUp(String sessionNo) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            insetPadding: EdgeInsets.zero,
            // title: Center(
            //   child: BigText(
            //     text: "",
            //     color: AppColors.black,
            //     size: Dimensions.font16,
            //   ),
            // ),
            content: SingleChildScrollView(
              child: SizedBox(
                child: Container(
                  // height: Dimensions.screenHeight + (Dimensions.height30 * 4),
                  width: Dimensions.screenWidth - (Dimensions.width30 * 4),
                  margin: EdgeInsets.only(top: Dimensions.height30),
                  padding: EdgeInsets.only(
                    top: Dimensions.height10,
                    left: Dimensions.width10,
                    right: Dimensions.width10,
                    bottom: Dimensions.height15,
                  ),
                  decoration: BoxDecoration(
                    //color: Colors.green,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(
                        Dimensions.radius20 * 2,
                      ),
                      topLeft: Radius.circular(
                        Dimensions.radius20 * 2,
                      ),
                    ),
                  ),
                  child: Column(
                    children: [
                      BigText(
                        text: "End The Session?",
                        color: AppColors.mainColor,
                        size: Dimensions.font16,
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              _closeOpenGroupSession(sessionNo);
                            },
                            child: Container(
                              height: Dimensions.height30 * 2,
                              width:
                                  Dimensions.width30 * Dimensions.width10 / 2,
                              decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius30),
                              ),
                              child: Center(
                                child: BigText(
                                  text: "Continue",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font16,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Container(
                              height: Dimensions.height30 * 2,
                              width:
                                  Dimensions.width30 * Dimensions.width10 / 2,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.circular(Dimensions.radius30),
                              ),
                              child: Center(
                                child: BigText(
                                  text: "Cancel",
                                  color: AppColors.textWhite,
                                  size: Dimensions.font16,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Dimensions.height15,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // actions: <Widget>[
            //   TextButton(
            //     child: Text('Cancel'),
            //     onPressed: () {
            //       Navigator.of(context).pop();
            //     },
            //   ),
            //   TextButton(
            //     child: Text('OK'),
            //     onPressed: () {
            //       setState(() {
            //         Navigator.of(context).pop();
            //       });
            //     },
            //   ),
            // ],
          );
        });
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    //§required this.farmerId,
    //required this.topics,
    //required this.selectedTopics,
    required this.onPreviousTrainingTopicsListChanged,
    this.category,
    required this.userId,
    //required this.farmername,
    //required this.onSelectednoteInfo,
  });

  // final List<String> cities;
  // final List<String> selectedCities;
  final ValueChanged<List<dynamic>> onPreviousTrainingTopicsListChanged;
  // final ValueChanged<List<Map<String, dynamic>>> onSelectedTopicsListChanged;
  // final ValueChanged<String> onSelectednoteInfo;
  // final List<Map<String, dynamic>> topics;
  // final List<Map<String, dynamic>> selectedTopics;
  // String farmername;
  var farmerId;
  var category;
  String userId;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  bool isLoading = false;
  final List<Map<String, dynamic>> topics = [];
  final List<Map<String, dynamic>> selectedTopics = [];
  final List<Map<String, dynamic>> _tempSelectedTopics = [];
  var coachingTitleController = TextEditingController();
  final List<dynamic> _singleFarmersLocal = [];
  String farmer_name = "";
  String member_no = "";
  List<dynamic> _allPreviousTrainingSessions = [];

  _getTrainingTopics() async {
    setState(() => isLoading = true);
    List<dynamic> resTrainingTopics = [];
    var res = await LocationController().getLocationsLocal(trainingTopicsTable);
    //print("Hello there"+ res[0].toString());
    if (res != null) {
      resTrainingTopics = res;
      for (var i in resTrainingTopics) {
        TrainingTopicsModelResultElement model =
            TrainingTopicsModelResultElement(
                resultId: i['Id'], name: i['name']);

        var newdata = {
          "id": model.resultId,
          "name": model.name,
          // "selected": false,
        };

        //print("Name: " + newdata['name'].toString() + "ID: " + newdata['id'].toString());

        topics.add(newdata);
      }
    }
    setState(() => isLoading = false);
  }

  // Future _getSingleFarmerLocal(String farmerId) async {
  //   setState(() => isLoading = true);
  //   print("At Function");
  //   _singleFarmersLocal = [];
  //   var res = await FarmersController().getSingleFarmerLocal(farmerId);

  //   if (res != null) {
  //     setState(() {
  //       _singleFarmersLocal = res;
  //     });
  //     if (_singleFarmersLocal.isNotEmpty) {
  //       print(_singleFarmersLocal[0]["full_name"]!.toString());
  //       farmer_name = _singleFarmersLocal[0]["full_name"]!.toString();
  //       member_no = _singleFarmersLocal[0]["member_no"]!.toString();

  //       print(farmer_name);

  //       await _getTrainingTopics();

  //       setState(() => isLoading = false);
  //     } else {
  //       print("Did not Get");
  //     }
  //   }
  // }

  _getAllGroupTrainingSessionsByDay(
      var column, var category, var column1) async {
    _allPreviousTrainingSessions = [];
    try {
      var res = await FarmersController().getAllGroupFarmerTrainingSessions(
          widget.userId, column, category, column1);
      if (res != null) {
        setState(() {
          _allPreviousTrainingSessions = res;
          widget.onPreviousTrainingTopicsListChanged(
              _allPreviousTrainingSessions);
        });
        print("Found the sessions");
      } else {
        print("Found No sessions");
      }
    } catch (e) {
      print("Error on sessions$e");
    }
  }

  @override
  void initState() {
    super.initState();

    _getTrainingTopics();
  }

  String generateUniqueVarChar(int length) {
    final rand = Random();
    const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    String result = '';
    for (int i = 0; i < length; i++) {
      result += chars[rand.nextInt(chars.length)];
    }
    return result;
  }

  _addCoachingTopics() async {
    if (coachingTitleController.text == '') {
    } else if (_tempSelectedTopics.isEmpty) {
    } else {
      String sessionId = generateUniqueVarChar(14).toString();
      for (var element in _tempSelectedTopics) {
        List<dynamic> topics = [];
        FarmersTrainingTopicsModel farmersTrainingTopicModel =
            FarmersTrainingTopicsModel();
        String transactionNumber =
            'TRN_${DateTime.now().millisecondsSinceEpoch.toString()}';

        //farmersTrainingTopicModel.notes = notesController.text;
        farmersTrainingTopicModel.topic = element['name'].toString();
        farmersTrainingTopicModel.topicId = element['member_no'];
        farmersTrainingTopicModel.sessionId = sessionId.toString();
        farmersTrainingTopicModel.transactionNo = transactionNumber;
        farmersTrainingTopicModel.syncId = "p";

        print(transactionNumber.toString());

        topics.add(farmersTrainingTopicModel);

        int i = await FarmersController()
            .saveAll(topics, farmerTrainingTopicsTable);

        if (i >= 0) {
          print("Succeeded");
        }
      }
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var UserId = localStorage.getString('UserId');

      DateTime today = DateTime.now();
      String dateStr = "${today.day}-${today.month}-${today.year}";
      FarmersTrainingMainModel farmerTrainingMainModel =
          FarmersTrainingMainModel();
      List<dynamic> farmerTrainingMainList = [];
      farmerTrainingMainModel.sessionNo = sessionId.toString();
      farmerTrainingMainModel.category = widget.category.toString();
      farmerTrainingMainModel.coachingDate = dateStr.toString();
      farmerTrainingMainModel.title = coachingTitleController.text.toString();
      farmerTrainingMainModel.syncId = "p";
      farmerTrainingMainModel.farmInspectorVisitNo = UserId.toString();
      //farmerTrainingMainModel.memberId = member_no.toString();
      farmerTrainingMainList.add(farmerTrainingMainModel);
      int j = await FarmersController().saveAll(farmerTrainingMainList, farmerTrainingMainTable);

      if (j >= 0) {
        print("Success Here");
        await _getAllGroupTrainingSessionsByDay(
            "farm_inspector_visitNo", widget.category.toString(), "category");

        Navigator.of(context).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: AlertDialog(
        insetPadding: EdgeInsets.zero,
        title: const Text('Select Topics'),
        content: SizedBox(
          width: Dimensions.screenWidth,
          height: Dimensions.screenHeight - (Dimensions.height30 * 4),
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height10,
              ),
              GestureDetector(
                onTap: () async {
                  print(FarmersController()
                      .deleteFarmerVisits(farmerTrainingTopicsTable)
                      .toString());

                  print(FarmersController()
                      .deleteFarmerVisits(farmerTrainingMainTable)
                      .toString());
                },
                child: BigText(
                  text: "Clear",
                  color: AppColors.black,
                  size: Dimensions.font16,
                ),
              ),
              BigText(
                text: "Enter Coaching Title",
                color: AppColors.black,
                size: Dimensions.font16,
              ),
              Container(
                padding: EdgeInsets.only(
                    top: Dimensions.height10, bottom: Dimensions.height10),
                child: TextField(
                  maxLines: 5,
                  autocorrect: true,
                  controller: coachingTitleController,
                  // onChanged: (value) {
                  //   notesController.text = value;
                  // },
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(
                    hintText: "Notes",
                    hintStyle: const TextStyle(color: Colors.white),
                    filled: true,
                    fillColor: Colors.white70,
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(Dimensions.radius15 - 5)),
                      borderSide:
                          BorderSide(color: AppColors.textGrey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(Dimensions.radius15)),
                      borderSide: const BorderSide(color: Colors.green, width: 2),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: Dimensions.height15,
              ),
              BigText(
                text: "Training Topics",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              Expanded(
                child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: topics.length,
                    itemBuilder: (BuildContext context, int index) {
                      final topicItem = topics[index];
                      return Container(
                        margin: EdgeInsets.only(
                          top: Dimensions.height10 / 2,
                          bottom: Dimensions.height10 / 2,
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.green),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CheckboxListTile(
                            activeColor: Colors.green,
                            checkColor: Colors.white,
                            title: Text(topicItem['name']),
                            value: _tempSelectedTopics.contains(topicItem),
                            onChanged: (bool? value) {
                              if (value == true) {
                                if (!_tempSelectedTopics.contains(topicItem)) {
                                  setState(() {
                                    _tempSelectedTopics.add(topicItem);
                                  });
                                }
                              } else {
                                if (_tempSelectedTopics.contains(topicItem)) {
                                  setState(() {
                                    _tempSelectedTopics.removeWhere(
                                        (var topic) => topic == topicItem);
                                  });
                                }
                              }
                            }),
                      );
                    }),
              ),
              SizedBox(
                height: Dimensions.height15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    child: const Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: const Text('OK'),
                    onPressed: () {
                      setState(() {
                        _addCoachingTopics();
                      });
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
