import 'dart:convert';
import 'dart:io';

import 'package:farmcapturev2/models/visit/coachingAttendance_model.dart';
import 'package:farmcapturev2/resources/widgets/small_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/farmers/allfarmers_model.dart';
import '../../../models/visit/farmerTrainingMain_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import 'addgroup_trainingtopics.dart';

class AddGroupSessionScreen extends StatefulWidget {
  const AddGroupSessionScreen({super.key});

  @override
  State<AddGroupSessionScreen> createState() => _AddGroupSessionScreenState();
}

class _AddGroupSessionScreenState extends State<AddGroupSessionScreen> {
  var sessionNo = Get.arguments[0];
  var isView = Get.arguments[1];
  bool isLoading = false;
  File? image;

  final List<Map<String, dynamic>> _allFarmersLocal = [];
  List<Map<String, dynamic>> selectedFarmersData = [];
  List<dynamic> _singleSessionLocal = [];
  List<dynamic> _singleSessionLocalMembersPresent = [];

  _getAllFarmersLocal() async {
    setState(() => isLoading = true);
    List<dynamic> resFarmers = [];
    var res = await FarmersController().getAllFarmersLocal();
    if (res != null) {
      resFarmers = res;
      for (var i in resFarmers) {
        FarmerModelDB model = FarmerModelDB(resultId: i['Id'],fullName: i['full_name'],memberNo: i['member_no']);

        var newdata = {
          "id": model.resultId,
          "full_name": model.fullName,
          "member_no": model.memberNo,
        };

        _allFarmersLocal.add(newdata);
      }

      isView ? _getSingleSessionLocal(sessionNo) : null;
    }
    setState(() => isLoading = false);
  }

  Future _getImage() async {
    final picker = ImagePicker();

    var pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (pickedFile != null) {
      final bytes = File(pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

      setState(() {
        image = File(pickedFile.path);
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    //return image;
  }

  Future<bool> checkAndRequestCameraPermissions() async {
    var status = await Permission.camera.status;
    if (status.isDenied) {
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
      return false;
    }
    return true;
  }

  _addGroupSession() async {
    if (selectedFarmersData.isEmpty || image.toString().isEmpty) {
    } else {
      for (var element in selectedFarmersData) {
        List<dynamic> session = [];
        DateTime today = DateTime.now();
        String dateStr = "${today.day}-${today.month}-${today.year}";
        var transactionNo = 'VST_${DateTime.now().millisecondsSinceEpoch.toString()}';
        FarmersCoachingModel groupModel = FarmersCoachingModel();
        groupModel.farmerId = element['member_no'].toString();
        groupModel.sessionId = sessionNo.toString();
        groupModel.dateToday = dateStr.toString();
        groupModel.transactionNo = transactionNo.toString();
        session.add(groupModel);

        int i = await FarmersController()
            .saveAll(session, farmerCoachingAttendanceTable);

        if (i >= 0) {
          print("Succeeded");
          DateTime today = DateTime.now();

          List<dynamic> farmerTrainingMainList = [];

          var object = {
            "start_time": today.toString(),
            "photo_url": image.toString(),
          };

          farmerTrainingMainList.add(object);

          int j = await FarmersController().updateGroupFarmerMainCoaching(farmerTrainingMainList, farmerTrainingMainTable, sessionNo);

          if (j > 0) {
            print("Print Success");
            Get.to(() => const GroupTrainingTopicsScreen(), arguments: sessionNo);
          }
        }
      }
    }
  }

  _getAllFarmersLocalPresentMembers(var id) async {
    List<dynamic> resFarmers = [];
    var res = await FarmersController().getSingleFarmerLocal(id);
    if (res != null) {
      resFarmers = res;
      for (var i in resFarmers) {
        print(i['full_name'].toString());
        FarmerModelDB model = FarmerModelDB(resultId: i['Id'],fullName: i['full_name'],memberNo: i['member_no']);

        var newdata = {
          "id": model.resultId,
          "full_name": model.fullName,
          "member_no": model.memberNo,
        };

        selectedFarmersData.add(newdata);
      }
    }
  }

  Future _getSingleSessionLocalMembers(String sessionId) async {
    _singleSessionLocalMembersPresent = [];
    var res = await FarmersController()
        .getSingleSessionLocalMembersPresent(sessionId);

    if (res != null) {
      _singleSessionLocalMembersPresent = res;

      if (_singleSessionLocalMembersPresent.isNotEmpty) {
        print(
            "Length is:${_singleSessionLocalMembersPresent.length}");
        for (var element in _singleSessionLocalMembersPresent) {
          print(element['farmerId'].toString());
          await _getAllFarmersLocalPresentMembers(
              element['farmerId'].toString());
        }
      }
    }
  }

  Future _getSingleSessionLocal(String sessionId) async {
    setState(() => isLoading = true);
    print("At Function");
    _singleSessionLocal = [];
    var res = await FarmersController().getSingleSessionLocal(sessionId);

    if (res != null) {
      setState(() {
        _singleSessionLocal = res;
      });
      if (_singleSessionLocal.isNotEmpty) {
        print(_singleSessionLocal[0]["title"]!.toString());

        image = File(_singleSessionLocal[0]["photo_url"]!.toString().substring(
            7, _singleSessionLocal[0]["photo_url"]!.toString().length - 1));

        await _getSingleSessionLocalMembers(sessionId);

        setState(() => isLoading = false);
      } else {
        print("Did not Get");
      }
    }
    setState(() => isLoading = false);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAllFarmersLocal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const GroupTrainingTopicsScreen());
                    },
                  ),
                  BigText(
                    text: "Add Group Session",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            color: Colors.white,
            width: Dimensions.screenWidth,
            child: isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
                : SizedBox(
                    child: Column(
                      children: [
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        isView
                            ? Container()
                            : GestureDetector(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return _MyDialog(
                                            farmers: _allFarmersLocal,
                                            selectedFarmers:
                                                selectedFarmersData,
                                            onSelectedFarmersListChanged:
                                                (farmers) {
                                              setState(() {
                                                selectedFarmersData = farmers;
                                              });
                                              print(selectedFarmersData);
                                            });
                                      });
                                },
                                child: Center(
                                  child: Container(
                                      width: Dimensions.screenWidth -
                                          (Dimensions.width30 * 3),
                                      height: Dimensions.height30 * 2,
                                      decoration: BoxDecoration(
                                        color: Colors.grey[300],
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius15 / 2),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.add,
                                            size: Dimensions.height30,
                                            color: Colors.black,
                                          ),
                                          SizedBox(
                                            width: Dimensions.width15,
                                          ),
                                          BigText(
                                            text: "Add Members Present",
                                            color: AppColors.black,
                                            size: Dimensions.font16,
                                          ),
                                        ],
                                      )),
                                ),
                              ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Center(
                          child: BigText(
                            text: "Members Present",
                            color: AppColors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                        SizedBox(
                          height: isView
                              ? Dimensions.screenHeight / 2
                              : Dimensions.screenHeight / 3.5,
                          //height: Dimensions.height20 * (Dimensions.height45 - Dimensions.height30 / 4),
                          child: ListView.builder(
                            shrinkWrap: true,
                            //physics: const NeverScrollableScrollPhysics(),
                            itemCount: selectedFarmersData.length,
                            itemBuilder: (context, index) {
                              //final item = visits[index];
                              return Container(
                                margin:
                                    EdgeInsets.only(left: Dimensions.width20),
                                decoration: const BoxDecoration(),
                                //width: Dimensions.screenWidth / 1.5,
                                child: Row(
                                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Image.asset(
                                      'assets/images/ic_user.png',
                                      width: Dimensions.width30 * 2,
                                      height: Dimensions.width30 * 2,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SmallText(
                                          text: selectedFarmersData[index]
                                                  ['full_name']
                                              .toString(),
                                          color: Colors.black,
                                          size: Dimensions.font12 + 1,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              BigText(
                                text: "Session Photos",
                                color: AppColors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                              Container(
                                height:
                                    Dimensions.height10 * Dimensions.height20,
                                width: Dimensions.screenWidth / 2,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20 / 2),
                                    color: AppColors.textGrey),
                                child: image != null
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius15),
                                        child: Image.file(image!,
                                            fit: BoxFit.cover),
                                      )
                                    : const Center(
                                        child: Text('Please add a photo'),
                                      ),
                              ),
                              SizedBox(
                                width: Dimensions.width30,
                              ),
                              isView
                                  ? Container()
                                  : MaterialButton(
                                      color: Colors.blue,
                                      onPressed: _getImage,
                                      child: Text(
                                        "Take a Photo",
                                        style: TextStyle(
                                            color: AppColors.textWhite,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                        isView
                            ? Container()
                            : GestureDetector(
                                onTap: () {
                                  _addGroupSession();
                                },
                                child: Container(
                                  height: Dimensions.height30 * 2,
                                  width:
                                      Dimensions.width30 * Dimensions.width10,
                                  decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius30),
                                  ),
                                  child: Center(
                                    child: BigText(
                                      text: "Submit",
                                      color: AppColors.textWhite,
                                      size: Dimensions.font16,
                                    ),
                                  ),
                                ),
                              ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}

class _MyDialog extends StatefulWidget {
  const _MyDialog({
    required this.farmers,
    required this.selectedFarmers,
    required this.onSelectedFarmersListChanged,
  });

  // final List<String> cities;
  // final List<String> selectedCities;
  //final ValueChanged<List<String>> onSelectedCitiesListChanged;
  final ValueChanged<List<Map<String, dynamic>>> onSelectedFarmersListChanged;
  final List<Map<String, dynamic>> farmers;
  final List<Map<String, dynamic>> selectedFarmers;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  String searchString = "";
  //List<String> _tempSelectedCities = [];
  List<Map<String, dynamic>> _tempSelectedFarmers = [];

  @override
  void initState() {
    _tempSelectedFarmers = widget.selectedFarmers;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: const Text('Select Members'),
      content: SizedBox(
        width: Dimensions.screenWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: Dimensions.height30 * 4,
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: Dimensions.screenWidth / 1.3,
                        padding: EdgeInsets.only(
                          top: Dimensions.height10,
                          bottom: Dimensions.height10,
                        ),
                        child: Center(
                          child: TextField(
                            onChanged: (value) {
                              setState(() {
                                searchString = value.toString().toLowerCase();
                              });
                            },
                            autocorrect: true,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: "Search Farmer",
                              hintStyle: TextStyle(color: Colors.grey[700]),
                              filled: true,
                              fillColor: Colors.grey[300],
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius30)),
                                borderSide: BorderSide(
                                    color: AppColors.textGrey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius30)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreenAccent, width: 2),
                              ),
                            ),
                          ),
                        )),
                    Flexible(
                      child: Container(
                        width: Dimensions.width30 * 2,
                        height: Dimensions.height30 * 2,
                        padding: EdgeInsets.only(
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.magnifyingGlass,
                            size: Dimensions.height20,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: Dimensions.height20,
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: widget.farmers.length,
                  itemBuilder: (BuildContext context, int index) {
                    final FarmerItem = widget.farmers[index];
                    return widget.farmers[index]["full_name"]!
                            .toString()
                            .toLowerCase()
                            .contains(searchString)
                        ? Container(
                            margin: EdgeInsets.only(
                              top: Dimensions.height10 / 2,
                              bottom: Dimensions.height10 / 2,
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.green),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: CheckboxListTile(
                                activeColor: Colors.green,
                                checkColor: Colors.white,
                                title: Text(FarmerItem['full_name']),
                                value:
                                    _tempSelectedFarmers.contains(FarmerItem),
                                onChanged: (bool? value) {
                                  if (value == true) {
                                    if (!_tempSelectedFarmers
                                        .contains(FarmerItem)) {
                                      setState(() {
                                        _tempSelectedFarmers.add(FarmerItem);
                                      });
                                    }
                                  } else {
                                    if (_tempSelectedFarmers
                                        .contains(FarmerItem)) {
                                      setState(() {
                                        _tempSelectedFarmers.removeWhere(
                                            (var farmer) =>
                                                farmer == FarmerItem);
                                      });
                                    }
                                  }
                                  widget.onSelectedFarmersListChanged(
                                      _tempSelectedFarmers);
                                }),
                          )
                        : Container();
                  }),
            ),
            SizedBox(
              height: Dimensions.height15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  child: const Text('OK'),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                    });
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
