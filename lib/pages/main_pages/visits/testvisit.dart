import 'dart:convert';
import 'dart:io';

import 'package:farmcapturev2/pages/main_pages/visits/addgroup_trainingtopics.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../controllers/farmers/farmers_controller.dart';
import '../../../models/farmers/allfarmers_model.dart';
import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //bool checkboxValueCity = false;
  //List<String> allCities = ['Alpha', 'Beta', 'Gamma'];

  //List<String> selectedCities = [];
  bool isLoading = false;
  File? image;

  final List<Map<String, dynamic>> _allFarmersLocal = [];
  List<Map<String, dynamic>> selectedFarmersData = [];

  _getAllFarmersLocal() async {
    setState(() => isLoading = true);
    List<dynamic> resFarmers = [];
    var res = await FarmersController().getAllFarmersLocal();
    if (res != null) {
      resFarmers = res;
      for (var i in resFarmers) {
        FarmerModelDB model = FarmerModelDB(
            resultId: i['id'],
            fullName: i['full_name'],
            memberNo: i['member_no']);

        var newdata = {
          "id": model.resultId,
          "full_name": model.fullName,
          "member_no": model.memberNo,
          "selected": false
        };

        _allFarmersLocal.add(newdata);
      }
    }
    setState(() => isLoading = false);
  }

  Future _getImage() async {
    final picker = ImagePicker();

    var pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (pickedFile != null) {
      final bytes = File(pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

      setState(() {
        image = File(pickedFile.path);
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    //return image;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getAllFarmersLocal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: const Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.to(() => const GroupTrainingTopicsScreen());
                    },
                  ),
                  BigText(
                    text: "Add Group Session",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  const SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            color: Colors.white,
            width: Dimensions.screenWidth,
            child: isLoading
                ? SizedBox(
                    height: Dimensions.screenHeight,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [CustomLoader()],
                    ),
                  )
                : SizedBox(
                    child: Column(
                      children: [
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return _MyDialog(
                                      farmers: _allFarmersLocal,
                                      selectedFarmers: selectedFarmersData,
                                      onSelectedFarmersListChanged: (farmers) {
                                        setState(() {
                                          selectedFarmersData = farmers;
                                        });
                                        print(selectedFarmersData);
                                      });
                                });
                          },
                          child: Center(
                            child: Container(
                                width: Dimensions.screenWidth -
                                    (Dimensions.width30 * 3),
                                height: Dimensions.height30 * 2,
                                decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius15 / 2),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.add,
                                      size: Dimensions.height30,
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      width: Dimensions.width15,
                                    ),
                                    BigText(
                                      text: "Add Members Present",
                                      color: AppColors.black,
                                      size: Dimensions.font16,
                                    ),
                                  ],
                                )),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Center(
                          child: BigText(
                            text: "Members Present",
                            color: AppColors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.screenHeight / 3.5,
                          //height: Dimensions.height20 * (Dimensions.height45 - Dimensions.height30 / 4),
                          child: ListView.builder(
                            shrinkWrap: true,
                            //physics: const NeverScrollableScrollPhysics(),
                            itemCount: selectedFarmersData.length,
                            itemBuilder: (context, index) {
                              //final item = visits[index];
                              return Container(
                                margin:
                                    EdgeInsets.only(left: Dimensions.width20),
                                decoration: const BoxDecoration(),
                                //width: Dimensions.screenWidth / 1.5,
                                child: Row(
                                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Image.asset(
                                      'assets/images/ic_user.png',
                                      width: Dimensions.width30 * 2,
                                      height: Dimensions.width30 * 2,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SmallText(
                                          text: selectedFarmersData[index]
                                                  ['full_name']
                                              .toString(),
                                          color: Colors.black,
                                          size: Dimensions.font12 + 1,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              BigText(
                                text: "Session Photos",
                                color: AppColors.black,
                                size: Dimensions.font16 - 2,
                              ),
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                              Container(
                                height:
                                    Dimensions.height10 * Dimensions.height20,
                                width: Dimensions.screenWidth / 2,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius20 / 2),
                                    color: AppColors.textGrey),
                                child: image != null
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius15),
                                        child: Image.file(image!,
                                            fit: BoxFit.cover),
                                      )
                                    : const Center(
                                        child: Text('Please add a photo'),
                                      ),
                              ),
                              SizedBox(
                                width: Dimensions.width30,
                              ),
                              MaterialButton(
                                color: Colors.blue,
                                onPressed: _getImage,
                                child: Text(
                                  "Take a Photo",
                                  style: TextStyle(
                                      color: AppColors.textWhite,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: Dimensions.height30 * 2,
                            width: Dimensions.width30 * Dimensions.width10,
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius:
                                  BorderRadius.circular(Dimensions.radius30),
                            ),
                            child: Center(
                              child: BigText(
                                text: "Submit",
                                color: AppColors.textWhite,
                                size: Dimensions.font16,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height30,
                        ),
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}

class _MyDialog extends StatefulWidget {
  const _MyDialog({
    required this.farmers,
    required this.selectedFarmers,
    required this.onSelectedFarmersListChanged,
  });

  // final List<String> cities;
  // final List<String> selectedCities;
  //final ValueChanged<List<String>> onSelectedCitiesListChanged;
  final ValueChanged<List<Map<String, dynamic>>> onSelectedFarmersListChanged;
  final List<Map<String, dynamic>> farmers;
  final List<Map<String, dynamic>> selectedFarmers;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  String searchString = "";
  //List<String> _tempSelectedCities = [];
  List<Map<String, dynamic>> _tempSelectedFarmers = [];

  @override
  void initState() {
    _tempSelectedFarmers = widget.selectedFarmers;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: const Text('Select Members'),
      content: SizedBox(
        width: Dimensions.screenWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: Dimensions.height30 * 4,
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: Dimensions.screenWidth / 1.3,
                        padding: EdgeInsets.only(
                          top: Dimensions.height10,
                          bottom: Dimensions.height10,
                        ),
                        child: Center(
                          child: TextField(
                            onChanged: (value) {
                              setState(() {
                                searchString = value.toString().toLowerCase();
                              });
                            },
                            autocorrect: true,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: "Search Farmer",
                              hintStyle: TextStyle(color: Colors.grey[700]),
                              filled: true,
                              fillColor: Colors.grey[300],
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius30)),
                                borderSide: BorderSide(
                                    color: AppColors.textGrey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius30)),
                                borderSide: const BorderSide(
                                    color: Colors.lightGreenAccent, width: 2),
                              ),
                            ),
                          ),
                        )),
                    Flexible(
                      child: Container(
                        width: Dimensions.width30 * 2,
                        height: Dimensions.height30 * 2,
                        padding: EdgeInsets.only(
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.magnifyingGlass,
                            size: Dimensions.height20,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: Dimensions.height20,
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: widget.farmers.length,
                  itemBuilder: (BuildContext context, int index) {
                    final FarmerItem = widget.farmers[index];
                    return widget.farmers[index]["full_name"]!
                            .toString()
                            .toLowerCase()
                            .contains(searchString)
                        ? Container(
                            margin: EdgeInsets.only(
                              top: Dimensions.height10 / 2,
                              bottom: Dimensions.height10 / 2,
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.green),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: CheckboxListTile(
                                activeColor: Colors.green,
                                checkColor: Colors.white,
                                title: Text(FarmerItem['full_name']),
                                value:
                                    _tempSelectedFarmers.contains(FarmerItem),
                                onChanged: (bool? value) {
                                  if (value == true) {
                                    if (!_tempSelectedFarmers
                                        .contains(FarmerItem)) {
                                      setState(() {
                                        _tempSelectedFarmers.add(FarmerItem);
                                      });
                                    }
                                  } else {
                                    if (_tempSelectedFarmers
                                        .contains(FarmerItem)) {
                                      setState(() {
                                        _tempSelectedFarmers.removeWhere(
                                            (var farmer) =>
                                                farmer == FarmerItem);
                                      });
                                    }
                                  }
                                  widget.onSelectedFarmersListChanged(
                                      _tempSelectedFarmers);
                                }),
                          )
                        : Container();
                  }),
            ),
            SizedBox(
              height: Dimensions.height15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  child: const Text('OK'),
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                    });
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}


// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../resources/base/custom_loader.dart';
// import '../../../resources/utils/colors.dart';
// import '../../../resources/utils/dimensions.dart';
// import '../../../resources/widgets/big_text.dart';
// import '../../../resources/widgets/small_text.dart';

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key? key, this.title}) : super(key: key);
//   final String? title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   //bool checkboxValueCity = false;
//   List<String> allCities = ['Alpha', 'Beta', 'Gamma'];
//   List<String> selectedCities = [];
//   bool isLoading = false;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size.fromHeight(Dimensions.height30 * 2),
//         child: AppBar(
//           backgroundColor: Colors.green,
//           actions: [
//             Container(
//               width: Dimensions.screenWidth,
//               height: Dimensions.height30 * 2,
//               padding: EdgeInsets.only(
//                 left: Dimensions.width15,
//                 right: Dimensions.width15,
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.green,
//                 boxShadow: [
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(5, 5),
//                       color: AppColors.gradientOne.withOpacity(0.1)),
//                   BoxShadow(
//                       blurRadius: 3,
//                       offset: Offset(-5, -5),
//                       color: AppColors.gradientOne.withOpacity(0.1))
//                 ],
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   IconButton(
//                     icon: Icon(
//                       Icons.arrow_back_ios,
//                       color: Colors.white,
//                       size: Dimensions.iconSize24,
//                     ),
//                     onPressed: () {
//                       Get.back();
//                     },
//                   ),
//                   BigText(
//                     text: "Training topics",
//                     color: AppColors.textWhite,
//                     size: Dimensions.font20,
//                   ),
//                   SizedBox()
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: SafeArea(
//           child: Container(
//             color: Colors.white,
//             width: Dimensions.screenWidth,
//             child: isLoading
//                 ? Container(
//                     height: Dimensions.screenHeight,
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [CustomLoader()],
//                     ),
//                   )
//                 : SizedBox(
//                     height: Dimensions.screenHeight - Dimensions.height30 * 3,
//                     child: Stack(
//                       children: [
//                         SizedBox(
//                           height: Dimensions.height20 * (Dimensions.height45 - Dimensions.height30 / 4),
//                           child: ListView.builder(
//                             shrinkWrap: true,
//                             //physics: const NeverScrollableScrollPhysics(),
//                             itemCount: selectedCities.length,
//                             itemBuilder: (context, index) {
//                               //final item = visits[index];
//                               return Container(
//                                 margin:
//                                     EdgeInsets.only(left: Dimensions.width20),
//                                 decoration: BoxDecoration(),
//                                 //width: Dimensions.screenWidth / 1.5,
//                                 child: Row(
//                                   //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                                   children: [
//                                     Image.asset(
//                                       'assets/images/ic_user.png',
//                                       width: Dimensions.width30 * 2,
//                                       height: Dimensions.width30 * 2,
//                                     ),
//                                     Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.start,
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.start,
//                                       children: [
//                                         SmallText(
//                                           text: selectedCities[index].toString(),
//                                           color: Colors.black,
//                                           size: Dimensions.font12 + 1,
//                                         ),
//                                       ],
//                                     ),
//                                   ],
//                                 ),
//                               );
//                             },
//                           ),
//                         ),
//                         Align(
//                           alignment: Alignment.bottomCenter,
//                           child: Padding(
//                             padding: EdgeInsets.only(
//                               bottom: Dimensions.height10,
//                             ),
//                             child: GestureDetector(
//                               onTap: () {
//                                 showDialog(
//                                   context: context,
//                                   builder: (context) {
//                                     return _MyDialog(
//                                       cities: allCities,
//                                       selectedCities: selectedCities,
//                                       onSelectedCitiesListChanged: (cities) {
//                                         setState(() {
//                                           selectedCities = cities;
//                                         });
//                                         print(selectedCities);
//                                       }
//                                     );
//                                   }
//                                 );
//                               },
//                               child: Container(
//                                 height: Dimensions.height30 * 2,
//                                 width:
//                                     Dimensions.screenWidth - Dimensions.width45,
//                                 decoration: BoxDecoration(
//                                   color: Colors.green,
//                                   borderRadius: BorderRadius.circular(
//                                       Dimensions.radius30),
//                                 ),
//                                 child: Center(
//                                   child: BigText(
//                                     text: "Add Visit",
//                                     color: AppColors.textWhite,
//                                     size: Dimensions.font16,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class _MyDialog extends StatefulWidget {
//   _MyDialog({
//     required this.cities,
//     required this.selectedCities,
//     required this.onSelectedCitiesListChanged,
//   });

//   final List<String> cities;
//   final List<String> selectedCities;
//   final ValueChanged<List<String>> onSelectedCitiesListChanged;

//   @override
//   _MyDialogState createState() => _MyDialogState();
// }

// class _MyDialogState extends State<_MyDialog> {
//   List<String> _tempSelectedCities = [];

//   @override
//   void initState() {
//     _tempSelectedCities = widget.selectedCities;
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       child: Column(
//         children: <Widget>[
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               Text(
//                 'CITIES',
//                 style: TextStyle(fontSize: 18.0, color: Colors.black),
//                 textAlign: TextAlign.center,
//               ),
//               GestureDetector(
//                 onTap: () {
//                   Navigator.pop(context);
//                 },
//                 child: Container(
//                   height: Dimensions.height30 * 2,
//                   width: Dimensions.width30 * Dimensions.width10,
//                   decoration: BoxDecoration(
//                     color: Colors.green,
//                     borderRadius: BorderRadius.circular(Dimensions.radius30),
//                   ),
//                   child: Center(
//                     child: BigText(
//                       text: "Done",
//                       color: AppColors.textWhite,
//                       size: Dimensions.font16,
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//           Expanded(
//             child: ListView.builder(
//                 itemCount: widget.cities.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   final cityName = widget.cities[index];
//                   return Container(
//                     child: CheckboxListTile(
//                         activeColor: Colors.green,
//                         checkColor: Colors.white,
//                         title: Text(cityName),
//                         value: _tempSelectedCities.contains(cityName),
//                         onChanged: (bool? value) {
//                           if (value == true) {
//                             if (!_tempSelectedCities.contains(cityName)) {
//                               setState(() {
//                                 _tempSelectedCities.add(cityName);
//                               });
//                             }
//                           } else {
//                             if (_tempSelectedCities.contains(cityName)) {
//                               setState(() {
//                                 _tempSelectedCities.removeWhere(
//                                     (String city) => city == cityName);
//                               });
//                             }
//                           }
//                           widget
//                               .onSelectedCitiesListChanged(_tempSelectedCities);
//                         }),
//                   );
//                 }),
//           ),
//         ],
//       ),
//     );
//   }
// }
