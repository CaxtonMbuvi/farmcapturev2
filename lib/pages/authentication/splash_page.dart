import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../resources/utils/dimensions.dart';
import '../../routes/route_helper.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin {
  final bool _isLoggedIn = false;
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
   
    super.initState();
    controller = AnimationController(vsync: this, duration: const Duration(seconds: 4))..forward();
    animation = CurvedAnimation(parent: controller, curve: Curves.linear);


    Timer(const Duration(seconds: 7),() => Get.offNamed(RouteHelper.getLogInPage()));
    //Timer(const Duration(seconds: 7),() => _isLoggedIn ? Get.to(() => const MyBusiness()): Get.to(() => const SignIn()));
  }

  @override
  Widget build(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    return Scaffold(
      backgroundColor: Colors.green,
      body: Center(
        child: Container(
          width: Dimensions.width10 * 28,
          height: Dimensions.width10 * 28,
          decoration: const BoxDecoration(
            image: DecorationImage(
              opacity: 0.5,
              image: AssetImage("assets/images/fc_logo.png"),
            ),
          ),
        ),
      ),
    );
  }
}