import 'package:package_info_plus/package_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:get/get.dart';
import '../../controllers/auth/auth_controller.dart';
import '../../controllers/farmers/farmers_controller.dart';
import '../../resources/base/progress_dialog.dart';
import '../../resources/base/snackbar.dart';
import '../../resources/utils/colors.dart';
import '../../resources/utils/dimensions.dart';
import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/custom_text_field.dart';
import '../main_pages/dashboard.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isObscure = true;
  var passwordController = TextEditingController();
  var userNameController = TextEditingController();

  String? versionName;

  PackageInfo? packageInfo;

  _getVersionName() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      versionName = packageInfo.version;
    });
    print(versionName.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getVersionName();
  }

  void _login() {
    String password = passwordController.text.trim();
    String userName = userNameController.text.trim();

    if (userName.isEmpty) {
      showCustomSnackBar("Type in your UserName", context, ContentType.failure,
          title: "UserName");
    } else if (password.isEmpty) {
      showCustomSnackBar("Type in your Password", context, ContentType.failure,
          title: "Password");
    } else {
      // var data = {
      //   "CurrentUser": {
      //     "PassWord": 'admin123A',
      //     "UserName": 'admin',
      //     "AccountName": "UTR",
      //     "Branch": "",
      //     "Language": "English"
      //   },
      //   "Language": "English",
      //   "IsRenewalPasswordRequest": false
      // };
      var data = {
        "CurrentUser": {
          "PassWord": "Tomcat.1926And26",
          "UserName": "admin",
          "Language": "English"
        },
        "Language": "English",
        "IsRenewalPasswordRequest": false
      };

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext c) {
            return ProgressDialog(
              message: "Please wait...",
            );
          });

      FarmersController().isInteret().then((status) async {
        if (status.isSuccess) {
          AuthController().login(data).then((status) {
            if (status.isSuccess) {
              showCustomSnackBar(status.message, context, ContentType.success,
                  title: "Success");
              Navigator.pop(context);
              print(data.toString());

              Get.to(() => const DashboardScreen(), arguments: data);
            } else {
              showCustomSnackBar(status.message, context, ContentType.failure,
                  title: "Failed");
              Navigator.pop(context);
            }
          });
        }else{
          showCustomSnackBar("Ooops No Internet!", context, ContentType.failure, title: "No Internet");
        }
      });

      // AuthController().login(data).then((status) {
      //   if (status.isSuccess) {
      //     showCustomSnackBar(status.message, context, ContentType.success,
      //         title: "Success");
      //     Navigator.pop(context);
      //     print(data.toString());

      //     Get.to(() => const DashboardScreen(), arguments: data);
      //   } else {
      //     showCustomSnackBar(status.message, context, ContentType.failure,
      //         title: "Failed");
      //     Navigator.pop(context);
      //   }
      // });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: Dimensions.screenWidth,
          height: Dimensions.screenHeight,
          padding: EdgeInsets.only(
            left: Dimensions.width15,
            right: Dimensions.width15,
          ),
          decoration: BoxDecoration(
            color: AppColors.mainColor.withOpacity(1),
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.2), BlendMode.dstATop),
              image: const AssetImage("assets/images/crops.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30,
              ),
              Center(
                child: Column(
                  children: [
                    SizedBox(
                      height: Dimensions.height30,
                    ),
                    Image.asset("assets/images/fc_logo.png",
                        fit: BoxFit.contain, height: Dimensions.height45),
                  ],
                ),
              ),
              SizedBox(
                height: Dimensions.height45 * 3,
              ),
              BigText(
                text: "UserName",
                color: AppColors.textWhite,
                size: Dimensions.font16,
              ),
              CustomTextField(
                readOnly: false,
                controller: userNameController,
                hintText: "Username",
                isObscure: false,
                onSaved: (value) {},
                onTap: (value) {},
              ),
              SizedBox(
                height: Dimensions.height30,
              ),
              BigText(
                text: "Password",
                color: AppColors.textWhite,
                size: Dimensions.font16,
              ),
              SizedBox(
                child: TextFormField(
                  controller: passwordController,
                  obscureText: _isObscure,
                  cursorColor: Colors.black,
                  style: const TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    focusColor: Theme.of(context).primaryColor,
                    hintText: "Password",
                    contentPadding: const EdgeInsets.only(top: 15, left: 5),
                    suffixIcon: InkWell(
                      onTap: () {
                        setState(() {
                          _isObscure = !_isObscure;
                        });
                      },
                      child: const Icon(
                        Icons.visibility,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: Dimensions.height30 * 2,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  child: Container(
                    width: Dimensions.width30 * Dimensions.width20,
                    padding: EdgeInsets.only(
                      top: Dimensions.height15,
                      bottom: Dimensions.height15,
                      left: Dimensions.width30,
                      right: Dimensions.width30,
                    ),
                    decoration: BoxDecoration(
                        color: AppColors.textWhite,
                        borderRadius:
                            BorderRadius.circular(Dimensions.radius20)),
                    child: Center(
                        child: BigText(
                      text: "SIGN IN",
                      size: Dimensions.font18,
                      color: AppColors.black,
                    )),
                  ),
                  onTap: () {
                    _login();
                    // Get.to(() => const DashboardScreen());
                  },
                ),
              ),
              SizedBox(
                height: Dimensions.height30 * 2,
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Image.asset("assets/images/cs_logo_clear.png",
                          width: Dimensions.width45 * 2,
                          height: Dimensions.height30),
                      const SizedBox(
                        width: 10,
                      ),
                      BigText(
                        text: "V$versionName",
                        color: AppColors.textWhite,
                        size: Dimensions.font16,
                      ),
                    ],
                  )
                ],
              )
            ],

            //   Padding(
            //   padding: const EdgeInsets.all(16.0),
            //   child: Column(
            //     children: [
            //       SizedBox(
            //         height: Dimensions.height10,
            //       ),
            //       Padding(
            //         padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
            //         child: Container(
            //           width: 280,
            //           height: 100,
            //           decoration: const BoxDecoration(
            //             image: DecorationImage(
            //               image: AssetImage("assets/images/fc_logo.png"),
            //             ),
            //           ),
            //         ),
            //       ),
            //       BigText(
            //         text: "Sign In",
            //         size: Dimensions.font20,
            //         color: Colors.white,
            //       ),
            //       Form(
            //           child: Center(
            //             child: Column(children: [
            //               Container(
            //                 margin: EdgeInsets.only(top: 30, left: 10, right: 10),
            //                 child: Column(
            //                   children: [
            //                     CustomTextField(
            //                       readOnly: false,
            //                       controller: userNameController,
            //                       hintText: "Username",
            //                       isObscure: false,
            //                       onSaved: (value) {},
            //                       onTap: (value) {},
            //                     ),
            //                     SizedBox(
            //                       height: 15,
            //                     ),
            //                     Container(
            //                       margin: const EdgeInsets.all(10.0),
            //                       child: TextFormField(
            //                         controller: passwordController,
            //                         obscureText: _isObscure,
            //                         cursorColor: Colors.black,
            //                         style: TextStyle(color: Colors.black),
            //                         decoration: InputDecoration(
            //                           border: UnderlineInputBorder(),
            //                           focusColor: Theme.of(context).primaryColor,
            //                           hintText: "Password",
            //                           contentPadding:
            //                           EdgeInsets.only(top: 15, left: 5),
            //                           suffixIcon: InkWell(
            //                             onTap: () {
            //                               setState(() {
            //                                 _isObscure = !_isObscure;
            //                               });
            //                             },
            //                             child: Icon(
            //                               Icons.visibility,
            //                               color: Colors.green,
            //                             ),
            //                           ),
            //                         ),
            //                       ),
            //                     )
            //                   ],
            //                 ),
            //               ),
            //             ]),
            //           )),
            //       SizedBox(
            //         height: 50,
            //       ),
            //       Align(
            //         alignment: Alignment.center,
            //         child: GestureDetector(
            //           child: Container(
            //             width: 300,
            //             padding: EdgeInsets.only(
            //                 top: 13, bottom: 13, left: 25, right: 25),
            //             decoration: BoxDecoration(
            //                 color: AppColors.subColor,
            //                 borderRadius:
            //                 BorderRadius.circular(Dimensions.radius20)),
            //             child: Center(
            //                 child: BigText(
            //                   text: "sign in".toUpperCase(),
            //                   size: Dimensions.font18,
            //                   color: AppColors.textWhite,
            //                 )),
            //           ),
            //           onTap: () {
            //             print("Sign In");
            //             _login();
            //           },
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
          ),
        ),
      ),
    );
  }
}
