import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CallApi {
  Future<http.Response> postData(var data, String apiUrl) async {
    var fullUrl = apiUrl;
    var url = Uri.https('example.com', 'whatsit/create');
    return await http.post(Uri.parse(fullUrl),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
        },
        body: jsonEncode(data));
  }

  Future<http.Response> postData2(var data, String apiUrl) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();

    var token = localStorage.getString('Token');
    
    var fullUrl = apiUrl;
    return await http.post(Uri.parse(fullUrl),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(data));
  }
}
