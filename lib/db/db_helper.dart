import 'dart:io';

import 'package:encrypt/encrypt.dart';
import 'package:farmcapturev2/models/additional_info/nextofkin_model.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sqflite_sqlcipher/sqflite.dart' as d;
import 'package:path/path.dart' as p;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// import 'package:sqflite_sqlcipher/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._init();

  static d.Database? _database;

  DatabaseHelper._init();

  // Future<bool> _requestPermissions(Permission permission) async {
  //   if (await permission.isGranted) {
  //     return true;
  //   } else {
  //     var result = await permission.request();
  //     if (result == PermissionStatus.granted) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   }
  // }

  //Get the Database
  Future<d.Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('FarmCapture.db');

    return _database!;
  }

  //Initialize the database
  Future<d.Database> _initDB(String filepath) async {
    final dbPath = await d.getDatabasesPath();

    final path = p.join(dbPath, filepath);
    print("path is $path");

    return await d.openDatabase(
      path,
      version: 10,
      onCreate: _createDB,
      onUpgrade: (db, oldVersion, newVersion) {
        if (oldVersion != newVersion) {
          _createDB(db, newVersion);
          print("Updated");
        }
      },
      password: "farmCapture",
    );
  }

  Future _createDB(d.Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    const boolType = 'BOOLEAN NOT NULL';
    const integerType = 'INTEGER';
    const varcharType = 'VARCHAR';

    await db.execute('CREATE TABLE IF NOT EXISTS users('
        '_Id $idType,'
        'resultId INTEGER,'
        'username $varcharType,'
        'password $varcharType,'
        'account_id $varcharType,'
        'status_id $varcharType,'
        'email_address $varcharType,'
        'phone_number $varcharType,'
        'use_sms $varcharType,'
        'use_mail $varcharType,'
        'use_code $varcharType,'
        'password_duration $varcharType,'
        'access_level_id $varcharType,'
        'user_image_path $varcharType)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers('
        '_Id $idType,'
        'syncId VARCHAR DEFAULT "s" NOT NULL,'
        'Id INTEGER,'
        'member_no VARCHAR,'
        'full_name VARCHAR,'
        'first_name VARCHAR,'
        'last_name VARCHAR,'
        'middle_name VARCHAR,'
        'phone1 VARCHAR,'
        'dob VARCHAR,'
        'nat_id INTEGER UNIQUE,'
        'gender_type_id INTEGER,'
        'marital_status INTEGER,'
        'member_category_id INTEGER,'
        'member_sub_category_id INTEGER,'
        'emp_staff_login_assignment_id INTEGER,'
        'Is_active BOOLEAN,'
        'division_id INTEGER,'
        'route_id INTEGER,'
        'collection_centre_id INTEGER,'
        'gang_id INTEGER,'
        'department_id INTEGER,'
        'datecomparer VARCHAR UNIQUE,'
        'regionId INTEGER,'
        'districtId INTEGER,'
        'wardId INTEGER,'
        'villageId INTEGER,'
        'passporturl VARCHAR,'
        'photoString VARCHAR,'
        'nationalityId INTEGER,'
        'bankdetailsId INTEGER,'
        'bankbranchId INTEGER,'
        'accNo VARCHAR,'
        'main_category_code VARCHAR )');

    await db.execute('CREATE TABLE IF NOT EXISTS nextOfKin('
        '_Id $idType,'
        //'Id INTEGER,'
        'sid VARCHAR,'
        'employee_id VARCHAR,'
        'member_no VARCHAR,'
        'birth_certificate_no VARCHAR,'
        'full_name VARCHAR,'
        'date_of_birth VARCHAR,'
        'is_schooled VARCHAR,'
        'relationship_id VARCHAR,'
        'marital_status_id VARCHAR,'
        'phonenumber VARCHAR,'
        'age VARCHAR,'
        'gender_type_id VARCHAR,'
        'level_of_education_id VARCHAR,'
        'Is_active VARCHAR,'
        'datecomparer VARCHAR,'
        'is_orphaned VARCHAR )');

    await db.execute('CREATE TABLE IF NOT EXISTS counties('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'color_code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS districts('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'color_code VARCHAR,'
        'region_id VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS wards('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'color_code VARCHAR,'
        'districtId VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS village('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'color_code VARCHAR,'
        'wardId VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS gendertypes('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS nationalities('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS maritalstatus('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS relation('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS polygoncategory('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS educationlevel('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS bankbranches('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'bank_details_id INTEGER,'
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS banks('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS alldivisionspolygon('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'syncId VARCHAR DEFAULT "s" NOT NULL,'
        'blockArea VARCHAR,'
        'isDivision INTEGER DEFAULT 1 NOT NULL,'
        'Id INTEGER,'
        'main_division_id INTEGER DEFAULT 0 NOT NULL,'
        'division_id INTEGER,'
        'polygon_id VARCHAR,'
        'longi VARCHAR,'
        'latitude VARCHAR,'
        'new_dev_block_id VARCHAR,'
        'transaction_no VARCHAR,'
        'isactive VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS divisionpolygon('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'syncId VARCHAR DEFAULT "s" NOT NULL,'
        'blockArea VARCHAR,'
        'isDivision INTEGER DEFAULT 1 NOT NULL,'
        'Id INTEGER,'
        'main_division_id INTEGER DEFAULT 0 NOT NULL,'
        'division_id INTEGER,'
        'polygon_id VARCHAR,'
        'longi VARCHAR,'
        'latitude VARCHAR,'
        'new_dev_block_id VARCHAR,'
        'transaction_no VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS deductions('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmerdeductions('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'member_no VARCHAR,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS divisions('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'code VARCHAR,'
        'name VARCHAR,'
        'mobi_code VARCHAR,'
        'datecomparer VARCHAR UNIQUE,'
        'division_head_user_id INTEGER,'
        'rubber_source_id INTEGER,'
        'main_division_id INTEGER)');

    await db.execute('CREATE TABLE IF NOT EXISTS input_questions('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'unitCode VARCHAR,'
        'name VARCHAR,'
        'level VARCHAR,'
        'identifier_code VARCHAR,'
        'main_category_id VARCHAR,'
        'mainCategory VARCHAR,'
        'is_multiple_choice VARCHAR,'
        'datecomparer VARCHAR UNIQUE,'
        'answer_type VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS input_questionaire_answers('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'quizid VARCHAR,'
        'answers VARCHAR,'
        'quizcode VARCHAR,'
        'status VARCHAR,'
        'datecomparer VARCHAR)');
        

    await db.execute('CREATE TABLE IF NOT EXISTS action_reasons('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farm_inspections_quiz_expected_answers_item_id VARCHAR,'
        'answer VARCHAR,'
        'separator_code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS blocks('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'datecomparer VARCHAR UNIQUE,'
        'code VARCHAR,'
        'main_division_id INTEGER,'
        'block_tbl_division_id INTEGER,'
        'category_separator VARCHAR,'
        'planted_year VARCHAR,'
        'type_of_planting_id INTEGER,'
        'spacing VARCHAR,'
        'rubber_clones_id INTEGER,'
        'area_acre VARCHAR,'
        'year_of_opening VARCHAR,'
        'no_tapping_tasks VARCHAR,'
        'type_of_plantingname VARCHAR,'
        'rubber_clones_Name VARCHAR,'
        'tapping_system_id INTEGER,'
        'tapping_system_Name VARCHAR,'
        'panel_position_Name VARCHAR,'
        'panel_position_id INTEGER,'
        'stand_per_acre VARCHAR,'
        'no_of_Trees_in_the_block VARCHAR,'
        'is_plotted VARCHAR DEFAULT "No" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS rubberClones('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS plantingTypes('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS infrastructureTypes('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'isative VARCHAR,'
        'coordinate_type VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS reserveTypes('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'isactive VARCHAR,'
        'sync_status VARCHAR,'
        'coordinate_type VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS tappingSystems('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS cpa_type('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS cpa_commercial_name('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS weather_type('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS intervention_type('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS natural_damage_cause('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS pesticide_disease_damage('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS intervention_application_method('
            '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
            'Id INTEGER ,' //this is the sid
            'name VARCHAR,'
            'code VARCHAR,'
            'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS expected_answer_action_reason('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farm_inspections_quiz_expected_answers_item_id VARCHAR,'
        'answer VARCHAR,'
        'separator_code VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS mainDivisions('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'code VARCHAR,'
        'division_name VARCHAR,'
        'datecomparer VARCHAR UNIQUE)');

    await db.execute('CREATE TABLE IF NOT EXISTS visitReason('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'code VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS trainingTopics('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'description VARCHAR,'
        'datecomparer VARCHAR,'
        'added_date VARCHAR,'
        'modified_date VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS trainingSubTopics('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'name VARCHAR,'
        'description VARCHAR,'
        'training_topics_id INTEGER,'
        'AddedDate VARCHAR,'
        'ModifiedDate VARCHAR,'
        'datecomparer VARCHAR)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_visit_inspection_table('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id VARCHAR ,' //this is the sid
        'visit_transaction_code VARCHAR,'
        'visit_reason VARCHAR,'
        'visit_date VARCHAR,'
        'member_No VARCHAR,'
        'new_dev_blocks_id VARCHAR,'
        'comments VARCHAR,'
        'verification_date VARCHAR,'
        'visit_reason_id INTEGER,'
        'user_id INTEGER,'
        'lat VARCHAR,'
        'lon VARCHAR,'
        'visit_status VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_coaching_main('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'sessionNo VARCHAR ,' //this is the sid
        'title VARCHAR,'
        'notes VARCHAR,'
        'userId VARCHAR,'
        'coaching_date VARCHAR,'
        'photo_url VARCHAR,'
        'start_time VARCHAR,'
        'end_time VARCHAR,'
        'farm_inspector_visitNo VARCHAR,'
        'numofpeople VARCHAR,'
        'member_id VARCHAR,'
        'no_fslb_farmers VARCHAR,'
        'no_non_fslb_farmers VARCHAR,'
        'no_family_members VARCHAR,'
        'category VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_coaching_topics('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'topic VARCHAR,'
        'topicId INTEGER,'
        'notes VARCHAR,'
        'sessionId VARCHAR,'
        'transaction_no VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_coaching_attendance('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farmerId VARCHAR,'
        'topic VARCHAR,'
        'date_today VARCHAR,'
        'sessionId VARCHAR,'
        'transaction_no VARCHAR,'
        'notes VARCHAR,'
        'comments VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_input_questionaire_answers('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farm_visit_transactionNo VARCHAR,'
        'farm_inspections_quiz_id VARCHAR,'
        'answer_option VARCHAR,'
        'answer_id VARCHAR,'
        'farm_inspector_visit_id VARCHAR,'
        'transaction_no VARCHAR,'
        'datetime VARCHAR,'
        'sync_status VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_blocks_tree_count('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'main_division_id VARCHAR,'
        'main_division_name VARCHAR,'
        'main_division_code VARCHAR,'
        'block_id VARCHAR,'
        'block_code VARCHAR,'
        'block_name VARCHAR,'
        'total_trees VARCHAR,'
        'trees_in_tapping VARCHAR,'
        'immature_trees VARCHAR,'
        'unproductive_trees VARCHAR,'
        'plot_name VARCHAR,'
        'farmer_name VARCHAR,'
        'farmer_member_no VARCHAR,'
        'plot_no VARCHAR,'
        'no_of_line VARCHAR,'
        'planted_trees VARCHAR,'
        'no_of_live_trees VARCHAR,'
        'no_of_tree_supplied_planting VARCHAR,'
        'average_girth VARCHAR,'
        'lastupdatedate VARCHAR,'
        'datecomparer VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_ipm_table('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'main_division_id VARCHAR,'
        'block_id VARCHAR,'
        'disease_type_id VARCHAR,'
        'affected_trees VARCHAR,'
        'photo_evidence VARCHAR,'
        'category VARCHAR,'
        'user_id VARCHAR,'
        'transaction_no VARCHAR,'
        'member_id VARCHAR,'
        'damage_caused VARCHAR,'
        'date_today VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_ipm_photos('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'transaction_no VARCHAR,'
        'disease_type_id VARCHAR,'
        'photo_url VARCHAR,'
        'photo_uniq_no VARCHAR,'
        'date_today VARCHAR,'
        'main_division_id VARCHAR,'
        'block_id VARCHAR,'
        'member_id VARCHAR,'
        'user_id VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_interventions_fc('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER,' //this is the sid
        'main_division_id VARCHAR,'
        'block_id VARCHAR,'
        'intervention_type VARCHAR,'
        'category_used VARCHAR,'
        'amount_applied VARCHAR,'
        'date_applied VARCHAR,'
        'cpa_name VARCHAR,'
        'cpa_type_used VARCHAR,'
        'application_methd VARCHAR,'
        'weather_type VARCHAR,'
        'user_id VARCHAR,'
        'transaction_no VARCHAR,'
        'member_id VARCHAR,'
        'comment VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS gap_filling_fc('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'main_division_id VARCHAR,'
        'block_id VARCHAR,'
        'line_no VARCHAR,'
        'no_of_resupplied_trees VARCHAR,'
        'requisition_no VARCHAR,'
        'dominant_clone VARCHAR,'
        'date_today VARCHAR,'
        'dominant_clone_id VARCHAR,'
        'user_id VARCHAR,'
        'transaction_no VARCHAR,'
        'clone_replanting VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS girth_measurement('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'main_division_id VARCHAR,'
        'block_id VARCHAR,'
        'girth_measure VARCHAR,'
        'tree_no VARCHAR,'
        'date_today VARCHAR,'
        'user_id VARCHAR,'
        'transaction_no VARCHAR,'
        'member_id VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_damage_tree_count_fc('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farmer_name VARCHAR,'
        'farmer_member_no VARCHAR,'
        'main_division_id VARCHAR,'
        'block_id VARCHAR,'
        'damage_type VARCHAR,'
        'impacted_trees VARCHAR,'
        'date_done VARCHAR,'
        'user_id VARCHAR,'
        'plot_no VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_bio_surveys_answers('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farm_visit_transactionNo VARCHAR,'
        'farm_inspections_quiz_id VARCHAR,'
        'quiz_transaction_no VARCHAR,'
        'category VARCHAR,'
        'member_no VARCHAR,'
        'user_id VARCHAR,'
        'block_id VARCHAR,'
        'main_div_id VARCHAR,'
        'comments VARCHAR,'
        'answ_option VARCHAR,'
        'activity_status VARCHAR,'
        'estimated_acreage VARCHAR,'
        'datetime VARCHAR,'
        'sync_status VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_biod_surveys_coordinates('
            '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
            'Id INTEGER ,' //this is the sid
            'farm_visit_transactionNo VARCHAR,'
            'farm_inspections_quiz_id VARCHAR,'
            'quiz_transaction_no VARCHAR,'
            'latitude VARCHAR,'
            'longitude VARCHAR,'
            'member_no VARCHAR,'
            'user_id VARCHAR,'
            'block_id VARCHAR,'
            'main_div_id VARCHAR,'
            'coordinate_unique_no VARCHAR,'
            'datetime VARCHAR,'
            'sync_status VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_biod_surveys_photos('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'farm_visit_transactionNo VARCHAR,'
        'farm_inspections_quiz_id VARCHAR,'
        'quiz_transaction_no VARCHAR,'
        'photo_url VARCHAR,'
        'photo_uniqueNo VARCHAR,'
        'category VARCHAR,'
        'member_no VARCHAR,'
        'user_id VARCHAR,'
        'block_id VARCHAR,'
        'main_div_id VARCHAR,'
        'datetime VARCHAR,'
        'sync_status VARCHAR DEFAULT "p" NOT NULL)');

      await db.execute('CREATE TABLE IF NOT EXISTS farmers_infrastructure_category('
        
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,' //this is the sid
        'infrastructure_type_id VARCHAR,'
        'main_div_id VARCHAR,'
        'transaction_no VARCHAR,'
        'datetime VARCHAR,'
        'sync_status VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_infrastructure_polygon('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'infrastructure_type_id VARCHAR,'
        'polygon_id VARCHAR,'
        'infrastructure_name VARCHAR,'
        'longitude VARCHAR,'
        'latitude VARCHAR,'
        'main_div_id VARCHAR,'
        'block_id VARCHAR,'
        'transaction_no VARCHAR,'
        'datecomparer VARCHAR,'
        'isactive VARCHAR,'
        'map_category VARCHAR,'
        'user_id VARCHAR,'
        'landsize VARCHAR,'
        'is_farmer_or_estate VARCHAR,'
        'location_category_id VARCHAR,'
        'datetime VARCHAR,'
        'sync_status VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmers_infrastructure_point('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'polygon_id VARCHAR,'
        'infrastructure_name VARCHAR,'
        'longitude VARCHAR,'
        'latitude VARCHAR,'
        'main_div_id VARCHAR,'
        'block_id VARCHAR,'
        'transaction_no VARCHAR,'
        'datecomparer VARCHAR,'
        'isactive VARCHAR,'
        'map_category VARCHAR,'
        'user_id VARCHAR,'
        'landsize VARCHAR,'
        'is_farmer_or_estate VARCHAR,'
        'location_category_id VARCHAR,'
        'datetime VARCHAR,'
        'sync_status VARCHAR DEFAULT "p" NOT NULL)');

    await db.execute('CREATE TABLE IF NOT EXISTS farmer_reserve_polygons('
        '_Id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'Id INTEGER ,' //this is the sid
        'reserve_polygon_unique_id VARCHAR,'
        'polygon_id VARCHAR,'
        'longi VARCHAR,'
        'latitude VARCHAR,'
        'isActive VARCHAR,'
        'landSize VARCHAR,'
        'reserve_area_id,'
        'user_id VARCHAR,'
        'reserve_name VARCHAR,'
        'map_category VARCHAR,'
        'land_details_id VARCHAR,'
        'is_farmer_or_estate VARCHAR,'
        'is_complete VARCHAR,'
        'datecomparer VARCHAR,'
        'syncId VARCHAR DEFAULT "p" NOT NULL)');

     // try{
     //    var res = await getIfColumnExists("nextKin", "datecomparer");
     //    print("Count:: $res");
     //
     // }catch(e){

       // if(res == 0){ //column doesn't exist
       //   await db.execute("ALTER TABLE nextKin ADD COLUMN datecomparer VARCHAR");
       //   await db.execute("ALTER TABLE nextKin ADD COLUMN Is_active VARCHAR");
       // } else {
       //
       // }
    // }


    // var res = await countDbAlterations();
    // if(res == null){
    //     await db.execute("ALTER TABLE nextKin ADD COLUMN datecomparer VARCHAR");
    //     await db.execute("ALTER TABLE nextKin ADD COLUMN Is_active VARCHAR");
    //   } else {
    //
    //   }
    // }catch(e){
    //
    // }

    print("Db created");
  }

  Future<int> countDbAlterations() async {
    d.Database db = await instance.database;

    var result =
    await DatabaseHelper.instance.readMaxDateComparerUsers(nextKinTable);

    return int.parse(result.toString());

    // final result = await db.rawQuery("SELECT count(datecomparer) FROM nextKin");
    // //print("At Result" + result.toString());
    // return result;
  }

  Future<int> getIfColumnExists(var table, var columnName) async{
    d.Database db = await instance.database;

    final result = await db.rawQuery("SELECT count($columnName) FROM $table");

    return int.parse(result.toString());
  }


  Future<List<Map<String, dynamic>>> readAllUsers(
      String userName, String pass) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query('users',
        where: 'username = ? and password = ?', whereArgs: [userName, pass]);
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<int> insert(String table, var row) async {
    d.Database db = await instance.database;
    return await db.insert(table, row.toJson(),
        conflictAlgorithm: d.ConflictAlgorithm.replace);
  }

  Future<int> updateFarmerCoachingMain(
      String table,
      String startTime,
      String noOfPeople,
      String fslb,
      String nonfslb,
      String noOfFamily,
      String photoUrl,
      String sessionId) async {
    d.Database db = await instance.database;
    return await db.update(
      table,
      {
        'start_time': startTime,
        'numofpeople': noOfPeople,
        'no_fslb_farmers': fslb,
        'no_non_fslb_farmers': nonfslb,
        'no_family_members': noOfFamily,
        'photo_url': photoUrl
      },
      where: 'sessionNo = ?',
      whereArgs: [sessionId],
    );
  }

  Future<int> updateGroupFarmerCoachingMain(
      String table, String startTime, String photoUrl, String sessionId) async {
    d.Database db = await instance.database;
    return await db.update(
      table,
      {'start_time': startTime, 'photo_url': photoUrl},
      where: 'sessionNo = ?',
      whereArgs: [sessionId],
    );
  }

  Future<int> updateEndTimeFarmerCoachingMain(
      String table, String endTime, String sessionId) async {
    d.Database db = await instance.database;
    return await db.update(
      table,
      {
        'end_time': endTime,
      },
      where: 'sessionNo = ?',
      whereArgs: [sessionId],
    );
  }

  Future<List<Map<String, dynamic>>> select(String table) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result =
        await db.query(table, orderBy: "_Id DESC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectGroupBy(String table, String columnId, String id, String groupBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result =
        await db.query(table, where: '$columnId = ?', whereArgs: [id], groupBy: "$groupBy");
    return result;
  }

  Future<List<Map<String, dynamic>>> selectAll(
      String table, String columnId, String id, String orderBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query(table,
        where: '$columnId = ?', whereArgs: [id], orderBy: "$orderBy ASC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectAllUnupdated(
      String table,
      String columnId,
      String id,
      String columnId1,
      String id1,
      String orderBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query(table,
        where: '$columnId = ? and $columnId1 = ?',
        whereArgs: [id, id1],
        orderBy: "$orderBy ASC");

    print(db.toString());
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectByTwoFields(
      String table,
      String column1,
      String id,
      String column2,
      String id2,
      String orderBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query(table,
        where: '$column1 = ? and $column2 = ?',
        whereArgs: [id, id2],
        orderBy: "$orderBy ASC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectBioRecords(
      String table,
      String column1,
      String id,
      String column2,
      String id2,
      String column3,
      String id3,
      String column4,
      String id4,
      String orderBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.rawQuery(
        "SELECT t1.estimated_acreage,t1.datetime, t1.comments, t2.longitude, t2.latitude, t3.name FROM farmers_bio_surveys_answers AS t1 JOIN farmers_biod_surveys_coordinates AS t2 ON t1.block_id = t2.block_id JOIN input_questions AS t3 ON t1.farm_inspections_quiz_id = t3.Id WHERE t1.block_id = '${id2}' AND t1.main_div_id = '${id}' AND t1.datetime >= '${id3}' AND t1.datetime <= '${id4}'");

    // final List<Map<String, dynamic>> result = await db.query(table,
    //     where: '$column1 = ? and $column2 = ? and $column3 >= ? and $column4 <= ?',
    //     whereArgs: [id, id2, id3, id4],
    //     orderBy: "$orderBy ASC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectGroupVisits(
      String table,
      String column1,
      String id,
      String column2,
      String id2,
      String column3,
      var id3,
      String orderBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query(table,
        where: '$column1 = ? and $column2 = ? and $column3 = ?',
        whereArgs: [id, id2, id3],
        orderBy: "$orderBy ASC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectAllTrainingSessionsByDate(
      String table,
      String column1,
      String id,
      String column2,
      String id2,
      String orderBy) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query(table,
        where: '$column1 = ? and $column2 = ?',
        whereArgs: [id, id2],
        orderBy: "$orderBy ASC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  Future<List<Map<String, dynamic>>> selectSingle(String table, String column, String id) async {
    d.Database db = await instance.database;

    final List<Map<String, dynamic>> result = await db.query(table,
        where: '$column = ?', whereArgs: [id], orderBy: "_Id DESC");
    //print("At Result" + result.toString());
    //return result.map((json) => ResultResult.fromJson(json)).toList();
    return result;
  }

  // Future<List<Map<String, dynamic>>> selectById(String table,String columnId, String id) async {
  //   d.Database db = await instance.database;

  //   final List<Map<String, dynamic>> result = await db.query(
  //     table,
  //     where: '${columnId} = ?',
  //     whereArgs: [id],
  //   );
  //   print("At Result" + result.toString());
  //   //return result.map((json) => ResultResult.fromJson(json)).toList();
  //   return result;
  // }

  // Future<List<Map>> getDivisionPolygon(var divisionId) async{
  //   d.Database db = await instance.database;

  //   List<Map> result = await db.rawQuery('SELECT latitude && longi FROM divisionpolygon WHERE polygon_id=?', ['$divisionId']);

  //   return result;
  // }

  Future<List<Map<String, dynamic>>> getDivisionPolygon(var divisionId) async {
    d.Database db = await instance.database;
    final columns = ['longi', 'latitude']; // columns to select
    final result = await db.query(
      'divisionpolygon',
      columns: columns,
      where: 'polygon_id = ? and isDivision = ?',
      whereArgs: [divisionId, 1],
      //orderBy: "_Id DESC"
    );
    return result;
  }

  Future<List<Map<String, dynamic>>> getAllDivisionPolygon(String id) async {
    d.Database db = await instance.database;
    final columns = ['longi', 'latitude']; // columns to select
    // final result = await db.rawQuery(
    //     "SELECT longi, latitude FROM alldivisionspolygon WHERE main_division_id = $id new_dev_block_id IS NULL");

    final result = await db.query(
      'alldivisionspolygon',
      columns: columns,
      where: 'main_division_id = ?',
      whereArgs: [id]
      //orderBy: "_Id DESC"
    );
    return result;
  }

  Future<List<Map<String, dynamic>>> getallBlocksPolygon(
      var divisionId, var blockId) async {
    d.Database db = await instance.database;
    final columns = ['longi', 'latitude']; // columns to select
    final result = await db.query(
      'divisionpolygon',
      columns: columns,
      where: 'polygon_id = ? and new_dev_block_id = ?',
      whereArgs: [divisionId, blockId],
      //orderBy: "_Id DESC"
    );
    return result;
  }

  Future<List<Map<String, dynamic>>> getBlockPolygon(var blockId) async {
    d.Database db = await instance.database;
    final columns = ['longi', 'latitude', 'blockArea']; // columns to select
    final result = await db.query(
      'divisionpolygon',
      columns: columns,
      where: 'new_dev_block_id = ?',
      whereArgs: [blockId],
      //orderBy: "_Id DESC"
    );
    return result;
  }

  Future<List<Map<String, dynamic>>> getAllColumsById(String table, String column,String id) async {
    d.Database db = await instance.database;
    final columns = ['longitude', 'latitude', 'infrastructure_name']; // columns to select
    // final result = await db.rawQuery(
    //     "SELECT longi, latitude FROM alldivisionspolygon WHERE main_division_id = $id new_dev_block_id IS NULL");

    final result = await db.query(
      table,
      columns: columns,
      where: '$column = ?',
      whereArgs: [id]
      //orderBy: "_Id DESC"
    );
    return result;
  }

  Future<String> readMaxDateComparerUsers(var table) async {
    d.Database db = await instance.database;

    final result = await db.rawQuery("SELECT MAX(datecomparer) FROM $table");
    print("At Result${result[0]}");
    print("At Result${result[0]["MAX(datecomparer)"]}");
    return result[0]["MAX(datecomparer)"].toString();
  }

  Future<String> readMaxDateComparerBlocks(var table, var id) async {
    d.Database db = await instance.database;

    final result = await db.rawQuery(
        "SELECT MAX(datecomparer) FROM $table WHERE main_division_id=?", [id]);
    print("At Result${result[0]}");
    print("At Result${result[0]["MAX(datecomparer)"]}");
    return result[0]["MAX(datecomparer)"].toString();
  }

  Future<String> checkActiveSession(String sessionId) async {
    d.Database db = await instance.database;

    final result = await db.rawQuery(
        "SELECT start_time FROM farmers_coaching_main WHERE sessionNo=?",
        [sessionId]);
    print("At Result${result[0]}");
    print("At Result${result[0]["start_time"]}");
    return result[0]["start_time"].toString();
  }

  // Future<String> readVisitTransactionNumber() async {
  //   d.Database db = await instance.database;

  //   final result = await db.rawQuery("SELECT visit_transaction_code FROM farmer_visit_inspection_table WHERE ");
  //   print("At Result" + result[0].toString());
  //   print("At Result" + result[0]["visit_transaction_code"].toString());
  //   return result[0]["visit_transaction_code"].toString();
  // }

  Future<int> updateFarmer(String table, var data, var memberNo) async {
    final db = await instance.database;

    return db.update(table, data.toJson(),
        where: '${'member_no'} = ?', whereArgs: [memberNo]);
  }

  Future<int> update(String table, var data, var column, var value) async {
    final db = await instance.database;

    return db
        .update(table, data.toJson(), where: '$column = ?', whereArgs: [value]);
  }

  // Future<Future<List<Map<String, Object?>>>> update1(String table, var data, var column, var value) async {
  //   final db = await instance.database;

  //   return db.rawQuery("UPDATE $table SET $column = '$data' WHERE ID = $value");
  // }

  Future<int> update1(
      String table, var column, var data, var column1, var value) async {
    d.Database db = await instance.database;
    return await db.update(
      table,
      {column: data},
      where: '$column1 = ?',
      whereArgs: [value],
    );
  }

  // Delete
  static Future<void> deleteItem(String table, String column, String id) async {
    final db = await instance.database;
    try {
      await db.delete(table, where: "$column = ?", whereArgs: [id]);
    } catch (err) {}
  }

  // Delete
  static Future<int> deleteAllItems(String table) async {
    final db = await instance.database;
    return await db.delete(table);
  }

  Future<String> getItemCount(String table, String column, String column1, String id1) async{
    final db = await instance.database;
    // SELECT COUNT(birth_certificate_no) FROM nextOfKin WHERE  member_no  =  '10672' 

    final result = await db.rawQuery("SELECT COUNT($column) FROM $table WHERE $column1 = ?", [id1]);
  

    return result.length.toString();
  }

  Future<List<Map<String, dynamic>>> selectLivingConditions(
      String table, String column, String id) async {
    final db = await instance.database;

    final result = await db.rawQuery(
        //"SELECT input_questions.name, input_questions.id, input_questionaire_answers.id, input_questionaire_answers.answers FROM input_questions INNER JOIN input_questionaire_answers ON input_questions.unitCode = input_questionaire_answers.quizcode WHERE input_questions.$column = ? GROUP BY input_questions.unitCode",

        "SELECT input_questions.name, input_questions.id,input_questions.main_category_id, GROUP_CONCAT(input_questionaire_answers.answers || '|' || input_questionaire_answers.id, ':') AS choices_text FROM input_questions INNER JOIN input_questionaire_answers ON input_questions.unitCode = input_questionaire_answers.quizcode WHERE input_questions.$column = ? GROUP BY input_questions.unitCode",
        [id]);

    return result;
  }

  // // Delete
  // static Future<int> deleteItem(String table,String column1, String id, String column2, String id2) async {
  //   final db = await instance.database;
  //   return await db.delete(table, where: "$column1 = ? and $column2 = ?", whereArgs: [id, id2]);
  //   // try {

  //   // } catch (err) {

  //   // }
  // }
}


// SELECT  t1.estimated_acreage,t1.datetime, t2.longitude,  t2.latitude, t3.name
// FROM farmers_bio_surveys_answers AS t1
// JOIN farmers_biod_surveys_coordinates AS t2 ON t1.block_id = t2.block_id
// JOIN input_questions AS t3 ON t1.farm_inspections_quiz_id = t3.Id
// WHERE t1.block_id = "707" AND t1.main_div_id = "1" AND t1.datetime >= "2023-07-16 00:00:00.000" AND t1.datetime <= "2023-07-18 00:00:00.000"